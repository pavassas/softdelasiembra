<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    //return $router->app->version();
    $url= 'http://'. $_SERVER['SERVER_NAME'] . ':8080/delasiembralumen/public';
	$html = '<h1>De la siembra  </h1>';
	$html .= '<ol>';	
	$html .= '</ol>';
	$html .= '<li> <a href=' . $url . '/perfiles>Perfiles  </a> - Listado Completo de Perfiles';
	$html .= '<li> <a href=' . $url . '/usuarios>Usuarios  </a> - Listado Completo de Usuarios';
	$html .= '<li> <a href=' . $url . '/usuarios/1/andres.199207@gmail.com>Usuarios 1 </a> - Listado Usuarios';
	$html .= '<li> <a href=' . $url . '/productos>Productos  </a> - Listado Completo de Productos';
	$html .= '<li> <a href=' . $url . '/productos/1/0005>Producto 0005 </a> - Buscar Producto';
	return $html;
});
$router->get('perfiles', 'PerfilesController@index');
$router->get('/perfiles/{perfiles}', 'PerfilesController@getPerfiles');
$router->get('usuarios', 'UsuariosController@index');
$router->get('/usuarios/{usuarios}/{email}', 'UsuariosController@getUsuarios');
$router->post('/perfiles', 'PerfilesController@createPerfiles');
$router->get('productos', 'ProductosController@index');
$router->get('/productos/{idusuario}/{nombre}', 'ProductosController@getProductos');