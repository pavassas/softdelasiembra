<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Perfiles extends Model{
	protected $table = 'tbl_perfiles';
	protected $primaryKey = 'prf_clave_int';
	protected $fillable = ['prf_clave_int','prf_descripcion','est_clave_int'];
	protected $hidden = ['prf_usu_actualiz','prf_fec_actualiz'];
	
}