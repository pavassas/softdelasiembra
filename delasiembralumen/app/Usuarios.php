<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Usuarios extends Model{
	protected $table = 'tbl_usuario';
	protected $primaryKey = 'usu_clave_int';
	protected $fillable = ['usu_clave_int','usu_usuario','usu_nombre','usu_color','usu_email'];
	protected $hidden = ['usu_usu_actualiz','usu_fec_actualiz'];
	
}