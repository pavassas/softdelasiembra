<?php
namespace App\Http\Controllers;
use DB;
use App\Perfiles;
use Illuminate\Http\Request;
class PerfilesController extends Controller
{
	public function index()
	{

		$perfiles = DB::select("select * from tbl_perfil");
		return response()->json($perfiles, 200);
        
        
        //return response()->json("hola", 200);
	}
	public function getPerfiles($id)
	{
		//$perfiles = Perfiles::find($id);
		$perfil = DB::select("select * from tbl_perfil where prf_clave_int = ?",[$id]);
		if($perfil)
		{
			return response()->json($perfil, 200);
		}
		return response()->json(["Perfil no encontrado"], 404);
	}
	 public function createPerfiles(Request $request)
	 {
        $perfil = Perfiles::create($request->all());  
        return response()->json($perfil,201);  
    }
}