<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class Productos extends Model{
	protected $table = 'tbl_productos';
	protected $primaryKey = 'pro_clave_int';
	protected $fillable = ['pro_clave_int','pro_descripcion','pro_activo','pro_estado','pro_codigo'];
	protected $hidden = ['pro_usu_actualiz','pro_fec_actualiz'];
	
}