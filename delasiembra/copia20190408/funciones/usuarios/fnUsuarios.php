<?php
session_start();
error_reporting(0);
$IP = $_SERVER['REMOTE_ADDR'];
include ("../../data/Conexion.php");
use  PHPMailer\PHPMailer\PHPMailer;
use  PHPMailer\PHPMailer\Exception;
require ('../../PHPMailer-master/src/PHPMailer.php');
require ('../../PHPMailer-master/src/Exception.php');
require ('../../PHPMailer-master/src/SMTP.php');
require_once('../../clases/pdf/html2pdf.class.php');



function rgb2hex($rgb) {
   $rgb = array(str_replace(")","",str_replace("rgba(","",$rgb)));
   $hex = "#";
   $hex .= str_pad(dechex($rgb[0]), 2, "0", STR_PAD_LEFT);
   $hex .= str_pad(dechex($rgb[1]), 2, "0", STR_PAD_LEFT);
   $hex .= str_pad(dechex($rgb[2]), 2, "0", STR_PAD_LEFT);

   return $hex; // returns the hex value including the number sign (#)
}

function hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   // $rgb = array($r, $g, $b);
   $rgb = 'rgba('.$r.','.$g.','.$b.', 0.5)';
   //return implode(",", $rgb); // returns the rgb values P12 by commas
   return $rgb; // returns an array with the rgb values
}
	function decrypt($string, $key)
	{
		$result = "";
		$string = base64_decode($string);
		for($i=0; $i<strlen($string); $i++) 
		{
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)-ord($keychar));
			$result.=$char;
		}
		return $result;
	}
	function encrypt($string, $key) 
	{
		$result = "";	
		for($i=0; $i<strlen($string); $i++) 
		{	
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)+ord($keychar));
			$result.=$char;
		}
		return base64_encode($result);
	}
	function CalculaEdad( $fec ) {
    	list($Y,$m,$d) = explode("-",$fec);
    	return( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y );
	}
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
	//$idUsuario= $_SESSION["idusuario"];
$idUsuario = $_COOKIE["usIdentificacion"];
$conusu= pg_query($dbconn,"SELECT mer_clave_int,dir_clave_int,prf_clave_int,usu_imagen from tbl_usuario where usu_clave_int = '".$idUsuario."'");
$datusu = pg_fetch_array($conusu);
$idmercado = $datusu['mer_clave_int'];
$ultimadireccion = $datusu['dir_clave_int'];
$perfil = $datusu['prf_clave_int'];
$imgperfil = $datusu['usu_imagen'];
if($imgperfil=="" || $imgperfil==NULL)
{
  $imgperfil = "../../dist/img/default-user.png";
}
else 
{
	$imgperfil = "https://wwww.delasiembra.com/".$imgperfil;
}
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
$opcion = $_POST['opcion'];

if($opcion=="NUEVO")
{
		?>
        <form name="form1" id="form1" action="_" method="post" enctype="multipart/form-data" class="form-horizontal">
		     <div class="form-group">
		         <div class="col-xs-6"><strong>Nombre:<span class="symbol required"></span></strong>
		             <input type="text" name="txtnombre" id="txtnombre" class="form-control input-sm" autocomplete="off">
		         </div>
		          <div class="col-xs-6"><strong>Apellido:<span class="symbol required"></span></strong>
		             <input type="text" name="txtapellido" id="txtapellido" class="form-control input-sm" autocomplete="off">
		         </div>
		     
		         <div class="col-xs-6 hide"><strong>Usuario:<span class="symbol required"></span></strong>
		             <input type="text" name="txtusuario" id="txtusuario" class="form-control input-sm" autocomplete="off">
		         </div>
		          <div class="col-xs-6"><strong>Documento:<span class="symbol required"></span></strong>
		             <input type="text" name="txtdocumento" id="txtdocumento" class="form-control input-sm" autocomplete="off">
		         </div>
		         <div class="col-xs-6"><strong>E-mail:<span class="symbol required"></span></strong>
		             <input type="email" name="txtemail" id="txtemail" class="form-control input-sm" value="" autocomplete="off">
		         </div>
		     </div>
		     <div class="form-group">
		         <div class="col-xs-6"><strong>Contraseña:<span class="symbol required"></span></strong>
		             <input type="password" name="txtpass" id="txtpass" class="form-control input-sm" autocomplete="off">
		         </div>
		     
		         <div class="col-xs-6"><strong>Verificar Contraseña:<span class="symbol required"></span></strong>
		             <input type="password" name="txtpass1" id="txtpass1" onkeyup="VALIDAR('CONTRASENA')" class="form-control input-sm" autocomplete="off">
		         </div>
		     </div>
                 
			  <div class="form-group">
			  	  
		         <div class="col-xs-6"><strong>Telefono:<span class="symbol required"></span></strong>
		             <input type="text" name="txttelefono" id="txttelefono" class="form-control input-sm" autocomplete="off">
		         </div>
			         <div class="col-xs-6"><strong>Perfil:<span class="symbol required"></span></strong>
			            <select class="form-control input-sm selectpicker" name="selperfil" id="selperfil" onchange="CRUDUSUARIOS('VALIDARPERFIL','')">
			            <option value="">-Seleccione-</option>
						<?php
							$con = pg_query($dbconn,"select * from tbl_perfil where est_clave_int!=2  order by prf_descripcion");
							$num = pg_num_rows($con);
							for($i = 0; $i < $num; $i++)
							{
								$dato = pg_fetch_array($con);
								$clave = $dato['prf_clave_int'];
								$perfil = $dato['prf_descripcion'];
						?>
							<option value="<?php echo $clave; ?>"><?php echo $perfil; ?></option>
						<?php
							}
						?>
						</select>
			         </div>
			     </div>
			    

			     <div class="form-group">
			     	<div class="col-md-6" id="divmercado" style="display: none">
			     		<strong>Mercado:</strong>
			     		<select name="selmercado" id="selmercado" class="form-control selectpicket">
			     			<option value="0">-Seleccione-</option>
			     			<?php
			     			$conmer = pg_query($dbconn,"SELECT mer_clave_int, mer_nombre from tbl_mercado where mer_activo = 1");
			     			while($datmer = pg_fetch_array($conmer))
			     			{
			     				?>
			     				<option value="<?php echo $datmer['mer_clave_int'];?>"><?php echo $datmer['mer_nombre'];?></option>
			     				<?php
			     			}
			     			?>
			     			
			     		</select>
			     	</div>
			         <div class="col-md-3"><strong>Color:</strong><input class="form-control input-sm" type="color" name="favcolor" id="favcolor" value="#FFFFFF"></div>
			         <div class="col-md-3"><strong>Activo:</strong>
                    <input name="ckactivo" type="checkbox" checked="checked" value="1" /> 
			         </div>
			     </div>
			     <div class="form-group hide" id="divsectores">
			     	<h4 class="col-md-12">Sectores</h4>
			     	<div class="col-md-12">
			     		
			     		<select multiple id="selsectores" name="selsectores" class="form-control selectpicker" placeholder="seleccione los sectores a los que aplica el domiciliario" data-header="Buscar por nombre" multiple data-actions-box="true" data-live-search="true" data-selected-text-format="count > 6">
			     			<?php
			     			$consec = pg_query($dbconn, "SELECT sec_clave_int,sec_nombre FROM tbl_sector where est_clave_int = '1' order by sec_clave_int ASC");
			     			while($datsec = pg_fetch_array($consec))
			     			{
			     				$sec = $datsec['sec_clave_int'];
			     				$nomsec = $datsec['sec_nombre'];
			     				?>
			     			<option value="<?php echo $sec;?>"><?php echo $nomsec; ?></option>
			     			<?php
			     			}
			     			?>
			     			
			     		</select>
			     	</div>
			     	<h4 class="col-md-12">Info Vehiculo</h4>
			     	<div class="col-md-4">
			     		<label for="txtplaca">Placa:</label>
			     			<input id="txtplaca" name="txtplaca" type="text" class="form-control input-sm">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txtmarca">Marca:</label>
			     			<input id="txtmarca" name="txtmarca" type="text" class="form-control input-sm">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txtlinea">Linea:</label>
			     			<input id="txtlinea" name="txtlinea" type="text" class="form-control input-sm">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txtmodelo">Modelo:</label>
			     			<input id="txtmodelo" name="txtmodelo" type="text" class="form-control input-sm" onkeypress="return validar_texto(event)">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txtcilindrada">Cilindrada:</label>
			     			<input id="txtcilindrada" name="txtcilindrada" type="text" class="form-control input-sm" onkeypress="return validar_texto(event)">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txtcolor">Color:</label>
			     			<input id="txtcolor" name="txtcolor" type="text" class="form-control input-sm">
			     	</div>

			     	<div class="col-md-4">
			     		<label for="txtsoat">Nro Soat:</label>
			     			<input id="txtsoat" name="txtsoat" type="text" class="form-control input-sm">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txtsoatcompany">Compañia Soat:</label>
			     			<input id="txtsoatcompany" name="txtsoatcompany" type="text" class="form-control input-sm">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txtsoatvencimiento">Vencimiento:</label>
			     			<input id="txtsoatvencimiento" name="txtsoatvencimiento" type="text" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" autocomplete="off">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txttecno">Nro Tecno:</label>
			     			<input id="txttecno" name="txttecno" type="text" class="form-control input-sm">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txttecnocompany">Compañia Tecno:</label>
			     			<input id="txttecnocompany" name="txttecnocompany" type="text" class="form-control input-sm">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txttecnovencimiento">Vencimiento:</label>
			     			<input id="txttecnovencimiento" name="txttecnovencimiento" type="text" class="form-control input-sm datepicker" data-date-format="yyyy-mm-dd" autocomplete="off">
			     	</div>
			     </div>
                 <div class="form-group">
                <div class="col-md-12">
					<div class="box box-primary">
						<div class="box-body box-profile">
							<img style="display: none" class="profile-user-img img-responsive img-circle" id="picture" src="" alt="Imagen de Perfil">

							<h3 class="profile-username text-center"><input class="form-control input-sm dropify"  name="archivo" id="archivo" type="file" onChange="setpreview('rutausuario','archivo','form1')" data-allowed-file-extensions="jpg png gif jpeg"/>
							<span id="rutausuario"></span>
							</h3>
						</div>
					<!-- /.box-body -->
					</div>
                </div>
             </div>
			</form>
             <iframe src="about:blank" name="null" style="display:none"></iframe>
        <?Php
        	echo "<script>INICIALIZARCONTENIDO();</script>";
}
else
if($opcion == 'EDITAR')
{
		$usuedi = $_POST['id'];
		$con = pg_query($dbconn,"select * from tbl_usuario where usu_clave_int = '".$usuedi."'"); 
		$dato = pg_fetch_array($con); 
		$nom = $dato['usu_nombre'];
		$ape = $dato['usu_apellido'];
		$usu = $dato['usu_usuario'];
		//$con = $dato['usu_clave'];
		$con= decrypt($dato['usu_clave'],'p4v4svasquez');
		
		$ema = $dato['usu_email'];
		$act = $dato['est_clave_int'];
		$per = $dato['prf_clave_int'];
		$car = $dato['epr_clave_int'];
		$col = rgb2hex($dato['usu_color']);
		$hex = $dato['usu_color_hex'];
		$rut = $dato['usu_imagen'];
		$mer = $dato['mer_clave_int'];
		$conf = $dato['usu_confirmar'];
		$doc = $dato['usu_documento'];
		$tel = $dato['usu_telefono'];
		$rutarunt = $dato['usu_runt']; 
		if($rutarunt=="") { $rutarunt=""; $tit1 = "No hay runt adjunto"; }else { $tit1 = "Clic para ver documento";$rutarunt =  "href='".$rutarunt."'";}
		$rutasoat = $dato['usu_soat']; 
		if($rutasoat=="") { $rutasoat=""; $tit2 = "No hay soat adjunto";}else { $tit2 = "Clic para ver documento";  $rutasoat =  "href='".$rutasoat."'";}
		$rutatecno = $dato['usu_tecno']; 
		if($rutatecno=="") { $rutatecno=""; $tit3 = "No hay tecnicomecanica adjunta";}else { $tit3 = "Clic para ver documento"; $rutatecno = "href='".$rutatecno."'";}
		$rutapase = $dato['usu_pase']; 
		if($rutapase=="") { $rutapase=""; $tit4 = "No hay pase adjunto";}else {  $tit4 = "Clic para ver documento"; $rutapase = "href='".$rutapase."'";}
		$rutacedula = $dato['usu_cedula']; 
		if($rutacedula=="") { $rutacedula=""; $tit5 = "No hay cedula adjunta";}else { $tit5 = "Clic para ver documento"; $rutacedula =  "href='".$rutacedula."'";}

		//info vehiculo
		$conv = pg_query($dbconn, "SELECT uve_placa, uve_marca,uve_linea,uve_modelo,uve_cilindrada,uve_color,uve_soat,uve_soat_company,uve_soat_vencimiento,uve_tecno,uve_tecno_company,uve_tecno_company,uve_tecno_vencimiento FROM tbl_usuario_vehiculo WHERE 	usu_clave_int = '".$usuedi."'");
		$datv = pg_fetch_array($conv);
		$placa = $datv['uve_placa'];
		$marca = $datv['uve_marca'];
		$linea = $datv['uve_linea'];
		$modelo = $datv['uve_modelo'];
		$cilindrada = $datv['uve_cilindrada'];
		$color = $datv['uve_color'];
		$soat = $datv['uve_soat'];
		$soatcompany = $datv['uve_soat_company'];
		$soatven = $datv['uve_soat_vencimiento'];
		$tecno = $datv['uve_tecno'];
		$tecnocompany = $datv['uve_tecno_company'];
		$tecnoven = $datv['uve_tecno_vencimiento'];
?>
		<form name="form1" id="form1" class="form-horizontal" method="post" action="_" enctype="multipart/form-data">
        <input type="hidden" name="idedicion" id="idedicion" value="<?php echo $usuedi?>"/>
		     <div class="form-group">
		         <div class="col-xs-6"><strong>Nombre:<span class="symbol required"></span></strong>
		             <input type="text" name="txtnombre" id="txtnombre" class="form-control input-sm" value="<?php echo $nom; ?>" autocomplete="off">
		         </div>
		          <div class="col-xs-6"><strong>Apellido:<span class="symbol required"></span></strong>
		             <input type="text" name="txtapellido" id="txtapellido" class="form-control input-sm" value="<?php echo $ape; ?>" autocomplete="off">
		         </div>
		     
		         <div class="col-xs-6 hide"><strong>Usuario:<span class="symbol required"></span></strong>
		             <input type="text" name="txtusuario" id="txtusuario" class="form-control input-sm" value="<?php echo $usu; ?>" autocomplete="off">
		         </div>
		          <div class="col-xs-6"><strong>Documento:<span class="symbol required"></span></strong>
		             <input type="text" name="txtdocumento" id="txtdocumento" class="form-control input-sm" value="<?php echo $doc; ?>" autocomplete="off">
		         </div>
		          <div class="col-xs-6"><strong>E-mail:<span class="symbol required"></span></strong>
		             <input type="email" name="txtemail" id="txtemail" class="form-control input-sm" value="<?php echo $ema; ?>" autocomplete="off">
		         </div>
		     </div>
		     <div class="form-group">
		         <div class="col-xs-6"><strong>Contraseña:<span class="symbol required"></span></strong>
		             <input type="password" name="txtpass" id="txtpass" class="form-control input-sm" value="<?php echo $con; ?>" autocomplete="off">
		         </div>
		     
		         <div class="col-xs-6"><strong>Verificar contraseña:<span class="symbol required"></span></strong>
		             <input type="password" name="txtpass1" id="txtpass1" class="form-control input-sm" value="<?php echo $con; ?>" autocomplete="off">
		         </div>
		     		         
			     
		        
		          <div class="col-xs-6"><strong>Telefono:<span class="symbol required"></span></strong>
		             <input type="text" name="txttelefono" id="txttelefono" class="form-control input-sm" value="<?php echo $tel; ?>" autocomplete="off">
		         </div>
		    
		         <div class="col-md-6"><strong>Perfil:<span class="symbol required"></span></strong>
		            <select <?php if($conf==0){ echo "disabled"; } ?> class="form-control input-sm selectpicker" name="selperfil" id="selperfil"  onchange="CRUDUSUARIOS('VALIDARPERFIL','')">
					<?php
						$con = pg_query($dbconn,"select * from tbl_perfil where (est_clave_int!=2 or prf_clave_int='".$per."') and prf_clave_int!=7 order by prf_descripcion");
						$num = pg_num_rows($con);
						for($i = 0; $i < $num; $i++)
						{
							$dato = pg_fetch_array($con);
							$clave = $dato['prf_clave_int'];
							$perfil = $dato['prf_descripcion'];
					?>
						<option value="<?php echo $clave; ?>" <?php if($per == $clave){  echo "selected"; } ?>><?php echo $perfil; ?></option>
					<?php
						}
					?>
					</select>
		         </div>
		     
		     	<div class="col-md-6" id="divmercado" <?php if($per!=3){ echo "style='display:none'";}?>>
			     		<strong>Mercado:</strong>
			     		<select name="selmercado" id="selmercado" class="form-control selectpicket">
			     			<option value="0">-Seleccione-</option>
			     			<?php
			     			$conmer = pg_query($dbconn,"SELECT mer_clave_int, mer_nombre from tbl_mercado where mer_activo = 1");
			     			while($datmer = pg_fetch_array($conmer))
			     			{
			     				?>
			     				<option <?php if($mer==$datmer['mer_clave_int']){ echo "selected"; }?> value="<?php echo $datmer['mer_clave_int'];?>"><?php echo $datmer['mer_nombre'];?></option>
			     				<?php
			     			}
			     			?>
			     			
			     		</select>
			     	</div>
		        <div class="col-md-3 hide"><strong>Color:</strong><input class="form-control input-sm" type="color" name="favcolor" id="favcolor" value="#FFF"></div>
		         <div class="col-md-3"><strong>Activo:</strong>
                 
		             <input <?php if($act == 1){ echo 'checked="checked"'; } ?> name="ckactivo" type="checkbox" value="1" />
                     
		         </div>
		         <div id="divdescarte" class="col-md-6 hide">
		         	<br>
		         	<label for="radconfirmar1">Activar Domiciliario<input type="radio" name="radconfirmar" id="radconfirmar1" value="1" <?php if($conf==1){ echo "checked";}?>></label>
		         	<label for="radconfirmar2">Descartar Domiciliario<input type="radio" name="radconfirmar" id="radconfirmar2" value="2" <?php if($conf==2){ echo "checked";}?>></label>
		         </div>
		     </div>
		     <div class="form-group <?php if($per!="2"){  echo ' hide';}?>" id="divsectores">
		     	<h4 class="col-md-12">Sectores</h4>
			     	<div class="col-md-12">
			     		
			     		<select multiple id="selsectores" name="selsectores" class="form-control selectpicker" placeholder="seleccione los sectores a los que aplica el domiciliario" data-header="Buscar por nombre" multiple data-actions-box="true" data-live-search="true" data-selected-text-format="count > 6">
			     			<?php
			     			$consec = pg_query($dbconn, "SELECT sec_clave_int,sec_nombre FROM tbl_sector where est_clave_int = '1' order by sec_clave_int");
			     			while($datsec = pg_fetch_array($consec))
			     			{
			     				$sec = $datsec['sec_clave_int'];
			     				$nomsec = $datsec['sec_nombre'];
			     				$veri = pg_query($dbconn,"select * from tbl_usuario_sector where usu_clave_int = '".$usuedi."' and sec_clave_int = '".$sec."'");
			     				$numv = pg_num_rows($veri);
			     				?>
			     			<option <?php if($numv>0){ echo "selected"; }?> value="<?php echo $sec;?>"><?php echo $nomsec; ?></option>
			     			<?php
			     			}
			     			?>
			     			
			     		</select>
			     	</div>
			     	<h4 class="col-md-12">Info Vehiculo</h4>
			     	<div class="col-md-4">
			     		<label for="txtplaca">Placa:</label>
			     			<input id="txtplaca" name="txtplaca" type="text" class="form-control input-sm" value="<?php echo $placa;?>">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txtmarca">Marca:</label>
			     			<input id="txtmarca" name="txtmarca" type="text" class="form-control input-sm" value="<?php echo $marca;?>">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txtlinea">Linea:</label>
			     			<input id="txtlinea" name="txtlinea" type="text" class="form-control input-sm" value="<?php echo $linea;?>">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txtmodelo">Modelo:</label>
			     			<input id="txtmodelo" name="txtmodelo" type="text" class="form-control input-sm" value="<?php echo $modelo;?>" onkeypress="return validar_texto(event)">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txtcilindrada">Cilindrada:</label>
			     			<input id="txtcilindrada" name="txtcilindrada" type="text" class="form-control input-sm" value="<?php echo $cilindrada;?>" onkeypress="return validar_texto(event)">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txtcolor">Color:</label>
			     			<input id="txtcolor" name="txtcolor" type="text" class="form-control input-sm" value="<?php echo $color;?>" >
			     	</div>

			     	<div class="col-md-4">
			     		<label for="txtsoat">Nro Soat:</label>
			     			<input id="txtsoat" name="txtsoat" type="text" class="form-control input-sm" value="<?php echo $soat;?>">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txtsoatcompany">Compañia Soat:</label>
			     			<input id="txtsoatcompany" name="txtsoatcompany" type="text" class="form-control input-sm" value="<?php echo $soatcompany;?>">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txtsoatvencimiento">Vencimiento:</label>
			     			<input id="txtsoatvencimiento" name="txtsoatvencimiento" type="text" class="form-control input-sm datepicker" value="<?php echo $soatven;?>" data-date-format="yyyy-mm-dd">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txttecno">Nro Tecno:</label>
			     			<input id="txttecno" name="txttecno" type="text" class="form-control input-sm" value="<?php echo $tecno;?>">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txttecnocompany">Compañia Tecno:</label>
			     			<input id="txttecnocompany" name="txttecnocompany" type="text" class="form-control input-sm" value="<?php echo $tecnocompany;?>">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txttecnovencimiento">Vencimiento:</label>
			     			<input id="txttecnovencimiento" name="txttecnovencimiento" type="text" class="form-control input-sm datepicker" value="<?php echo $tecnoven;?>" data-date-format="yyyy-mm-dd">
			     	</div>
			     	<h4 class="btn-block"></h4>
			     	<div class="col-md-4">
			  			<a data-toggle="tooltip" title="<?php echo $tit5;?>" <?php echo $rutacedula;?> target="_blank" style="cursor: pointer;"> Ver Cedula</a>
			  		</div>
			  		<div class="col-md-4">
			  			<a  data-toggle="tooltip" title="<?php echo $tit4;?>" <?php echo $rutapase;?> target="_blank" style="cursor: pointer;">Ver Pase</a>
			  		</div>
			  		<div class="col-md-4">
			  			<a  data-toggle="tooltip" title="<?php echo $tit2;?>" <?php echo $rutasoat;?> target="_blank" style="cursor: pointer;">Ver Soat</a>
			  		</div>
			  		<div class="col-md-4">
			  			<a  data-toggle="tooltip" title="<?php echo $tit3;?>" <?php echo $rutatecno;?>  target="_blank" style="cursor: pointer;">Ver Tecnico Mecanica</a>
			  		</div>
			  		<div class="col-md-4">
			  			<a   data-toggle="tooltip" title="<?php echo $tit1;?>" <?php echo $rutarunt;?>  target="_blank" style="cursor: pointer;">Ver Runt</a>
			  		</div>
			 </div>
			  
             <div class="form-group">
                <div class="col-md-12">
                 <div class="box box-primary">
            <div class="box-body box-profile">              
               <span id="rutausuario" style='display: none'><?php echo $rut;
			   ?></span> 
              <h3 class="profile-username text-center"><input  class="form-control input-sm dropify" name="archivo" id="archivo" type="file" onChange="setpreview('rutausuario','archivo','form1')" data-default-file="<?php echo $rut;?>" data-allowed-file-extensions="jpg png gif jpeg" /></h3>
            </div>
            <!-- /.box-body -->
          </div>
                </div>
             </div>
		     
		     <div id="datos">
			 </div>
		</form>
        <iframe src="about:blank" name="null" style="display:none"></iframe>
	<?php
		echo "<script>INICIALIZARCONTENIDO();</script>";
}
else
if($opcion=="GUARDAR")
{
		$fecha=date("Y/m/d H:i:s");
		$nom = $_POST['nombre'];
		$ape = $_POST['apellido'];
		$usu = $_POST['usuario'];
		$con1 = $_POST['pass'];
		$con1 = encrypt($con1,'p4v4svasquez');
		$per = $_POST['perfil'];		
		$ema = $_POST['email'];
		$act = $_POST['activo'];
		$color = hex2rgb($_POST['color']);
		$hex = $_POST['color'];
		$ruta = $_POST['ruta'];
		$mer = $_POST['mer'];
		$trozos = explode(".", $ruta); 
		$rutaold = "../../".$ruta;
		$extension = end($trozos);
		$sec = implode(",",$_POST['sec']);
		$doc = $_POST['doc'];
		$tel = $_POST['tel'];

		//info vehiculo

		$placa = $_POST['placa'];
		$marca = $_POST['marca'];
		$linea = $_POST['linea'];
		$modelo = $_POST['modelo'];
		$cilindrada = $_POST['cilindrada'];
		$colvehiculo = $_POST['colvehiculo'];
		$soat = $_POST['soat'];
		$soatcom = $_POST['soatcom'];
		$soatven = $_POST['soatven'];
		$tecno = $_POST['tecno'];
		$tecnoven = $_POST['tecnoven'];
		$tecnocom = $_POST['tecnocom'];
		
		$sql = pg_query($dbconn,"select * from tbl_usuario where  UPPER(usu_email) = UPPER('".$ema."') and est_clave_int!=2");//(UPPER(usu_usuario) = UPPER('".$usu."') OR
		
		$dato = pg_fetch_array($sql);
		$conusu = $dato['usu_usuario'];
		$conema = $dato['usu_email'];
		if($act == '' || $act==NULL){ $swact = 0; }else{ $swact = 1; }
	
		/*if(STRTOUPPER($conusu) == STRTOUPPER($usu))
		{
			$res =  'error1';
			$rutanew = "";			
		}
		else*/
		if(STRTOUPPER($conema) == STRTOUPPER($ema))
		{
			$res =  'error2';
			$rutanew = "";				
		}
		else
		{
			    
			$con = pg_query($dbconn,"insert into tbl_usuario (usu_usuario,usu_clave,usu_nombre,prf_clave_int,est_clave_int,usu_email,usu_usu_actualiz,usu_fec_actualiz,usu_color,usu_color_hex,mer_clave_int,usu_apellido,usu_documento,usu_telefono) values('".$usu."','".$con1."','".$nom."','".$per."','".$swact."','".$ema."','".$usuario."','".$fecha."','".$color."','".$hex."','".$mer."','".$ape."','".$doc."','".$tel."')");
			$uid = pg_fetch_row(pg_query($dbconn,"SELECT setval('usuario_id_seq',nextval('usuario_id_seq')-1) as id;"));
			$id = $uid[0];
			if($con > 0)
			{			
				
				if($per==2)
				{
					$inssec = pg_query($dbconn,"INSERT INTO tbl_usuario_sector (usu_clave_int,use_usu_actualiz,use_fec_actualiz,sec_clave_int) SELECT '".$id."','".$usuario."','".$fecha."',sec_clave_int from tbl_sector where sec_clave_int in(".$sec.")");

					//info vehiculo

					$insvec = pg_query($dbconn,"INSERT INTO tbl_usuario_vehiculo(usu_clave_int,uve_placa,uve_marca,uve_linea,uve_modelo,uve_cilindrada,uve_color,uve_soat,uve_soat_company,uve_soat_vencimiento,uve_tecno,uve_tecno_company,uve_tecno_vencimiento) VALUES('".$id."','".$placa."','".$marca."','".$linea."','".$modelo."','".$cilindrada."','".$colvehiculo."','".$soat."','".$soatcom."','".$soatven."','".$tecno."','".$tecnocom."','".$tecnoven."')");
				}
				// $SQL = pg_query($dbconn,"INSERT INTO permisos_usuario(pea_sigla,usu_clave_int,pea_usu_actualiz,pea_fec_actualiz,pea_tipo) SELECT pea_sigla,'".$id."',pea_usu_actualiz,pea_fec_actualiz,pea_tipo from permisos_perfil where prf_clave_int = '".$per."'");
				//move_uploaded_file($ruta, $rutan);
				$rutan = "../../modulos/usuarios/fotosUsuarios/".$id.".".$extension;
				
				if(rename($rutaold,$rutan))
				{
					$rutanew = "modulos/usuarios/fotosUsuarios/".$id.".".$extension;
					$update = pg_query($dbconn,"update tbl_usuario set usu_imagen='".$rutanew."' where usu_clave_int='".$id."'");
					unlink($rutalold);
					if($id==$idUsuario){}else{$rutanew="";}
				}
				else
				{
				//$res =  'error4';
					$rutanew = "";
				}
				$res =  'ok';				
			 
			}
			else
			{
				$res =  'error3';
				$rutanew = "";
			}
		}	
		$datos[] = array("res"=>$res,"imagen"=>$rutanew,"id"=>$id);
		echo json_encode($datos);
}
else
if($opcion == 'GUARDAREDICION')
{
		
		$fecha=date("Y/m/d H:i:s");
		$nom = $_POST['nombre'];
		$ape = $_POST['apellido'];
		$usu = $_POST['usuario'];
		$con1 = $_POST['pass'];
		$con1 = encrypt($con1,'p4v4svasquez');
		
		$per = $_POST['perfil'];		
		$ema = $_POST['email'];
		$act = $_POST['activo'];
		$color =  hex2rgb($_POST['color']);
		$hex = $_POST['color'];
		$edi = $_POST['edi'];
		$mer = $_POST['mer'];
		$sec = implode(",",$_POST['sec']);
		$doc = $_POST['doc'];
		$tel = $_POST['tel'];
		if($act == '' || $act==NULL){ $swact = 0; }else{ $swact = 1; }

		//info vehiculo

		$placa = $_POST['placa'];
		$marca = $_POST['marca'];
		$linea = $_POST['linea'];
		$modelo = $_POST['modelo'];
		$cilindrada = $_POST['cilindrada'];
		$colvehiculo = $_POST['colvehiculo'];
		$soat = $_POST['soat'];
		$soatcom = $_POST['soatcom'];
		$soatven = $_POST['soatven'];
		$tecno = $_POST['tecno'];
		$tecnoven = $_POST['tecnoven'];
		$tecnocom = $_POST['tecnocom'];

		$sql = pg_query($dbconn,"select * from tbl_usuario where  UPPER(usu_email) = UPPER('".$ema."')) AND usu_clave_int <> '".$edi."' and est_clave_int!=2");//(UPPER(usu_usuario) = UPPER('".$usu."') OR
		$dato = pg_fetch_array($sql);
		$conusu = $dato['usu_usuario'];
		$conema = $dato['usu_email'];
		$condoc = $dato['usu_documento'];
		$rutaa = $dato['usu_imagen'];
		$ruta = $_POST['ruta'];
		$trozos = explode(".", $ruta); 
		$rutaold = "../../".$ruta;
		$extension = end($trozos);
		$rutan = "../../modulos/usuarios/fotosUsuarios/".$edi.".".$extension;
		$indv2 = $_POST['indv2'];		
		
		$conpe = pg_query($dbconn,"select * from tbl_usuario where usu_clave_int ='".$edi."'");
		$datpe = pg_fetch_array($conpe);
		$pera = $datpe['prf_clave_int'];
		$confa = $datpe['usu_confirmar'];
	
		/*if(STRTOUPPER($conusu) == STRTOUPPER($usu))
		{
			$res =  'error1';//echo "<div style='color:maroon;text-align:center' align='center'>El usuario ingresado ya existe</div>";
		     $rutanew = "";
		     $msn = "Ya existe el usuario ingresado";
		}
		else*/
		if(STRTOUPPER($conema) == STRTOUPPER($ema))
		{
			$res =  'error2';//echo "<div style='color:maroon;text-align:center' align='center'>El e-mail ingresado ya existe</div>";
			$rutanew  = "";
			$msn = "Ya hay un usuario con el correo electrónico indicado";
		}
		else if($doc==$condoc and $doc!="")
		{
			$res =  'error4';//echo "<div style='color:maroon;text-align:center' align='center'>El e-mail ingresado ya existe</div>";
			$rutanew  = "";
			$msn = "Ya hay un usuario con el número de documento indicado";
		}
		else
		{
				
			$con = pg_query($dbconn,"update tbl_usuario set usu_usuario = '".$usu."', usu_clave = '".$con1."', usu_nombre = '".$nom."', prf_clave_int = '".$per."', est_clave_int = '".$swact."', usu_email = '".$ema."', usu_usu_actualiz = '".$usuario."', usu_fec_actualiz = '".$fecha."',usu_color = '".$color."',usu_color_hex = '".$hex."',mer_clave_int = '".$mer."',usu_apellido = '".$ape."',usu_documento = '".$doc."',usu_telefono = '".$tel."' where usu_clave_int = '".$edi."'");
				
			if($con > 0)
			{   
				  
				  $del = pg_query($dbconn,"delete from tbl_usuario_sector where usu_clave_int = '".$edi."'");
				  if($per==2)
				  {
				  	$inssec = pg_query($dbconn,"INSERT INTO tbl_usuario_sector (usu_clave_int,use_usu_actualiz,use_fec_actualiz,sec_clave_int) SELECT '".$edi."','".$usuario."','".$fecha."',sec_clave_int from tbl_sector where sec_clave_int in(".$sec.")");

				  	//verificar info vehiculo
				  	$veriv = pg_query($dbconn,"select * from tbl_usuario_vehiculo where usu_clave_int = '".$edi."'");
				  	$numv = pg_num_rows($veriv);
				  	if($numv>0)
				  	{
				  		$updve = pg_query($dbconn,"UPDATE tbl_usuario_vehiculo SET uve_placa = '".$placa."',uve_marca = '".$marca."',uve_linea = '".$linea."',uve_modelo = '".$modelo."',uve_cilindrada = '".$cilindrada."',uve_color = '".$colvehiculo."',uve_soat = '".$soat."',uve_soat_company = '".$soatcom."',uve_soat_vencimiento = '".$soatven."',uve_tecno = '".$tecno."',uve_tecno_company = '".$tecnocom."',uve_tecno_vencimiento = '".$tecnoven."' WHERE usu_clave_int =  '".$edi."'");
				  	}
				  	else
				  	{
				  		$insve = pg_query($dbconn,"INSERT INTO tbl_usuario_vehiculo(usu_clave_int,uve_placa,uve_marca,uve_linea,uve_modelo,uve_cilindrada,uve_color,uve_soat,uve_soat_company,uve_soat_vencimiento,uve_tecno,uve_tecno_company,uve_tecno_vencimiento) VALUES('".$edi."','".$placa."','".$marca."','".$linea."','".$modelo."','".$cilindrada."','".$colvehiculo."','".$soat."','".$soatcom."','".$soatven."','".$tecno."','".$tecnocom."','".$tecnoven."')");
				  	}
				  	$errorv = pg_last_error($dbconn);
				  	$sqle = "INSERT INTO tbl_usuario_vehiculo(usu_clave_int,uve_placa,uve_marca,uve_linea,uve_modelo,uve_cilindrada,uve_color,uve_soat,uve_soat_company,uve_soat_vencimiento,uve_tecno,uve_tecno_company,uve_tecno_vencimiento) VALUES('".$edi."','".$placa."','".$marca."','".$linea."','".$modelo."','".$cilindrada."','".$colvehiculo."','".$soat."','".$soatcom."','".$soatven."','".$tecno."','".$tecnocom."','".$tecnoven."')";
				  }

				   //$SQL = pg_query($dbconn,"INSERT INTO permisos_usuario(pea_sigla,usu_clave_int,pea_usu_actualiz,pea_fec_actualiz,pea_tipo) SELECT pea_sigla,'".$edi."',pea_usu_actualiz,pea_fec_actualiz,pea_tipo from permisos_perfil where prf_clave_int = '".$per."'");	
				   
					if(rename($rutaold,$rutan) and $rutaa!=$ruta)
					{ 
					    $rutanew = "modulos/usuarios/fotosUsuarios/".$edi.".".$extension;
						$update = pg_query($dbconn,"update tbl_usuario set usu_imagen='".$rutanew."' where usu_clave_int='".$edi."'");
						//unlink($ruta);
						
						if($edi==$idUsuario){}else{$rutanew="";}
					}
					else
					{
					   // $res =  'error4';
						$rutanew = "";
					}
					$res =  'ok';
					//echo "<div style='color:green;text-align:center' align='center'>Datos grabados correctamente</div>";
				}
				else
				{
					$res =  'error3';//"<div style='color:maroon;text-align:center' align='center'>No se han podido guardar los datos</div>";            
					$rutanew = "";
					$msn = "Surgió un error al actualizar usuario. Error BD(".pg_last_error($dbconn).")";
				}
		}	
		
		$datos[] = array("res"=>$res,"imagen"=>$rutanew."?".time(),"msn"=>$msn,"erro"=>$errorv,"sq"=>$sqle);
		echo json_encode($datos);
}
else
if($opcion == 'GUARDARCONFIRMAR')
{		
	$fecha=date("Y/m/d H:i:s");
	$nom = $_POST['nombre'];
	$ape = $_POST['apellido'];
	$usu = $_POST['usuario'];
	$con1 = $_POST['pass'];
	$con1 = encrypt($con1,'p4v4svasquez');
	
	$per = $_POST['perfil'];		
	$ema = $_POST['email'];
	$act = $_POST['activo'];
	$color =  hex2rgb($_POST['color']);
	$hex = $_POST['color'];
	$edi = $_POST['edi'];
	$mer = $_POST['mer'];
	$sec = implode(",",$_POST['sec']);
	$doc = $_POST['doc'];
	$confi = $_POST['confi'];
	$tel  = $_POST['tel'];

	//info vehiculo

	$placa = $_POST['placa'];
	$marca = $_POST['marca'];
	$linea = $_POST['linea'];
	$modelo = $_POST['modelo'];
	$cilindrada = $_POST['cilindrada'];
	$colvehiculo = $_POST['colvehiculo'];
	$soat = $_POST['soat'];
	$soatcom = $_POST['soatcom'];
	$soatven = $_POST['soatven'];
	$tecno = $_POST['tecno'];
	$tecnoven = $_POST['tecnoven'];
	$tecnocom = $_POST['tecnocom'];

	if($confi==2)
	{
		$con = pg_query($dbconn,"update tbl_usuario set est_clave_int = '2',usu_confirmar = '".$confi."', usu_usu_actualiz = '".$usuario."', usu_fec_actualiz = '".$fecha."',usu_fec_activacion = '".$fecha."' where usu_clave_int = '".$edi."'");
		if($con>0)
		{
			$res = "ok";
			$msn = "Domiciliario descartado correctamente";
		}
		else
		{
			$res = "error";
			$msn = "Surgió un error al descartar domiciliario";
		}
	}
	else
	{


		if($act == '' || $act==NULL){ $swact = 0; }else{ $swact = 1; }
		$sql = pg_query($dbconn,"select * from tbl_usuario where UPPER(usu_email) = UPPER('".$ema."')) AND usu_clave_int <> '".$edi."' and est_clave_int!=2");//(UPPER(usu_usuario) = UPPER('".$usu."') OR 
		$dato = pg_fetch_array($sql);
		$conusu = $dato['usu_usuario'];
		$conema = $dato['usu_email'];
		$condoc = $dato['usu_documento'];
		$rutaa = $dato['usu_imagen'];
		$ruta = $_POST['ruta'];
		$trozos = explode(".", $ruta); 
		$rutaold = "../../".$ruta;
		$extension = end($trozos);
		$rutan = "../../modulos/usuarios/fotosUsuarios/".$edi.".".$extension;
		$indv2 = $_POST['indv2'];		
		
		$conpe = pg_query($dbconn,"select * from tbl_usuario where usu_clave_int ='".$edi."'");
		$datpe = pg_fetch_array($conpe);
		$pera = $datpe['prf_clave_int'];			
	
		/*if(STRTOUPPER($conusu) == STRTOUPPER($usu))
		{
			$res =  'error';
		    $msn = "Ya existe el usuario ingresado";
		}
		else*/
		if(STRTOUPPER($conema) == STRTOUPPER($ema))
		{
			$res =  'error';
			$rutanew  = "";
			$msn = "Ya hay un usuario con el correo electrónico indicado";
		}
		else if($doc==$condoc and $doc!="")
		{
			$res =  'error';
			$rutanew  = "";
			$msn = "Ya hay un usuario con el número de documento indicado";
		}
		else
		{				
			$con = pg_query($dbconn,"update tbl_usuario set usu_usuario = '".$usu."', usu_clave = '".$con1."', usu_nombre = '".$nom."', prf_clave_int = '".$per."', est_clave_int = '".$swact."', usu_email = '".$ema."', usu_usu_actualiz = '".$usuario."', usu_fec_actualiz = '".$fecha."',usu_color = '".$color."',usu_color_hex = '".$hex."',mer_clave_int = '".$mer."',usu_apellido = '".$ape."',usu_documento = '".$doc."',usu_confirmar = '".$confi."',usu_fec_activacion = '".$fecha."',usu_telefono = '".$tel."' where usu_clave_int = '".$edi."'");
			
			if($con > 0)
			{   
			  
				$del = pg_query($dbconn,"delete from tbl_usuario_sector where usu_clave_int = '".$edi."'");
				if($per==2)
				{
					$inssec = pg_query($dbconn,"INSERT INTO tbl_usuario_sector (usu_clave_int,use_usu_actualiz,use_fec_actualiz,sec_clave_int) SELECT '".$edi."','".$usuario."','".$fecha."',sec_clave_int from tbl_sector where sec_clave_int in(".$sec.")");
					//verificar info vehiculo
				  	$veriv = pg_query($dbconn,"select * from tbl_usuario_vehiculo where usu_clave_int = '".$edi."'");
				  	$numv = pg_num_rows($veriv);
				  	if($numv>0)
				  	{
				  		$updve = pg_query($dbconn,"UPDATE tbl_usuario_vehiculo SET uve_placa = '".$placa."',uve_marca = '".$marca."',uve_linea = '".$linea."',uve_modelo = '".$modelo."',uve_cilindrada = '".$cilindrada."',uve_color = '".$colvehiculo."',uve_soat = '".$soat."',uve_soat_company = '".$soatcom."',uve_soat_vencimiento = '".$soatven."',uve_tecno = '".$tecno."',uve_tecno_company = '".$tecnocom."',uve_tecno_vencimiento = '".$tecnoven."' WHERE usu_clave_int =  '".$edi."'");
				  	}
				  	else
				  	{
				  		$insve = pg_query($dbconn,"INSERT INTO tbl_usuario_vehiculo(usu_clave_int,uve_placa,uve_marca,uve_linea,uve_modelo,uve_cilindrada,uve_color,uve_soat,uve_soat_company,uve_soat_vencimiento,uve_tecno,uve_tecno_company,uve_tecno_vencimiento) VALUES('".$edi."','".$placa."','".$marca."','".$linea."','".$modelo."','".$cilindrada."','".$colvehiculo."','".$soat."','".$soatcom."','".$soatven."','".$tecno."','".$tecnocom."','".$tecnoven."')");
				  	}
				}				   
			   
				if(rename($rutaold,$rutan) and $rutaa!=$ruta)
				{ 
				    $rutanew = "modulos/usuarios/fotosUsuarios/".$edi.".".$extension;
					$update = pg_query($dbconn,"update tbl_usuario set usu_imagen='".$rutanew."' where usu_clave_int='".$edi."'");						
				}
				else
				{
				   // $res =  'error4';
					$rutanew = "";
				}

					ob_start();
					include('../../modulos/usuarios/domiciliariosconfirmar.php');	
					$content = ob_get_clean();
					$asunto1 = "Aceptacion Domiciliario delasiembra.com";
					$mail = new PHPMailer(true);
	                $mail->isSMTP();
	                $mail->SMTPAuth = true;
	                $maul->SMTPDebug   = 1;
	                $mail->SMTPSecure = "tls";
	                $mail->Host = "smtp.gmail.com";
	                $mail->Port = 587;
	                $mail->SMTPOptions = array(
	                    'ssl' => array(
	                        'verify_peer' => false,
	                        'verify_peer_name' => false,
	                        'allow_self_signed' => true
	                    )
	                );
	                 //Nuestra cuenta
					$mail->Username ='andres.199207@gmail.com';
					$mail->Password = 'Bayron.1214'; //Su password

					//$mail->From = "adminpavas@pavas.com.co";
					// Establecer de quién se va a enviar el mensaje
					$mail->setFrom("andres.199207@gmail.com", "delasiembra.com");
					 // Establecer a quién se enviará el mensaje

	                $mail->addAddress($ema, "Usuario: " . $usu);
	                $mail->Subject = utf8_decode($asunto1);
	                $mail->msgHTML($content);
	                if (!$mail->send())
	                {
	                	 $msn = 'No se envio mensaje al siguiente email<strong>(' . $ema . ')</strong>' . $mail->ErrorInfo . '<br>';
	                }
	                else
	                {
	                	$msn = "Correo enviado a".$ema."<br>";
	                }

				$res =  'ok';
				$msn.= "Domiciliario activado correctamente";
				
			}
			else
			{
				$res =  'error';      
				$rutanew = "";
				$msn = "Surgió un error al actualizar usuario. Error BD(".pg_last_error($dbconn).")";
			}
		}	
	}
	$datos[] = array("res"=>$res,"imagen"=>$rutanew."?".time(),"msn"=>$msn);
	echo json_encode($datos);
}
else
if($opcion == "LISTAUSUARIOS")
{
	?>
	<script type="text/javascript" src="jsdatatable/usuarios/jslistausuarios.js"></script>
	<div class="table-responsive" style="padding:10px">
	<table id="tbUsuarios" class="table table-striped table-bordered compact" cellspacing="0" width="100%" style="font-size:11px">
	<thead>
	<tr>
	<th class="dt-head-center" style="width: 180px"><strong>Nombre</strong></th>
	<th class="dt-head-center"><strong>Mercado</strong></th>
	<th class="dt-head-center" style="width: 121px"><strong>Perfil</strong></th>
	<th class="dt-head-center" style="width: 200px"><strong>E-mail</strong></th>
	<th class="dt-head-center" style="width: 98px"><strong>Telefono</strong></th>
	<th class="dt-head-center" style="width: 127px"><strong>Fecha Registro</strong></th>
	<th class="dt-head-center" style="width: 29px"><strong>Activo</strong></th>
	<th class="dt-head-center" style="width: 20px">&nbsp;</th>
	<th class="dt-head-center" style="width: 20px">&nbsp;</th>
	</tr>
	</thead>
	<tfoot>
	<tr>
	<th class="dt-head-center" style="width: 180px"><strong></strong></th>
	<th class="dt-head-center"></th>
	<th class="dt-head-center" style="width: 121px"><strong></strong></th>
	<th class="dt-head-center" style="width: 200px"><strong></strong></th>
	<th class="dt-head-center" style="width: 98px"><strong></strong></th>
	<th class="dt-head-center" style="width: 127px"><strong></strong></th>
	<th class="dt-head-center" style="width: 29px"><strong></strong></th>
	<th class="dt-head-center" style="width: 20px">&nbsp;</th>
	<th class="dt-head-center" style="width: 20px">&nbsp;</th>
	</tr>
	</tfoot>

	</table>
	</div>
	<?pHP
}
else if($opcion=="LISTACONFIRMAR")
{
	?>
	<script type="text/javascript" src="jsdatatable/usuarios/jslistaconfirmar.js"></script>
	<div class="table-responsive" style="padding:10px">
	<table id="tbUsuariosConfirmar" class="table table-striped table-bordered compact" cellspacing="0" width="100%" style="font-size:11px">
	<thead>
	<tr>	
	<th class="dt-head-center" style="width: 200px"><strong>E-mail</strong></th>
	<th class="dt-head-center" style="width: 200px"><strong>Celular</strong></th>	
	<th class="dt-head-center" style="width: 200px"><strong>IP</strong></th>
	<th class="dt-head-center" style="width: 200px"><strong>Fecha Registro</strong></th>
	<th class="dt-head-center" style="width: 20px">&nbsp;</th>
	<th class="dt-head-center" style="width: 20px">&nbsp;</th>
	</tr>
	</thead>

	</table>
	</div>
	<?php
}
else if($opcion=="FILTROS")
{
	?>
		<div class="col-md-12">
		<label>Nombre:</label>                    
		<input class="form-control input-sm"  name="busnombre" id="busnombre" maxlength="70" type="text" value="" placeholder="Nombre" /></div>
		<div class="col-md-12">
		<label>Correo:</label>
		<input class="form-control input-sm"  name="buscorreo" id="buscorreo" maxlength="70" type="text" value="" placeholder="E-Mail" /></div>
		<div class="col-md-12 hide">
		<label>Usuario:</label>
		<input class="form-control input-sm" name="bususuario" id="bususuario" maxlength="70" type="text" value="" placeholder="Usuarios"/>
		</div>
		<div class="col-md-12">
		<button onclick="CRUDUSUARIOS('LISTAUSUARIOS','');" type="button" class="btn btn-success btn-block btn-sm">Buscar <i class="fa fa-search fa-2x"></i></button>
		</div>
		<script  type="text/javascript" charset="utf-8">
			$(document).ready(function($) {
				$('#busnombre').on('keydown',function (e) {
					if(e.which==13) {
						CRUDUSUARIOS('LISTAUSUARIOS','');
					}
				});
				$('#buscorreo').on('keydown',function (e) {
					if(e.which==13) {
						CRUDUSUARIOS('LISTAUSUARIOS','');
					}
				});
				$('#bususuario').on('keydown',function (e) {
					if(e.which==13) {
						CRUDUSUARIOS('LISTAUSUARIOS','');
					}
				});
			});
		</script>
			<?php
			//echo "<script>CRUDUSUARIOS('LISTAUSUARIOS','','Todos');</script>";

}
else if($opcion=="ACTIVAR")
{
   $id = $_POST['id'];
   $indv2 = $_POST['indv2'];
   if($indv2>0)
   {
   		$con = pg_query($dbconnweb,"update tbl_usuario set est_clave_int = '1',usu_usu_actualiz='".$usuario."',usu_fec_actualiz= '".$fecha."' where usu_clave_int = '".$id."'");
		$con = pg_query($dbconn,"update tbl_usuario set est_clave_int = '1',usu_usu_actualiz='".$usuario."',usu_fec_actualiz= '".$fecha."' where usu_clave_int = '".$id."'");
   }
   else
   {
	   $con = pg_query($dbconn,"update tbl_usuario set est_clave_int = '1',usu_usu_actualiz='".$usuario."',usu_fec_actualiz= '".$fecha."' where usu_clave_int = '".$id."'");
	   
   }
   if($con>0)
   {
	   echo 1;
   }
   else 
   {
      echo 2;
   }
}
else if($opcion=="INACTIVAR")
{
   $id = $_POST['id'];
   
   $con = pg_query($dbconn,"update usuario set est_clave_int = '0',usu_usu_actualiz='".$usuario."',usu_fec_actualiz= '".$fecha."' where usu_clave_int = '".$id."'");
  
   if($con>0)
   {
	   echo 1;
   }
   else 
   {
      echo 2;
   }
}
else if($opcion=="ELIMINAR")
{
   $id = $_POST['id'];
   if($id==$idUsuario)
   {
	   $res = "error";
	   $msn = "No te puedes dar de baja del sistema";
   }
   else
   {
	  
		$con = pg_query($dbconn,"update tbl_usuario set est_clave_int = '2',usu_usu_actualiz='".$usuario."',usu_fec_actualiz= '".$fecha."' where usu_clave_int = '".$id."'");
	  
	    if($con>0)
	    {
		   $res =  "ok";
		   $msn = "Usuario eliminado correctamente";
	    }
	    else 
	    {
		   $res = "error";
		   $msn = "Surgió un error al eliminar el usuario seleccionado. Verificar";
	    } 
   }
   $datos[] = array( "res"=> $res, "msn"=> $msn);
   echo json_encode($datos);
}
else if($opcion=="TODOS")
{
	?>
	 <ul class="nav nav-tabs pull-left">              
		<li class="active"><a  onclick="CRUDUSUARIOS('LISTAUSUARIOS','','Todos')" data-toggle="tab" aria-expanded="true"><span id="todo">Todos
		<?php
		$con = pg_query($dbconn,"select COUNT(*) cant from tbl_usuario where est_clave_int!=2");
		$dato = pg_fetch_array($con);
		echo $dato['cant'];
		?>
		</span></a></li>
       <?php 
       $cons = pg_query($dbconn, "select p.prf_clave_int, UPPER(p.prf_descripcion) per from tbl_perfil p where p.est_clave_int = 1");
		while($dats = pg_fetch_array($cons))
		{
			$desp = $dats['per'];
			$clap  =$dats['prf_clave_int'];
			$con = pg_query($dbconn,"select COUNT(*) cant from tbl_usuario where prf_clave_int = '".$clap."' and est_clave_int!=2 and usu_confirmar=1");
			$dato = pg_fetch_array($con);
			?>
			<li class="">
			<a onclick="CRUDUSUARIOS('LISTAUSUARIOS','','<?php echo $clap;?>')"  data-toggle="tab" aria-expanded="false">
			 <span id="adm"><?php echo $desp." <span class='badge'>".$dato['cant']."</span>"; ?></span>
			</a>
			</li>
			<?php
		}

		$condom = pg_query($dbconn, "SELECT * FROM tbl_usuario WHERE prf_clave_int = 2 and usu_confirmar = 0 and est_clave_int!=2");
		$numdom = pg_num_rows($condom);
		if($numdom>0)
		{
			?>
			<li class="">
			<a onclick="CRUDUSUARIOS('LISTACONFIRMAR','','2')"  data-toggle="tab" aria-expanded="false">
			 <span id="admd">CONFIRMAR DOMICILIARIOS <span class='badge'><?php echo $numdom;?></span></span>
			</a>
			</li>
			<?php
		}
		?>
         </ul>
        <?PHP
	
}
else if($opcion=="CAMBIOCONTRASENA")
{
        $usu = $_POST['usuario'];
        $ema = $_POST['email'];
		$ant = $_POST['ant'];
		$nue = $_POST['nue'];
		$conf = $_POST['conf'];
		$capt = $_POST['captcha'];
		$imgcapt = $_SESSION['captcha'];
		$fecha=date("Y/m/d H:i:s");	
		$clave = $_COOKIE["clave"];
		$indv2 = $_POST['indv2'];
		$veria =  decrypt($clave,"p4v4svasquez");
        $sql = pg_query($dbconn,"select * from tbl_usuario where (UPPER(usu_usuario) = UPPER('".$usu."') OR UPPER(usu_email) = UPPER('".$ema."')) AND usu_clave_int <> '".$idUsuario."' and est_clave_int!=2");
        $dato = pg_fetch_array($sql);
        $conusu = $dato['usu_usuario'];
        $conema = $dato['usu_email'];
        $rutaa = $dato['usu_imagen'];
        $ruta = $_POST['ruta'];
        $trozos = explode(".", $ruta);
        $rutaold = "../".$ruta;
        $extension = end($trozos);
        $rutan = "../modulos/usuarios/fotosUsuarios/".$idUsuario.".".$extension;

        /*if(STRTOUPPER($conusu) == STRTOUPPER($usu))
        {
            $res =  2;
            $msn = "El usuario ingresado ya existe";
            $rutanew = "";
        }
        else*/
        if(STRTOUPPER($conema) == STRTOUPPER($ema))
        {
            $res =  2;
            $msn = "El correo electrónico ingresado ya existe";
            $rutanew  = "";
        }
        else
		if($veria <> $ant)
		{
			$res = 2;
			$msn = 'La contraseña anterior no es válida';
            $rutanew = "";
		}
		else		
		if($nue <> $conf)
		{
			$res = 2;
			$msn =  'Las contraseñas no coinciden';
            $rutanew = "";
		}
		else
		if($capt!=$imgcapt)
		{
			$res = 2;
			$msn =  'Los caracteres de verificacion no coinciden con los de la imagen';
            $rutanew = "";
		}
		else
		{
			$nue  = encrypt($nue,"p4v4svasquez");
			if($indv2>0)
			{
				$sql = pg_query($dbconnweb,"update tbl_usuario set usu_email = '".$ema."', usu_usuario = '".$usu."', usu_clave = '".$nue."',usu_fec_actualiz = '".$fecha."' where usu_clave_int = '".$idUsuario."'");
			}
			else
			{
				$sql = pg_query($dbconn,"update tbl_usuario set usu_email = '".$ema."',usu_usuario = '".$usu."',usu_clave = '".$nue."',usu_fec_actualiz = '".$fecha."' where usu_clave_int = '".$idUsuario."'");
			}
			
			if($sql >0)
			{		
                if(rename($rutaold,$rutan))
                {
                    $rutanew = "modulos/usuarios/fotosUsuarios/".$idUsuario.".".$extension;
                    $update = pg_query($dbconn,"update tbl_usuario set usu_imagen='".$rutanew."' where usu_clave_int='".$idUsuario."'");
                }
                else
                {                    
                    $rutanew = "";
                }
				$res = 1;				
				$msn =  'Información Guardada Correctamente';
			}
			else
			{
				$res = 2;
				$msn =  'No se han podido guardar los datos';
                $rutanew = "";
			}
		}

		$datos[] = array("res"=>$res,"msn"=>$msn,"cla"=>$veria,"imagen"=>$rutanew."?".time());
		echo json_encode($datos);
}
else if($opcion=="VERICAPTCHA")
{
	$capt = $_POST['captcha'];
	$imgcapt = $_SESSION['captcha'];
	if($capt!=$imgcapt)
	{
	  $res = 2;
	}
	else
	{
	  $res = 1;
	}
	$datos[] = array("res"=>$res,"capt"=>$imgcapt,"captn"=>$capt);
	echo json_encode($datos);
}
else if($opcion=="GUARDARPERMISO")
{
	$sigla = $_POST['id'];
	$usu = $_POST['usu'];
	$ck = $_POST['ck'];
	$indv2 = $_POST['indv2'];
	if($ck==1)
	{
		if($indv2>0)
		{
			$SQL = pg_query($dbconnweb,"INSERT INTO permisos_usuario(pea_sigla,usu_clave_int,pea_usu_actualiz,pea_fec_actualiz) VALUES('".$sigla."','".$usu."','".$usuario."','".$fecha."')");
			$SQL = pg_query($dbconn,"INSERT INTO permisos_usuario(pea_sigla,usu_clave_int,pea_usu_actualiz,pea_fec_actualiz) VALUES('".$sigla."','".$usu."','".$usuario."','".$fecha."')");
		}
		else
		{
			$SQL = pg_query($dbconn,"INSERT INTO permisos_usuario(pea_sigla,usu_clave_int,pea_usu_actualiz,pea_fec_actualiz) VALUES('".$sigla."','".$usu."','".$usuario."','".$fecha."')");
		}
	}
	else if($ck==0)
	{
		if($indv2>0)
		{
			$SQL = pg_query($dbconnweb,"DELETE FROM permisos_usuario where pea_sigla ='".$sigla."' and usu_clave_int = '".$usu."'");
			$SQL = pg_query($dbconn,"DELETE FROM permisos_usuario where pea_sigla ='".$sigla."' and usu_clave_int = '".$usu."'");
		}
		else
		{
			$SQL = pg_query($dbconn,"DELETE FROM permisos_usuario where pea_sigla ='".$sigla."' and usu_clave_int = '".$usu."'");
			
		}
	}
	if($SQL>0)
	{
	   $res = 1;
	   $msn = "";
	}
	else
	{
	   $res = 2;
	   $msn = "Surgió un Error. Verificar";
	}
	$datos[] = array("res"=>$res,"msn"=>$msn);
	echo json_encode($datos);
}
else if($opcion=="GUARDARPERMISO2")
{
	$sigla = $_POST['id'];
	$usu = $_POST['usu'];
	$ck = $_POST['ck'];
	$tip = $_POST['tip'];
	$indv2 = $_POST['indv2'];
	if($ck==1)
	{
		if($indv2>0)
		{
			//$SQL = pg_query($dbconnweb,"INSERT INTO permisos_usuario(pea_sigla,usu_clave_int,pea_usu_actualiz,pea_fec_actualiz,pea_tipo) VALUES('".$sigla."','".$usu."','".$usuario."','".$fecha."','".$tip."')");
			$SQL = pg_query($dbconn,"INSERT INTO permisos_usuario(pea_sigla,usu_clave_int,pea_usu_actualiz,pea_fec_actualiz,pea_tipo) VALUES('".$sigla."','".$usu."','".$usuario."','".$fecha."','".$tip."')");
		}
		else
		{
			$SQL = pg_query($dbconn,"INSERT INTO permisos_usuario(pea_sigla,usu_clave_int,pea_usu_actualiz,pea_fec_actualiz,pea_tipo) VALUES('".$sigla."','".$usu."','".$usuario."','".$fecha."','".$tip."')");
		}
	}
	else if($ck==0)
	{
		if($indv2>0)
		{
			//$SQL = pg_query($dbconnweb,"DELETE FROM permisos_usuario where pea_sigla ='".$sigla."' and usu_clave_int = '".$usu."' and pea_tipo='".$tip."'");
			$SQL = pg_query($dbconn,"DELETE FROM permisos_usuario where pea_sigla ='".$sigla."' and usu_clave_int = '".$usu."' and pea_tipo='".$tip."'");
		}
		else
		{
			$SQL = pg_query($dbconn,"DELETE FROM permisos_usuario where pea_sigla ='".$sigla."' and usu_clave_int = '".$usu."' and pea_tipo ='".$tip."'");
			
		}
	}
	if($SQL>0)
	{
	   $res = 1;
	   $msn = "";
	}
	else
	{
	   $res = 2;
	   $msn = "Surgió un Error. Verificar";
	}
	$datos[] = array("res"=>$res,"msn"=>$msn);
	echo json_encode($datos);
}
else if($opcion=="AJUSTECUENTA")
{
	$con = pg_query($dbconn,"SELECT usu_usuario,usu_nombre,usu_apellido,usu_email,usu_genero,usu_fec_nacimiento,usu_telefono,usu_genero,usu_modo FROM tbl_usuario WHERE usu_clave_int= '".$idUsuario."' limit 1");
	$dat = pg_fetch_array($con);
	$nom = $dat['usu_nombre'];
	$ape = $dat['usu_apellido'];
	$ema = $dat['usu_email'];
	$gen = $dat['usu_genero'];
	$fec = $dat['usu_fec_nacimiento'];
	$tel = $dat['usu_telefono'];
	$gen = $dat['usu_genero'];
	$mod = $dat['usu_modo'];
	?>
	
	<div class="row">
        <h3>Información de la cuenta</h3>
        <div class="col-md-6">
        <label for="txtnombre" class="bmd-label-floating">Nombre(s):</label>
        <input type="text" class="form-control" id="txtnombre" name="txtnombre" data-old="<?php echo $nom;?>" value="<?php echo $nom;?>">
       
        </div>
        <div class="col-md-6">
        <label for="txtapellido" class="bmd-label-floating">Apellido(s):</label>
        <input type="text" class="form-control" id="txtapellido" name="txtapellido" data-old="<?php echo $ape;?>" value="<?php echo $ape;?>">
        
        </div>
         <div class="col-md-6">
        <label for="txtcorreo" class="control-label">Correo electronico:</label>
        <input type="email" class="form-control" id="txtcorreo" name="txtcorreo" data-old="<?php echo $ema;?>" value="<?php echo $ema;?>" data-error="La dirección de correo electrónico no es válida" <?php if($mod=="facebook"){ echo "disabled";}?>>
        <div class="help-block with-errors"></div>
       
        </div>
        <div class="col-md-6">
        <label for="txttelefono" class="control-label">Telefono(s):</label>
        <input type="text" class="form-control" id="txttelefono" name="txttelefono" value="<?php echo $tel;?>">
        <div class="help-block with-errors"></div>
        
        </div>
        <div class="col-md-6">
        <label for="txtfecha" class="bmd-label-floating">Fecha Nacimiento(s):</label>
        <input type="date" class="form-control" id="txtfecha" name="txtfecha" data-date-format="yyyy-mm-dd" autocomplete="off" data-old="<?php echo $fec;?>" value="<?php echo $fec;?>">
        
        </div>
        <div class="col-md-6 col-xs-6">
        	<br>
             
                <label>
                  <input type="radio" name="radgenero"  value="M" <?php if($gen==""|| $gen=="M"){ echo "checked";}?>>
                  Masculino
                </label>
                <label>
                  <input type="radio" name="radgenero" value="F"  <?php if($gen=="F"){ echo "checked";}?>>
                  Femenino
                </label><br><br>
           
          </div>
          <div class="col-md-6">
        <label for="txtpass1" class="bmd-label-floating">Contraseña(s):</label>
        <input type="password" class="form-control" id="txtpass1" name="txtpass1" autocomplete="off" data-old="">
        
        </div>
        <div class="col-md-6">
        <label for="txtpass2" class="bmd-label-floating">Repite Contraseña:</label>
        <input type="password" class="form-control" id="txtpass2" name="txtpass2" autocomplete="off" data-old="">
        
        </div>
    </div>
    <div class="row">
          <div class="col-md-12">
          	<button onclick="CRUDCUENTA('GUARDARAJUSTE','<?php echo $idUsuario;?>')" type="button" id="btnguardarajuste" class="btn btn-success" disabled>Actualizar datos</button>
          </div>
    </div>
      <script>
      	$(document).ready(function(e) {
      		
      		$('#txtnombre,#txtapellido,#txtfecha,#txttelefono,#txtcorreo,#txtpass1,#txtpass2').on('keyup change',function(){
      			var old = $(this).attr('data-old');
      			var act = $(this).val();
      			var type = $(this).attr('type');
      			
      			var btnguardar = $('#btnguardarajuste');
      			
      			if(type=="email" && act!="" && (act.indexOf('@', 0) == -1 || act.indexOf('.', 0) == -1) )
      			{
      				btnguardar.prop('disabled', true);
      			}
      			else if(act!=old)
      			{
      				
      				btnguardar.prop('disabled', false);
      			}	
      			else
      			{
      				console.log(old);
      				btnguardar.prop('disabled', true);
      			}
      		});

      		$('input:radio[name=radgenero]').on('click', function(event) {
      			var btnguardar = $('#btnguardarajuste');
      			btnguardar.prop('disabled', false);
      		});
      		
      	});
      </script>


	<?php
		echo "<script>INICIALIZARCONTENIDO();</script>";
}
else if($opcion=="NUEVODOMICILIARIO")
{
	?>
	<form id="formcuenta" name="formcuenta" class="form-horizontal" method="post" action="_" enctype="multipart/form-data">
          <div class="row">        
            <div class="col-md-8 col-md-offset-2 col-xs-12 col-sm-12" >
            	<div class="col-md-6"><label>Nombre:<span class="symbol required"></label></strong>
		             <input type="text" name="txtnombre" id="txtnombre" class="form-control input-sm" autocomplete="off">
		         </div>
		          <div class="col-md-6"><label>Apellido:<span class="symbol required"></label></strong>
		             <input type="text" name="txtapellido" id="txtapellido" class="form-control input-sm" autocomplete="off">
		         </div>
		     
		        
		          <div class="col-md-6" ><label>Documento:<span class="symbol required"></label></strong>
		             <input type="text" name="txtdocumento" id="txtdocumento" class="form-control input-sm" autocomplete="off">
		         </div>
				<div class="col-md-6">
				<label for="txtcorreo" >Correo electronico:</label>
				<input type="email" class="form-control  input-sm" id="txtcorreo" name="txtcorreo"  data-error="La dirección de correo electrónico no es válida">
				

				</div>
				<div class="col-md-6">
				<label for="txttelefono" >Celular:</label>
				<input type="text" class="form-control  input-sm" id="txttelefono" name="txttelefono">
				

				</div>
				<div class="col-md-6">
				<label for="txtpass1" >Contraseña(s):</label>
				<input type="password" class="form-control  input-sm" id="txtpass1" name="txtpass1" autocomplete="off" >

				</div>
				<div class="col-md-6">
				<label for="txtpass2" >Repite Contraseña:</label>
				<input type="password" class="form-control  input-sm" id="txtpass2" name="txtpass2" autocomplete="off">

				</div>
            </div>
        </div>
        <div class="row" id="divsectores">
        	<div class="col-md-8 col-md-offset-2 col-xs-12 col-sm-12" >
		     	<h4 class="col-md-12">Sectores a los que aplica</h4>
			     	<div class="col-md-12">
			     		
			     		<select multiple id="selsectores" name="selsectores" class="form-control selectpicker" placeholder="seleccione los sectores a los que aplica el domiciliario" data-header="Buscar por nombre" multiple data-actions-box="true" data-live-search="true" data-selected-text-format="count > 6">
			     			<?php
			     			$consec = pg_query($dbconn, "SELECT sec_clave_int,sec_nombre FROM tbl_sector where est_clave_int = '1' order by sec_clave_int");
			     			while($datsec = pg_fetch_array($consec))
			     			{
			     				$sec = $datsec['sec_clave_int'];
			     				$nomsec = $datsec['sec_nombre'];
			     				$veri = pg_query($dbconn,"select * from tbl_usuario_sector where usu_clave_int = '".$usuedi."' and sec_clave_int = '".$sec."'");
			     				$numv = pg_num_rows($veri);
			     				?>
			     			<option <?php if($numv>0){ echo "selected"; }?> value="<?php echo $sec;?>"><?php echo $nomsec; ?></option>
			     			<?php
			     			}
			     			?>
			     			
			     		</select>
			     	</div>
			     	<h4 class="col-md-12">Información Vehiculo</h4>
			     	<div class="col-md-4">
			     		<label for="txtplaca">Placa:</label>
			     			<input id="txtplaca" name="txtplaca" type="text" class="form-control input-sm" value="<?php echo $placa;?>">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txtmarca">Marca:</label>
			     			<input id="txtmarca" name="txtmarca" type="text" class="form-control input-sm" value="<?php echo $marca;?>">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txtlinea">Linea:</label>
			     			<input id="txtlinea" name="txtlinea" type="text" class="form-control input-sm" value="<?php echo $linea;?>">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txtmodelo">Modelo:</label>
			     			<input id="txtmodelo" name="txtmodelo" type="text" class="form-control input-sm" value="<?php echo $modelo;?>" onkeypress="return validar_texto(event)">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txtcilindrada">Cilindrada:</label>
			     			<input id="txtcilindrada" name="txtcilindrada" type="text" class="form-control input-sm" value="<?php echo $cilindrada;?>" onkeypress="return validar_texto(event)">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txtcolor">Color:</label>
			     			<input id="txtcolor" name="txtcolor" type="text" class="form-control input-sm" value="<?php echo $color;?>" >
			     	</div>

			     	<div class="col-md-4">
			     		<label for="txtsoat">Nro Soat:</label>
			     			<input id="txtsoat" name="txtsoat" type="text" class="form-control input-sm" value="<?php echo $soat;?>">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txtsoatcompany">Compañia Soat:</label>
			     			<input id="txtsoatcompany" name="txtsoatcompany" type="text" class="form-control input-sm" value="<?php echo $soatcompany;?>">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txtsoatvencimiento">Vencimiento:</label>
			     			<input id="txtsoatvencimiento" name="txtsoatvencimiento" type="text" class="form-control input-sm datepicker" value="<?php echo $soatven;?>" data-date-format="yyyy-mm-dd">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txttecno">Nro Tecno:</label>
			     			<input id="txttecno" name="txttecno" type="text" class="form-control input-sm" value="<?php echo $tecno;?>">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txttecnocompany">Compañia Tecno:</label>
			     			<input id="txttecnocompany" name="txttecnocompany" type="text" class="form-control input-sm" value="<?php echo $tecnocompany;?>">
			     	</div>
			     	<div class="col-md-4">
			     		<label for="txttecnovencimiento">Vencimiento:</label>
			     			<input id="txttecnovencimiento" name="txttecnovencimiento" type="text" class="form-control input-sm datepicker" value="<?php echo $tecnoven;?>" data-date-format="yyyy-mm-dd">
			     	</div>
	     	 		

			     	
			     	<h4 class="btn-block">Adjuntar la siguiente informacion<em>(Pantallazo o copia)</em></h4>
			     	<div class="col-md-4">
	     	 			<label>Cedula:</label>
	     	 			<input type="file" class="dropify" id="filecedula" name="filecedula" onChange="setpreview('rutacedula','filecedula','formcuenta')" data-allowed-file-extensions="jpg png gif jpeg pdf">	     					
	     				<span id="rutacedula"  class="hide"></span>
	     			</div>
	     			<div class="col-md-4">
	     	 			<label>Pase:</label>
	     	 			<input type="file" class="dropify" id="filepase" name="filepase" onChange="setpreview('rutapase','filepase','formcuenta')" data-allowed-file-extensions="jpg png gif jpeg pdf ">	     					
	     				<span id="rutapase"  class="hide"></span>
	     			</div>
			     	
	     			<div class="col-md-4">
	     	 			<label>Soat:</label>
	     	 			<input type="file" class="dropify" id="filesoat" name="filesoat" onChange="setpreview('rutasoat','filesoat','formcuenta')" data-allowed-file-extensions="jpg png gif jpeg pdf">	     					
	     				<span id="rutasoat"  class="hide"></span>
	     			</div>
	     			<div class="col-md-4">
	     	 			<label>Técnicomecanica(opcional):</label>
	     	 			<input type="file" class="dropify" id="filetecno" name="filetecno" onChange="setpreview('rutatecno','filetecno','formcuenta')" data-allowed-file-extensions="jpg png gif jpeg pdf">	     					
	     				<span id="rutatecno"  class="hide"></span>
	     			</div>
	     			<div class="col-md-4">
	     	 			<label>Runt:</label>
	     	 			<input type="file" class="dropify" id="filerunt" name="filerunt" onChange="setpreview('rutarunt','filerunt','formcuenta')" data-allowed-file-extensions="jpg png gif jpeg pdf">
	     					<span class="help-block">
	     						<a style="cursor:pointer;" target="_blank" data-toggle="tooltip" title="Clic para dirigirse a la consulta de tu runt" href="https://www.runt.com.co/consultaCiudadana/#/consultaVehiculo">https://www.runt.com.co/consultaCiudadana/#/consultaVehiculo</a>
	     					</span>
	     				<span id="rutarunt" class="hide"></span>
	     			</div>
			</div>
			    
		</div>
			 
        <div class="row"><!----> 
        	<div class="col-md-6 col-md-offset-3 col-xs-12 col-sm-12"><br>
        		<button type="button" class="btn btn-danger col-sm-12 col-xs-12 col-md-12" onclick="CRUDUSUARIOS('GUARDARDOMICILIARIO','')"> Enviar solicitud</button> 
        		<!---->
        	</div>
        </div>
       </form>   
	 <iframe src="about:blank" name="null" style="display:none"></iframe>
	<?php
	echo "<script>INICIALIZARCONTENIDO();</script>";
}
else if($opcion=="GUARDARAJUSTE")
{
	$id = $_POST['id'];
	$nom = $_POST['nom'];
	$ape = $_POST['ape'];
	$tel = $_POST['tel'];
	$fec = $_POST['fec'];
	$gen = $_POST['gen'];
	
	$pass1 = $_POST['pass1'];
	$pass2 = $_POST['pass2'];
	$ruta = $_POST['rut'];
	$trozos = explode(".", $ruta); 
	$rutaold = "../../".$ruta;
	$extension = end($trozos);
	$rutan = "../../modulos/usuarios/fotosUsuarios/".$id.".".$extension;

	$sql = pg_query($dbconn,"select * from tbl_usuario where usu_clave_int = '".$id."' limit 1");
	$dato = pg_fetch_array($sql);
	$rutaa = $dato['usu_imagen'];

	/*if($fec!="" and CalculaEdad($fec)<18)
	{
		$res = "error";
		$msn = "Debe"
	}
	else
	{*/
		$upd = pg_query($dbconn,"UPDATE tbl_usuario SET usu_nombre = '".$nom."',usu_apellido ='".$ape."',usu_telefono ='".$tel."',usu_genero ='".$gen."',usu_fec_nacimiento = '".$fec."',usu_fec_actualiz='".$fecha."',usu_usu_actualiz='".$usuario."' where usu_clave_int='".$id."'");
		if($upd>0)
		{
			if($pass1!="")
			{
				$pass1 = encrypt($pass1,'p4v4svasquez');
				$upd1 = pg_query($dbconn,"update tbl_usuario SET usu_clave = '".$pass1."' where usu_clave_int = '".$id."'");
				
			}
			if(rename($rutaold,$rutan) and $rutaa!=$ruta)
			{ 
				$rutanew = "modulos/usuarios/fotosUsuarios/".$id.".".$extension;
				$update = pg_query($dbconn,"update tbl_usuario set usu_imagen='".$rutanew."' where usu_clave_int='".$id."'");
			}
			$res = "ok";
			$msn = "Haz actualizado la Información de tu cuenta correctamente";

		}
		else
		{
			$res = "error";
			$msn = "Surgió un error al actualizar tu cuenta. Error BD(".pg_last_error($dbconn).")";
		}
		$datos[] = array("res"=>$res,"msn"=>$msn);
		echo json_encode($datos);
	//}


}
else if($opcion=="APLICARDOMICILIARIO")
{
   ?>
   <div class="row">
        <h3>Registro como Domiciliario</h3>
        
         <div class="col-md-6">
        <label for="txtcorreo" class="bmd-label-floating">Correo electronico:</label>
        <input type="email" class="form-control" id="txtcorreo" name="txtcorreo" data-old="<?php echo $ema;?>" value="<?php echo $ema;?>">
       
        </div>
        <div class="col-md-6">
        <label for="txttelefono" class="bmd-label-floating">Celular:</label>
        <input type="text" class="form-control" id="txttelefono" name="txttelefono">
        
        </div>
        <div class="col-md-6">
        <label for="txtpassword1" class="bmd-label-floating">Contraseña:</label>
        <input type="password" class="form-control" id="txtpassword1" name="txtpassword1">
        
        </div>
         <div class="col-md-6">
	        <label for="txtpassword1" class="bmd-label-floating">Repite la Contraseña:</label>
	        <input type="password" class="form-control" id="txtpassword2" name="txtpassword2">        
        </div>
        
        <div class="col-md-12">
          	<button type="button" id="btnregistrarse" class="btn btn-danger" disabled>Registrarse</button>
         </div>
    </div>
   <?PHP
}
else if($opcion=="RECUPERARCONTRASENA")
{
		//header("Cache-Control: no-store, no-cache, must-revalidate");		
		$email = $_POST['email'];
		$con = pg_query($dbconn,"select * from tbl_usuario where usu_email = '".$email."' and est_clave_int !=2 limit 1");
		$dato = pg_fetch_array($con);
		$usucla = $dato['usu_clave_int'];
		$usu = $dato['usu_usuario'];
		$ema = $dato['usu_email'];
		$act = $dato['est_clave_int'];
		$clave = $dato['usu_clave'];
		
		$con = pg_query($dbconn,"select * from tbl_recuperar where usu_clave_int = '".$usucla."' and rec_estado = 0");
		$num = pg_num_rows($con);
		
		if($num > 0)
		{
			$dato = pg_fetch_array($con);
			$random = $dato['rec_codigo'];
		}
		else
		{
			$length = 50;
			$random = "";
			$characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"; // change to whatever characters you want
			while ($length > 0) {
				$random .= $characters[mt_rand(0,strlen($characters)-1)];
				$length -= 1;
			}
			$con = pg_query($dbconn,"insert into tbl_recuperar(rec_codigo,usu_clave_int,rec_estado) values('".$random."','".$usucla."','0')");
		}
		
		if($email != '')
		{
			if(($ema == $email))
			{
				$asunto1 = "Recuperación Clave DELASIEMBRA.COM";
							
				// Cuerpo del mensaje
				$mensaje = "------------------------------------------- <br>";
				$mensaje.= "<strong>Hola</strong>, ".$usu."!<br><br>";
				$mensaje.= "DELASIEMBRA.COM registra que has hecho una solicitud de recuperacion de contraseña.<br>";
				$mensaje.= "------------------------------------------- <br>";
				$mensaje.= "Para restablecer su contrasena, solo debes presionar clic en el siguiente enlace o copielo y peguelo en la barra de direcciones de su navegador:<br>";
				
				$mensaje.= "<a href='https://delasiembra.com/restablecer.php?codigo=".$random."' target='_blank'>Clic aqui</a> o :https://delasiembra.com/restablecer.php?codigo=".$random."<br>";
				$mensaje.= "<strong>Fecha:</strong>".date("d/m/Y H:m:s")."<br><br>";

				$contenido = file_get_contents('../../modulos/plantillas/correousuario.php');

				$contenido = str_replace('%contenido%', $mensaje, $contenido);

				$mail = new PHPMailer(true);
                $mail->isSMTP();
                $mail->SMTPAuth = true;
                $maul->SMTPDebug   = 1;
                $mail->SMTPSecure = "tls";
                $mail->Host = "smtp.gmail.com";
                $mail->Port = 587;
                $mail->SMTPOptions = array(
                    'ssl' => array(
                        'verify_peer' => false,
                        'verify_peer_name' => false,
                        'allow_self_signed' => true
                    )
                );
                 //Nuestra cuenta
				$mail->Username ='andres.199207@gmail.com';
				$mail->Password = 'Bayron.1214'; //Su password

				//$mail->From = "adminpavas@pavas.com.co";
				// Establecer de quién se va a enviar el mensaje
				$mail->setFrom("andres.199207@gmail.com", "delasiembra.com");
				 // Establecer a quién se enviará el mensaje

                $mail->addAddress($ema, "Usuario: " . $usu);
                $mail->Subject = utf8_decode($asunto1);
                $mail->msgHTML($contenido);
                if (!$mail->send())
                {
                	 $res = "error";
                	 $msn .= 'No se envio mensaje al siguiente email<strong>(' . $ema . ')</strong>' . $mail->ErrorInfo . '<br>';
                }
                else
                {
                	$res = "ok";
                	$msn = "Su contraseña a sido recuperada satisfactoriamente<br> Por favor revise su correo:".$ema;
                }							
			
			}
			else
			{
				$res=  "error";
				$msn = "Correo electronico incorrecto";
			}
		}
		else
		{
			$res =  "error";
			$msn = "Correo electronico incorrecto";
		}
		$datos[] = array("res"=>$res,"msn"=>$msn);
		echo json_encode($datos);
}
else if($opcion=="RESTABLECER")
{
	$cod = $_POST['cod'];
	$con1 = $_POST['con1'];
	$con2 = $_POST['con2'];
	
	$con = pg_query($dbconn,"select * from tbl_recuperar where rec_codigo = '".$cod."' and rec_estado = 1");
	$num = pg_num_rows($con);
	
	if($num > 0)
	{
		$res = "error";
		$msn = "Este codigo ya fue usado anteriormente";
	}
	else
	{
		if($con1 == $con2)
		{
			$con = pg_query($dbconn,"select * from tbl_recuperar where rec_codigo = '".$cod."'");
			$dato = pg_fetch_array($con);
			$usu = $dato['usu_clave_int'];
			
			$con = pg_query($dbconn,"update tbl_recuperar set rec_estado = 1 where rec_codigo = '".$cod."'");
			$con = pg_query($dbconn,"update tbl_usuario set usu_clave = '".encrypt($con1,'p4v4svasquez')."' where usu_clave_int = '".$usu."'");
			
			if($con >= 1)
			{
				$res = "ok";
				$msn  = "Su contraseña a sido restablecida correctamente!</div>";
			}
			else
			{
				$res = "error";
				$msn =  "Error al restablecer la contraseña";
			}
		}
		else
		{
			$res = "error";
			$msn = "Las contraseñas no coinciden>";
		}
	}
	$datos[] = array("res"=>$res,"msn"=>$msn);
	echo json_encode($datos);
}
else if($opcion=="NUEVOREGISTRO")
{
	$fecha=date("Y/m/d H:i:s");
	$nom = $_POST['nombre'];
	$ape = $_POST['apellido'];
	$con1 = $_POST['pass'];
	$con1 = encrypt($con1,'p4v4svasquez');
	$per = "3";		
	$mer = "3";
	$correo = $_POST['email'];
	$swact = "1";
	$tel = $_POST['telefono'];
	
	
	$sql = pg_query($dbconn,"select * from tbl_usuario where UPPER(usu_email) = UPPER('".$correo."') and est_clave_int!=2");
	
	$dato = pg_fetch_array($sql);
	$conusu = $dato['usu_usuario'];
	$conema = $dato['usu_email'];
	
	if(STRTOUPPER($conema) == STRTOUPPER($correo))
	{
		$res = "error";
		$msn  =" Ya se encuentra un usuario registrado con el correo electronico diligenciado: ".$correo;
	}
	else
	{
		    
		$con = pg_query($dbconn,"insert into tbl_usuario (usu_clave,usu_nombre,prf_clave_int,est_clave_int,usu_email,usu_fec_actualiz,mer_clave_int,usu_apellido,usu_ip,usu_fec_registro,usu_telefono) values('".$con1."','".$nom."','".$per."','".$swact."','".$correo."','".$fecha."','".$mer."','".$ape."','".$IP."','".$fecha."','".$tel."')");
		$uid = pg_fetch_row(pg_query($dbconn,"SELECT setval('usuario_id_seq',nextval('usuario_id_seq')-1) as id;"));
		$id = $uid[0];
		if($con > 0)
		{			
			//envio de correo aadministrador sobre nuevo registro

				$asunto1 = "Nuevo Registro DELASIEMBRA.COM";
				$conu = pg_query($dbconn, "SELECT usu_usuario,usu_email,usu_nombre,usu_apellido from tbl_usuario where prf_clave_int = 1 and est_clave_int = 1 and usu_clave_int = 1");
				$numu = pg_num_rows($conu);
				if($numu>0)
				{
					for($n=0;$n<$numu;$n++)
					{
						$datu = pg_fetch_array($conu);
						$usu = $datu['usu_usuario'];
						$ema = $datu['usu_email'];
						$nombre = $datu['usu_nombre']." ".$datu['usu_apellido'];
							
						// Cuerpo del mensaje
						$mensaje = "------------------------------------------- <br>";
						$mensaje.= "<strong>Hola</strong>, ".$usu."!<br><br>";
						$mensaje.= "DELASIEMBRA.COM registra que se ha hecho una nuevo registro de usuario.<br>";
						$mensaje.= "------------------------------------------- <br>";
						$mensaje.= "<strong>Nombre:</strong>".$nom."<br>";						
						$mensaje.= "<strong>Correo electronico:</strong> ".$correo."<br>";
						$mensaje.= "<strong>Telefono:</strong> ".$tel."<br>";
						$mensaje.= "<strong>IP registro:</strong> ".$IP."<br>";
						$mensaje.= "<strong>Fecha:</strong>".date("d/m/Y H:m:s")."<br><br>";

						$contenido = file_get_contents('../../modulos/plantillas/correousuario.php');

						$contenido = str_replace('%contenido%', $mensaje, $contenido);

						$mail = new PHPMailer(true);
		                $mail->isSMTP();
		                $mail->SMTPAuth = true;
		                $maul->SMTPDebug   = 1;
		                $mail->SMTPSecure = "tls";
		                $mail->Host = "smtp.gmail.com";
		                $mail->Port = 587;
		                $mail->SMTPOptions = array(
		                    'ssl' => array(
		                        'verify_peer' => false,
		                        'verify_peer_name' => false,
		                        'allow_self_signed' => true
		                    )
		                );
		                 //Nuestra cuenta
						$mail->Username ='andres.199207@gmail.com';
						$mail->Password = 'Bayron.1214'; //Su password

						//$mail->From = "adminpavas@pavas.com.co";
						// Establecer de quién se va a enviar el mensaje
						$mail->setFrom("andres.199207@gmail.com", "delasiembra.com");
						 // Establecer a quién se enviará el mensaje

		                $mail->addAddress($ema, "Usuario: " . $usu);
		                $mail->Subject = utf8_decode($asunto1);
		                $mail->msgHTML($contenido);
		                if (!$mail->send())
		                {
		                	 //$res = "error";
		                	 $msn.= 'No se envio mensaje al siguiente email<strong>(' . $ema . ')</strong>' . $mail->ErrorInfo . '<br>';
		                }
		                else
		                {
		                	//$res = "ok";
		                	
		                }		
                	}
                }					
			$res =  'ok';			
			$msn = "Gracias por registrarte en nuestra plataforma. Ya podras acceder a nuestros servicios";
		 
		}
		else
		{
			$res =  'error3';
			$msn = "Surgió un error en el registro";
		}
	}	
	$datos[] = array("res"=>$res,"msn"=>$msn,"id"=>$id);
	echo json_encode($datos);
}
else if($opcion=="GUARDARDOMICILIARIO")
{
	$fecha=date("Y/m/d H:i:s");
	
	$tel = $_POST['tel'];
	$con1 = $_POST['pass'];
	$con1 = encrypt($con1,'p4v4svasquez');
	$per = "2";		
	$mer = "0";
	$correo = $_POST['email'];
	$swact = "1";
	$swcon = "0";

	$nom = $_POST['nombre'];
	$ape = $_POST['apellido'];
	$sec = implode(",",$_POST['sec']);
	$doc = $_POST['doc'];


	//info vehiculo

	$placa = $_POST['placa'];
	$marca = $_POST['marca'];
	$linea = $_POST['linea'];
	$modelo = $_POST['modelo'];
	$cilindrada = $_POST['cilindrada'];
	$colvehiculo = $_POST['colvehiculo'];
	$soat = $_POST['soat'];
	$soatcom = $_POST['soatcom'];
	$soatven = $_POST['soatven'];
	$tecno = $_POST['tecno'];
	$tecnoven = $_POST['tecnoven'];
	$tecnocom = $_POST['tecnocom'];


	$rutarunt = $_POST['rutarunt'];
	$trozosrunt = explode(".", $rutarunt); 
	$rutaoldrunt = "../../".$rutarunt;
	$extensionrunt = end($trozosrunt);

	$rutasoat = $_POST['rutasoat'];
	$trozossoat = explode(".", $rutasoat); 
	$rutaoldsoat = "../../".$rutasoat;
	$extensionsoat = end($trozossoat);

	$rutatecno = $_POST['rutatecno'];
	$trozostecno = explode(".", $rutatecno); 
	$rutaoldtecno = "../../".$rutatecno;
	$extensiontecno = end($trozostecno);

	$rutapase = $_POST['rutapase'];
	$trozospase = explode(".", $rutapase); 
	$rutaoldpase = "../../".$rutapase;
	$extensionpase = end($trozospase);


	$rutacedula = $_POST['rutacedula'];
	$trozoscedula = explode(".", $rutacedula); 
	$rutaoldcedula = "../../".$rutacedula;
	$extensioncedula = end($trozoscedula);


	
	
	$sql = pg_query($dbconn,"select * from tbl_usuario where UPPER(usu_email) = UPPER('".$correo."') and est_clave_int!=2");
	
	$dato = pg_fetch_array($sql);
	$conusu = $dato['usu_usuario'];
	$conema = $dato['usu_email'];
	$condoc = $dato['usu_documento'];
	

	if(STRTOUPPER($condoc) == STRTOUPPER($doc))
	{
		$res = "error";
		$msn  =" Ya se encuentra un usuario registrado con el numero de documento  diligenciado: ".$condoc;
	}
	else if(STRTOUPPER($conema) == STRTOUPPER($correo))
	{
		$res = "error";
		$msn  =" Ya se encuentra un usuario registrado con el correo electronico diligenciado: ".$correo;
	}
	else
	{
		    
		$con = pg_query($dbconn,"insert into tbl_usuario (usu_nombre,usu_apellido,usu_documento,usu_clave,usu_telefono,prf_clave_int,est_clave_int,usu_email,usu_fec_actualiz,mer_clave_int,usu_confirmar,usu_ip,usu_fec_registro) values('".$nom."','".$ape."','".$doc."','".$con1."','".$tel."','".$per."','".$swact."','".$correo."','".$fecha."','".$mer."','0','".$IP."','".$fecha."')");

		

		if($con > 0)
		{	
				$uid = pg_fetch_row(pg_query($dbconn,"SELECT setval('usuario_id_seq',nextval('usuario_id_seq')-1) as id;"));
				$id = $uid[0];
				//daTOS SECTORES
				$del = pg_query($dbconn,"delete from tbl_usuario_sector where usu_clave_int = '".$id."'");
				
				$inssec = pg_query($dbconn,"INSERT INTO tbl_usuario_sector (usu_clave_int,use_usu_actualiz,use_fec_actualiz,sec_clave_int) SELECT '".$id."','".$usuario."','".$fecha."',sec_clave_int from tbl_sector where sec_clave_int in(".$sec.")");
				//verificar info vehiculo
			  	$veriv = pg_query($dbconn,"select * from tbl_usuario_vehiculo where usu_clave_int = '".$id."'");
			  	$numv = pg_num_rows($veriv);
			  	if($numv>0)
			  	{
			  		$updve = pg_query($dbconn,"UPDATE tbl_usuario_vehiculo SET uve_placa = '".$placa."',uve_marca = '".$marca."',uve_linea = '".$linea."',uve_modelo = '".$modelo."',uve_cilindrada = '".$cilindrada."',uve_color = '".$colvehiculo."',uve_soat = '".$soat."',uve_soat_company = '".$soatcom."',uve_soat_vencimiento = '".$soatven."',uve_tecno = '".$tecno."',uve_tecno_company = '".$tecnocom."',uve_tecno_vencimiento = '".$tecnoven."' WHERE usu_clave_int =  '".$id."'");
			  	}
			  	else
			  	{
			  		$insve = pg_query($dbconn,"INSERT INTO tbl_usuario_vehiculo(usu_clave_int,uve_placa,uve_marca,uve_linea,uve_modelo,uve_cilindrada,uve_color,uve_soat,uve_soat_company,uve_soat_vencimiento,uve_tecno,uve_tecno_company,uve_tecno_vencimiento) VALUES('".$id."','".$placa."','".$marca."','".$linea."','".$modelo."','".$cilindrada."','".$colvehiculo."','".$soat."','".$soatcom."','".$soatven."','".$tecno."','".$tecnocom."','".$tecnoven."')");
			  	}


			  	//DAOS ADJUNTOS
			  	$rutanrunt = "../../modulos/usuarios/docDomiciliario/Runt/".$id.".".$extensionrunt;				
				if(rename($rutaoldrunt,$rutanrunt))
				{
					$rutanewrunt = "modulos/usuarios/docDomiciliario/Runt/".$id.".".$extensionrunt;
					$update = pg_query($dbconn,"update tbl_usuario set usu_runt='".$rutanewrunt."' where usu_clave_int='".$id."'");
					unlink($rutaloldrunt);
					
				}

				$rutansoat = "../../modulos/usuarios/docDomiciliario/Soat/".$id.".".$extensionsoat;				
				if(rename($rutaoldsoat,$rutansoat))
				{
					$rutanewsoat = "modulos/usuarios/docDomiciliario/Soat/".$id.".".$extensionsoat;
					$update = pg_query($dbconn,"update tbl_usuario set usu_soat='".$rutanewsoat."' where usu_clave_int='".$id."'");
					unlink($rutaoldsoat);
					
				}

				$rutantecno = "../../modulos/usuarios/docDomiciliario/Tecno/".$id.".".$extensiontecno;				
				if(rename($rutaoldtecno,$rutantecno))
				{
					$rutanewtecno = "modulos/usuarios/docDomiciliario/Tecno/".$id.".".$extensiontecno;
					$update = pg_query($dbconn,"update tbl_usuario set usu_tecno='".$rutanewtecno."' where usu_clave_int='".$id."'");
					unlink($rutaoldtecno);
					
				}

				$rutanpase = "../../modulos/usuarios/docDomiciliario/Pase/".$id.".".$extensionpase;				
				if(rename($rutaoldpase,$rutanpase))
				{
					$rutanewpase = "modulos/usuarios/docDomiciliario/Pase/".$id.".".$extensionpase;
					$update = pg_query($dbconn,"update tbl_usuario set usu_pase='".$rutanewpase."' where usu_clave_int='".$id."'");
					unlink($rutaoldpase);
					
				}

				$rutancedula = "../../modulos/usuarios/docDomiciliario/Cedula/".$id.".".$extensioncedula;				
				if(rename($rutaoldcedula,$rutancedula))
				{
					$rutanewcedula = "modulos/usuarios/docDomiciliario/Cedula/".$id.".".$extensioncedula;
					$update = pg_query($dbconn,"update tbl_usuario set usu_cedula='".$rutanewcedula."' where usu_clave_int='".$id."'");
					unlink($rutaoldcedula);
				}
				
				

			//envio de correo aadministrador sobre nuevo registro

				$asunto1 = "Nueva Solicitud Domiciliario delasiembra.com";
				$conu = pg_query($dbconn, "SELECT usu_usuario,usu_email,usu_nombre,usu_apellido from tbl_usuario where prf_clave_int = 1 and est_clave_int = 1 and usu_clave_int = 1");
				$numu = pg_num_rows($conu);
				if($numu>0)
				{
					for($n=0;$n<$numu;$n++)
					{
						$datu = pg_fetch_array($conu);
						$usu = $datu['usu_usuario'];
						$ema = $datu['usu_email'];
						$nombre = $datu['usu_nombre']." ".$datu['usu_apellido'];
							
						// Cuerpo del mensaje
						$mensaje = "------------------------------------------- <br>";
						$mensaje.= "<strong>Hola</strong>, ".$usu."!<br><br>";
						$mensaje.= "DELASIEMBRA.COM registra que se ha hecho una nueva solicitud para aplicar como domiciliario.<br>";
						$mensaje.= "------------------------------------------- <br>";
						$mensaje.= "<strong>Nombre:</strong> ".$nom." ".$ape."<br>";
						$mensaje.= "<strong>Documento:</strong> ".$doc."<br>";					
						$mensaje.= "<strong>Correo electronico:</strong> ".$correo."<br>";
						$mensaje.= "<strong>Celular:</strong> ".$tel."<br>";
						$mensaje.= "<strong>IP registro:</strong> ".$IP."<br>";	
						$mensaje.= "<strong>Fecha:</strong>".date("d/m/Y H:m:s")."<br><br>";

						$contenido = file_get_contents('../../modulos/plantillas/correousuario.php');

						$contenido = str_replace('%contenido%', $mensaje, $contenido);

						$mail = new PHPMailer(true);
						/*
						
		                $mail->isSMTP();
		                $mail->SMTPAuth = true;
		                $maul->SMTPDebug   = 1;
		                $mail->SMTPSecure = "tls";
		                $mail->Host = "smtp.gmail.com";
		                $mail->Port = 587;
		                $mail->SMTPOptions = array(
		                    'ssl' => array(
		                        'verify_peer' => false,
		                        'verify_peer_name' => false,
		                        'allow_self_signed' => true
		                    )
		                );*/
		                 //Nuestra cuenta
						//$mail->Username ='andres.199207@gmail.com';
						//$mail->Password = 'Bayron.1214'; //Su password
						// Establecer de quién se va a enviar el mensaje
						//$mail->setFrom("andres.199207@gmail.com", "delasiembra.com");
						 // Establecer a quién se enviará el mensaje

						 $mail->SetFrom("admin@delasiembra.com", "delasiembra.com");
					//Usamos el AddReplyTo para decirle al script a quien tiene que responder el correo
						$mail->AddReplyTo("admin@delasiembra.com","delasiembra.com");


		                $mail->addAddress($ema, "Usuario: " . $usu);

		               
						

		                $mail->Subject = utf8_decode($asunto1);
		                $mail->msgHTML($contenido);
		                if (!$mail->send())
		                {
		                	 //$res = "error";
		                	 $msn.= 'No se envio mensaje al siguiente email<strong>(' . $ema . ')</strong>' . $mail->ErrorInfo . '<br>';
		                }
		                else
		                {
		                	//$res = "ok";
		                	
		                }		
                	}
                }					
			$res =  'ok';			
			$msn.="Gracias por registrarte en nuestra plataforma. En breve nos estaremos comunicando contigo";
		 
		}
		else
		{
			$res =  'error';
			$msn = "Surgió un error en el registro. Error BD(".pg_last_error($dbconn).")";
		}
	}	
	$datos[] = array("res"=>$res,"msn"=>$msn,"id"=>$id);
	echo json_encode($datos);
}
else if($opcion=="DESCARTAR")
{
	$edi = $_POST['edi'];
	$con = pg_query($dbconn,"update tbl_usuario set est_clave_int = '2',usu_confirmar = '2', usu_usu_actualiz = '".$usuario."', usu_fec_actualiz = '".$fecha."',usu_fec_activacion = '".$fecha."' where usu_clave_int = '".$edi."'");
		if($con>0)
		{
			$res = "ok";
			$msn = "Domiciliario descartado correctamente";
		}
		else
		{
			$res = "error";
			$msn = "Surgió un error al descartar domiciliario";
		}
		$datos[] = array("res"=>$res, "msn"=>$msn);
		echo json_encode($datos);
}
else if($opcion=="LOGIN")
{
	?>
	<script src='https://www.google.com/recaptcha/api.js'></script>
            <form id="formlogin" name="formlogin" method="post">
            <div class="row">
            	<div class="col-md-12 col-xs-12">
            		<label>Correo electrónico:</label>
	              <div class="form-group has-feedback">
	              	
	                <input type="email" class="form-control" placeholder="Correo electrónico" id="loginmail" name="loginmail" required>
	                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
	              </div>
          		</div>
          		<div class="col-md-12 col-xs-12">
          			<label>Contraseña:</label>
	              <div class="form-group has-feedback">
	                <input type="password" class="form-control" placeholder="Contraseña" id="loginpass" name="loginpass" required>
	                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
	              </div>
          		</div>
              <div class="form-group">
              	<div class="col-xs-6 hide">
                 <div class="g-recaptcha" data-size="compact"  data-badge="inline" data-sitekey="6LeGRDoUAAAAAMHadNkp4SIxreOUP9Gv7Y7cX_e3"></div>
                  </div>
                  <div class="col-xs-12 hide">
	                  <div class="checkbox icheck">
	                    <label>
	                      <input type="checkbox" id="ckrecordarme" name="ckrecordarme" value="1"> Recordarme
	                    </label>
	                  </div>
                	</div>
                 <div class="col-xs-12">
                  <button type="submit" id="btnlogin" class="btn btn-primary btn-block" onclick="INICIARSESION()">Ingresar</button>
                  	
                </div>
                <div class="col-xs-12 text-center"><a onclick="CRUDUSUARIOS('RECUPERAR','','')">¿Olvidaste tu contraseña?</a>
                </div>
                <div class="col-xs-12 text-center"> <a onclick="CRUDUSUARIOS('REGISTRONUEVO','','')" class="text-center">Registrate</a>
                </div>
            
              </div>
          	</div>
             
               
                <span id="status"></span>
                <div class="row social-auth-links text-center hide">
                  <p>- ó inicia sesión con -</p>
                  <div class="col-md-offset-2 col-md-4 col-xs-6">
                  <button type="button"  class="btn btn-block btn-social btn-facebook btn-flat loginfacebook"><i class="fa fa-facebook"></i>Facebook</button>
                </div>
                <div class="col-md-4 col-xs-6">
                  <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Google+</a>
                  </div>
                </div>
            </form>
            <script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
<script src="dist/js/loginfacebook.js?<?php echo time(); ?>"></script>
	<?php
}
else if($opcion=="REGISTRONUEVO")
{
	?>
	<form action="login.php" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Nombre" id="txtnombrenue" name="txtnombrenue">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
       <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Apellido" id="txtapellidonue" name="txtapellidonue">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Telefono" id="txttelefononue" name="txttelefononue">
        <span class="glyphicon glyphicon-phone form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email" id="txtemailnue" name="txtemailnue">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Contraseña" id="txtpassnue" name="txtpassnue">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Repetir contraseña" id="txtpass1nue" name="txtpass1nue">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="checkbox icheck">
            <label for="ckterminos">
              <input type="checkbox" id="ckterminos" name="ckterminos" value="1"> Estoy de acuerdo con los <span onclick="CRUDUSUARIOS('POLITICAS','','')" style="color:blue;cursor: pointer;">terminos de privacidad</span>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4 col-xs-offset-8">
          <button onclick="CRUDUSUARIOS('NUEVOREGISTRO','')" type="button" class="btn btn-primary btn-block btn-flat">Registrarse</button>
        </div>
        <!-- /.col -->
      </div>
      <a onclick="CRUDUSUARIOS('LOGIN','','')" class="text-center">Ya tengo una cuenta</a>
    </form>
    <script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
	<?php
}
else if($opcion=="RECUPERAR")
{
	?>
	<form class="" id="formrecuperar">
		<div class="row">
			<div class="col-md-12">
				<label>Ingresa tu correo electrónico para recuperar tu cuenta</label>
				<input type="email" id="txtrecuperar" name="txtrecuperar" class="form-control" required> 
			</div>
		</div>
		<div class="row">
			<hr>
			<div class="col-md-6 col-xs-6">
				<button id="btnrecuperar" type="submit" class="btn btn-primary btn-block" onclick="RECUPERARCONTRASENA()">Recuperar contraseña</button>
			</div>
			<div class="col-md-6  col-xs-6">
				<button id="btnrecuperar" type="button" class="btn btn-warning btn-block" onclick="CRUDUSUARIOS('LOGIN','','')">Cancelar</button>
			</div>
		</div>
	</form>
	<?php
}
else if($opcion=="REGLASNEGOCIO")
{
	$con = pg_query($dbconn, "SELECT reg_monto,reg_horaped,reg_horaent,reg_horamax FROM tbl_reglas LIMIT 1 ");
	$dat = pg_fetch_array($con);
	$mon = $dat['reg_monto'];
	$horaped = $dat['reg_horaped'];
	$horaent = $dat['reg_horaent'];
	$horamax = $dat['reg_horamax'];
	?>
	<div class="row">
		<div class="col-md-12">
			<h5>Monto maximo para aplicar a domicilio gratis</h5>
			<input type="text" class="form-control input-sm currency" id="txtmonto" name="txtmonto" value="<?php echo $mon;?>">
		</div>
	</div>
	<div class="row">
		<div class="col-md-4">
			<h5>Hora maxima para realizar pedidos</h5>
			<div class="input-group bootstrap-timepicker">
				<input type="text" id="txthoraped" name="txthoraped" class="form-control input-sm timepicker" value="<?php echo $horaped;?>">
				<div class="input-group-addon">
				<i class="fa fa-clock-o"></i>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<h5>Hora inicial entrega pedidos</h5>
			<div class="input-group bootstrap-timepicker">
				<input type="text" id="txthoraent" name="txthoraent" class="form-control input-sm timepicker" value="<?php echo $horaent;?>">
				<div class="input-group-addon">
				<i class="fa fa-clock-o"></i>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<h5>Hora maxima entrega pedidos</h5>
			<div class="input-group bootstrap-timepicker">
				<input type="text" id="txthoramaxent" name="txthoramaxent" class="form-control input-sm timepicker" value="<?php echo $horamax;?>">
				<div class="input-group-addon">
				<i class="fa fa-clock-o"></i>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<h5>Fechas sin despachos:</h5>
		<div class="col-md-6">
			<input type="date" id="txtfechaomitir" name="txtfechaomitir" class="form-control input-sm">
		</div>
		<div class="col-md-1">
			<button type="button" class="btn btn-sm btn-primary"  onclick="CRUDREGLAS('GUARDARFECHA')" >Agregar</button>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" id="divfechasomitir"></div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<button type="button" onclick="CRUDREGLAS('GUARDARREGLA')" class="btn btn-primary btn-block">Guardar</button>
		</div>
	</div>
	<?php
	echo "<script>CRUDREGLAS('LISTAFECHASOMITIR');</script>";
	echo "<script>INICIALIZARCONTENIDO();</script>";
}
else if($opcion=="LISTAFECHASOMITIR")
{
	$con = pg_query($dbconn,"SELECT ref_clave_int,ref_fecha,ref_usu_actualiz,ref_fec_actualiz FROM tbl_reglas_fechas order by ref_fecha DESC");
	?>
	<table class="table table-bordered" id="tblFechas">
		<thead>
			<tr><th></th></tr>
		</thead>
		<tbody>
			<?php  while($dat = pg_fetch_array($con)) { $idf = $dat['ref_clave_int']; $fec = $dat['ref_fecha']; ?>
			<tr id="rowf_<?php echo $idf;?>"><td><?php echo $fec;?></td>
				<td>
					<a class="btn btn-xs btn-danger" onclick="CRUDREGLAS('ELIMINARFECHA','<?PHP echo $idf;?>')">
						<i class="fa fa-trash"></i>
					</a>
				</td>
			</tr>
			<?php } ?>
		</tbody>
	</table>
	<?PHP
}
else if($opcion=="ELIMINARFECHA")
{
	$id = $_POST['id'];
	$del = pg_query($dbconn,"DELETE FROM tbl_reglas_fechas where ref_clave_int = '".$id."'");
	if($del>0)
	{
		$res = "ok";
		$msn = "Fecha eliminada correctamente";

	}
	else
	{
		$res = "error";
		$msn = "Surgió un error al eliminar la fecha seleccionada";
	}
	$datos[] = array("res"=>$res,"msn"=>$msn);
	echo json_encode($datos);
}
else if($opcion=="GUARDARFECHA")
{
	$fec = $_POST['fec'];
	$veri = pg_query($dbconn, "SELECT * FROM tbl_reglas_fechas WHERE ref_fecha = '".$fec."'");
	$numv = pg_num_rows($veri);
	if($numv>0)
	{
		$res = "error";
		$msn = "La fecha seleccionada la esta ingresada";
	}
	else
	{
		$ins = pg_query($dbconn, "INSERT INTO tbl_reglas_fechas(ref_fecha,ref_usu_actualiz,ref_fec_actualiz) VALUES('".$fec."','".$usuario."','".$fecha."')");
		if($ins>0)
		{
			$res = "ok";
			$msn  = "Fecha agregada";
		}
		else
		{
			$res = "error";
			$msn = "Surgió un error al agregar la fecha";
		}
	}
	$datos[] = array("res"=>$res,"msn"=>$msn);
	echo json_encode($datos);
}
else if($opcion=="GUARDARREGLA")
{
	$mon = $_POST['mon'];
	$horaent = $_POST['horaent'];
	$horaped = $_POST['horaped'];
	$horamax = $_POST['horamax'];
	$veri = pg_query($dbconn, "SELECT *  FROM tbl_reglas");
	$numv = pg_num_rows($veri);
	if($numv>0)
	{
		$upd = pg_query($dbconn, "UPDATE tbl_reglas SET reg_monto = '".$mon."',reg_horaped = '".$horaped."',reg_horamax = '".$horamax."', reg_horaent = '".$horaent."',reg_fec_actualiz = '".$fecha."',reg_usu_actualiz = '".$usuario."'");
		if($upd>0)
		{
			$res = "ok";
			$msn = "Reglas guardadas correctamente";
		}
		else
		{
			$res = "error";
			$msn = "Surgió un error al guardar reglas. Error BD(".pg_last_error($dbconn).")";
		}
	}
	else
	{
		$ins = pg_query($dbconn, "INSERT INTO tbl_reglas(reg_monto,reg_horaped,reg_horaent,reg_horamax,reg_usu_actualiz,reg_fec_actualiz) VALUES('".$mon."','".$horaped."','".$horaent."','".$horamax."','".$usuario."','".$fecha."')");
		if($ins>0)
		{
			$res = "ok";
			$msn = "Reglas guardadas correctamente";
		}
		else
		{
			$res = "error";
			$msn = "Surgió un error al guardar reglas. Error BD(".pg_last_error($dbconn).")";
		}
	}
	$datos[] = array("res"=>$res, "msn"=>$msn);
	echo json_encode($datos);
}
else if($opcion=="POLITICAS")
{
	?>
	
	<p class="text-justify">
		Mediante el registro de tus datos personales en este formulario estás autorizando a <strong>DELASIEMBRA.COM S.A.S</strong> para que realice el debido uso, almacenamiento y/o tratamiento de los mismos; permitiéndole a su vez el envío de información acerca de sus productos y servicios, actividades de mercadeo, evaluaciones de calidad de los productos y servicios y el suministro de información a las entidades gubernamentales y de control, cumpliendo así con la política de tratamiento de datos la cual puedes consultar aquí
	</p>
	<h3>
		AUTORIZACION TRATAMIENTO DE DATOS PERSONALES 
	</h3>
	<p class="text-justify">
		En mi calidad de titular de la información, actuando libre y voluntariamente, autorizo de manera previa y expresa a <strong>DELASIEMBRA.COM S.A.S</strong>, ubicada en la carrera 58 N°77 – 41, interior 1051, en el municipio de Itagüí, teléfono 3108611064 y página web <a>www.delasiembra.com</a>,  y a cualquier cesionario o beneficiario presente o futuro de sus obligaciones y derechos, para que directamente o a través de terceros realice el tratamiento a mi información personal, el cual consiste en recolectar, almacenar, usar, circular, registrar, administrar, procesar, confirmar, suprimir y actualizar mi información de carácter personal que le he suministrado, o que sobre mi recoja. Lo anterior con el fin de:<br><br>  
		<strong>Gestión de clientes:</strong> <br> Brindar solución a las de peticiones, quejas y/o reclamos asociados a los productos y/o prestación de servicios. 
		<br> Hacer seguimiento y evaluar la prestación de los servicios. 
		<br> Confirmar los datos necesarios para la entrega de productos y/o prestación de servicios. 
		<br> Proveer nuestros productos y/o prestación de servicios requeridos directamente o a través de terceros, y recibir retroalimentación. 
		<br> Aplicar encuestas de satisfacción de cliente para evaluar la calidad de nuestros productos y/o prestación de servicios. 
		<br> Aplicar estudios de mercado que permitan establecer preferencias de consumo o determinar hábitos de pago. 
		<br> Implementar programas de fidelización. Publicidad y prospección comercial:  Enviar publicidad propia e información sobre eventos, actividades, productos, servicios y en general información asociada a la actividad de <strong>DELASIEMBRA.COM S.A.S</strong>; esto a través de diferentes medios. 
		<br> Realizar actividades de georreferenciación y estudios estadísticos. He sido informado que las políticas para el tratamiento de mi información personal y el aviso de privacidad, así como el procedimiento para elevar cualquier solicitud, petición, queja o reclamo, podrán ser consultados en la página, y de manera expresa manifiesto que conozco, entiendo y he sido informado que mis derechos como titular de los datos personales suministrados son los siguientes: <br>
		<br>- Conocer, actualizar y rectificar mis datos personales. <br>- Solicitar prueba de la autorización otorgada - Ser informado por la Entidad, previa solicitud, respecto del uso que le ha dado a mis datos personales 
		<br>- Presentar quejas ante la Superintendencia de Industria y Comercio por infracciones a la ley. 
		<br>- Revocar la autorización y/o solicitar la supresión de mi(s) dato(s) en los casos en que sea procedente 
		<br>- Acceder en forma gratuita a los mismos.
		<br><br>
Con aceptar estos términos se autoriza a <strong>DELASIEMBRA.COM S.A.S</strong> para que con fines estadísticos, de control, supervisión y de información comercial, reporte a las Centrales de Información de Riesgo Crediticio, el nacimiento, desarrollo, modificación, extinción y cumplimiento de obligaciones contraídas o que llegue a contraer, fruto de contratos celebrados o los que en el futuro se celebren, la existencia de deudas vencidas sin cancelar. La presente autorización comprende no solo la facultad de reportar, procesar y divulgar, sino también la de solicitar información sobre las relaciones comerciales con cualquier otra entidad a las Centrales de Riesgo Crediticio. Las consecuencias de esta autorización serán la consulta e inclusión de sus datos financieros en las Centrales de Información de Riesgo Crediticio, pudiendo las entidades afiliadas conocer su comportamiento relacionado con el cumplimiento o incumplimiento de sus obligaciones.

	</p>
	<?php
}