<?php
	include('../../data/Conexion.php');
	session_start();
    error_reporting(0);
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_COOKIE['usuario'];
	$idUsuario= $_SESSION["idusuario"];
	$clave= $_COOKIE["clave"];
	$identificacion = $_COOKIE["usIdentificacion"];
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");

     
    $opcion = $_POST['opcion'];
    if($opcion=="NUEVO")
    {
    	?>
    	<form  id="form1" name="form1" action="_" method="post" enctype="multipart/form-data" class="form-horizontal">
    		<div class="row">
				<div class="col-md-12">
				<label for="txtnombre">Nombre:</label>
				<input type="text" class="form-control" id="txtnombre" name="txtnombre">
				</div>
				<div class="col-md-12" style="display: none;">
				<label for="txtpalabra">Palabra Sustituta:(Ingresar palabras que tengan el mismo significado)</label>
				<input type="text" class="form-control" id="txtpalabra" name="txtpalabra">
				</div>
				<div class="col-md-12">
				<label for="txtdescripcion">Descripción detallada:</label>
				<textarea class="form-control" id="txtdescripcion" name="txtdescripcion"></textarea>
				</div>
				<div class="col-md-12">
				<label for="txtdescripcionbreve">Descripción breve:</label>
				<textarea class="form-control" id="txtdescripcionbreve" name="txtdescripcionbreve"></textarea>
				</div>
				<div class="col-md-12 hide">
				<label for="txtmarca">Marca:</label>
				<input type="text" class="form-control" id="txtmarca" name="txtmarca">
				</div>
				<div class="col-md-12">
				<label for="selcategoria">Categoria:</label>
				<select class="form-control selectpicker" id="selcategoria" name="selcategoria">
					<option value="">--Seleccione--</option>
					<?php 
					$concat = pg_query($dbconn, "select * from tbl_categorias WHERE cat_activo in(0,1)");
					while($datcat = pg_fetch_array($concat))
					{
						$imgc = $datcat['cat_imagen'];
						$nomc = $datcat['cat_nombre'];
						?>
						 <option data-content="<img src='<?php echo $imgc;?>' width='20'/> <?php echo $nomc;?>" value="<?php echo $datcat['cat_clave_int'];?>"><?php echo $nomc;?></option>
						<?php
					}
					?>
				</select>
				</div>
				<h4 class="btn-block">Unidades de Compra</h4>
				<div class="col-md-3 col-xs-6">
					<div class="form-group2">
						<input type="checkbox" id="ckunidadcompra" name="ckunidadcompra" value="1" checked>
						<div class="btn-group">
						<label for="ckunidadcompra" class="btn btn-default">
						<span class="glyphicon glyphicon-ok"></span>
						<span> </span>
						</label>
						<label for="ckunidadcompra" class="btn btn-default active">Por Unidad</label>
						</div>
					</div>
					</div>
					<div class="col-md-3 col-xs-6">				
				
					<div class="has-feedback">
						<input type="text" class="form-control input-sm currency4" placeholder="unidad min" id="txtunidadcompra" name="txtunidadcompra" value="1">
						<span class=" form-control-feedback">UND</span>
					</div>
				</div>
				<div class="col-md-3 col-xs-6">
					<div class="form-group2">
						<input type="checkbox" id="ckpesocompra" name="ckpesocompra" value="1">
						<div class="btn-group">
						<label for="ckpesocompra" class="btn btn-default">
						<span class="glyphicon glyphicon-ok"></span>
						<span> </span>
						</label>
						<label for="ckpesocompra" class="btn btn-default active">Por Peso</label>
						</div>
					</div></div>
				<div class="col-md-3 col-xs-6">				
				
					<div class="has-feedback">
						<input type="text" class="form-control input-sm currency4" placeholder="peso min" id="txtpesocompra" name="txtpesocompra">
						<span class=" form-control-feedback">GR</span>
					</div>
				</div>
				<h4 class="btn-block">Unidades de venta</h4>
				<div class="col-md-3 col-xs-6">
					<div class="form-group2">
						<input type="checkbox" id="ckunidadventa" name="ckunidadventa" value="1" checked>
						<div class="btn-group">
						<label for="ckunidadventa" class="btn btn-default">
						<span class="glyphicon glyphicon-ok"></span>
						<span> </span>
						</label>
						<label for="ckunidadventa" class="btn btn-default active">Por Unidad</label>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-xs-6">
					<div class="has-feedback">
						<input type="text" class="form-control input-sm currency4" placeholder="unidad min" id="txtunidadventa" name="txtunidadventa" value="1">
						<span class=" form-control-feedback">UND</span>
					</div>
				</div>
				<div class="col-md-3 col-xs-6">
					<div class="form-group2">
						<input type="checkbox" id="ckpesoventa" name="ckpesoventa" value="1">
						<div class="btn-group">
						<label for="ckpesoventa" class="btn btn-default">
						<span class="glyphicon glyphicon-ok"></span>
						<span> </span>
						</label>
						<label for="ckpesoventa" class="btn btn-default active">Por Peso</label>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-xs-6">				
				
					<div class="has-feedback">
						<input type="text" class="form-control input-sm currency4" placeholder="peso min" id="txtpesoventa" name="txtpesoventa">
						<span class="form-control-feedback">GR</span>
					</div>
				</div>
				<!--
				<div class="col-md-6" style="display: none">
				<label for="selunicompra">Unidad Compra:</label>
				<select class="form-control selectpicker" id="selunicompra" name="selunicompra">
					<option value="">--Seleccione--</option>
					<?php 
					$conuni = pg_query($dbconn, "select * from tbl_unidades WHERE est_clave_int in(1)");
					while($datuni = pg_fetch_array($conuni))
					{
						$codu = $datuni['uni_codigo'];
						$nomu = $datuni['uni_nombre'];
						?>
						 <option  value="<?php echo $datuni['uni_clave_int'];?>"><?php echo $nomu;?></option>
						<?php
					}
					?>
				</select>
				</div>
				<div class="col-md-6" style="display: none">
				<label for="seluniconsumo">Unidad Consumo:</label>
				<select class="form-control selectpicker" id="seluniconsumo" name="seluniconsumo">
					<option value="">--Seleccione--</option>
					<?php 
					$conuni = pg_query($dbconn, "select * from tbl_unidades WHERE est_clave_int in(1)");
					while($datuni = pg_fetch_array($conuni))
					{
						$codu = $datuni['uni_codigo'];
						$nomu = $datuni['uni_nombre'];
						?>
						 <option  value="<?php echo $datuni['uni_clave_int'];?>"><?php echo $nomu;?></option>
						<?php
					}
					?>
				</select>
				</div>-->

				<div class="col-md-6" style="display: none">
				<label for="txtcodigo">Código PLU:</label>
				<input type="text" class="form-control" id="txtcodigo" name="txtcodigo">
				</div>
			</div>
			<div class="row">
				<h4 class="btn-block">Visualización</h4>
				<div class="col-md-3 col-xs-6">
					<div class="form-group2">
						<input type="checkbox" name="ckreserva" id="ckreserva" value="1" checked>
						<div class="btn-group">
						<label for="ckreserva" class="btn btn-default">
						<span class="glyphicon glyphicon-ok"></span>
						<span> </span>
						</label>
						<label for="ckreserva" class="btn btn-default active">Reserva</label>
						</div>
					</div>
					
				</div>
				<div class="col-md-3 col-xs-6">
					<div class="form-group2">
						<input type="checkbox" name="ckactivo" id="ckactivo" value="1" checked>
						<div class="btn-group">
						<label for="ckactivo" class="btn btn-default">
						<span class="glyphicon glyphicon-ok"></span>
						<span> </span>
						</label>
						<label for="ckactivo" class="btn btn-default active">Activo</label>
						</div>
					</div>
					
				</div>
				<div class="col-md-3 col-xs-6">
					<div class="form-group2">
						<input type="checkbox" name="ckagotado" id="ckagotado" value="1" checked>
						<div class="btn-group">
						<label for="ckagotado" class="btn btn-default">
						<span class="glyphicon glyphicon-ok"></span>
						<span> </span>
						</label>
						<label for="ckagotado" class="btn btn-default active">Mostrar Agotado</label>
						</div>
					</div>					
				</div>
				<div class="col-md-3 col-xs-6">
					<div class="form-group2">
						<input type="checkbox" name="ckmaduracion" id="ckmaduracion" value="1" checked>
						<div class="btn-group">
						<label for="ckmaduracion" class="btn btn-default">
						<span class="glyphicon glyphicon-ok"></span>
						<span> </span>
						</label>
						<label for="ckmaduracion" class="btn btn-default active">Maduración</label>
						</div>
					</div>					
				</div>
				<div class="col-md-3 col-xs-6">
					<div class="form-group2">
						<input type="checkbox" name="cktamano" id="cktamano" value="1" checked>
						<div class="btn-group">
						<label for="cktamano" class="btn btn-default">
						<span class="glyphicon glyphicon-ok"></span>
						<span> </span>
						</label>
						<label for="cktamano" class="btn btn-default active">Tamaño</label>
						</div>
					</div>					
				</div>
				
				
				<div class="col-md-12">
				<label for="imgproducto">Imagen:</label>
				<input type="file" class="form-control dropify" id="imgproducto" name="imgproducto" onChange="setpreview('rutaproducto','imgproducto','form1')" data-allowed-file-extensions="jpg png gif jpeg">
				<span id="rutaproducto"></span>
				</div>
			</div>
			<div class="row">
				<h4 class="btn-block">Lista Precios</h4>
				<div class="col-md-12">
				
					<div class="col-md-3">
						<div class="form-group2" title="Seleccionar si el costo del producto equivalente a kilogramo" data-toggle="tooltip">
							<input type="checkbox" name="ckkilo" id="ckkilo" value="1">
							<div class="btn-group">
							<label for="ckkilo" class="btn btn-default">
							<span class="glyphicon glyphicon-ok"></span>
							<span> </span>
							</label>
							<label for="ckkilo" class="btn btn-default active">Precio/Kilo</label>
							</div>
						</div>	
					</div>
					<div class="col-md-3 hide" id="undkilo" >
						<div class="input-group  has-feedback" title="Ingresar unidades promedio equivalentes a un kilogramo" data-toggle="tooltip">
							<span class="input-group-addon" title="Indique el peso por unidad" data-toggle="tooltip">
								Und/kilo:
								</span>
							<input type="text" class="form-control input-sm currency4" placeholder="" id="txtundkilo" name="txtundkilo" value="">
							<span class="form-control-feedback">UND</span>
						</div>
					</div>
				</div>
				<h4 class="btn-block" >Precio Costo:</h4>
				<div class="col-md-12">

					<?php 
					$concla = pg_query($dbconn, "SELECT cla_nombre, cla_clave_int FROM tbl_clasificacion");
					while($datcla = pg_fetch_array($concla))
					{
						$concos = pg_query($dbconn, "SELECT prm_costo,prm_peso_unidad FROM tbl_precios_mercado WHERE cla_clave_int = '".$datcla['cla_clave_int']."' and prm_estado = 0 and usu_clave_int ='".$idUsuario."' ORDER BY prm_costo DESC LIMIT 1");
						$datcos = pg_fetch_array($concos);
						$costo = $datcos['prm_costo'];
						$pesund = $datcos['prm_peso_unidad'];
						?>
						<div class="col-md-4 text-center">
							<label><?php echo $datcla['cla_nombre'];?></label>
							
							<div class="input-group">
								<span class="input-group-addon"	>
								$
								</span>
								<input type="text" id="pre_<?php echo $datcla['cla_clave_int'];?>" class="form-control input-sm currency2" aria-label="..."  onchange="CRUDPRODUCTOS('EDITARCOSTON','','','<?php echo $datcla['cla_clave_int'];?>')"  onkeypress="return validar_texto(event)" value="<?php echo $costo;?>">
							</div>
							<div class="input-group has-feedback">
								<span class="input-group-addon" title="Indique el peso por unidad" data-toggle="tooltip">
								Peso/Und:
								</span>
								<input onkeypress="return validar_texto(event)" style="width: 110px; border-top-right-radius: 25px; border-bottom-right-radius: 25px" class="form-control input-sm" id="pesund_<?PHP echo $datcla['cla_clave_int'];?>" onchange="CRUDPRODUCTOS('EDITARCOSTON','','','<?php echo $datcla['cla_clave_int'];?>')" value="<?php echo $pesund;?>" >
								<span class="form-control-feedback">GR</span>
							</div>
						
						</div>
						<?php
					}
					?>
				</div>
				<h4>Precio Venta</h4>
				<div class="col-md-12" id="listacosto" style="height: 105px">
					
				</div>
			</div>
		</form>
			<script>

				$('#ckunidadcompra').on('click',function(e){
				if($(this).is(':checked'))
				{
					//$('#undkilo').removeClass('hide');
				}
				else
				{
					$('#txtunidadcompra').val(0);
					//$('#undkilo').addClass('hide');
				}
			});

			$('#ckunidadventa').on('click',function(e){
				if($(this).is(':checked'))
				{
					//$('#undkilo').removeClass('hide');
				}
				else
				{
					$('#txtunidadventa').val(0);
					//$('#undkilo').addClass('hide');
				}
			});

			$('#ckpesocompra').on('click',function(e){
				if($(this).is(':checked'))
				{
					//$('#undkilo').removeClass('hide');
				}
				else
				{
					$('#txtpesocompra').val(0);
					//$('#undkilo').addClass('hide');
				}
			});

			$('#ckpesoventa').on('click',function(e){
				if($(this).is(':checked'))
				{
					//$('#undkilo').removeClass('hide');
				}
				else
				{
					$('#txtpesoventa').val(0);
					//$('#undkilo').addClass('hide');
				}
			});


			$('#ckkilo').on('click',function(e){
				CRUDPRODUCTOS('LISTACOSTOSN','','','');
				if($(this).is(':checked'))
				{
					//$('#undkilo').removeClass('hide');
				}
				else
				{
					$('#txtundkilo').val(0);
					//$('#undkilo').addClass('hide');
				}
			})
		</script>
		 <iframe src="about:blank" name="null" style="display:none"></iframe>

    	<?php

    	echo "<script>INICIALIZARCONTENIDO();</script>";
    	echo "<script>CRUDPRODUCTOS('LISTACOSTOSN','','','');</script>";
    }
    else if($opcion=="EDITAR")
    {
    	$id = $_POST['id'];
    	$con = pg_query($dbconn, "select pro_nombre,pro_descripcion,pro_descripcionbreve,cat_clave_int,pro_activo,pro_mostrar_agotado,pro_palabra,pro_codigo,pro_marca,pro_uni_compra,pro_mu_compra,pro_pes_compra,pro_mp_compra,pro_uni_venta,pro_mu_venta,pro_pes_venta,pro_mp_venta,pro_reserva,pro_estado,pro_kilo,pro_und_kilo from tbl_productos where pro_clave_int ='".$id."'");
    	$dat = pg_fetch_array($con);
    	$nom = $dat['pro_nombre'];
    	$desc = br2nl($dat['pro_descripcion']);
    	$descorta = br2nl($dat['pro_descripcionbreve']);
    	$cat = $dat['cat_clave_int'];
    	$act = $dat['pro_activo'];
    	$ago = $dat['pro_mostrar_agotado'];
    	$mad = $dat['pro_estado'];    
    	$pal = $dat['pro_palabra'];
    	$cod = $dat['pro_codigo'];
    	$mar = $dat['pro_marca'];
    	$puc = $dat['pro_uni_compra'];
    	$ppc = $dat['pro_pes_compra'];
    	$pmuc = $dat['pro_mu_compra'];
    	$pmpc = $dat['pro_mp_compra'];

    	$puv = $dat['pro_uni_venta'];
    	$ppv = $dat['pro_pes_venta'];
    	$pmuv = $dat['pro_mu_venta'];
    	$pmpv = $dat['pro_mp_venta'];

    	$reserva = $dat['pro_reserva'];
    	$tamano = $dat['pro_tamano'];
    	$kilo = $dat['pro_kilo'];
    	$undkilo = $dat['pro_und_kilo'];

    	//$unicom = $dat['pro_uni_compra'];
    	//$unicon = $dat['pro_uni_consumo'];
    	?>
    	<form  id="form1" name="form1" action="_" method="post" enctype="multipart/form-data" class="form-horizontal">
    		<div class="row">
				<div class="col-md-12">
				<label for="txtnombre">Nombre:</label>
				<input type="text" class="form-control" id="txtnombre" name="txtnombre" value="<?php echo $nom;?>">
				</div>
				<div class="col-md-12" style="display: none;">
				<label for="txtpalabra">Palabra Sustituta:(Ingresar palabras que tengan el mismo significado)</label>
				<input type="text" class="form-control" id="txtpalabra" name="txtpalabra" value="<?php echo $pal;?>">
				</div>
				<div class="col-md-12">
				<label for="txtdescripcion">Descripción detallada:</label>
				<textarea class="form-control" id="txtdescripcion" name="txtdescripcion"><?php echo $desc;?></textarea>
				</div>
				<div class="col-md-12">
				<label for="txtdescripcionbreve">Descripción breve:</label>
				<textarea class="form-control" id="txtdescripcionbreve" name="txtdescripcionbreve"><?php echo $descorta;?></textarea>
				</div>
				<div class="col-md-12 hide">
				<label for="txtmarca">Marca:</label>
				<input type="text" class="form-control" id="txtmarca" name="txtmarca" value="<?php echo $mar;?>">
				</div>
				<div class="col-md-12">
				<label for="selcategoria">Categoria:</label>
				<select class="form-control selectpicker" id="selcategoria" name="selcategoria">
					<option value="">--Seleccione--</option>
					<?php 
					$concat = pg_query($dbconn, "select * from tbl_categorias WHERE cat_activo in(0,1)");
					while($datcat = pg_fetch_array($concat))
					{
						$idc = $datcat['cat_clave_int'];
						$imgc = $datcat['cat_imagen'];
						$nomc = $datcat['cat_nombre'];
						?>
						 <option data-content="<img src='<?php echo $imgc;?>' width='20'/> <?php echo $nomc;?>" <?php if($cat==$idc){ echo "selected"; } ?> value="<?php echo $idc;?>"><?php echo $nomc;?></option>
						<?php
					}
					?>
				</select>
				</div>
				<h4 class="btn-block">Unidades de Compra</h4>
				<div class="col-md-3 col-xs-6">
					<div class="form-group2">
						<input type="checkbox" id="ckunidadcompra" name="ckunidadcompra" value="1" <?php if($puc==1){echo "checked";}?>>
						<div class="btn-group">
						<label for="ckunidadcompra" class="btn btn-default">
						<span class="glyphicon glyphicon-ok"></span>
						<span> </span>
						</label>
						<label for="ckunidadcompra" class="btn btn-default active">Por Unidad</label>
						</div>
					</div>
					</div>
					<div class="col-md-3 col-xs-6">				
				
					<div class="has-feedback">
						<input type="text" class="form-control input-sm currency4" placeholder="unidad min" id="txtunidadcompra" name="txtunidadcompra" value="<?php echo $pmuc;?>">
						<span class=" form-control-feedback">UND</span>
					</div>
				</div>
				<div class="col-md-3 col-xs-6">
					<div class="form-group2">
						<input type="checkbox" id="ckpesocompra" name="ckpesocompra" <?php if($ppc==1){ echo "checked";}?> value="1">
						<div class="btn-group">
						<label for="ckpesocompra" class="btn btn-default">
						<span class="glyphicon glyphicon-ok"></span>
						<span> </span>
						</label>
						<label for="ckpesocompra" class="btn btn-default active">Por Peso</label>
						</div>
					</div></div>
				<div class="col-md-3 col-xs-6">				
				
					<div class="has-feedback">
						<input type="text" class="form-control input-sm currency4" placeholder="peso min" id="txtpesocompra" name="txtpesocompra" value="<?php echo $pmpc;?>">
						<span class="form-control-feedback">GR</span>
					</div>
				</div>
				<h4 class="btn-block">Unidades de venta</h4>
				<div class="col-md-3 col-xs-6">
					<div class="form-group2">
						<input type="checkbox" id="ckunidadventa" name="ckunidadventa" <?php if($puv==1){ echo "checked";}?> value="1">
						<div class="btn-group">
						<label for="ckunidadventa" class="btn btn-default">
						<span class="glyphicon glyphicon-ok"></span>
						<span> </span>
						</label>
						<label for="ckunidadventa" class="btn btn-default active">Por Unidad</label>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-xs-6">
					<div class="has-feedback">
						<input type="text" class="form-control input-sm currency4" placeholder="unidad min" id="txtunidadventa" name="txtunidadventa" value="<?php echo $pmuv;?>">
						<span class=" form-control-feedback">UND</span>
					</div>
				</div>
				<div class="col-md-3 col-xs-6">
					<div class="form-group2">
						<input type="checkbox" id="ckpesoventa" name="ckpesoventa" value="1" <?php if($ppv==1){ echo "checked";}?>>
						<div class="btn-group">
						<label for="ckpesoventa" class="btn btn-default">
						<span class="glyphicon glyphicon-ok"></span>
						<span> </span>
						</label>
						<label for="ckpesoventa" class="btn btn-default active">Por Peso</label>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-xs-6">				
				
					<div class="has-feedback">
						<input type="text" class="form-control input-sm currency4" placeholder="peso min" id="txtpesoventa" name="txtpesoventa" value="<?php echo $pmpv;?>">
						<span class="form-control-feedback">GR</span>
					</div>
				</div>
				<!--
				<div class="col-md-6">
				<label for="selunicompra">Unidad Compra:</label>
				<select class="form-control selectpicker" id="selunicompra" name="selunicompra">
					<option value="">--Seleccione--</option>
					<?php 
					$conuni = pg_query($dbconn, "select * from tbl_unidades WHERE est_clave_int in(1)");
					while($datuni = pg_fetch_array($conuni))
					{
						$idu = $datuni['uni_clave_int'];
						$codu = $datuni['uni_codigo'];
						$nomu = $datuni['uni_nombre'];
						?>
						 <option  <?php if($unicom==$idu){ echo "selected"; }?> value="<?php echo $idu;?>"><?php echo $nomu;?></option>
						<?php
					}
					?>
				</select>
				</div>
				<div class="col-md-6">
				<label for="seluniconsumo">Unidad Consumo:</label>
				<select class="form-control selectpicker" id="seluniconsumo" name="seluniconsumo">
					<option value="">--Seleccione--</option>
					<?php 
					$conuni = pg_query($dbconn, "select * from tbl_unidades WHERE est_clave_int in(1)");
					while($datuni = pg_fetch_array($conuni))
					{
						$idu = $datuni['uni_clave_int'];
						$codu = $datuni['uni_codigo'];
						$nomu = $datuni['uni_nombre'];
						?>
						 <option <?php if($unicon==$idu){ echo "selected"; }?>  value="<?php echo $idu;?>"><?php echo $nomu;?></option>
						<?php
					}
					?>
				</select>
				</div>
			-->
				<div class="col-md-6" style="display: none">
				<label for="txtcodigo">Código PLU:</label>
				<input type="text" maxlength="4" onkeypress="return validar_texto(event)" class="form-control" id="txtcodigo" name="txtcodigo" value="<?php echo $cod;?>">
				</div>
			</div>
			<div class="row">
				<h4 class="btn-block">Visualización:</h4>
				<div class="col-md-3 col-xs-6">
					<div class="form-group2">
						<input type="checkbox" name="ckreserva" id="ckreserva" value="1" <?php if($reserva==1){ echo "checked";}?>>
						<div class="btn-group">
						<label for="ckreserva" class="btn btn-default">
						<span class="glyphicon glyphicon-ok"></span>
						<span> </span>
						</label>
						<label for="ckreserva" class="btn btn-default active">Reserva</label>
						</div>
					</div>
					
				</div>
				<div class="col-md-3 col-xs-6">
					<div class="form-group2">
						<input type="checkbox" name="ckactivo" id="ckactivo" value="1" <?php if($act==1){ echo "checked";}?>>
						<div class="btn-group">
						<label for="ckactivo" class="btn btn-default">
						<span class="glyphicon glyphicon-ok"></span>
						<span> </span>
						</label>
						<label for="ckactivo" class="btn btn-default active">Activo</label>
						</div>
					</div>
					
				</div>
				<div class="col-md-3 col-xs-6">
					<div class="form-group2">
						<input type="checkbox" name="ckagotado" id="ckagotado" value="1" <?php if($ago==1){ echo "checked";}?>>
						<div class="btn-group">
						<label for="ckagotado" class="btn btn-default">
						<span class="glyphicon glyphicon-ok"></span>
						<span> </span>
						</label>
						<label for="ckagotado" class="btn btn-default active">Mostrar Agotado</label>
						</div>
					</div>					
				</div>	
				<div class="col-md-3 col-xs-6">
					<div class="form-group2">
						<input type="checkbox" name="ckmaduracion" id="ckmaduracion" value="1" <?php if($mad==1){ echo "checked";}?>>
						<div class="btn-group">
						<label for="ckmaduracion" class="btn btn-default">
						<span class="glyphicon glyphicon-ok"></span>
						<span> </span>
						</label>
						<label for="ckmaduracion" class="btn btn-default active">Maduración</label>
						</div>
					</div>					
				</div>

				<div class="col-md-3 col-xs-6">
					<div class="form-group2">
						<input type="checkbox" name="cktamano" id="cktamano" value="1" <?php if($tamano==1){ echo "checked";}?>>
						<div class="btn-group">
						<label for="cktamano" class="btn btn-default">
						<span class="glyphicon glyphicon-ok"></span>
						<span> </span>
						</label>
						<label for="cktamano" class="btn btn-default active">Tamaño</label>
						</div>
					</div>					
				</div>							
			</div>
			<div class="row">
				<h4 class="btn-block">Lista Precios</h4>
				<div class="col-md-12">
				
					<div class="col-md-3">
						<div class="form-group2" title="Seleccionar si el costo del producto equivalente a kilogramo" data-toggle="tooltip">
							<input type="checkbox" name="ckkilo" id="ckkilo" value="1" <?php if($kilo==1){ echo "checked";}?>>
							<div class="btn-group">
							<label for="ckkilo" class="btn btn-default">
							<span class="glyphicon glyphicon-ok"></span>
							<span> </span>
							</label>
							<label for="ckkilo" class="btn btn-default active">Precio/Kilo</label>
							</div>
						</div>	
					</div>
					<div class="col-md-3 hide" id="undkilo" >
						<div class="input-group  has-feedback" title="Ingresar unidades promedio equivalentes a un kilogramo" data-toggle="tooltip">
							<span class="input-group-addon" title="Indique el peso por unidad" data-toggle="tooltip">
								Und/kilo:
								</span>
							<input type="text" class="form-control input-sm currency4" placeholder="" id="txtundkilo" name="txtundkilo" value="<?php echo $undkilo;?>">
							<span class="form-control-feedback">UND</span>
						</div>
					</div>
				</div>
				<h4 class="btn-block" >Precio Costo:</h4>
				<div class="col-md-12">

					<?php 
					$concla = pg_query($dbconn, "SELECT cla_nombre, cla_clave_int FROM tbl_clasificacion");
					while($datcla = pg_fetch_array($concla))
					{
						$concos = pg_query($dbconn, "SELECT prm_costo,prm_peso_unidad FROM tbl_precios_mercado WHERE pro_clave_int = '".$id."' and cla_clave_int = '".$datcla['cla_clave_int']."' ORDER BY prm_costo DESC LIMIT 1");
						$datcos = pg_fetch_array($concos);
						$costo = $datcos['prm_costo'];
						$pesund = $datcos['prm_peso_unidad'];
						?>
						<div class="col-md-4 text-center">
							<label><?php echo $datcla['cla_nombre'];?></label>
							
							<div class="input-group">
								<span class="input-group-addon"	>
								$
								</span>
								<input type="text" id="pre_<?php echo $datcla['cla_clave_int'];?>" class="form-control input-sm currency2" aria-label="..."   onchange="CRUDPRODUCTOS('EDITARCOSTO','<?PHP echo $id;?>','','<?php echo $datcla['cla_clave_int'];?>')"  onkeypress="return validar_texto(event)" value="<?php echo $costo;?>">
							</div>
							<div class="input-group has-feedback">
								<span class="input-group-addon" title="Indique el peso por unidad" data-toggle="tooltip">
								Peso/Und:
								</span>
								<input onkeypress="return validar_texto(event)" style="width: 110px; border-top-right-radius: 25px; border-bottom-right-radius: 25px" class="form-control input-sm" id="pesund_<?PHP echo $datcla['cla_clave_int'];?>" onchange="CRUDPRODUCTOS('EDITARCOSTO','<?PHP echo $id;?>','','<?php echo $datcla['cla_clave_int'];?>')" value="<?php echo $pesund;?>" >
								<span class="form-control-feedback">GR</span>
							</div>
						
						</div>
						<?php
					}
					?>
				</div>
				<h4>Precio Venta</h4>
				<div class="col-md-12" id="listacosto" style="height: 105px">
					
				</div>
			</div>
			<hr>
			<?php
			//PROVEEDORES DEL PRODUCTO POR CALIDAD
				$conpro = pg_query($dbconn, "select pr.prv_nombre,pr.prv_clave_int FROM tbl_proveedor pr join tbl_proveedor_productos pp ON pp.prv_clave_int = pr.prv_clave_int WHERE pp.pro_clave_int = '".$id."' and pp.cla_clave_int>0 GROUP BY pr.prv_clave_int");
				$numpro = pg_num_rows($conpro);
				?>
				<div class="row <?php if($numpro<=0){ echo 'hide';}?>">
					<div class="col-md-12">
						<div class="box box-primary collapsed-box">
							<div class="box-header with-border">
								<h3 class="box-title">Proveedores del producto</h3>

								<div class="box-tools pull-right">
								<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
								</button>              
								</div>
							</div>
						<!-- /.box-header -->
							<div class="box-body" style="display: none;">
						<?php 
						while($datpro = pg_fetch_array($conpro)){
							$idprov = $datpro['prv_clave_int'];
							$nomprov = $datpro['prv_nombre'];

							$concla = pg_query($dbconn, "SELECT c.cla_clave_int, c.cla_nombre,pp.ppr_compra FROM tbl_proveedor_productos pp JOIN tbl_clasificacion c ON c.cla_clave_int = pp.cla_clave_int WHERE pp.prv_clave_int = '".$idprov."' and pp.pro_clave_int = '".$id."' GROUP BY c.cla_clave_int,pp.ppr_compra ORDER BY c.cla_nombre ASC");

							?>
							<h5><?php echo $nomprov;?></h5>
							<div class="row">
							<?php
							while($datcla = pg_fetch_array($concla))
							{
								$nomcla = $datcla['cla_nombre'];
								$costo = $datcla['ppr_compra'];
								?>
								<div class="col-md-3 text-center">
							
									<div class="input-group">
										<span class="input-group-addon">
										<?php echo $nomcla;?>
										</span>
										<input onkeypress="return validar_texto(event)" style="width: 110px; border-top-right-radius: 25px; border-bottom-right-radius: 25px" class="form-control input-sm currency" value="<?php echo $costo;?>" disabled >
										
									</div>
						
								</div>
								<?php
							}
							?>
							</div>
							<?php
						}
						?>
						</div>
	            <!-- /.box-body -->	            
	          		</div>

				</div>
			</div>
		</form>
		<script>

			
			$('#ckunidadcompra').on('click',function(e){
				if($(this).is(':checked'))
				{
					//$('#undkilo').removeClass('hide');
				}
				else
				{
					$('#txtunidadcompra').val(0);
					//$('#undkilo').addClass('hide');
				}
			});

			$('#ckunidadventa').on('click',function(e){
				if($(this).is(':checked'))
				{
					//$('#undkilo').removeClass('hide');
				}
				else
				{
					$('#txtunidadventa').val(0);
					//$('#undkilo').addClass('hide');
				}
			});

			$('#ckpesocompra').on('click',function(e){
				if($(this).is(':checked'))
				{
					//$('#undkilo').removeClass('hide');
				}
				else
				{
					$('#txtpesocompra').val(0);
					//$('#undkilo').addClass('hide');
				}
			});

			$('#ckpesoventa').on('click',function(e){
				if($(this).is(':checked'))
				{
					//$('#undkilo').removeClass('hide');
				}
				else
				{
					$('#txtpesoventa').val(0);
					//$('#undkilo').addClass('hide');
				}
			});

			$('#ckkilo').on('click',function(e){
				CRUDPRODUCTOS('LISTACOSTOS','<?php echo $id;?>','','');
				if($(this).is(':checked'))
				{
					//$('#undkilo').removeClass('hide');
				}
				else
				{
					$('#txtundkilo').val(0);
					//$('#undkilo').addClass('hide');
				}
			})
		</script>
		 <iframe src="about:blank" name="null" style="display:none"></iframe>

    	<?php
    	echo "<script>INICIALIZARCONTENIDO();</script>";
    	echo "<script>CRUDPRODUCTOS('LISTACOSTOS','".$id."','','');</script>";
    }
    else if($opcion=="EDITARCOSTO")
    {
    	$idp = $_POST['idp'];
    	$idc = $_POST['idc'];
    	$val = $_POST['pre']; if($val<=0 || $val==null){$val =0;}
    	$pu = $_POST['pes']; if($pu<=0 || $pu==null){$pu =0;}
    	$upd = 0;
    	$ins = 0;
    	$ent = 0;
    	$clas = "";

    	$conm = pg_query($dbconn,"SELECT mer_clave_int,mer_clasificacion,mer_porcentaje FROM tbl_mercado
    	 WHERE mer_activo = 1");
    	$numm = pg_num_rows($conm);
    	for($p=0;$p<$numm;$p++)
    	{
			$datp = pg_fetch_array($conm);
			$idm = $datp['mer_clave_int'];    			
			$porc = $datp['mer_porcentaje'];
			$clas.="".$datp['mer_clasificacion'];
			$idcla = explode(",",$datp['mer_clasificacion']);

			if(in_array($idc, $idcla))
			{
				$ent++;
			
    			$ven = $val + ($val*($porc/100));
    			$ven = ceil($ven/50)*50;
    			if($ven<=0 || $ven==NULL)
    			{
    				$ven = 0;
    			}
    			$veri = pg_query($dbconn, "SELECT * FROM tbl_precios_mercado WHERE pro_clave_int = '".$idp."' and mer_clave_int ='".$idm."' and cla_clave_int = '".$idc."'");
    			$numv = pg_num_rows($veri);
    			if($numv>0)
    			{
    				$sql = pg_query($dbconn,"UPDATE tbl_precios_mercado SET prm_costo = '".$val."',prm_venta = '".$ven."',prm_usu_actualiz = '".$usuario."',prm_fec_actualiz = '".$fecha."',prm_peso_unidad = '".$pu."'  WHERE pro_clave_int = '".$idp."' and mer_clave_int ='".$idm."' and cla_clave_int = '".$idc."'");
    				$upd++;
    			}
    			else
    			{
    				$sql = pg_query($dbconn, "INSERT INTO tbl_precios_mercado(pro_clave_int,mer_clave_int,cla_clave_int,prm_costo,prm_usu_actualiz,prm_fec_actualiz,prm_venta,prm_peso_unidad) VALUES('".$idp."','".$idm."','".$idc."','".$val."','".$usuario."','".$fecha."','".$ven."','".$pu."')");
    				$inse++;
    				
    			}	
			}
    	}

    	$mod = $upd + $inse;
    	if($mod>0)
    	{
    		$res = "ok";
    		$msn = "";
    	}
    	else
    	{
    		$res = "error";
    		$msn = "Surgio un error al modificar costos";
    	}
    	//seleccionar los mercados donde esta la clasificacion editada;
    	$datos[] = array("res"=>$res,"msn"=>$msn,"mod"=>$mod,"ent"=>$ent,"clas"=>$clas);
    	echo json_encode($datos);

    }
    else if($opcion=="EDITARCOSTON")
    {
    	//$idp = $_POST['idp'];
    	$idc = $_POST['idc'];
    	$val = $_POST['pre']; if($val<=0 || $val==null){$val =0;}
    	$pu = $_POST['pes']; if($pu<=0 || $pu==null){$pu =0;}
    	$upd = 0;
    	$inse = 0;
    	$ent = 0;
    	$clas = "";

    	$conm = pg_query($dbconn,"SELECT mer_clave_int,mer_clasificacion,mer_porcentaje FROM tbl_mercado
    	 WHERE mer_activo = 1");
    	$numm = pg_num_rows($conm);
    	for($p=0;$p<$numm;$p++)
    	{
			$datp = pg_fetch_array($conm);
			$idm = $datp['mer_clave_int'];    			
			$porc = $datp['mer_porcentaje'];
			$clas.="".$datp['mer_clasificacion'];
			$idcla = explode(",",$datp['mer_clasificacion']);

			if(in_array($idc, $idcla))
			{
				$ent++;
			
    			$ven = $val + ($val*($porc/100));
    			$ven = ceil($ven/50)*50;
    			if($ven<=0 || $ven==NULL)
    			{
    				$ven = 0;
    			}
    			$veri = pg_query($dbconn, "SELECT * FROM tbl_precios_mercado WHERE usu_clave_int = '".$idUsuario."' and mer_clave_int ='".$idm."' and cla_clave_int = '".$idc."' and prm_estado = 0");
    			$numv = pg_num_rows($veri);
    			if($numv>0)
    			{
    				$sql = pg_query($dbconn,"UPDATE tbl_precios_mercado SET prm_costo = '".$val."',prm_venta = '".$ven."',prm_usu_actualiz = '".$usuario."',prm_fec_actualiz = '".$fecha."',prm_peso_unidad = '".$pu."'  WHERE usu_clave_int = '".$idUsuario."' and mer_clave_int ='".$idm."' and cla_clave_int = '".$idc."' and prm_estado = 0");
    				$upd++;
    			}
    			else
    			{
    				$sql = pg_query($dbconn, "INSERT INTO tbl_precios_mercado(usu_clave_int,mer_clave_int,cla_clave_int,prm_costo,prm_usu_actualiz,prm_fec_actualiz,prm_venta,prm_peso_unidad) VALUES('".$idUsuario."','".$idm."','".$idc."','".$val."','".$usuario."','".$fecha."','".$ven."','".$pu."')");
    				if($sql>0)
    				{
    					$inse++;
    				}
    				
    			}	
			}
    	}

    	$mod = $upd + $inse;
    	if($mod>0)
    	{
    		$res = "ok";
    		$msn = "";
    	}
    	else
    	{
    		$res = "error";
    		$msn = "Surgio un error al modificar costos";
    	}
    	//seleccionar los mercados donde esta la clasificacion editada;
    	$datos[] = array("res"=>$res,"msn"=>$msn,"mod"=>$mod,"ent"=>$ent,"clas"=>$clas,"upd"=>$upd,"inse"=>$inse);
    	echo json_encode($datos);

    }
    else if($opcion=="LISTACOSTOS")
    {
    	$idp = $_POST['idp'];
    	$con=  pg_query($dbconn, "SELECT * FROM tbl_mercado where mer_activo=1 ");
    	$con2 = pg_query($dbconn,"SELECT * FROM tbl_clasificacion");
    	$num2 = pg_num_rows($con2);
    	$cla = array();
    	$kil = $_POST['kil'];


    	//datos producto
    	$conp = pg_query($dbconn, "select pre_clave_int,pre_costo,pre_venta from tbl_precios WHERE pro_clave_int = '".$idp."'");
    	$datp = pg_fetch_row($conp);
    	$pre = $datp[0];
    	$cos = $datp[1];
    	$ven = $datp[2];
    	if($ven>0)
    	{
    		$gan = (($ven - $cos)/$ven) * 100;
    	}
    	else
    	{
    		$gan = 0;
    	}

    	/*for($nc=0;$nc<$num2;$nc++)
    	{
    		$dat2 = pg_fetch_array($con2);
    		$cla[$nc] = array("idc"=>$dat2['cla_clave_int'],"nomc"=>$dat2['cla_nombre']);
    		echo $cla["nomc"][$nc];
    	}*/
    	?>
    	
    	<table class="table table-bordered compact" style="font-size: 12px">
    	<tr>
		<?php
   
   		$conmer = pg_query($dbconn, "SELECT mer_clave_int,mer_nombre,mer_clasificacion from tbl_mercado where mer_activo = 1 order by mer_clave_int = 3 ASC");
    	while($datm = pg_fetch_array($conmer))
    	{
    		$idm = $datm['mer_clave_int'];
    		$nom = $datm['mer_nombre'];
    		$clas = $datm['mer_clasificacion'];
    		$clasi = explode(",", $clas);
    		$num = count($clasi);
    		?>
    		<th class="bg-info text-center" colspan="<?php echo $num;?>"><?php echo $nom;?></th>
    		<?php
    	}
    	?>
    	
    	<tr>
		<?php
    	$conmer = pg_query($dbconn, "SELECT mer_clave_int,mer_nombre,mer_clasificacion,mer_porcentaje from tbl_mercado where mer_activo = 1  order by mer_clave_int = 3 ASC");

    	while($datm = pg_fetch_array($conmer))
    	{
    		$idm = $datm['mer_clave_int'];
    		$nom = $datm['mer_nombre'];
    		$clas = $datm['mer_clasificacion'];
    		$porc = $datm['mer_porcentaje'];
    		
    		$concla = pg_query($dbconn,"SELECT * FROM tbl_clasificacion where cla_clave_int in(".$clas.")");
    		$numcla = pg_num_rows($concla);
    		for($n=0;$n<$numcla;$n++)
    		{
    			$datc = pg_fetch_array($concla);
    			$idc = $datc['cla_clave_int'];
    			$nomc = $datc['cla_nombre'];

    			$convalact = pg_query($dbconn,"SELECT  prm_costo,prm_venta,prm_peso_unidad,pro_kilo,pro_und_kilo FROM tbl_precios_mercado m JOIN tbl_productos p on p.pro_clave_int = m.pro_clave_int WHERE m.pro_clave_int = '".$idp."' and mer_clave_int ='".$idm."' and cla_clave_int = '".$idc."' limit 1");
    			$datval = pg_fetch_array($convalact);
    			$valcos = $datval['prm_costo'];
    			$valact = $datval['prm_venta'];
    			$pesund = $datval['prm_peso_unidad'];
    			//$kil = $datval['pro_kilo'];


    			//$undkilo = $datval['pro_und_kilo'];
    			if($kil==1){
    				
    				if($pesund<=0){ $undkilo = 0; $costund = $valact/1; }
    				else { $undkilo = 1000/$pesund; $costund = $valact/$undkilo; }

    			}
    			else
    			{
    				$undkilo = 1;
    				$costund = "";
    			}


    			
    		?>
    			<th><?php echo $nomc;?><br>    				
    				
    				<span data-porcentaje="<?php echo $porc;?>" class="currency">$<?php echo number_format($valact,0,',',',');?></span><br>
    				<span class="<?php if($costund<=0){ echo "hide"; }?>" style="color:orange"><span class="currency">$<?php echo number_format($costund,0,',',',');?></span>/und</span>
    				<br>
    				<span style="color:green"><span class="currency4"><?php echo number_format($undkilo,0,',',',');?></span> Und/kilo</span>
    				
    			

    			</th>

    		<?php
    		}
    	}
    	?>
    	</tr>
    	
    	</table>
    
    	
    	<?php
    	//echo "<script>INICIALIZARCONTENIDO();</script>";
    }
    else if($opcion=="LISTACOSTOSN")
    {
    	$idp = $_POST['idp'];
    	$con=  pg_query($dbconn, "SELECT * FROM tbl_mercado where mer_activo=1 ");
    	$con2 = pg_query($dbconn,"SELECT * FROM tbl_clasificacion");
    	$num2 = pg_num_rows($con2);
    	$cla = array();
    	$kil = $_POST['kil'];

    	?>
    	
    	<table class="table table-bordered compact" style="font-size: 12px">
    	<tr>
		<?php
   
   		$conmer = pg_query($dbconn, "SELECT mer_clave_int,mer_nombre,mer_clasificacion from tbl_mercado where mer_activo = 1 order by mer_clave_int = 3 ASC");
    	while($datm = pg_fetch_array($conmer))
    	{
    		$idm = $datm['mer_clave_int'];
    		$nom = $datm['mer_nombre'];
    		$clas = $datm['mer_clasificacion'];
    		$clasi = explode(",", $clas);
    		$num = count($clasi);
    		?>
    		<th class="bg-info text-center" colspan="<?php echo $num;?>"><?php echo $nom;?></th>
    		<?php
    	}
    	?>
    	
    	<tr>
		<?php
    	$conmer = pg_query($dbconn, "SELECT mer_clave_int,mer_nombre,mer_clasificacion,mer_porcentaje from tbl_mercado where mer_activo = 1  order by mer_clave_int = 3 ASC");

    	while($datm = pg_fetch_array($conmer))
    	{
    		$idm = $datm['mer_clave_int'];
    		$nom = $datm['mer_nombre'];
    		$clas = $datm['mer_clasificacion'];
    		$porc = $datm['mer_porcentaje'];
    		
    		$concla = pg_query($dbconn,"SELECT * FROM tbl_clasificacion where cla_clave_int in(".$clas.")");
    		$numcla = pg_num_rows($concla);
    		for($n=0;$n<$numcla;$n++)
    		{
    			$datc = pg_fetch_array($concla);
    			$idc = $datc['cla_clave_int'];
    			$nomc = $datc['cla_nombre'];

    			$convalact = pg_query($dbconn,"SELECT  prm_costo,prm_venta,prm_peso_unidad FROM tbl_precios_mercado WHERE usu_clave_int = '".$idUsuario."' and mer_clave_int ='".$idm."' and cla_clave_int = '".$idc."' and prm_estado = 0 limit 1");
    			$datval = pg_fetch_array($convalact);
    			$valcos = $datval['prm_costo'];
    			$valact = $datval['prm_venta'];
    			$pesund = $datval['prm_peso_unidad'];

    			//$undkilo = $datval['pro_und_kilo'];
    			if($kil==1){
    				if($pesund<=0){ $undkilo = 0; $costund = $valact/1; }else { $undkilo = 1000/$pesund; $costund = $valact/$undkilo; }
    			}
    			else
    			{
    				$undkilo = 1;
    				$costund = "";
    			}



    			
    		?>
    			<th><?php echo $nomc;?><br>    				
    				
    				<span data-porcentaje="<?php echo $porc;?>"  class="currency">$<?php echo number_format($valact,0,',',',');?></span><br>
    				<span class="<?php if($costund<=0){ echo "hide"; }?>" style="color:orange"><span class="currency"><?php echo number_format($costund,0,',',',');?></span>/und</span>
    				<br>
    				<span><span class="currency4"><?php echo number_format($undkilo,0,',',',');?></span>Und/kilo</span>
    				
    			

    			</th>

    		<?php
    		}
    	}
    	?>
    	</tr>
    	
    	</table>
    
    	
    	<?php
    	//echo "<script>INICIALIZARCONTENIDO();</script>";
    }
    else if($opcion=="GUARDAR")
    {
    	$nom = $_POST['nom'];
    	$desc = nl2br($_POST['desc']);
    	$descbreve = nl2br($_POST['descbreve']);
    	$cat = $_POST['cat'];
    	$cod = $_POST['cod'];
    	$mar = $_POST['mar'];
    	$act = $_POST['act'];
    	$ago = $_POST['ago'];
    	$ruta = $_POST['ruta'];
    	$pal = $_POST['pal'];
    	//$unicom = $_POST['unicom'];
    	//$unicon = $_POST['unicon'];
    	$puc = $_POST['puc'];
    	$pmuc = $_POST['pmuc'];
    	$ppc = $_POST['ppc'];
    	$pmpc = $_POST['pmpc'];

    	$puv = $_POST['puv'];
    	$pmuv = $_POST['pmuv'];
    	$ppv = $_POST['ppv'];
    	$pmpv = $_POST['pmpv'];

    	$rese = $_POST['rese'];
    	$mad = $_POST['mad'];
    	$tam = $_POST['tam'];
    	$kil = $_POST['kil'];
    	$undkilo = $_POST['undkilo'];
    	
    	$trozos = explode(".", $ruta); 
		$rutaold = "../../".$ruta;
		$extension = end($trozos);

		$veri = pg_query($dbconn, "SELECT * FROM tbl_productos where pro_nombre = '".$nom."' and pro_activo!=2");
		$numv = pg_num_rows($veri);

		if($nom=="" || $nom==NULL)
		{
			$res = "error";
			$msn = "Diligenciar el nombre del producto. Verificar";
		}
		else if($desc =="" || $desc==NULL)
		{
			$res = "error";
			$msn = "Diligenciar la descripción del producto. Verificar";
		}
		else if($numv>0)
		{
			$res = "error";
			$msn = "Ya hay una producto con el nombre ingresado. Verificar";
		}
		else
		{
			$ins = pg_query($dbconn,"INSERT INTO tbl_productos(pro_nombre,pro_descripcion,pro_descripcionbreve,pro_palabra,cat_clave_int,pro_activo,pro_mostrar_agotado,pro_marca,pro_usu_actualiz,pro_fec_actualiz,pro_uni_compra,pro_mu_compra,pro_pes_compra,pro_mp_compra,pro_uni_venta,pro_mu_venta,pro_pes_venta,pro_mp_venta,pro_reserva,pro_estado,pro_tamano,pro_kilo,pro_und_kilo) VALUES('".$nom."','".$desc."','".$descbreve."','".$pal."','".$cat."','".$act."','".$ago."','".$mar."','".$usuario."','".$fecha."','".$puc."','".$pmuc."','".$ppc."','".$pmpc."','".$puv."','".$pmuv."','".$ppv."','".$pmpv."','".$rese."','".$mad."','".$tam."','".$kil."','".$undkilo."')");
			if($ins>0)
			{
				$uid = pg_fetch_row(pg_query($dbconn,"SELECT setval('pro_id_seq',nextval('pro_id_seq')-1) as id;"));
				$idp = $uid[0];

				$codpro = sprintf('%04d',$idp);
				$updpro = pg_query($dbconn, "UPDATE tbl_productos SET pro_codigo = '".$codpro."' where pro_clave_int = '".$idp."'");

				//UPDATE MERCADO PRECIOS
				$udppremercado = pg_query($dbconn, "UPDATE tbl_precios_mercado  SET pro_clave_int = '".$idp."',prm_estado = '1' WHERE usu_clave_int = '".$idUsuario."' and prm_estado = 0");

				//$idp = pg_last_oid($ins);

				$carpetaa2 = '../../modulos/productos/imagenes/COD'.$idp;
				//CREACION DE CARPETA PARA LAS IMAGENES DEL PRODUCTOS
				if(!file_exists($carpetaa2)){  
					if(mkdir($carpetaa2, 0777, true)){ $insim = 1; } else { $insim = 0; } }
					else{ $insim = 1;}

				if($_POST['ruta']!="")
				{
					$inserimagenes = pg_query($dbconn,"INSERT INTO tbl_productos_imagen (pro_clave_int,pri_usu_actualiz,pri_fec_actualiz) VALUES('".$idp."','".$usuario."','".$fecha."')");
					if($inserimagenes>0)
					{
						$uid = pg_fetch_row(pg_query($dbconn,"SELECT setval('pri_id_seq',nextval('pri_id_seq')-1) as id;"));
						$idip = $uid[0];

						//$idip = pg_last_oid($inserimagenes);
						
						//SI EXISTE LA CARPETA DE LAS IMAGENES PARA PRODUCTOS SE MUEVE LA IMAGEN
						if($insim==1)
						{
							$rutan = "../../modulos/productos/imagenes/COD".$idp."/".$idip.".".$extension;
							if(rename($rutaold,$rutan) and $_POST['ruta']!="")
							{
								$rutanew = "modulos/productos/imagenes/COD".$idp."/".$idip.".".$extension;	
								$update = pg_query($dbconn,"update tbl_productos_imagen set pri_imagen='".$rutanew."' where pri_clave_int='".$idip."'");
								unlink($rutalold);
							}
						}
					}
				}	
					
				$res ="ok";
				$msn = "Producto guardado con exito";
			}
			else
			{
				$res = "error";
				$msn = "Error de BD (".pg_last_error($dbconn)."). No se guardo el producto. Verificar";

			}
		}
		$datos[] = array('res' => $res , 'msn' =>$msn );
		echo json_encode($datos);
    }
    else if($opcion=="GUARDAREDICION")
    {
    	$id = $_POST['id'];
    	$nom = $_POST['nom'];
    	$desc = nl2br($_POST['desc']);
    	$descbreve = nl2br($_POST['descbreve']);
    	$cat = $_POST['cat'];
    	$cod = $_POST['cod'];
    	$mar = $_POST['mar'];
    	$act = $_POST['act'];
    	$ago = $_POST['ago'];  
    	$pal = $_POST['pal']; 	
    	$mad = $_POST['mad'];
    	//$unicom = $_POST['unicom'];
    	//$unicon = $_POST['unicon'];

    	$puc = $_POST['puc'];
    	$pmuc = $_POST['pmuc'];
    	$ppc = $_POST['ppc'];
    	$pmpc = $_POST['pmpc'];

    	$puv = $_POST['puv'];
    	$pmuv = $_POST['pmuv'];
    	$ppv = $_POST['ppv'];
    	$pmpv = $_POST['pmpv'];

    	$rese = $_POST['rese'];
    	$tam = $_POST['tam'];
    	$kil = $_POST['kil'];
    	$undkilo = $_POST['undkilo'];


		$veri = pg_query($dbconn, "SELECT * FROM tbl_productos where pro_nombre = '".$nom."' and pro_activo!=2 and pro_clave_int!='".$id."'");
		$numv = pg_num_rows($veri);		

		if($nom=="" || $nom==NULL)
		{
			$res = "error";
			$msn = "Diligenciar el nombre del producto. Verificar";
		}
		else if($desc =="" || $desc==NULL)
		{
			$res = "error";
			$msn = "Diligenciar la descripción del producto. Verificar";
		}
		else if($cat=="" || $cat==NULL)
		{
			$res = "error";
			$msn = "Seleccionar la categoria del producto. Verificar";
		}
		else if($numv>0)
		{
			$res = "error";
			$msn = "Ya hay una producto con el nombre ingresado. Verificar";
		}
		else
		{
			$ins = pg_query($dbconn,"UPDATE tbl_productos SET pro_nombre = '".$nom."',pro_descripcion = '".$desc."',pro_descripcionbreve = '".$descbreve."',pro_palabra = '".$pal."',pro_marca = '".$mar."', cat_clave_int = '".$cat."', pro_activo= '".$act."', pro_mostrar_agotado= '".$ago."',pro_usu_actualiz = '".$usuario."',pro_fec_actualiz = '".$fecha."',pro_uni_compra = '".$puc."', pro_mu_compra = '".$pmuc."',pro_pes_compra = '".$ppc."',pro_mp_compra ='".$pmpc."',pro_uni_venta = '".$puv."', pro_mu_venta = '".$pmuv."',pro_pes_venta = '".$ppv."',pro_mp_venta ='".$pmpv."',pro_reserva = '".$rese."',pro_estado = '".$mad."',pro_tamano = '".$tam."',pro_kilo = '".$kil."',pro_und_kilo = '".$undkilo."' WHERE pro_clave_int = '".$id."'");
			if($ins>0)
			{	
				$codpro = sprintf('%04d',$id);
				$updpro = pg_query($dbconn, "UPDATE tbl_productos SET pro_codigo = '".$codpro."' where pro_clave_int = '".$id."'");
				$res ="ok";
				$msn = "Producto actualizado con exito";
			}
			else
			{
				$res = "error";
				$msn = "Error de BD(".pg_last_error($dbconn)."). No se actualizo la categoria. Verificar";

			}
		}
		//,"q"=>"UPDATE tbl_productos SET pro_nombre = '".$nom."',pro_descripcion = '".$desc."',pro_descripcionbreve = '".$descbreve."',pro_palabra = '".$pal."',pro_marca = '".$mar."',pro_codigo = '".$cod."', cat_clave_int = '".$cat."', pro_activo= '".$act."', pro_mostrar_agotado= '".$ago."',pro_usu_actualiz = '".$usuario."',pro_fec_actualiz = '".$fecha."' WHERE pro_clave_int = '".$id."'" 
		$datos[] = array('res' => $res , 'msn' =>$msn);
		echo json_encode($datos);
    }
    else if($opcion=="FILTROS" || $opcion=="FILTROS2" || $opcion=="FILTROS3"  || $opcion=="FILTROS4" || $opcion=="FILTROS5" || $opcion=="FILTROS6" || $opcion=="FILTROS7" || $opcion=="FILTROS8" || $opcion=="FILTROS9")
    {
    	if($opcion=="FILTROS"){ $opc = "LISTAPRODUCTOS"; }
    	else if($opcion=="FILTROS2"){ $opc= "LISTAPRECIOS";}
    	else if($opcion=="FILTROS3" || $opcion =="FILTROS4"){ $opc="VISTAMOSAICO";}
    	else if($opcion=="FILTROS5"){ $opc="LISTAOFERTAS"; }
    	else if($opcion=="FILTROS6"){ $opc = "LISTAINVENTARIO"; }
    	else if($opcion=="FILTROS7"){ $opc = "LISTAENTRADAS"; }
    	else if($opcion=="FILTROS8"){ $opc = "LISTASALIDAS"; }
    	else if($opcion=="FILTROS9"){ $opc = "LISTAPRODUCTOS";}
    	?>
    	<div class="row">
    		<div class="col-md-12 <?php if($opcion=="FILTROS9"){ echo 'hide';}?>">
    			<label>Nombre producto</label>
    			<input type="text" name="busnombre" id="busnombre" value="" class="form-control">
    		</div>
    		<div class="col-md-12">
    			<label>Codigo producto</label>
    			<input type="text" name="buscodigo" id="buscodigo" value="" class="form-control">
    		</div>
    		<div class="col-md-12 hide">
    			<label>Marca</label>
    			<input type="text" name="busmarca" id="busmarca" value="" class="form-control">
    		</div>
    		<div class="col-md-12">
    			<label>Categoria</label>
    			<select  name="buscategoria" id="buscategoria" multiple class="form-control selectpicker">    				
					<?php 
					$concat = pg_query($dbconn, "select * from tbl_categorias WHERE cat_activo in(0,1)");
					while($datcat = pg_fetch_array($concat))
					{
						$idc = $datcat['cat_clave_int'];
						$imgc = $datcat['cat_imagen'];
						$nomc = $datcat['cat_nombre'];
						?>
						 <option data-content="<img src='<?php echo $imgc;?>' width='20'/> <?php echo $nomc;?>" <?php if($cat==$idc){ echo "selected"; } ?> value="<?php echo $idc;?>"><?php echo $nomc;?></option>
						<?php
					}
					?>  				
    			</select>
    		</div>
    		<div class="col-md-12 <?php if($opcion!="FILTROS3"){ echo 'hide';}?>">
    			<label>Mercado</label>
    			<select name="busmercado" id="busmercado" class="form-control input-sm selectpicker" title="Seleccione mercado">
            				<?php
            				$conm = pg_query($dbconn, "SELECT mer_clave_int,mer_nombre FROM tbl_mercado WHERE mer_activo = 1");
            				while($datm = pg_fetch_array($conm))
            				{
            				?>
							<option value="<?php echo $datm['mer_clave_int'];?>"><?php echo $datm['mer_nombre'];?></option>
							<?php
							}
						?>
				</select>
    		</div>
    		
    		<!--<div class="col-md-12">
    			<label>Unidad</label>
    			<select  name="busunidad" id="busunidad" multiple class="form-control selectpicker">    				
					<?php 
					$conuni = pg_query($dbconn, "select * from tbl_unidades WHERE est_clave_int in(1)");
					while($datuni = pg_fetch_array($conuni))
					{
						$idu = $datuni['uni_clave_int'];						
						$nomu = $datuni['uni_nombre'];
						$codigo = $datuni['uni_codigo'];
						?>
						 <option   value="<?php echo $idc;?>"><?php echo $codigo;?></option>
						<?php
					}
					?>  				
    			</select>
    		</div>-->
    		<?php
    		 if($opcion=="FILTROS4" || $opcion=="FILTROS5")
    		 {
    		 	?>
    		 	<div class="col-md-12">
    		 		<label>Oferta:</label>
	    			<select  name="busoferta" id="busoferta" multiple class="form-control selectpicker">    				
						<?php 
						$conofe = pg_query($dbconn, "select * from tbl_ofertas");
						while($datofe = pg_fetch_array($conofe))
						{
							$ido = $datofe['ofe_clave_int'];						
							$nomo = $datofe['ofe_titulo'];
							
							?>
							 <option   value="<?php echo $ido;?>"><?php echo $nomo;?></option>
							<?php
						}
						?>  				
	    			</select>
    		 	</div>
    		 	<div class="col-md-12">
    		 		<label>Estado Oferta:</label>
	    			<select  name="busestadooferta" id="busestadooferta" multiple class="form-control selectpicker"> 
					<option   value="0">Inactiva</option>
					<option   value="1">Activa</option>
					<option   value="2">Vencida</option>
	    			</select>
    		 	</div>
    		 	<div class="col-md-12">
    			<button onclick="CRUDOFERTAS('<?php echo $opc;?>','');" type="button" class="btn btn-success btn-block btn-sm">Buscar <i class="fa fa-search fa-2x"></i></button>
    		</div>
    		 	<?php
    		 }
    		 else
    		 {
    		 	if($opcion=="FILTROS" || $opcion=="FILTROS2" || $opcion=="FILTROS3")
    		 	{
    		 		$onclick = "onclick=CRUDPRODUCTOS('".$opc."','')";
    		 		$click = "CRUDPRODUCTOS('".$opc."','');";
    		 	}
    		 	else if($opcion=="FILTROS4" || $opcion=="FILTROS5")
    		 	{
    		 		$onclick = "onclick=CRUDOFERTAS('".$opc."','')";
    		 		$click = "CRUDOFERTAS('".$opc."','');";
    		 	}
    		 	else
    		 	if($opcion=="FILTROS6" || $opcion=="FILTROS7" || $opcion=="FILTROS8")
    		 	{
    		 		$onclick = "onclick=CRUDINVENTARIO('".$opc."','')";
    		 		$click = "CRUDINVENTARIO('".$opc."','');";
    		 	}
    		 	else if($opcion=="FILTROS9")
    		 	{
    		 		$onclick = "onclick=CRUDPEDIDOS('".$opc."','')";
    		 		$click = "CRUDPEDIDOS('".$opc."','');";
    		 	}

    		 	
    		?>
    		<div class="col-md-12">
    			<button <?php echo $onclick;?> type="button" class="btn btn-success btn-block btn-sm">Buscar <i class="fa fa-search fa-2x"></i></button>
    		</div>
    		<?php 
    		} 
    		?>
    	</div>
    	
    	<script>
    		$(document).ready(function(e) {
    			
				$('#busnombre').on('keydown',function (e) {
					if(e.which==13) {
						<?php echo $click;?>
					}
				});
				$('#buscodigo').on('keyup',function (e) {
					if(e.which==13) {
						<?php echo $click;?>
					}
				});
				$('#busmarca').on('keyup',function (e) {
					if(e.which==13) {
						<?php echo $click;?>
					}
				});
				
				$('#buscategoria').on('keyup change',function (e) {
					if(e.which==13) {
						<?php echo $click;?>
					}
				});

				$('#busmercado').on('keyup change',function (e) {
					if(e.which==13) {
						<?php echo $click;?>
					}
				});

				
				/*$('#busunidad').on('keyup change',function (e) {
					if(e.which==13) {
						<?php echo $click;?>
					}
				});*/
				
    	});
    	</script>
    	<?php
    	  	
    	if($opcion=="FILTROS4" || $opcion=="FILTROS5")
    	{
    		?>
    		<script>
    		$(document).ready(function(e) {
    			
    			$('#busoferta').on('keyup change',function (e) {
					if(e.which==13) {
						<?php echo $click;?>
					}
				});
				
				$('#busestadooferta').on('keyup change',function (e) {
					if(e.which==13) {
						<?php echo $click;?>
					}
				});
				
    		});
    		</script>
    		<?php
    	}

    	if($opcion=="FILTROS8" || $opcion=="FILTROS7")
    	{
    		?>
    		<?php
    	}

    	echo "<script>INICIALIZARCONTENIDO();</script>";
    	if($opcion=="FILTROS4" || $opcion=="FILTROS5")
    	{
    		echo "<script>CRUDOFERTAS('".$opc."','');</script>";
    	}
    	else if($opcion=="FILTROS6" || $opcion=="FILTROS7" || $opcion=="FILTROS8")
    	{
    		echo "<script>CRUDINVENTARIO('".$opc."','');</script>";
    	}
    	else if($opcion=="FILTROS9")
    	{
    		echo "<script>CRUDPEDIDOS('".$opc."','');</script>";
    	}
    	else
    	{
    		echo "<script>CRUDPRODUCTOS('".$opc."','');</script>";
    	}


    }
    else if($opcion=="LISTAPRODUCTOS")
    {
    	?>
    	<script src="jsdatatable/productos/jslistaproductos.js" type="text/javascript"></script>
    	<div class="table-responsive">
    	<table class="table table-bordered table-striped" id="tbproductos" width="100%">    		
    		<thead>
    			<tr>
    				<th rowspan="2" width="20px">Precios</th>
    				<th colspan="3"  width="20px" class="dt-head-center">Acciones</th>
    				
    				<th rowspan="2" class="dt-head-center">Nombre</th>
    				<th rowspan="2" class="dt-head-center">Código</th>    				
    				<th rowspan="2" class="dt-head-center">Categoria</th>
    				<th rowspan="2" class="dt-head-center">Estado</th>
    				    			
    				<th colspan="4" class="dt-head-center">Compra 
    				<th colspan="7" class="dt-head-center">Venta</th>

    			</tr>

    			<tr><th></th>
    				<th></th>
    				<th></th>

    				<th class="dt-head-center" style="width:40px !important">X Und</th> 
    				<th class="dt-head-center">Min.Und</th> 
    				<th class="dt-head-center">X Peso</th> 
    				<th class="dt-head-center">Min.Pes</th>  
    				<th class="dt-head-center">X Und</th> 
    				<th class="dt-head-center">Min.Und</th> 
    				<th class="dt-head-center">X Peso</th> 
    				<th class="dt-head-center">Min.Pes</th>  
    				<th class="dt-head-center">Reserva</th> 
    				<th class="dt-head-center">Maduración</th> 
    				<th class="dt-head-center">Tamano</th>     				
    			</tr>
    		</thead>
    		<tfoot>
    			<tr>
    				<th></th>
    				<th></th>
    				<th></th>
    				<th></th>
    				<th></th>    				
    				<th></th>
    				
    				<th></th>
    				<th></th>
    				<th></th>
    				<th></th>
    				<th></th>
    				<th></th>
    				<th></th>
    				<th></th>
    				<th></th>
    				<th></th>
    				<th></th>
    				<th></th>
    				<th></th>
    			</tr>
    		</tfoot>
    	</table>
    </th>
    	<?php
    }
    else if($opcion=="VISTAMOSAICO")
    {
		$nom = $_POST['nom'];
		$cod = $_POST['cod'];
		$cat = $_POST['cat']; $cat = implode(', ', (array)$cat); if($cat==""){$cat1="'0'";}else {$cat1=$cat;}
		$mar = $_POST['mar'];
		$mer = $_POST['mer'];
		if($mer=="" || $mer==NULL){ $mer = 3; }
		
		$conmer = pg_query($dbconn, "SELECT mer_tamano,mer_clasificacion FROM tbl_mercado WHERE mer_clave_int = '".$mer."'");
		$datmer = pg_fetch_array($conmer);
		//$aplicatamano = $datmer['mer_tamano'];
		$clasificacion = $datmer['mer_clasificacion'];
		$arrayclasificacion = explode(",", $clasificacion);
		
		//$uni = $_POST['uni']; $uni = implode(', ', (array)$uni); if($uni==""){$uni1="'0'";}else {$uni1=$uni;}

		$pro = $_POST['pro']; $pro = implode(', ', (array)$pro); if($pro==""){$pro1="'0'";}else {$pro1=$pro;}

    	$sql = "SELECT pro_clave_int,pro_nombre,pro_descripcionbreve,pro_uni_venta,pro_pes_venta,pro_mu_venta,pro_mp_venta,pro_estado,pro_tamano,cat_imagen,pro_kilo,pro_und_kilo FROM tbl_productos AS p JOIN tbl_categorias c ON c.cat_clave_int = p.cat_clave_int";
    	$sql.= " WHERE ( p.pro_nombre ILIKE REPLACE('".$nom."%',' ','%') OR p.pro_palabra ILIKE REPLACE('".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '' ) and ( p.pro_codigo ILIKE REPLACE('".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '' ) and ( p.cat_clave_int IN(".$cat1.") OR '".$cat."' IS NULL OR '".$cat."' = '')  and ( p.pro_clave_int IN(".$pro1.") OR '".$pro."' IS NULL OR '".$pro."' = '') and pro_activo!=2 order by LOWER(pro_nombre) ASC";//and ( p.pro_uni_compra IN(".$uni1.") OR '".$uni."' IS NULL OR '".$uni."' = '') and ( p.pro_uni_consumo IN(".$uni1.") OR '".$uni."' IS NULL OR '".$uni."' = '')

    	$conpro = pg_query($dbconn,$sql);
    	$numpro = pg_num_rows($conpro);
    	$totalpro = 0;

    	?>
	
				

				<ul class="col-md-12" id="divlistaproductos" style="margin:10px 0px !important">
					<?php
					while($datp = pg_fetch_array($conpro))
					{
						$idp = $datp['pro_clave_int'];
						$nomp = $datp['pro_nombre'];
						$descp = $datp['pro_descripcionbreve'];
						$puv = $datp['pro_uni_venta'];
						$ppv = $datp['pro_pes_venta'];
						$pmuv = $datp['pro_mu_venta'];
						$pmpv = $datp['pro_mp_venta'];
						$mad = $datp['pro_estado'];
						$tam = $datp['pro_tamano'];
						$imcat = $datp['cat_imagen'];
						$kil = $datp['pro_kilo'];
						$undkilo = $datp['pro_und_kilo'];

                        $bn = "";

						if($puv==1)
						{
						$unidadmin = $pmuv;
						$dataunidad = 0;//por unidad
						}
						else if($ppv==1)
						{
						$unidadmin = $pmpv;
						$dataunidad = 1; //venta por peso
						}

						$con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idp."' and pri_activo = 1 LIMIT 1");
						$dat = pg_fetch_row($con);
						$cimg = $dat[0];
						$img = $dat[1];
						if($img=="" || $img==NULL)
						{
							$img= $urlweb."/dist/img/nofoto.png";
							$bn = "bn";
						}
						else
						{
							$img = $urlweb.$img;
						}


							$conpre = pg_query($dbconn, "select prm_venta,prm_peso_unidad from tbl_precios_mercado where pro_clave_int ='".$idp."' and cla_clave_int in  (SELECT cla_clave_int FROM tbl_clasificacion where cla_clave_int = '1' ORDER BY cla_clave_int ASC LIMIT 1) and mer_clave_int = '".$mer."'");
							$datpre = pg_fetch_array($conpre);
							$venta = $datpre['prm_venta'];
							if($venta<=0){ $venta = 0;}

							$pesounidad = $datpre['prm_peso_unidad']; if($pesounidad<=0 || $pesounidad==NULL){ $pesounidad = 0; }
							$pesounidadkilo = $pesounidad/1000;
							$pesototalkilo = $pesounidadkilo * $unidadmin;


							//informacion de la oferta
							$conoferta = pg_query($dbconn, "SELECT o.ofe_clave_int,ofe_titulo,ofe_descripcion,ofe_inicio,ofe_fin,ofe_tipo_descuento,ofe_descuento,ofe_estado,ofe_imagen FROM tbl_ofertas o join tbl_ofertas_producto op ON op.ofe_clave_int = o.ofe_clave_int   where o.ofe_estado = 1 and op.pro_clave_int = '".$idp."' and o.mer_clave_int = '".$mer."' order by ofe_inicio ASC limit 1");
							$numoferta = pg_num_rows($conoferta);
							if($numoferta>0)
							{
								$datoferta = pg_fetch_array($conoferta);
								$tipo = $datoferta['ofe_tipo_descuento'];
								$descuento = $datoferta['ofe_descuento'];
								if($tipo==1)
								{
									$ventaold = $venta;
									$ventaoferta = $descuento;
									$ahorrooferta = $venta - $ventaoferta;
									$descuentooferta = 100 - ($ventaoferta*100/$venta);
								}
								else if($tipo==2)
								{
									$ventaold = $venta;
									$ventaoferta = $venta - ($venta*$descuento/100);
									$ahorrooferta = $venta - $ventaoferta;
									$descuentooferta = $descuento;
								}
							}
							else
							{
								$ventaold  = 0;
								$ventaoferta = $venta;
							}

							if($kil==1)
							{
								//$undkilo = 1000/$pesounidad;
								//$ventaoferta = $ventaoferta/$undkilo;
								if($pesounidad<=0){ $undkilo = 0; $ventaoferta = $ventaoferta/1; $ventaold = $ventaold/1; }
								else { $undkilo = 1000/$pesounidad; $ventaoferta = $ventaoferta/$undkilo; $ventaold = $ventaold/$undkilo; }
							}

							$ventaoferta = ceil($ventaoferta/50)*50;
							$ventaold = ceil($ventaold/50)*50;

							//sc-product-item
							$save = 0;
							$maduracion = "";
							$tamano = "";
							if(count($arrayclasificacion)<=1 and $tam!=1 and $mad==0 and (($puv==1 and $ppv==0) || ($ppv==1 and $puv==0)))
							{
								$clasifica = $arrayclasificacion[0];
								$maduracion = "";
								$tamano = "";
								$save = 1;
							}

							if($ventaoferta>0)
							{
								$totalpro++;
						?>


		    		<li class="col-md-3 col-sm-6 col-lg-2 data-row2  thumbnail " data-placement="top" data-toggle="tooltip">
			                  	<figure data-toggle="modal" data-target="#modalinfo"  onclick="CRUDPRODUCTOS('INFOPRODUCTO2','<?PHP echo $idp;?>')">								
								<a href="" class="pixelProducto">									
									<center>
										<ul class="caption-style-4 ">
											<li><!--data-name="product_image"-->
											<img class="img-responsive btn-block <?php echo $bn;?>" src="<?php echo $img;?>" alt="img">
											<div class="caption">
											<div class="blur"></div>
											<div class="caption-text hide">	
												<p><?php echo $descp;?></p>
											</div>
											</div>
											</li>
										</ul>									
									</center>
								</a>
							</figure>
							<div class="col-md-12 col-xs-12" style="height: 80px !important; max-height: 80px">
								<h5>
								<small class="pricesmalloferta currency2 <?php if($numoferta<=0){ echo 'hide';}?>" ><?php echo $ventaold;?></small>
							<!--id="precio_<?php echo $idp;?>" data-name="product_name"-->
								<small style="display: inline-block;" class="pricesmall currency2" ><?php echo $ventaoferta;?></small><br>
								<span ><?php echo $nomp;?></span></h5>

								<p style="display: none"><?php echo $descp;?></p>
								<em id="emestimado_<?PHP echo $idp;?>" class="peso-estimado">Peso Estimado: <span id="esti_<?php echo $idp;?>"><?php echo $pesototalkilo."kg";?></span></em>
							</div>
							 <div class="" data-idp="<?php echo $idp;?>">
									

									
					           <div class="caption">
					                       
					            <div class="row">
									
					               	<div class="col-md-8 col-xs-8 col-lg-8">
										

									<div class="plusminus2 horiz">
									<button  type="button" class="btn-number" disabled="disabled" data-type="minus" data-field="product_quantity_<?php echo $idp;?>"></button>
									<input onkeypress="return validar_texto(event)"  data-clasificacion = "<?php echo $clasifica;?>" data-unidad = "<?php echo $dataunidad; ?>" data-save = "0" data-detalle = "<?php echo $idp;?>" data-un = '<?php if($dataunidad==1){ echo "GR"; }else{ echo "UND";}?>' type="number" name="product_quantity2" id="product_quantity_<?php echo $idp;?>" value="<?php echo $unidadmin;?>" min="<?php echo $unidadmin;?>" max="9999999" step="<?php if($dataunidad==1){ echo $unidadmin; }else{ echo 1;}?>" class="input-number <?php if($dataunidad==1){ echo "hide"; }else{ echo "";}?>" data-peso-unidad="<?php echo $pesounidad;?>" >
									<span class="<?php if($dataunidad==1){ echo ""; }else{ echo "hide";}?>"><?php echo $unidadmin;?></span>
									<button  type="button" class="btn-number" data-type="plus" data-field="product_quantity_<?php echo $idp;?>"></button> 
									</div>									
						               
					                </div>

					                <div class="col-md-4 col-xs-4 col-lg-4">
					                	<?php 
					                	  if($idUsuario>0)
						                	{
						                		if($save==1)
						                		{
						                			?>
						                			 <button class="btn btn-success btn-sm pull-right sc-add-to-cart"  title="Añadir al carro">Agregar</button>
						                			<?php

						                		}
						                		else
						                		{
						                		?>
						                    		<button class="btn btn-success btn-sm pull-right"  data-toggle="modal" data-target="#modalinfo" title="Añadir al carro" onclick="CRUDPRODUCTOS('INFOPRODUCTO2','<?PHP echo $idp;?>')">Agregar</button>
						                    <?php
						                		}
						                	}
						                	else
						                	{
						                			?>
						                    <button type="button" class="sc-logout btn btn-success btn-sm pull-right" title="Añadir al carro">Agregar</button>
						                    <?php
						                	}
						                	?>
						                	 <input name="product_price2" id="product_price2_<?php echo $idp;?>" value="<?php echo $ventaoferta;?>" type="hidden" />
	                                		<input class="sc-product-id" name="product_id2" value="<?php echo $idp;?>" type="hidden" />
					                </div>
					            </div>
					        </div>
					    </div>
					            	
	                </li>
            		<?php 
            			}
        			}
        			?>
    		</ul>
    		<div class="row paging-container tablePagingLista"> </div>
    	
	<script type="text/javascript">
	$(document).ready(function(){
		load = function() 
		{
				window.tp = new Pagination('.tablePagingLista', {
					itemsCount: <?php echo $totalpro;?>,
					pageRange:[18,24,30,-1],
					onPageSizeChange: function (ps) {
						console.log('changed to ' + ps);
					},
					onPageChange: function (paging) {
						//custom paging logic here
						console.log(paging);
						var start = paging.pageSize * (paging.currentPage - 1),
							end = start + paging.pageSize,
							$rows = $('#divlistaproductos').find('.data-row2');

						$rows.hide();

						for (var i = start; i < end; i++) {
							$rows.eq(i).show();
						}
					}
				});


			}

		load();
	    // Initialize Smart Cart    	
	    //$('#smartcart').smartCart();
	});
	</script>

    	<?php
    	echo "<script>INICIALIZARCONTENIDO();</script>";
    	echo "<script>IncDec('plusminus2');</script>";
    }
    else if($opcion=="INFOPRODUCTO2")
    {

   		$idproducto = $_POST['idproducto'];
   		$idmercado = $_POST['idmercado'];
   		$canp = $_POST['canp'];
   		$con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idproducto."' and pri_activo = 1 LIMIT 1");
    	$dat = pg_fetch_row($con);
    	$cimg = $dat[0];
    	$img = $dat[1];

		$conmer = pg_query($dbconn, "SELECT mer_tamano,mer_clasificacion FROM tbl_mercado WHERE mer_clave_int = '".$idmercado."'");
		$datmer = pg_fetch_array($conmer);
		//$aplicatamano = $datmer['mer_tamano'];
		$clasificacion = $datmer['mer_clasificacion'];
		$arrayclasificacion = explode(",", $clasificacion);

    	$sql = "SELECT pro_clave_int,pro_nombre,pro_descripcion,pro_descripcionbreve,pro_uni_venta,pro_pes_venta,pro_mu_venta,pro_mp_venta,pro_estado,pro_tamano,pro_kilo,pro_und_kilo FROM tbl_productos AS p JOIN tbl_categorias c ON c.cat_clave_int = p.cat_clave_int ";
		$sql.= " WHERE  (p.pro_clave_int = '".$idproducto."') order by pro_nombre ASC";
		$conpro = pg_query($dbconn,$sql);
		$datp = pg_fetch_array($conpro);
						
		//$idp = $datp['pro_clave_int'];
		$nomp = $datp['pro_nombre'];
		$desc = $datp['pro_descripcion'];
		$descp = $datp['pro_descripcionbreve'];
		$puv = $datp['pro_uni_venta'];
		$ppv = $datp['pro_pes_venta'];
		$pmuv = $datp['pro_mu_venta'];
		$pmpv = $datp['pro_mp_venta'];
		$mad = $datp['pro_estado'];
		$tam = $datp['pro_tamano'];
		$kil = $datp['pro_kilo'];
		$undkilo = $datp['pro_und_kilo'];

		if($puv==1)
		{
			$unidadmin = $pmuv;
			$dataunidad = 0;//por unidad
		}
		else if($ppv==1)
		{
			$unidadmin = $pmpv;
			$dataunidad = 1; //venta por peso
		}

		$con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idproducto."' and pri_activo = 1 LIMIT 1");
		$dat = pg_fetch_row($con);
		$cimg = $dat[0];
		$img = $dat[1];
		if($img=="" || $img==NULL)
		{
			$img= $urlweb."dist/img/nofoto.png";
			$bn = "bn";
		}
		else
		{
			$img = $urlweb.$img;
		}


		$conpre = pg_query($dbconn, "select prm_venta,prm_peso_unidad from tbl_precios_mercado where pro_clave_int ='".$idproducto."' and cla_clave_int = '1'  and mer_clave_int = '".$idmercado."'"); //in  (SELECT cla_clave_int FROM tbl_clasificacion where cla_clave_int in(".$clasificacion.") ORDER BY cla_clave_int ASC LIMIT 1)
		$datpre = pg_fetch_array($conpre);
		$venta = $datpre['prm_venta'];
		if($venta<=0){ $venta = 0;}
		$pesounidad = $datpre['prm_peso_unidad']; 
		if($pesounidad<=0 || $pesounidad==NULL){ $pesounidad = 0; }

	
		//informacion de la oferta
		$conoferta = pg_query($dbconn, "SELECT o.ofe_clave_int,ofe_titulo,ofe_descripcion,ofe_inicio,ofe_fin,ofe_tipo_descuento,ofe_descuento,ofe_estado,ofe_imagen FROM tbl_ofertas o join tbl_ofertas_producto op ON op.ofe_clave_int = o.ofe_clave_int   where o.ofe_estado = 1 and op.pro_clave_int = '".$idproducto."' and o.mer_clave_int = '".$idmercado."' order by ofe_inicio ASC limit 1");
		$numoferta = pg_num_rows($conoferta);
		if($numoferta>0)
		{
			$datoferta = pg_fetch_array($conoferta);
			$tipo = $datoferta['ofe_tipo_descuento'];
			$descuento = $datoferta['ofe_descuento'];
			if($tipo==1)
			{
				$ventaold = $venta;
				$ventaoferta = $descuento;
				$ahorrooferta = $venta - $ventaoferta;
				$descuentooferta = 100 - ($ventaoferta*100/$venta);
			}
			else if($tipo==2)
			{
				$ventaold = $venta;
				$ventaoferta = $venta - ($venta*$descuento/100);
				$ahorrooferta = $venta - $ventaoferta;
				$descuentooferta = $descuento;
			}
		}
		else
		{
			$ventaold  = 0;
			$ventaoferta = $venta;
		}
		if($kil==1)
		{
			//$undkilo = 1000/$pesounidad;
			if($pesounidad<=0){ $undkilo = 0; $ventaoferta = $ventaoferta/1; }
			else { $undkilo = 1000/$pesounidad; $ventaoferta = $ventaoferta/$undkilo; }

			//$ventaoferta = $ventaoferta/$undkilo;
		}
		$ventaoferta = ceil($ventaoferta/50)*50;
    	?>

    	<div class="row sc-product-item infoproducto" >
    		<div class="col-md-6 col-sm-6 col-xs-12 visorImg">
    			<figure class="visor">
    				<?php
					$con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idproducto."'  and pri_activo = 1 limit 1");
					$num = pg_num_rows($con);
					if($num>0)
					{
						
							
							$imga = $img;
							
							?>
							<a href="#" data-image="<?php echo $imga;?>" data-zoom-image="<?php echo $imga;?>">
							<img class="img-thumbnail" id="lupa<?php echo ($k+1);?>"    src="<?php echo $imga;?>" />
							</a>
							<?php
						
					}
					else
					{
						?>
							<a href="#" data-image="dist/img/nofoto.png" data-zoom-image="dist/img/nofoto.png">
							<img class="img-thumbnail" id="lupa1"    src="dist/img/nofoto.png" />
							</a>
						<?php
						
					}
					//}
				?>

    			</figure>    			
    		</div>
    		<div class="col-md-6 col-sm-6 col-xs-12">
    			<div class="col-xs-6">
    				<h6>						
						<a data-dismiss="modal" aria-label="Close" class="text-muted">	
							<i class="fa fa-reply"></i> Continuar Comprando
						</a>
					</h6>
				</div>
				<div class="clearfix"></div>
				<h3>
				<small class="pricesmalloferta currency2 <?php if($numoferta<=0){ echo ' hide';}?>" ><?php echo $ventaold;?></small>
				<small id="precio_<?php echo $idproducto;?>" style="display: inline-block;" class="pricesmall currency2" ><?php echo $ventaoferta;?></small><br>
				<span data-name="product_name"><?php echo $nomp;?></span></h3>
				<p><?php echo $desc;?></p>
				<hr/>
				<div class="col-md-12">
					<div class="col-md-8">
					
					   <div class="col-md-12 <?php if(count($arrayclasificacion)<=1){ echo ' hide';}?>"">
			                    <h5>Clasificacion: </h5>
			                	
			                    <select name="product_clasificacion" id="product_clasificacion_<?php echo $idproductp;?>" class="form-control input-sm selectpicker" onchange="CRUDPRODUCTOS('CAMBIARPRECIO','<?php echo $idproducto; ?>','',this.value)">
                            	<?php
                            	
                            		for($c=0;$c<count($arrayclasificacion);$c++)
                            		{ 
                            			$concla = pg_query($dbconn,"SELECT * FROM tbl_clasificacion where cla_clave_int = '".$arrayclasificacion[$c]."'");
                            			$datc = pg_fetch_array($concla);
                            			$sql = pg_query($dbconn, "select prm_venta,prm_peso_unidad from tbl_precios_mercado where pro_clave_int ='".$idproducto."' and cla_clave_int = '".$datc['cla_clave_int']."' and mer_clave_int = '".$idmercado."'");
										$dat = pg_fetch_array($sql);
										$ven = $dat['prm_venta']; if($ven<=0){ $ven = 0;}
										if($numoferta>0)
										{
											if($tipo==1)
											{
												$venold = $ven;
												$venoferta = $descuento;
												$ahorrooferta = $ven - $venoferta;
												$descuentooferta = 100 - ($venoferta*100/$ven);
											}
											else if($tipo==2)
											{
												$venold = $ven;
												$venoferta = $ven - ($ven*$descuento/100);
												$ahorrooferta = $ven - $venoferta;
												$descuentooferta = $descuento;
											}
										}
										else
										{
											$venold  = 0;
											$venoferta = $ven;
										}

										if($venoferta>0)
										{
                            			?>
                                    	<option <?php if($arrayclasificacion[$c]==1){ echo "selected";}?> value="<?php echo $arrayclasificacion[$c];?>"><?php echo $datc['cla_nombre'];?></option>	
                                    <?php 
                                		}
                                	}
                                ?>
                                </select>
	                    </div>
					              
						<div class="col-md-12 <?php if($tam==1){ }else{ echo ' hide'; } ?>">
						<h5>Selecciona la tamaño de tu producto:</h5>
						<div class="toggle_radio">
								<input data-name="product_tam" name="product_tamano_<?php echo $idproducto;?>" type="radio" class="toggle_option grande_toggle" id="tamano1_<?php echo $idproducto;?>" name="toggle_option" value="Grande">
								<input data-name="product_tam" name="product_tamano_<?php echo $idproducto;?>" type="radio"  class="toggle_option mediano_toggle" id="tamano2_<?php echo $idproducto;?>" name="toggle_option" value="Mediano">
								<input data-name="product_tam" name="product_tamano_<?php echo $idproducto;?>" type="radio" class="toggle_option pequeno_toggle" id="tamano3_<?php echo $idproducto;?>" name="toggle_option" value="Pequeño">
								<label class="info" for="tamano1_<?php echo $idproducto;?>"><p>Grande</p></label>
								<label class="info" for="tamano2_<?php echo $idproducto;?>"><p>Mediano</p></label>
								<label class="info" for="tamano3_<?php echo $idproducto;?>"><p>Pequeño</p></label>
								<div class="toggle_option_slider">
								</div>
						</div>
					</div>
					<div class="col-md-12 <?php if($mad==0){ echo "hide";}?>">
						<h5>Selecciona la madurez de tu producto:</h5>
						<div class="toggle_radio info">
							<input data-name="product_est" name="product_estado_<?php echo $idproducto;?>" type="radio" class="toggle_option first_toggle" id="estado1_<?php echo $idproducto;?>" name="toggle_option" value="Verde">
							<input data-name="product_est" name="product_estado_<?php echo $idproducto;?>" type="radio" checked class="toggle_option second_toggle" id="estado2_<?php echo $idproducto;?>" name="toggle_option" value="Pinton">
							<input data-name="product_est" name="product_estado_<?php echo $idproducto;?>" type="radio" class="toggle_option third_toggle" id="estado3_<?php echo $idproducto;?>" name="toggle_option" value="Maduro">
							<label class="info" for="estado1_<?php echo $idproducto;?>"><p>Verde</p></label>
							<label class="info"  for="estado2_<?php echo $idproducto;?>"><p>Pintón</p></label>
							<label class="info"  for="estado3_<?php echo $idproducto;?>"><p>Maduro</p></label>
							<div class="toggle_option_slider "></div>
						</div>	
					</div>
				</div>

				<div class="nav-tabs-custom col-md-12 sel-unidad <?php if(($puv==1 and $ppv==0) || ($ppv==1 and $puv==0)){ echo " hide"; } ?>" >
					<ul class="nav nav-tabs pull-left">    
						<li class="active unidad" >
							<a  data-toggle="tab" aria-expanded="true" onclick="ckradio('product_unidad_<?php echo $idproducto;?>','0')" >
								<label>Unidad</label>
								<input style="display: none;" data-name="product_uni" name="product_unidad_<?php echo $idproducto;?>" type="radio" id="unidad1_<?php echo $idproducto;?>" value="0" <?php if($dataunidad==0){ echo "checked";}?> data-uni-min = '<?php echo $pmuv;?>' data-text-min="UND" data-idp="<?php echo $idproducto;?>"  data-uni-step="1">
							</a>
						</li>
						<li class="peso" onclick="ckradio('product_unidad_<?php echo $idproducto;?>','1')">
							<a data-toggle="tab" aria-expanded="false">
								<label>Peso</label>
								<input style="display: none;" data-name="product_uni" name="product_unidad_<?php echo $idproducto;?>" type="radio" id="unidad2_<?php echo $idproducto;?>" value="1" <?php if($dataunidad==1){ echo "checked";}?> data-uni-min = '<?php echo $pmpv;?>' data-text-min="GR" data-idp="<?php echo $idproducto;?>"  data-uni-step="<?php echo $pmpv;?>">
							</a>
						</li>
					</ul>
				</div>
				<div class="col-md-7 col-md-offset-1 col-xs-12">					
					<div class="plusminus3 horiz" id="plus_<?php echo $idproducto;?>">
					<!--<button></button>
					<input data-unidad = "0" data-detalle ="" data-save = "0" data-producto = "<?php echo $idproducto;?>" data-un = '<?php if($dataunidad==1){ echo "GR"; }else{ echo "UND";}?>' type="number" name="product_quantity" id="product_quantity2_<?php echo $idproducto;?>" value="<?php if($canp>0){ echo $canp;} else { echo $unidadmin; } ?>" min="<?php echo $unidadmin;?>" max="9999999" step="<?php echo $unidadmin;?>" >
					<span id="spanproduct_quantity2_<?php echo $idproducto;?>"></span>
					<button></button> -->

					<button  type="button" class="btn-number" data-type="minus" data-field="product_quantity4_<?php echo $idproducto;?>"></button>
					<input data-unidad = "0" data-save = "0" data-detalle = "<?php echo $idproducto;?>" data-un = '<?php if($dataunidad==1){ echo "GR"; }else{ echo "UND";}?>' type="number" name="product_quantity" id="product_quantity4_<?php echo $idproducto;?>" value="<?php if($canp>0){ echo $canp;} else { echo $unidadmin; } ?>" min="<?php echo $unidadmin;?>" max="9999999" step="<?php if($dataunidad==1){ echo $unidadmin; }else{ echo "UND";}?>" class="input-number">
					<span class="'<?php if($dataunidad==1){ echo ""; }else{ echo "hide";}?>" id="spanproduct_quantity2_<?php echo $idproducto;?>"></span>
					<button  type="button" class="btn-number" data-type="plus" data-field="product_quantity4_<?php echo $idproducto;?>"></button>

					</div>	
				</div>
				<div class="col-md-4 col-xs-12 pull-right">
					<?php 
					if($idUsuario>0)
					{
						?>
					<button class="sc-add-to-cart btn btn-success btn-block pull-right">Agregar <i class="fa fa-opencart" title="Añadir al carro"></i></button>
					<?php
					}
					else
					{
							?>
					<button type="button" class="sc-logout btn btn-success  pull-right  btn-block">Agregar <i class="fa fa-opencart" title="Añadir al carro"></i></button>
					<?php
					}
					?>
				</div>
				 <input name="product_price" id="product_price_<?php echo $idproducto;?>" value="<?php echo $ventaoferta;?>" type="hidden" />
	            <input class="sc-product-id" name="product_id" value="<?php echo $idproducto;?>" type="hidden" />
				</div>
				<figure class="lupa">
					
					<img src="">

				</figure>
			</div>
		</div>
    	<script>
    		jQuery(document).ready(function(e) 
    		{   			

				/*=============================================
				EFECTO LUPA
				=============================================*/
				$(".infoproducto figure.visor img").mousedown(function(event){

					var capturaImg = $(this).attr("src");

					$(".lupa img").attr("src", capturaImg);

					$(".lupa").fadeIn("fast");

					$(".lupa").css({

						"height":$(".visorImg").height()+"px",
						"background":"#eee",
						"width":"100%"

					})

				})

				$(".infoproducto figure.visor img").mouseout(function(event){
					$(".lupa").fadeOut("fast");
				})

				$(".infoproducto figure.visor img").mousemove(function(event){

					var posX = event.offsetX;
					var posY = event.offsetY;

					$(".lupa img").css({

						"margin-left":-posX+"px",
						"margin-top":-posY+"px"
					})
				})    	
			
    		});
		
</script>
    	<?php
    	echo "<script>IncDec('plusminus3');</script>";
    	echo "<script>INICIALIZARCONTENIDO();</script>";
    }
     else if($opcion=="CAMBIARPRECIO")
    {
    	$cla = $_POST['cla'];
    	$idp = $_POST['idp'];
    	$idmercado = $_POST['idmercado'];
    	/*$concla = pg_query($dbconn,"SELECT cla_clave_int from tbl_clasificacion WHERE cla_nombre = '".$cla."'");
		$datcla = pg_fetch_array($concla);
		$codcla = $datcla['cla_clave_int'];*/


		$conoferta = pg_query($dbconn, "SELECT o.ofe_clave_int,ofe_titulo,ofe_descripcion,ofe_inicio,ofe_fin,ofe_tipo_descuento,ofe_descuento,ofe_estado,ofe_imagen FROM tbl_ofertas o join tbl_ofertas_producto op ON op.ofe_clave_int = o.ofe_clave_int   where o.ofe_estado = 1 and op.pro_clave_int = '".$idp."' and o.mer_clave_int = '".$idmercado."' order by ofe_inicio ASC limit 1");
		$numoferta = pg_num_rows($conoferta);
		$datoferta = pg_fetch_array($conoferta);
		$tipo = $datoferta['ofe_tipo_descuento'];
		$descuento = $datoferta['ofe_descuento'];



    	$sql = pg_query($dbconn, "select prm_venta,prm_peso_unidad,pro_kilo, from tbl_precios_mercado  m join tbl_productos p ON p.pro_clave_int = m.pro_clave_int where m.pro_clave_int ='".$idp."' and cla_clave_int = '".$cla."' and mer_clave_int = '".$idmercado."'");
    	$dat = pg_fetch_array($sql);
    	$ven = $dat['prm_venta']; if($ven<=0){ $ven = 0;}
    	$kil = $dat['pro_kilo'];
    	//$undkilo = $dat['pro_und_kilo'];
    	$pesund = $dat['prm_peso_unidad'];


    	if($numoferta>0)
		{
			if($tipo==1)
			{
				$venold = $ven;
				$venoferta = $descuento;
				$ahorrooferta = $ven - $venoferta;
				$descuentooferta = 100 - ($venoferta*100/$ven);
			}
			else if($tipo==2)
			{
				$venold = $ven;
				$venoferta = $ven - ($ven*$descuento/100);
				$ahorrooferta = $ven - $venoferta;
				$descuentooferta = $descuento;
			}
		}
		else
		{
			$venold  = 0;
			$venoferta = $ven;
		}

		if($kil==1)
		{ 
			if($pesund<=0){ $undkilo = 0; $venoferta = $venoferta/1; }
			else { $undkilo = 1000/$pesund; $venoferta = $venoferta/$undkilo; }

			//$venoferta = $venoferta/$undkilo; 
		}


    	$datos[]= array("venta"=>$venoferta);
    	echo json_encode($datos);
    }
     else if($opcion=="LISTAPRECIOS")
    
    
    {
    	?>
    	<script src="jsdatatable/productos/jslistaprecios.js" type="text/javascript"></script>
    	<table class="table table-striped" id="tbproductos" style="width: 100%">    		
    		<thead>
    			<tr>
    				
    				<th style="width: 20px"></th>
    				<th style="width: 20px"></th>
    				<th>Nombre</th>
    				<th>Código</th>    				
    				<th>Categoria</th>
    				<th>Descripción</th>
    								
    			</tr>
    		</thead>
    		<tfoot>
    			<tr>    				
    				<th></th>
    				<th></th>
    				<th></th>
    				<th></th>
    				<th></th>
    				<th></th>
    				
    			</tr>
    		</tfoot>
    	</table>
    	<?php
    }
    else if($opcion=="PRECIOS")
    {
    	$idp = $_POST['id'];
    	$con=  pg_query($dbconn, "SELECT * FROM tbl_mercado where mer_activo=1 ");
    	$con2 = pg_query($dbconn,"SELECT * FROM tbl_clasificacion");
    	$num2 = pg_num_rows($con2);
    	$cla = array();


    	//datos producto
    	$conp = pg_query($dbconn, "select pre_clave_int,pre_costo,pre_venta from tbl_precios WHERE pro_clave_int = '".$idp."'");
    	$datp = pg_fetch_row($conp);
    	$pre = $datp[0];
    	$cos = $datp[1];
    	$ven = $datp[2];
    	if($ven>0)
    	{
    		$gan = (($ven - $cos)/$ven) * 100;
    	}
    	else
    	{
    		$gan = 0;
    	}

    	/*for($nc=0;$nc<$num2;$nc++)
    	{
    		$dat2 = pg_fetch_array($con2);
    		$cla[$nc] = array("idc"=>$dat2['cla_clave_int'],"nomc"=>$dat2['cla_nombre']);
    		echo $cla["nomc"][$nc];
    	}*/
    	?>
    	
    	<table class="table table-bordered compact" style="font-size: 12px">
    	<tr>
		<?php
   
   		$conmer = pg_query($dbconn, "SELECT mer_clave_int,mer_nombre,mer_clasificacion from tbl_mercado where mer_activo = 1 order by mer_clave_int = 3 ASC");
    	while($datm = pg_fetch_array($conmer))
    	{
    		$idm = $datm['mer_clave_int'];
    		$nom = $datm['mer_nombre'];
    		$clas = $datm['mer_clasificacion'];
    		$clasi = explode(",", $clas);
    		$num = count($clasi);
    		?>
    		<th class="bg-info text-center" colspan="<?php echo $num;?>"><?php echo $nom;?></th>
    		<?php
    	}
    	?>
    	<th class="hide" rowspan="2" style="vertical-align: middle;"><button class="btn btn-success" type="button" onclick="CRUDPRODUCTOS('GUARDARCAMBIOPRECIO','<?PHP echo $idp;?>','','')"><i class="fa fa-save"></i> Guardar</button></th>
    	</tr>
    	<tr>
		<?php
    	$conmer = pg_query($dbconn, "SELECT mer_clave_int,mer_nombre,mer_clasificacion,mer_porcentaje from tbl_mercado where mer_activo = 1  order by mer_clave_int = 3 ASC");

    	while($datm = pg_fetch_array($conmer))
    	{
    		$idm = $datm['mer_clave_int'];
    		$nom = $datm['mer_nombre'];
    		$clas = $datm['mer_clasificacion'];
    		$porc = $datm['mer_porcentaje'];
    		
    		$concla = pg_query($dbconn,"SELECT * FROM tbl_clasificacion where cla_clave_int in(".$clas.")");
    		$numcla = pg_num_rows($concla);
    		for($n=0;$n<$numcla;$n++)
    		{
    			$datc = pg_fetch_array($concla);
    			$idc = $datc['cla_clave_int'];
    			$nomc = $datc['cla_nombre'];

    			$convalact = pg_query($dbconn,"SELECT  prm_costo,prm_venta,prm_peso_unidad FROM tbl_precios_mercado WHERE pro_clave_int = '".$idp."' and mer_clave_int ='".$idm."' and cla_clave_int = '".$idc."' limit 1");
    			$datval = pg_fetch_array($convalact);
    			$valcos = $datval['prm_costo'];
    			$valact = $datval['prm_venta'];
    			$pesund = $datval['prm_peso_unidad'];


    			$veri = pg_query($dbconn,"SELECT * FROM tbl_precios_tmp WHERE pro_clave_int = '".$idp."' and mer_clave_int ='".$idm."' and cla_clave_int = '".$idc."'");
    			$num = pg_num_rows($veri);
    			$title="";
    			if($num>0)
    			{
    				$datv = pg_fetch_array($veri);
    				$valcos = $datv['valor'];
    				if(($pesund<=0 || $pesund==null) and $pesund!=$datv['peso_unidad']){
    					$pesund = $datv['peso_unidad'];
    				}

    				if(($valcos<=0 || $valcos==null) and $valcos!=$datv['valor']){
    					$valcos = $datv['valor'];
    				}
    				
    				
    				
    				$title = "class ='bg-red' data-toogle='tooltip' title='Datos pendientes por guardar'";
    			}
    		?>
    			<th <?php echo $title ;?> ><?php echo $nomc;?>
    				<!-- onchange="CRUDPRODUCTOS('PRECIOTMP','<?php echo $idp;?>','<?php echo $idm;?>','<?php echo $idc;?>')"-->
    				<input onkeypress="return validar_texto(event)" style="width: 110px" class="form-control input-sm currency2" id="valorpro_<?php echo $idp;?>_mer_<?php echo $idm;?>_cla_<?php echo $idc; ?>" value="$<?php echo number_format($valcos,0,',',',');?>" disabled /><br>
    				PrecioVenta: <span data-porcentaje="<?php echo $porc;?>" id="spanventa_<?php echo $idp;?>_mer_<?php echo $idm;?>_cla_<?php echo $idc; ?>" class="currency">$<?php echo number_format($valact,0,',',',');?></span><br>
    				<strong>Peso/Und:</strong>
    				 <div class="has-feedback">
    				<input onkeypress="return validar_texto(event)" style="width: 110px" class="form-control input-sm" id="pesund_<?php echo $idp;?>_mer_<?php echo $idm;?>_cla_<?php echo $idc; ?>" onchange="CRUDPRODUCTOS('PRECIOTMP','<?php echo $idp;?>','<?php echo $idm;?>','<?php echo $idc;?>')" value="<?php echo $pesund;?>"  disabled/>
    				 <span class=" form-control-feedback">GR</span>
    				</div>

    			</th>

    		<?php
    		}
    	}
    	?>
    	</tr>
    	
    	</table>    
    	<?php
    	//echo "<script>INICIALIZARCONTENIDO();</script>";
    }
    else if($opcion=="PRECIOTMP")
    {
    	$idp = $_POST['id'];
    	$idm = $_POST['idm'];
    	$idc = $_POST['idc'];
    	$ven = $_POST['ven'];
    	$pu = $_POST['pu'];
    	if($ven<=0 || $ven==NULL){ $ven = 0; } 
    	if($pu<=0 || $pu==NULL){ $pu = 0; } 
    	$veri = pg_query($dbconn,"SELECT * FROM tbl_precios_tmp WHERE pro_clave_int = '".$idp."' and mer_clave_int ='".$idm."' and cla_clave_int = '".$idc."'");
    	$num = pg_num_rows($veri);
    	if($num>0)
    	{
    		$sql = pg_query($dbconn, "UPDATE tbl_precios_tmp SET valor = '".$ven."',peso_unidad ='".$pu."' WHERE pro_clave_int = '".$idp."' and mer_clave_int ='".$idm."' and cla_clave_int = '".$idc."'");
    	}
    	else
    	{
    		$sql = pg_query($dbconn, "INSERT INTO tbl_precios_tmp(pro_clave_int,mer_clave_int,cla_clave_int,valor,peso_unidad) VALUES(".$idp.",".$idm.",".$idc.",'".$ven."','".$pu."')");

    	}
    	if($sql>0)
    	{
    		$res = "ok";
    		$msn = "";
    	}
    	else
    	{
    		$res = "error";
    		$msn = "Surgió un error al a actualizar precio. Error BD(".pg_last_error($dbconn).")";
    	}
    	$datos[] = array("res"=>$res, "msn"=>$msn);
    	echo json_encode($datos);
    }
    else if($opcion=="GUARDARCAMBIOPRECIO")
    {
    	$idp = $_POST['idp'];
    	$upd = 0;
    	$inse = 0;
    	$conp = pg_query($dbconn, "SELECT * FROM  tbl_precios_tmp WHERE pro_clave_int = '".$idp."'");
    	$nump = pg_num_rows($conp);
    	$errores = "";
    	if($nump>0)
    	{
    		$inser = 0;
    		
    		for($p=0;$p<$nump;$p++)
    		{
    			$datp = pg_fetch_array($conp);
    			$idm = $datp['mer_clave_int'];
    			$conm = pg_query($dbconn,"SELECT mer_porcentaje FROM tbl_mercado WHERE mer_clave_int = '".$idm."'"); 
    			$datm = pg_fetch_array($conm);
    			$porc = $datm['mer_porcentaje'];
    			$idc = $datp['cla_clave_int'];
    			$val = $datp['valor'];
    			$pu = $datp['peso_unidad'];
    			$ven = $val + ($val*($porc/100));
    			
    			$ven = ceil($ven/50)*50;

    			if($ven<=0 || $ven==NULL)
    			{
    				$ven = 0;
    			}
    			$veri = pg_query($dbconn, "SELECT * FROM tbl_precios_mercado WHERE pro_clave_int = '".$idp."' and mer_clave_int ='".$idm."' and cla_clave_int = '".$idc."'");
    			$numv = pg_num_rows($veri);
    			if($numv>0)
    			{
    				$sql = pg_query($dbconn,"UPDATE tbl_precios_mercado SET prm_costo = '".$val."',prm_venta = '".$ven."',prm_usu_actualiz = '".$usuario."',prm_fec_actualiz = '".$fecha."',prm_peso_unidad = '".$pu."'  WHERE pro_clave_int = '".$idp."' and mer_clave_int ='".$idm."' and cla_clave_int = '".$idc."'");
    				$upd++;
    			}
    			else
    			{
    				$sql = pg_query($dbconn, "INSERT INTO tbl_precios_mercado(pro_clave_int,mer_clave_int,cla_clave_int,prm_costo,prm_usu_actualiz,prm_fec_actualiz,prm_venta,prm_peso_unidad) VALUES('".$idp."','".$idm."','".$idc."','".$val."','".$usuario."','".$fecha."','".$ven."','".$pu."')");
    				$inse++;
    				
    			}	

    			if($sql>0)
    			{
    				$inser++;
    			}
    			else
    			{
    				$errores.="Error Actualizacion (". pg_last_error($dbconn).")<br>";
    			}
    		}
    		if($inser>0)
    		{
    			$del = pg_query($dbconn, "DELETE FROM tbl_precios_tmp WHERE pro_clave_int = '".$idp."'");
    			$res = "ok";
    			$msn = "Precios modificados correctamente";
    		}
    		else
    		{
    			$res = "error";
    			$msn = "No se modificaron los precios";
    		}
    	}
    	else
    	{
    		$res = "error";
    		$msn = "No se ha realizado modificacion a los precios. Verificar";
    	}
    	$datos[] = array("res"=>$res, "msn"=>$msn, "nump"=>$nump, "errores"=>$errores,"correctos"=>$inser,"numv"=>$numv,"actualizados"=>$upd,"insertados"=>$inse);
    	echo json_encode($datos);
    }
    else if($opcion=="EDITARPRECIO")
    {
    	$id = $_POST['id'];
    	$idm = $_POST['idm'];
    	$idc = $_POST['idc'];

    	$con = pg_query($dbconn, "select pro_codigo,pro_nombre,cat_nombre from tbl_productos p JOIN tbl_categorias c on c.cat_clave_int = p.cat_clave_int where pro_clave_int = '".$id."'");
    	$dat = pg_fetch_row($con);
    	$cod = $dat[0];
    	$nom = $dat[1];
    	$nomcat = $dat[2];

    	$con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$id."' LIMIT 1");
    	$dat = pg_fetch_row($con);
    	$cimg = $dat[0];
    	$img = $dat[1];

    	$conc = pg_query($dbconn,"select cla_clave_int,cla_nombre from tbl_clasificacion where cla_clave_int = '".$idc."'");
    	$datc = pg_fetch_row($conc);
    	$nomc = $datc[1];

    	$conm = pg_query($dbconn,"select mer_clave_int,mer_nombre from tbl_mercado where mer_clave_int = '".$idm."'");
    	$datm = pg_fetch_row($conm);
    	$nomm = $datm[1];

    	

    	$title = '<img class="direct-chat-img" src="'.$img.'" alt="message user image"><span data-toggle="tooltip" title="Categoria:'.$nomcat.'" class="col-md-3 badge bg-orange">'.$nomcat.'</span><span data-toggle="tooltip" title="Codigo:'.$cod.'" class="col-md-3 badge bg-light-blue">'.$cod.'</span><span data-toggle="tooltip" title="Mercado:'.$nomm.'" class="col-md-4 badge bg-blue">'.$nomm.'</span><span data-toggle="tooltip" title="clasificación:'.$nomc.'" class="col-md-4 badge bg-green">'.$nomc.'</span><br><h3>'.$nom.'</h3>';


    	//datos producto
    	$conp = pg_query($dbconn, "select pre_clave_int,pre_costo,pre_venta from tbl_precios WHERE pro_clave_int = '".$id."'");
    	$datp = pg_fetch_row($conp);
    	$pre = $datp[0];
    	$cos = $datp[1];
    	$ven = $datp[2];

    	

    	if($ven>0)
    	{
    		$gan = (($ven - $cos)/$ven) * 100;
    	}
    	else
    	{
    		$gan = 0;
    	}
    	?>
    	<script>
    		$(document).ready(function() {
    			$('#titleright').html('<?php echo $title;?>')
    		});
    		
    	</script>
    	<div class="box box-default">
    		<div class="box-body text-center">
    			<?php
    			if($idm=="" and $idc=="")
    			{
    				//EDICION PRECIO BASE
    				?>
    				<div class="form-group" >
    					<label class="text-center text-light-blue">Precio Costo</label>
    					<div class="input-group">
		                <span class="input-group-addon">$</span>
		                <input type="text" class="form-control currency2" onkeypress="return validar_texto(event)" id="txtcosto" name="txtcosto" value="<?php echo $cos;?>" onchange="CRUDPRODUCTOS('GUARDARPRECIO','<?PHP echo $id;?>','','')">
		              	</div>
    				</div>	
    				<div class="form-group">
    					<label class="text-center text-light-blue">Precio Venta</label>
    					<div class="input-group">
		                <span class="input-group-addon">$</span>
		                <input type="text" class="form-control currency2"  onkeypress="return validar_texto(event)" id="txtventa" name="txtventa" value="<?php echo $ven;?>" onchange="CRUDPRODUCTOS('GUARDARPRECIO','<?PHP echo $id;?>','','')">
		              	</div>
    				</div>	
    				<div class="form-group">
    					<label class="text-center text-light-blue">Porcentaje Ganancia</label>
    					<div class="input-group">
		                
		                <input type="text" disabled class="form-control currency4" id="txtganancia" name="txtganancia" value="<?php echo $gan;?>">
		                <span class="input-group-addon">%</span>
		              	</div>
    				</div>	
    				<?PHP
    			}
    			else
    			{
    				//edición de precio mercado y categoria
					$conp2 = pg_query($dbconn, "select prm_clave_int,prm_venta,prm_costo from tbl_precios_mercado WHERE pro_clave_int = '".$id."' and mer_clave_int = '".$idm."' and cla_clave_int = '".$idc."'");
					$datp2 = pg_fetch_row($conp2);
					$prem = $datp2[0];					
					$venm = $datp2[1];
					$cosm = $datp2[2];

					//datos producto vs mercado		


					if($venm>0)
					{
						$gan2 = (($venm - $cosm)/$venm) * 100;
					}
					else
					{
						$gan2 = 0;
					}
    				?>
    				<div class="form-group" >
    					<label class="text-center text-light-blue" >Precio Costo</label>
    					<div class="input-group">
		                <span class="input-group-addon">$</span>
		                <input type="text"  class="form-control currency2" onkeypress="return validar_texto(event)" id="txtcosto" name="txtcosto" onchange="CRUDPRODUCTOS('GUARDARPRECIO2','<?PHP echo $id;?>','<?php echo $idm;?>','<?php echo $idc;?>')" value="<?php echo $cosm;?>">
		              	</div>
    				</div>	
    				<div class="form-group">
    					<label class="text-center text-light-blue">Precio Venta</label>
    					<div class="input-group">
		                <span class="input-group-addon">$</span>
		                <input type="text" class="form-control currency2"  onkeypress="return validar_texto(event)" id="txtventa" name="txtventa"  onchange="CRUDPRODUCTOS('GUARDARPRECIO2','<?PHP echo $id;?>','<?php echo $idm;?>','<?php echo $idc;?>')" value="<?php echo $venm;?>"> 
		              	</div>
    				</div>	
    				<div class="form-group">
    					<label class="text-center text-light-blue">Porcentaje Ganancia</label>
    					<div class="input-group">
		                
		                <input type="text" disabled class="form-control currency4" id="txtganancia" name="txtganancia" value="<?php echo $gan2;?>">
		                <span class="input-group-addon">%</span>
		              	</div>
    				</div>	
    				<?php
    			}
    			?>
    		</div>
    	</div>
    	<?php
    	echo "<script>INICIALIZARCONTENIDO();</script>";

    }
    else if($opcion=="GUARDARPRECIO")
    {
    	$id = $_POST['id'];
    	$cos = $_POST['cos'];if($cos=="" || $cos==NULL){ $cos = 0;}
    	$ven = $_POST['ven'];if($ven=="" || $ven==NULL){ $ven = 0;}
    	if($ven>0)
    	{
    		$gan = (($ven - $cos)/$ven) * 100;
    	}
    	else
    	{
    		$gan = 0;
    	}
    	$veri = pg_query($dbconn, "select * from tbl_precios where pro_clave_int = '".$id."'");
    	$num = pg_num_rows($veri);
    	if($num>0)
    	{
    		$sql = pg_query($dbconn, "UPDATE tbl_precios SET pre_costo = '".$cos."',pre_venta = '".$ven."',pre_ganancia = '".$gan."', pre_usu_actualiz = '".$usuario."',pre_fec_actualiz = '".$fecha."' where pro_clave_int = '".$id."'");
    	}
    	else
    	{
    		$sql = pg_query($dbconn, "INSERT INTO tbl_precios (pro_clave_int,pre_costo,pre_venta,pre_ganancia,pre_usu_actualiz,pre_fec_actualiz) VALUES('".$id."','".$cos."','".$ven."','".$gan."','".$usuario."','".$fecha."')");
    	}
    	if($sql>0)
    	{
    		$res = "ok";
    		//modificar los valores de porcentaje del mercado
    		$con2 = pg_query($dbconn,"select * from tbl_precios_mercado where pro_clave_int = '".$id."'");
    		$num2 = pg_num_rows($con2);
    		if($num2>0)
    		{
    			for($k = 0;$k<$num2; $k++)
    			{
    				$dat2 = pg_fetch_array($con2);
    				$ven2 = $dat2['prm_venta'];
    				$cla2 = $dat2['prm_clave_int'];
    				if($ven2>0)
			    	{
			    		$gan2 = (($ven2 - $cos)/$ven2) * 100;
			    	}
			    	else
			    	{
			    		$gan2 = 0;
			    	}
			    	//actualizar tabla con los nuevos datos de porcentaje
			    	$sql2 = pg_query($dbconn, "UPDATE tbl_precios_mercado SET prm_venta = '".$ven2."',prm_ganancia = '".$gan2."', prm_usu_actualiz = '".$usuario."',prm_fec_actualiz = '".$fecha."' where prm_clave_int = '".$cla2."'");
    			}
    		}

    		$msn = "Precio modificado correctamente";
    	}
    	else
    	{
    		$res = "error";
    		$msn = "Error BD(".pg_last_error($dbconn)."). No se modifico los precios. Verificar";
    	}
    	$datos[] = array("res"=>$res,"gan"=>$gan,"msn"=>$msn);
    	echo json_encode($datos);

    }
    else if($opcion=="GUARDARPRECIO2")
    {
    	$id = $_POST['id'];
    	$idm = $_POST['idm'];
    	$idc = $_POST['idc'];


    	$veri = pg_query($dbconn, "select * from tbl_precios where pro_clave_int = '".$id."'");
    	$num = pg_num_rows($veri);

    	if($num<=0)
    	{
    		$res = "error";
    		$msn = "Debe Ingresar los valores de costo";
    	}
    	else
    	{

    		$datp = pg_fetch_array($veri);
    		//$cos = $datp['pre_costo']; if($cos=="" || $cos==NULL){ $cos = 0;}
    		$cos = $_POST['cos']; 
    
	    	$ven = $_POST['ven'];if($ven=="" || $ven==NULL){ $ven = 0;}
	    	if($ven>0)
	    	{
	    		$gan = (($ven - $cos)/$ven) * 100;
	    	}
	    	else
	    	{
	    		$gan = 0;
	    	}

	    	$veri = pg_query($dbconn, "select * from tbl_precios_mercado where pro_clave_int = '".$id."' and mer_clave_int = '".$idm."' and cla_clave_int = '".$idc."'");
	    	$num = pg_num_rows($veri);
	    	if($num>0)
	    	{
	    		$sql = pg_query($dbconn, "UPDATE tbl_precios_mercado SET prm_costo = '".$cos."',prm_venta = '".$ven."',prm_ganancia = '".$gan."', prm_usu_actualiz = '".$usuario."',prm_fec_actualiz = '".$fecha."' where pro_clave_int = '".$id."'  and mer_clave_int = '".$idm."' and cla_clave_int = '".$idc."'");
	    	}
	    	else
	    	{
	    		$sql = pg_query($dbconn, "INSERT INTO tbl_precios_mercado (pro_clave_int,mer_clave_int,cla_clave_int,prm_venta,prm_ganancia,prm_usu_actualiz,prm_fec_actualiz,prm_costo) VALUES('".$id."','".$idm."','".$idc."','".$ven."','".$gan."','".$usuario."','".$fecha."','".$cos."')");
	    	}
	    	if($sql>0)
	    	{
	    		$res = "ok";
	    		$msn = "Precio venta mercado modificado correctamente";
	    	}
	    	else
	    	{
	    		$res = "error";
	    		$msn = "Error BD(".pg_last_error($dbconn)."). No se modifico el precio de venta del mercado. Verificar";
	    	}
	    }
    	$datos[] = array("res"=>$res,"gan"=>$gan,"msn"=>$msn);
    	echo json_encode($datos);

    }
    else if($opcion=="LISTAIMAGENES")
    {
    	$idp = $_POST['idp'];
    	$conp = pg_query($dbconn, "SELECT pri_clave_int,pri_imagen,pri_activo from tbl_productos_imagen where pro_clave_int = '".$idp."'");
    	?>
		<ul style="list-style: none">
			<li>
				<form id="form2" name="form2" action="_" method="post" enctype="multipart/form-data">
					Agregar nueva imagen:
					<input type="file" name="nuevaimagenproducto" id="nuevaimagenproducto" class="dropify" data-height="150" onChange="setpreview('rutanueva','nuevaimagenproducto','form2')" data-allowed-file-extensions="jpg png gif jpeg"   >
				<span id="rutanueva" style="display: none;"></span>&nbsp;<a class="btn btn-xs btn-success" onclick="CRUDPRODUCTOS('AGREGARIMAGENES','<?PHP echo $idp;?>');"><i class="fa fa-save"></i></a>
				</form>
				 <iframe src="about:blank" name="null" style="display:none"></iframe>
				
			</li>
			<li>Seleccione las imagenes que desea que se visualicen en el producto:</li>
			<?php

			while($dat = pg_fetch_array($conp))
			{
				$imp = $dat['pri_clave_int'];
				$uri = $dat['pri_imagen'];
				$act = $dat['pri_activo'];
			 	?>

				<li id="imgpro_<?php echo $imp;?>"><input   type="checkbox" name="imp<?php echo $imp;?>" id="imp<?php echo $imp;?>" <?php if($act==1){ echo "checked";}?> onclick="CRUDPRODUCTOS('ACTIVARIMAGEN','<?php echo $imp; ?>')" value="1" />
				<label class="labelimp" for="imp<?php echo $imp;?>"><img src="<?php echo $uri;?>" /></label>
				<a class="btn btn-danger btn-xs badge pull-right" onclick="CRUDPRODUCTOS('ELIMINARIMAGEN','<?php echo $imp; ?>')"><i class="fa fa-trash"></i></a>
				</li>

    		<?php
    		}
    		?>
    	</ul>
    	<script>
		var drEvent = $('#nuevaimagenproducto').dropify();

		drEvent.on('dropify.afterClear', function(event, element){
		   $('#rutanueva').html('')
		});
    		

    	</script>
    		<?php
    		echo "<script>INICIALIZARCONTENIDO();</script>";

    }
    else if($opcion=="ELIMINARIMAGEN")
    {
    	$id = $_POST['id'];
    	$seli = pg_query($dbconn,"SELECT pri_imagen FROM tbl_productos_imagen where pri_clave_int = '".$id."'");
    	$dat = pg_fetch_array($seli);
    	$img = $dat['pri_imagen'];


    	$del = pg_query($dbconn, "DELETE FROM tbl_productos_imagen  where pri_clave_int = '".$id."'");
    	if($del>0)
    	{
    		if(file_exists('../../'.$img))
    		{
    			unlink('../../'.$img);
    		}
    		$msn = "Imagen eliminada";
    		$res = "ok";
    	}
    	else
    	{
    		$res = "error";
    		$msn = "Error BD(".pg_last_error($dbconn)."). No se elimino la imagen";
    	}
    	$datos[] = array("res"=>$res,"msn"=>$msn);
    	echo json_encode($datos);
    }
    else if($opcion=="ELIMINARPRODUCTO")
    {
    	$id = $_POST['id'];
    	$del = pg_query($dbconn, "UPDATE tbl_productos SET pro_activo = 2,pro_usu_actualiz = '".$usuario."',pro_fec_actualiz = '".$fecha."' where pro_clave_int = '".$id."'");
    	if($del>0)
    	{
    		$msn = "Producto eliminado";
    		$res = "ok";
    	}
    	else
    	{
    		$res = "error";
    		$msn = "Error BD. No se elimino el producto";
    	}
    	$datos[] = array("res"=>$res,"msn"=>$msn);
    	echo json_encode($datos);
    }
    else if($opcion=="AGREGARIMAGENES")
    {
    	$idp = $_POST['id'];
    	$ruta = $_POST['ruta'];    	
    	$trozos = explode(".", $ruta); 
		$rutaold = "../../".$ruta;
		$extension = end($trozos);

		if($_POST['ruta']!="")
		{
			$inserimagenes = pg_query($dbconn,"INSERT INTO tbl_productos_imagen (pro_clave_int,pri_usu_actualiz,pri_fec_actualiz) VALUES('".$idp."','".$usuario."','".$fecha."')");
			if($inserimagenes>0)
			{
				$uid = pg_fetch_row(pg_query($dbconn,"SELECT setval('pri_id_seq',nextval('pri_id_seq')-1) as id;"));
				$idip = $uid[0];
				//$idip = pg_last_oid($inserimagenes);
				
				//SI EXISTE LA CARPETA DE LAS IMAGENES PARA PRODUCTOS SE MUEVE LA IMAGEN
				
				$rutan = "../../modulos/productos/imagenes/COD".$idp."/".$idip.".".$extension;
				if(rename($rutaold,$rutan) and $_POST['ruta']!="")
				{
					$rutanew = "modulos/productos/imagenes/COD".$idp."/".$idip.".".$extension;	
					$update = pg_query($dbconn,"update tbl_productos_imagen set pri_imagen='".$rutanew."' where pri_clave_int='".$idip."'");
					$res = "ok";
					$msn = "Imagen agregada correctamente";
					unlink($rutalold);
					
				}
				else
				{
					$delete = pg_query($dbconn,"DELETE FROM  tbl_productos_imagen WHERE pri_clave_int = '".$idip."'");

					$res = "error";
					$msn = "La imagen no fue movida a la carpeta del productos. Verificar";
				}
				
			}
		}	
		else
		{
			$res = "error";
			$msn = "No hay una imagen seleccionada para cargar. Verificar";
		}
		$datos[] = array('res' => $res , 'msn'=> $msn );
		echo json_encode($datos);
    }
    else if($opcion=="ACTIVARIMAGEN")
    {
    	$idip = $_POST['idip'];
    	$act = $_POST['act']; if($act<=0){ $act = 0;}
    	$update = pg_query($dbconn,"update tbl_productos_imagen set pri_activo='".$act."',pri_usu_actualiz = '".$usuario."',pri_fec_actualiz = '".$fecha."' where pri_clave_int='".$idip."'");
    	if($update>0)
    	{
    		$res = "ok";
    		$msn = "Estado imagen modificado correctamente";
    	}
    	else
    	{
    		$res = "error";
    		$msn = "Surgió un error al modificar estado de la imagen. Verificar";
    	}
    	$datos[] =  array('res' =>$res , "msn"=>$msn );
    	echo json_encode($datos);
    }
    else if($opcion=="ZOOMPRODUCTOS")
    {
    	$id = $_POST['id'];
    	$con = pg_query($dbconn, "select pro_codigo,pro_nombre,cat_nombre,pro_descripcion from tbl_productos p JOIN tbl_categorias c on c.cat_clave_int = p.cat_clave_int where pro_clave_int = '".$id."'");
    	$dat = pg_fetch_row($con);
    	$cod = $dat[0];
    	$nom = $dat[1];
    	$nomcat = $dat[2];
    	$desc = $dat[3];

    	$con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$id."' LIMIT 1");
    	$dat = pg_fetch_row($con);
    	$cimg = $dat[0];
    	$img = $dat[1];
    	?>
   	<div class="row">
        <div class="col-md-3">

          <!-- Profile Image -->
          <div class="box box-primary">
            <div class="box-body box-profile">
              <img class="profile-user-img img-responsive img-circle" src="<?php echo $img;?>" alt="Producto Picture">

              <h3 class="profile-username text-center"><?php echo $nom; ?></h3>

              <p class="text-muted text-center"><?php echo $nomcat;?></p>

              <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                  <b>Unidad:</b> <a class="pull-right">1,322</a>
                </li>
                <li class="list-group-item">
                  <b>Estado:</b> <a class="pull-right">543</a>
                </li>
                <li class="list-group-item">
                  <b>Mostrar Agotado</b> <a class="pull-right">13,287</a>
                </li>
              </ul>

              
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

          <!-- About Me Box -->
          <div class="box box-primary" style="display: none;">
          	
          
            <div class="box-header with-border">
              <h3 class="box-title">About Me</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <strong><i class="fa fa-book margin-r-5"></i> Education</strong>

              <p class="text-muted">
                B.S. in Computer Science from the University of Tennessee at Knoxville
              </p>

              <hr>

              <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>

              <p class="text-muted">Malibu, California</p>

              <hr>

              <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>

              <p>
                <span class="label label-danger">UI Design</span>
                <span class="label label-success">Coding</span>
                <span class="label label-info">Javascript</span>
                <span class="label label-warning">PHP</span>
                <span class="label label-primary">Node.js</span>
              </p>

              <hr>

              <strong><i class="fa fa-file-text-o margin-r-5"></i> Notes</strong>

              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fermentum enim neque.</p>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <div class="col-md-9">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
            	<li class="active"><a href="#masinfo" data-toggle="tab">Mas info </a></li>
              <li><a href="#listaprecios" data-toggle="tab">Lista Precio</a></li>
              <li><a href="#entradas" data-toggle="tab">Entradas</a></li>
              <li><a href="#salidas" data-toggle="tab">Salidas</a></li>
            </ul>
            <div class="tab-content">
        	  <div class="active tab-pane" id="masinfo">
        	  	<div class="post">
        	  		<div class="user-block">
                    <img class="img-circle img-bordered-sm" src="<?php echo $img;?>" alt="User Image">
                        <span class="username">
                          <a href="#"><?php echo $nomp;?></a>
                          <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                        </span>
                    <span class="description"><?php echo $numi;?> Fotos</span>
                  </div>
                   <p>
                    <?php echo $desc;?>
                  </p>
                  <div class="row margin-bottom">
                    <div class="col-sm-6">
                      <img class="img-responsive" src="<?php echo $img;?>" alt="Photo">
                    </div>
                    <!-- /.col -->
                    <div class="col-sm-6">
                      <div class="row">
                        <div class="col-sm-6">
                          <img class="img-responsive" src="<?php echo $img;?>" alt="Photo">
                          <br>
                          <img class="img-responsive" src="<?php echo $img;?>" alt="Photo">
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-6">
                          <img class="img-responsive" src="<?php echo $img;?>" alt="Photo">
                          <br>
                          <img class="img-responsive" src="<?php echo $img;?>" alt="Photo">
                        </div>
                        <!-- /.col -->
                      </div>
                      <!-- /.row -->
                    </div>
                    <!-- /.col -->
                  </div>
        	  		
        	  	</div>
        	  </div>
              <div class="tab-pane" id="listaprecios">
                <!-- Post -->
                <div class="post">
                 
                  <!-- /.user-block -->
                  <?PHP
		$con=  pg_query($dbconn, "SELECT * FROM tbl_mercado ");
		$con2 = pg_query($dbconn,"SELECT * FROM tbl_clasificacion");
		$num2 = pg_num_rows($con2);
		$cla = array();
    	//datos producto
    	$conp = pg_query($dbconn, "select pre_clave_int,pre_costo,pre_venta from tbl_precios WHERE pro_clave_int = '".$id."'");
    	$datp = pg_fetch_row($conp);
    	$pre = $datp[0];
    	$cos = $datp[1];
    	$ven = $datp[2];
    	if($ven>0)
    	{
    		$gan = (($ven - $cos)/$ven) * 100;
    	}
    	else
    	{
    		$gan = 0;
    	}

    	for($nc=0;$nc<$num2;$nc++)
    	{
    		$dat2 = pg_fetch_array($con2);
    		$cla[$nc] = array("idc"=>$dat2['cla_clave_int'],"nomc"=>$dat2['cla_nombre']);
    		echo $cla["nomc"][$nc];
    	}
    	?>
    	<table class="table table-bordered compact">
    		<caption></caption>
    		<thead>
    			<tr>
    				<th style="display: none">Costo</th>
    				<?php 
    				$merc = array();
    				while($dat = pg_fetch_array($con)){
    					$merc[] = $dat['mer_clave_int'];
    					$mer = $dat['mer_nombre'];
    					?>
    					<th><?php echo $mer;?></th>
    					<?php
    				}
    				?>
    			</tr>
    		</thead>
    		<tbody>
    			<tr>
    				<td style="display: none;"><span class="currency" style="display: none"><?php echo $cos;?></span><br>
    				<span class="currency3" style="display: none"><?php echo $gan;?></span><br>
    				<span class="currency"><?php echo $ven;?></span></td>
    				<?php 
    				$i = 0;
    				while($i < count($merc))
    				{
    			
    					?>
    					<td>
    						<dl>
    							<?php 
    							for($nc=0;$nc<count($cla);$nc++)
    							{
									$conp2 = pg_query($dbconn, "select prm_clave_int,prm_venta from tbl_precios_mercado WHERE pro_clave_int = '".$id."' and mer_clave_int = '".$merc[$i]."' and cla_clave_int = '".$cla[$nc]['idc']."'");
									$datp2 = pg_fetch_row($conp2);
									$prem = $datp2[0];					
									$venm = $datp2[1];
									if($venm=="" || $venm==NULL){$venm = 0;}
									//datos producto vs mercado		
									if($venm>0)
									{
										$gan2 = (($venm - $cos)/$venm) * 100;
									}
									else
									{
										$gan2 = 0;
									}
    								
    								?>
    							<dt style="cursor: pointer;"><?php echo $cla[$nc]["nomc"];?></dt>
    							<dd><span class="currency3" style="display: none"><?php echo $gan2;?></span></dd>
    							<dd><span class="currency"><?php echo $venm;?></span></dd>
    							<?php
    							}
    							?>
    						</dl>

    					</td>
    					<?php
    					$i++;
    				}
    				?>

    			</tr>
    		</tbody>
    	</table>
                
                  

                  
                </div>
                <!-- post -->                
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="entradas">
                <!-- The timeline -->
                <ul class="timeline timeline-inverse">
                  <!-- timeline time label -->
                  <li class="time-label">
                        <span class="bg-red">
                          10 Feb. 2014
                        </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-envelope bg-blue"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

                      <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>

                      <div class="timeline-body">
                        Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                        weebly ning heekya handango imeem plugg dopplr jibjab, movity
                        jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                        quora plaxo ideeli hulu weebly balihoo...
                      </div>
                      <div class="timeline-footer">
                        <a class="btn btn-primary btn-xs">Read more</a>
                        <a class="btn btn-danger btn-xs">Delete</a>
                      </div>
                    </div>
                  </li>
                  <!-- END timeline item -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-user bg-aqua"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>

                      <h3 class="timeline-header no-border"><a href="#">Sarah Young</a> accepted your friend request
                      </h3>
                    </div>
                  </li>
                  <!-- END timeline item -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-comments bg-yellow"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>

                      <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>

                      <div class="timeline-body">
                        Take me to your leader!
                        Switzerland is small and neutral!
                        We are more like Germany, ambitious and misunderstood!
                      </div>
                      <div class="timeline-footer">
                        <a class="btn btn-warning btn-flat btn-xs">View comment</a>
                      </div>
                    </div>
                  </li>
                  <!-- END timeline item -->
                  <!-- timeline time label -->
                  <li class="time-label">
                        <span class="bg-green">
                          3 Jan. 2014
                        </span>
                  </li>
                  <!-- /.timeline-label -->
                  <!-- timeline item -->
                  <li>
                    <i class="fa fa-camera bg-purple"></i>

                    <div class="timeline-item">
                      <span class="time"><i class="fa fa-clock-o"></i> 2 days ago</span>

                      <h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos</h3>

                      <div class="timeline-body">
                        <img src="http://placehold.it/150x100" alt="..." class="margin">
                        <img src="http://placehold.it/150x100" alt="..." class="margin">
                        <img src="http://placehold.it/150x100" alt="..." class="margin">
                        <img src="http://placehold.it/150x100" alt="..." class="margin">
                      </div>
                    </div>
                  </li>
                  <!-- END timeline item -->
                  <li>
                    <i class="fa fa-clock-o bg-gray"></i>
                  </li>
                </ul>
              </div>
              <!-- /.tab-pane -->

              <div class="tab-pane" id="salidas">
                <form class="form-horizontal">
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Name</label>

                    <div class="col-sm-10">
                      <input type="email" class="form-control" id="inputName" placeholder="Name">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                    <div class="col-sm-10">
                      <input type="email" class="form-control" id="inputEmail" placeholder="Email">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Name</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" placeholder="Name">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputExperience" class="col-sm-2 control-label">Experience</label>

                    <div class="col-sm-10">
                      <textarea class="form-control" id="inputExperience" placeholder="Experience"></textarea>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputSkills" class="col-sm-2 control-label">Skills</label>

                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputSkills" placeholder="Skills">
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-danger">Submit</button>
                    </div>
                  </div>
                </form>
              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- /.nav-tabs-custom -->
        </div>

    </div>
    	<?php
    	echo "<script>INICIALIZARCONTENIDO();</script>";
    }
	else if($opcion=="REFRESHPRODUCTOS")
	{
			$concate = pg_query($dbconn,"select cat_clave_int,cat_nombre from tbl_categorias where cat_activo = 1");
			$numcate = pg_num_rows($concate);
			if($numcate>0)
			{
				for($nc=0;$nc<$numcate;$nc++)
				{
					$datc = pg_fetch_array($concate);

					$datos[] = array("id"=>"","literal"=>$datc['cat_nombre'], "tipo"=>"grupo","img"=>"", "res"=>"si");
				
					$conpro = pg_query($dbconn,"select pro_clave_int,pro_nombre from tbl_productos where cat_clave_int = '".$datc['cat_clave_int']."' and pro_activo!=2 order by LOWER (pro_nombre) ASC");
					$numpro = pg_num_rows($conpro);

					for($np=0;$np<$numpro;$np++)
					{
						$datp = pg_fetch_array($conpro);
						$idp = $datp['pro_clave_int'];
						$nomp = $datp['pro_nombre'];
						$con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idp."' and pri_activo = 1 LIMIT 1");
						$dat = pg_fetch_row($con);
						$cimg = $dat[0];
						$img = $dat[1];
						if($img=="" || $img==NULL)
						{
							$img= "dist/img/nofoto.png";

						}
						else
						{
							$img = $urlweb.$img;
						}
						$imagen = '<img  src="'.$img.'" alt="message user image">';
						$datos[] = array("id"=>$idp,"literal"=>$nomp, "tipo"=>"opc","img"=>$img, "res"=>"si");
					}			
				}
			}
			else
			{
				$datos[] = array("id"=>"","literal"=>"", "tipo"=>"","img"=>"", "res"=>"no");
			}
		
			echo json_encode($datos);
	}   
	else if($opcion=="MODIFICARESTADO")
	{
		$idp = $_POST['idp'];
		$estp = $_POST['estp'];
		$upd = pg_query($dbconn, "UPDATE tbl_productos SET pro_activo = '".$estp."', pro_usu_actualiz = '".$usuario."',pro_fec_actualiz = '".$fecha."' WHERE pro_clave_int = '".$idp."'");
		if($upd>0)
		{
			$res = "ok";
			$msn = "Estado del producto modificado correctamente";

		}
		else
		{
			$res = "error";
			$msn = "Surgió un error al modificado el estado del producto. Error BD (".pg_last_error($dbconn).") ";

		}
		$datos[]  = array("res"=>$res, "msn" => $msn);
		echo json_encode($datos);
	} 

?>