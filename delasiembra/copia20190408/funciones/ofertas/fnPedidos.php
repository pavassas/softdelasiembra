<?php
include('../../data/Conexion.php');
session_start();
error_reporting(0);
$IP = $_SERVER['REMOTE_ADDR'];
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_SESSION["idusuario"];
$identificacion = $_COOKIE["usIdentificacion"];
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
$conusu= pg_query($dbconn,"SELECT mer_clave_int,dir_clave_int,usu_ult_telefono,prf_clave_int from tbl_usuario where usu_clave_int = '".$idUsuario."'");
$datusu = pg_fetch_array($conusu);
$idmercado = $datusu['mer_clave_int'];
$ultimadireccion = $datusu['dir_clave_int'];
$ultimotelefono = $datusu['usu_ult_telefono'];
$perfil = $datusu['prf_clave_int'];
if($idUsuario<=0)
{
  $idmercado = 3;
}
//pedidoactivo
$conped = pg_query($dbconn, "SELECT ped_clave_int FROM tbl_pedidos WHERE usu_clave_int = '".$idUsuario."' and ped_estado = 0");
$numped = pg_num_rows($conped);
$datped = pg_fetch_array($conped);
$idpedido = $datped['ped_clave_int'];

//datos domicilio
//COSTO DOMICILIO y TIEMPO DE ENTREGA
$condom = pg_query($dbconn,"select sec_domicilio,sec_horas,sec_minutos from tbl_sector s join tbl_barrio b on b.sec_clave_int = s.sec_clave_int join tbl_direcciones d on d.bar_clave_int = b.bar_clave_int where d.usu_clave_int = '".$idUsuario."' and d.dir_clave_int = '".$ultimadireccion."'");
$datdom = pg_fetch_array($condom);
$domicilio = $datdom['sec_domicilio']; if($domicilio<=0){ $domicilio = 0;}

$conmer = pg_query($dbconn, "SELECT mer_tamano,mer_clasificacion FROM tbl_mercado WHERE mer_clave_int = '".$idmercado."'");
$datmer = pg_fetch_array($conmer);
$aplicatamano = $datmer['mer_tamano'];
$clasificacion = $datmer['mer_clasificacion'];
$arrayclasificacion = explode(",", $clasificacion);
 
$opcion = $_POST['opcion'];
if($opcion=="LISTAPRODUCTOS")
{
	//$conped = pg_query($dbconn, "SELECT ped_clave_int FROM tbl_pedidos WHERE usu_clave_int = '".$idUsuario."' and ped_estado = 0");
	//$numped = pg_num_rows($conped);
	//$datped = pg_fetch_array($conped);
	//$idpedido = $datped['ped_clave_int'];

    $nom = $_POST['nom'];
	$cod = $_POST['cod'];
	$cat = $_POST['cat']; $cat = implode(', ', (array)$cat); if($cat==""){$cat1="'0'";}else {$cat1=$cat;}
	$mar = $_POST['mar'];
	//$uni = $_POST['uni']; $uni = implode(', ', (array)$uni); if($uni==""){$uni1="'0'";}else {$uni1=$uni;}

	$pro = $_POST['pro']; $pro = implode(', ', (array)$pro); if($pro==""){$pro1="'0'";}else {$pro1=$pro;}

	$idc= $_POST['idc'];
	$wh = "";
	if($idc!="")
	{
		$wh =" and p.cat_clave_int = '".$idc."'";
	}
	$sql = "SELECT pro_clave_int,pro_nombre,pro_descripcionbreve,pro_uni_venta,pro_pes_venta,pro_mu_venta,pro_mp_venta,pro_estado FROM tbl_productos AS p JOIN tbl_categorias c ON c.cat_clave_int = p.cat_clave_int ";
	$sql.= " WHERE ( p.pro_nombre ILIKE REPLACE('".$nom."%',' ','%') OR p.pro_palabra ILIKE REPLACE('".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '' ) and ( p.pro_codigo ILIKE REPLACE('".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '' ) and ( p.pro_marca ILIKE REPLACE('".$mar."%',' ','%') OR '".$mar."' IS NULL OR '".$mar."' = '' ) and ( p.cat_clave_int IN(".$cat1.") OR '".$cat."' IS NULL OR '".$cat."' = '')  and ( p.pro_clave_int IN(".$pro1.") OR '".$pro."' IS NULL OR '".$pro."' = '') and pro_activo =1  and (pro_uni_venta = 1 or pro_pes_venta = 1) ".$wh." order by pro_nombre ASC";//and ( p.pro_uni_compra IN(".$uni1.") OR '".$uni."' IS NULL OR '".$uni."' = '') and ( p.pro_uni_consumo IN(".$uni1.") OR '".$uni."' IS NULL OR '".$uni."' = '')


	$conpro = pg_query($dbconn,$sql);
	$numpro = pg_num_rows($conpro);

	?>
	
			
					
					<ul class="col-md-12" id="divlistaproductos" style="margin:10px 0px !important">
						<?php
						while($datp = pg_fetch_array($conpro))
						{
							$idp = $datp['pro_clave_int'];
							$nomp = $datp['pro_nombre'];
							$descp = $datp['pro_descripcionbreve'];
							$puv = $datp['pro_uni_venta'];
							$ppv = $datp['pro_pes_venta'];
							$pmuv = $datp['pro_mu_venta'];
							$pmpv = $datp['pro_mp_venta'];
							$mad = $datp['pro_estado'];

							if($puv==1)
							{
								$unidadmin = $pmuv;
								$dataunidad = 0;//por unidad
							}
							else if($ppv==1)
							{
								$unidadmin = $pmpv;
								$dataunidad = 1; //venta por peso
							}

							$con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idp."' and pri_activo = 1 LIMIT 1");
							$dat = pg_fetch_row($con);
							$cimg = $dat[0];
							$img = $dat[1];

							$conpre = pg_query($dbconn, "select prm_venta from tbl_precios_mercado where pro_clave_int ='".$idp."' and cla_clave_int in  (SELECT cla_clave_int FROM tbl_clasificacion where cla_clave_int in(".$clasificacion.") ORDER BY cla_clave_int ASC LIMIT 1) and mer_clave_int = '".$idmercado."'");
							$datpre = pg_fetch_array($conpre);
							$venta = $datpre['prm_venta'];
							if($venta<=0){ $venta = 0;}

							//VEIRIFCAR SI PRODUCTO ESTA EN EL PEDIDO ACTUAL EN PROCESO
							$conpedido = pg_query($dbconn,"select * from tbl_pedidos_detalle where ped_clave_int = '".$idpedido."' and pro_clave_int = '".$idp."'");
							$numpedido = pg_num_rows($conpedido);


							//informacion de la oferta
							$conoferta = pg_query($dbconn, "SELECT o.ofe_clave_int,ofe_titulo,ofe_descripcion,ofe_inicio,ofe_fin,ofe_tipo_descuento,ofe_descuento,ofe_estado,ofe_imagen FROM tbl_ofertas o join tbl_ofertas_producto op ON op.ofe_clave_int = o.ofe_clave_int   where o.ofe_estado = 1 and op.pro_clave_int = '".$idp."' order by ofe_inicio ASC limit 1");
							$numoferta = pg_num_rows($conoferta);
							if($numoferta>0)
							{
								$datoferta = pg_fetch_array($conoferta);
								$tipo = $datoferta['ofe_tipo_descuento'];
								$descuento = $datoferta['ofe_descuento'];
								if($tipo==1)
								{
									$ventaold = $venta;
									$ventaoferta = $descuento;
									$ahorrooferta = $venta - $ventaoferta;
									$descuentooferta = 100 - ($ventaoferta*100/$venta);
								}
								else if($tipo==2)
								{
									$ventaold = $venta;
									$ventaoferta = $venta - ($venta*$descuento/100);
									$ahorrooferta = $venta - $ventaoferta;
									$descuentooferta = $descueto;
								}
							}
							else
							{
								$ventaold  = 0;
								$ventaoferta = $venta;
							}
							//sc-product-item
			                ?>
			                  <li class="col-md-3 col-sm-6 col-lg-2 data-row  thumbnail <?php if($numpedido>0){ echo ' sc-added-item';} ?>" id="producto-item-<?php echo $idp;?>">
			                  	<figure>
								
								<a href="" class="pixelProducto">
									
									<center>
										<ul class="caption-style-4 ">
											<li><!--data-name="product_image"-->
											<img class="img-responsive" src="<?php echo $img;?>" alt="img">
											<div class="caption">
											<div class="blur"></div>
											<div class="caption-text">
											<h1 data-toggle="modal" data-target="#modalinfo" onclick="CRUDPEDIDOS('INFOPRODUCTO','<?PHP echo $idp;?>')"><i class="fa fa-plus fa-2x"></i></h1>
											<p><?php echo $descp;?></p>
											</div>
											</div>
											</li>
										</ul>
									
									</center>

								</a>

							</figure>
								<div class="col-md-12 col-xs-12">
									<h4>
									<small class="pricesmalloferta currency2 <?php if($numoferta<=0){ echo 'hide';}?>" ><?php echo $ventaold;?></small>
									<!--id="precio_<?php echo $idp;?>" data-name="product_name"-->
									<small style="display: inline-block;" class="pricesmall currency2" ><?php echo $ventaoferta;?></small><br>
									<span ><?php echo $nomp;?></span></h4>

									<p style="display: none" data-name="product_desc"><?php echo $descp;?></p>
								</div>
					            <div class="" data-idp="<?php echo $idp;?>">
									

									
					           <div class="caption">
					                       
					            <div class="row">
									<div class="col-md-1"></div>
					               	<div class="col-md-8 col-xs-8">
										<div class="input-group pull-right">
											<span class="input-group-btn">
												<button type="button" class="btn btn-default btn-number btn-xs" disabled="disabled" data-type="minus" data-field="product_quantity_<?php echo $idp;?>">
												<span class="glyphicon glyphicon-minus"></span>
												</button>
											</span>
											<div class="has-feedback">
											<input type="text" id="product_quantity_<?php echo $idp;?>" name="product_quantity" class="form-control input-number input-xs sc-cart-item-qty " step="<?php echo $unidadmin?>" value="<?php echo $unidadmin;?>" min="<?php echo $unidadmin;?>" max="9999999" data-unidad="<?php echo $datunidad; ?>" >
											<span class="form-control-feedback" id="unidadproducto<?php echo $idp;?>"><?php if($dataunidad==1){ echo "GR"; }else{ echo "UND";}?></span>
											</div>
										
											<span class="input-group-btn">
												<button type="button" class="btn btn-default btn-number btn-xs" data-type="plus" data-field="product_quantity_<?php echo $idp;?>">
												<span class="glyphicon glyphicon-plus"></span>									
												</button>
											</span>
										</div>									
						               
					                </div>
					                <div class="col-md-2 col-xs-2">
					                	<?php if($idUsuario>0)
						                	{
						                		?>
						                    <button class="btn btn-success btn-sm pull-right  btn-xs"  data-toggle="modal" data-target="#modalinfo" onclick="CRUDPEDIDOS('INFOPRODUCTO','<?PHP echo $idp;?>')"><i class="fa fa-opencart" title="Añadir al carro"></i></button>
						                    <?php
						                	}
						                	else
						                	{
						                			?>
						                    <button type="button" class="sc-logout btn btn-success btn-sm pull-right  btn-xs"><i class="fa fa-opencart" title="Añadir al carro"></i></button>
						                    <?php
						                	}
						                	?>
					                </div>
	                </li>

	                <!--
			      	 <div class="col-md-3 col-sm-6 col-lg-3 data-row "  >
			      	 	 <div class="sc-product-item  <?php if($numpedido>0){ echo ' sc-added-item';} ?>" data-idp="<?php echo $idp;?>">
			       <figure class="effect-terry ">
						<img data-name="product_image" src="<?php echo $img;?>" alt="img" style="width: 100%; height: 150px">
						<div class="cssdescuento <?php if($numoferta<=0){ echo 'hide';}?>" ><small class="currency3"><?php echo $descuentooferta;?></small>OFF</div>
						<figcaption>
							<h2>
								<small  style="display: inline-block;" class="pricesmalloferta currency2 <?php if($numoferta<=0){ echo 'hide';}?>" ><?php echo $ventaold;?></small>
<small id="precio_<?php echo $idp;?>" style="display: inline-block;" class="pricesmall currency2" ><?php echo $ventaoferta;?></small><br>
								<span data-name="product_name"><?php echo $nomp;?></span></h2>
							<p style="display: none" data-name="product_desc"><?php echo $descp;?></p>
							<p class="mr">
								<a data-toggle="modal" data-target="#modalinfo"  onclick="CRUDPEDIDOS('INFOPRODUCTO','<?PHP echo $idp;?>')"><i class="fa fa-eye fa-2x"></i></a>
								<a href="#"><i class="fa fa-fw fa-2x"></i></a>
								<a href="#"><i class="fa fa-fw fa-2x"></i></a>
								
							</p>
							<div class="mr2">	
								<div class="toggle_radio <?php if($aplicatamano==1){ }else{ echo ' invisible'; } ?>">
										<input data-name="product_tam" name="product_tamano_<?php echo $idp;?>" type="radio" class="toggle_option grande_toggle" id="tamano1_<?php echo $idp;?>" name="toggle_option" value="Grande">
										<input data-name="product_tam" name="product_tamano_<?php echo $idp;?>" type="radio"  class="toggle_option mediano_toggle" id="tamano2_<?php echo $idp;?>" name="toggle_option" value="Mediano">
										<input data-name="product_tam" name="product_tamano_<?php echo $idp;?>" type="radio" class="toggle_option pequeno_toggle" id="tamano3_<?php echo $idp;?>" name="toggle_option" value="Pequeño">
										<label for="tamano1_<?php echo $idp;?>"><p>Grande</p></label>
										<label for="tamano2_<?php echo $idp;?>"><p>Mediano</p></label>
										<label for="tamano3_<?php echo $idp;?>"><p>Pequeño</p></label>
										<div class="toggle_option_slider">
										</div>
								</div>
								<div class="toggle_radio <?php if($mad==0){ echo "invisible";}?>">
									<input data-name="product_est" name="product_estado_<?php echo $idp;?>" type="radio" class="toggle_option first_toggle" id="estado1_<?php echo $idp;?>" name="toggle_option" value="Verde">
									<input data-name="product_est" name="product_estado_<?php echo $idp;?>" type="radio" checked class="toggle_option second_toggle" id="estado2_<?php echo $idp;?>" name="toggle_option" value="Pinton">
									<input data-name="product_est" name="product_estado_<?php echo $idp;?>" type="radio" class="toggle_option third_toggle" id="estado3_<?php echo $idp;?>" name="toggle_option" value="Maduro">
									<label for="estado1_<?php echo $idp;?>"><p>Verde</p></label>
									<label for="estado2_<?php echo $idp;?>"><p>Pintón</p></label>
									<label for="estado3_<?php echo $idp;?>"><p>Maduro</p></label>
										<div class="toggle_option_slider">
										</div>
								</div>	
								<div class="nav-tabs-custom sel-unidad <?php if(($puv==1 and $ppv==0) || ($ppv==1 and $puv==0)){ echo " invisible"; } ?>" style="margin:0px; background-color: transparent;">
											<ul class="nav nav-tabs pull-right">    
												<li class="active unidad" ><a  data-toggle="tab" aria-expanded="true" onclick="ckradio('product_unidad_<?php echo $idp;?>','0')" style="padding: 0px !important"><label>Unidad</label>
											<input style="display: none;" data-name="product_uni" name="product_unidad_<?php echo $idp;?>" type="radio" id="unidad1_<?php echo $idp;?>" value="0" <?php if($dataunidad==0){ echo "checked";}?> data-uni-min = '<?php echo $pmuv;?>' data-text-min="UND" data-idp="<?php echo $idp;?>">
											</a>
												</li>
												<li class="peso" onclick="ckradio('product_unidad_<?php echo $idp;?>','1')">
												<a data-toggle="tab" aria-expanded="false" style="padding: 0px  !important"><label>Peso</label>
													<input style="display: none;" data-name="product_uni" name="product_unidad_<?php echo $idp;?>" type="radio" id="unidad2_<?php echo $idp;?>" value="1" <?php if($dataunidad==1){ echo "checked";}?> data-uni-min = '<?php echo $pmpv;?>' data-text-min="GR" data-idp="<?php echo $idp;?>">

												</a></li>
												<li>
													<select name="product_clasificacion" id="product_clasificacion_<?php echo $idp;?>" class="form-control input-sm <?php if(count($arrayclasificacion)<=1){ echo ' invisible';}?>" onchange="CRUDPEDIDOS('CAMBIARPRECIO','<?php echo $idp; ?>',this.value)">
                                    	<?php
                                    	
                                    		for($c=0;$c<count($arrayclasificacion);$c++)
                                    		{ 
                                    			$concla = pg_query($dbconn,"SELECT * FROM tbl_clasificacion where cla_clave_int = '".$arrayclasificacion[$c]."'");
                                    			$datc = pg_fetch_array($concla)
                                    			?>
	                                        	<option value="<?php echo $arrayclasificacion[$c];?>"><?php echo $datc['cla_nombre'];?></option>	
	                                        <?php 
	                                    	}
	                                    ?>
	                                    </select>
												</li>

											</ul>
										</div>
										<div class="input-group pull-right" style="width: 70%" >
										<span class="input-group-btn">
										<button type="button" class="btn btn-default btn-number btn-xs" disabled="disabled" data-type="minus" data-field="product_quantity_<?php echo $idp;?>">
										<span class="glyphicon glyphicon-minus"></span>
										</button>
										</span>
										<div class="has-feedback">
										<input type="text" id="product_quantity_<?php echo $idp;?>" name="product_quantity" class="form-control input-number input-xs sc-cart-item-qty" step="<?php echo $unidadmin?>" value="<?php echo $unidadmin;?>" min="<?php echo $unidadmin;?>" max="9999999" data-unidad="<?php echo $datunidad; ?>">
										<span class="form-control-feedback" id="unidadproducto<?php echo $idp;?>"><?php if($dataunidad==1){ echo "GR"; }else{ echo "UND";}?></span>
										</div>
										
										<span class="input-group-btn">
										<button type="button" class="btn btn-default btn-number btn-xs" data-type="plus" data-field="product_quantity_<?php echo $idp;?>">
										<span class="glyphicon glyphicon-plus"></span>									
										</button>

										</span>
										</div>							
								<?php if($idUsuario>0)
								{
								?>
								<button class="sc-add-to-cart btn btn-success btn-xs pull-right">Añadir</button>
								<?php
								}
								else
								{
								?>
								<button type="button" class="sc-logout btn btn-success btn-xs pull-right">Añadir</button>
								<?php
								}
								?>
							</div>
							 <input name="product_price" id="product_price_<?php echo $idp;?>" value="<?php echo $ventaoferta;?>" type="hidden" />
	                                <input class="sc-product-id" name="product_id" value="<?php echo $idp;?>" type="hidden" />
							</p>
						</figcaption>			
					</figure>
					</div>
				</div>
-->
			              
                    <?php
                	}
                	?>
					</div>
					<div class="col-md-12 paging-container tablePagingLista"> </div>
				
		
    	<script type="text/javascript">
	$(document).ready(function(){
		load = function() 
		{
				window.tp = new Pagination('.tablePagingLista', {
					itemsCount: <?php echo $numpro;?>,
					onPageSizeChange: function (ps) {
						console.log('changed to ' + ps);
					},
					onPageChange: function (paging) {
						//custom paging logic here
						console.log(paging);
						var start = paging.pageSize * (paging.currentPage - 1),
							end = start + paging.pageSize,
							$rows = $('#divlistaproductos').find('.data-row');

						$rows.hide();

						for (var i = start; i < end; i++) {
							$rows.eq(i).show();
						}
					}
				});


			}

		load();
	    // Initialize Smart Cart  
	    $(".sc-add-to-cart").unbind();
		
	    var ini = $('#inismart').val();
	    if(ini==1)
	    {
	    	
	    }	
	    else
	    {
	    	$('#inismart').val(1);
	    	//$('#smartcart').smartCart({submitSettings:{ submitType: 'ajax' }});
	    }


	    //FUNCION AÑADIR AL CARRITO
	    $('.sc-logout').on('click',function(e){
	    	e.preventDefault();
	    	$('#titlecustom').html("Hola, usuario");
	    	$('#modalcustom').modal('show');
	    	$('#contenidomodalcustom').html("<h3 style='margin:0px'>Debes iniciar sesión primero</h3><a class='btn btn-success btn-block' href='login.php'>Ingresar</a>")
	    });
/*
	    $('.sc-add-to-cart').on('click',function(e){
	    	e.preventDefault();
	    	
	    	var pad = $(this).parents('.sc-product-item');
	    	//var idp = $(this).parents('.sc-product-item').attr('data-idp');
	    	var idp = pad.find("input[name='product_id']").val();
	    	var pre = pad.find("input[name='product_price']").val();
	    	var can = pad.find("input[name='product_quantity']").val();
	    	var mad = pad.find("[data-name='product_est']:checked").val();
	    	var tam = pad.find("[data-name='product_tam']:checked").val();
	    	var cla = pad.find("select[name='product_clasificacion']").val();	    	
	    	var uni = pad.find("[data-name='product_uni']:checked").val();
	    	var min = pad.find("[data-name='product_uni']:checked").attr('data-uni-min');


	    	if(tam==undefined){ tam = "";}
	    	if(can<=0 || can==null)
	    	{
	    		error("La cantidad debe ser mayor a cero");
	    	}
	    	else if(cla=="" || cla==undefined)
	    	{
	    		error("No hay clasificacion del producto asociado");
	    	}
	    	else
	    	{
	    		$.post('funciones/pedidos/fnPedidos.php', {opcion: 'ANADIRPRODUCTO',idp:idp,pre:pre,can:can,mad:mad,tam:tam,cla:cla,uni:uni,min:min}, function(data, textStatus, xhr) {
	    			
	    			var res = data[0].res;
	    			var msn = data[0].msn;
	    			var idpedido = data[0].idpedido;
	    			var can = data[0].can;
	    			if(res=="ok")
	    			{
	    				$(this).parents('.sc-product-item').addClass('sc-added-item');
	    				ok(msn);
	    				$('#numpedido').html(can);
	    				setTimeout(CRUDPEDIDOS('DETALLEPEDIDO',idpedido),1000);

	    				//funciones alerta pedido en proceso
	    			}
	    			else
	    			{
	    				error(msn);
	    			}
	    		},"json");
	    	}
	    	
	    	//console.info(idp + " - " + pre + ' - ' + can + ' - ' +est + ' - '+cla + '-' + tam); 
	    })*/

	});
	</script>
    	<?php
    	
    	echo "<style onload=INICIALIZARCONTENIDO()></style>";
    	echo "<style onload=IncDec()></style>";
    	echo "<style onload=CRUDPEDIDOS('DETALLEPEDIDO','')></style>";
    
    }
    else if($opcion=="LISTADIRECCIONES")
    {
    	$id = $_POST['id'];
    	//$tel = $_POST['tel'];
    	?>
    	 <div class="input-group">

	    	<select title="Seleccione la dirección de envio"  data-width="100%" class="form-control selectpicker" name="dirpedido" id="dirpedido" title="Selecciona la direccion de envio..." onchange="CRUDPEDIDOS('ACTUALIZARDIRECCION','')">
	    	
	    		<?php
	    		$sql = pg_query($dbconn,"select d.dir_clave_int,d.dir_descripcion des,d.dir_detalle as det,s.sec_nombre as sec,b.bar_nombre as bar,dir_nomenclatura,dir_letra,dir_numero1,dir_numero2 from tbl_direcciones d JOIN tbl_barrio b ON b.bar_clave_int = d.bar_clave_int JOIN tbl_sector s on s.sec_clave_int = b.sec_clave_int WHERE d.usu_clave_int ='".$idUsuario."' order by s.sec_clave_int,b.bar_clave_int");
	    		while($dat = pg_fetch_array($sql))
	    		{
	    			$sec=  $dat['sec'];
	    			$bar = $dat['bar'];
	    			$des = $dat['des'];
	    			$direccion = $dat['dir_nomenclatura']." ".$dat['dir_letra']." #".$dat['dir_numero1']."-".$dat['dir_numero2'];
	    			?>
	    			<option data-content="<span class='label label-success'><?php echo $sec;?></span><span class='label label-primary'><?php echo $bar;?></span><span class='label label-default'><?php echo $direccion;?></span>" <?php if($ultimadireccion==$dat['dir_clave_int']){ echo "selected"; }?> value="<?php echo $dat['dir_clave_int'];?>"></option>
	    			<?php
	    		}
	    		?>
	    		
	    	</select>
	    	<a data-toggle='modal' data-target="#modalright" title="Agregar nueva dirección" class="input-group-addon"  onclick="CRUDPEDIDOS('NUEVADIRECCION','')"><i class="fa fa-plus"></i></a>
    	</div>
    	<div class="form-group">
    		<input placeholder="Telefono de envio del pedido" type="text" class="form-control input-sm" maxlength="20" id="txttelefonoenvio" name="txttelefonoenvio" value="<?php echo $ultimotelefono;?>" onkeyup="CRUDPEDIDOS('ACTUALIZARDIRECCION','')">
    	</div>
    	<?php
    	echo "<style onload=INICIALIZARCONTENIDO()></style>";
    }
    else if($opcion=="NUEVADIRECCION")
    {
    	?>
    	<div class="row">
    		<div class="col-md-12">
    			<label for="selsector">Sector:</label>
    			
    			<select name="selsector" id="selsector" class="form-control input-sm selectpicker" onchange="cargarbarrio('selbarrio','selsector')">
    				<option value="">-Seleccione-</option>
    				<?php
    				$sql = pg_query($dbconn,"SELECT s.sec_clave_int,s.sec_nombre from tbl_sector s join tbl_barrio b on b.sec_clave_int = s.sec_clave_int  where s.est_clave_int != 2  group by s.sec_clave_int order by s.sec_clave_int");
    				while($dat = pg_fetch_array($sql))
    				{
    					?>
    					<option value="<?php echo $dat['sec_clave_int'];?>"><?php echo $dat['sec_nombre'];?></option>
    					<?php
    				}
    				?>
    			</select>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-md-12">
    			<label for="selbarrio">Barrio:</label>
    			<select name="selbarrio" id="selbarrio" class="form-control input-sm selectpicker">
    				<option value="">-Seleccione-</option>
    			</select>
    		</div>
    	</div>
    	<hr>
    	<div class="row">
    		<div class="col-md-4">
    			<select id="selcalle" name="selcalle" class="form-control input-sm selectpicker" title="Calle">
    				<option value="Calle">Calle</option>
    				<option value="Carrera">Carrera</option>
    				<option value="Avenida">Avenida</option>
    				<option value="Avenida Carrera">Avenida Carrera</option>
    				<option value="Avenida Calle">Avenida Calle</option>
    				<option value="Circular">Circular</option>
    				<option value="Circunvalar">Circunvalar</option>
    				<option value="Diagonal">Diagonal</option>
    				<option value="Manzana">Manzana</option>
    				<option value="Transversal">Transversal</option>
    				<option value="Via">Via</option>
    			</select>
    		</div>
    		
    		<div class="col-md-2"><input type="text" id="txtletdir" name="txtletdir" class="form-control input-sm"></div>
    		<div class="col-md-3">
    			<div class="has-feedback">
					<span class="form-control-feedback left" >#</span>
					<input type="text" id="txtnumdir1" name="txtnumdir1" class="form-control input-sm" >
				</div>
    		</div>
    		<div class="col-md-3">
    			<div class="has-feedback">
					<span class="form-control-feedback left" >-</span>
					<input type="text" id="txtnumdir2" name="txtnumdir2" class="form-control input-sm">
				</div>
    		</div>
    	</div>
    	<div class="row" style="display: none;">
    		<div class="col-md-12">
    			<label for="txtdescripcion">Descripción:</label>
    			<input type="text" id="txtdescripcion" name="txtdescripcion" class="form-control input-sm">
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-md-12">
    			<label for="txtdetalle">Detalle:</label>
    			<textarea name="txtdetalle" id="txtdetalle" class="form-control input-sm"></textarea>

    		</div>
    	</div>
    	<div class="row">
    		<div class="col-md-12">
    			
				<label>¿De donde es tu dirección?</label><br>
				<div class="radio-toolbar">
				<label class="btn btn-app" for="radtipodireccion1">
				<input data-icon="fa fa-home" type="radio" name="radtipodireccion" id="radtipodireccion1" value="Casa" checked>
				<i class="fa fa-home"></i> Casa
				</label>
				<label class="btn btn-app" for="radtipodireccion2">
					<input data-icon="fa fa-building-o" type="radio" name="radtipodireccion" id="radtipodireccion2" value="Oficina">
				<i class="fa fa-building-o"></i> Oficina
				</label>
				<label class="btn btn-app" for="radtipodireccion3">
					<input data-icon="fa fa-heart" type="radio" name="radtipodireccion" id="radtipodireccion3" value="Pareja">
				<i class="fa fa-heart"></i> Pareja
				</label>
				<label class="btn btn-app" for="radtipodireccion4">
					<input data-icon="fa fa-plus" type="radio" name="radtipodireccion" id="radtipodireccion4" value="Otro">
				<i class="fa fa-plus"></i> Otro
				</label>
          		</div>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-md-12">
    			<a class="btn btn-primary" onclick="CRUDPEDIDOS('GUARDARDIRECCION','')">Crear Dirección</a>
    		</div>
    	</div>
    	<div class="row">
    		<?php
	    		$sql = pg_query($dbconn,"select d.dir_clave_int,d.dir_descripcion des,d.dir_detalle as det,s.sec_nombre as sec,b.bar_nombre as bar,dir_nomenclatura,dir_letra,dir_numero1,dir_numero2,dir_tipo,dir_tipo_icon from tbl_direcciones d JOIN tbl_barrio b ON b.bar_clave_int = d.bar_clave_int JOIN tbl_sector s on s.sec_clave_int = b.sec_clave_int WHERE d.usu_clave_int ='".$idUsuario."' order by s.sec_clave_int,b.bar_clave_int");
	    		$num = pg_num_rows($sql);
	    		if($num>0)
	    		{
	    			?>
	    			<h4 class="col-md-12">Direcciones</h4>
	    			<?php
		    		while($dat = pg_fetch_array($sql))
		    		{
		    			$sec=  $dat['sec'];
		    			$bar = $dat['bar'];
		    			$des = $dat['des'];
		    			$direccion = $dat['dir_nomenclatura']." ".$dat['dir_letra']." #".$dat['dir_numero1']."-".$dat['dir_numero2'];
		    			$ico = $dat['dir_tipo_icon'];

		    			?>
		    			<div class="col-md-12">
		    			<span cclass='label label-default'><i class="<?php echo $ico;?>"></i></span>
		    			<span class='label label-success'><?php echo $sec;?></span><span class='label label-primary'><?php echo $bar;?></span><span class='label label-default'><?php echo $direccion;?></span>
		    			<span class='label label-warning pull-right' onclick="CRUDPEDIDOS('EDITARDIRECCION','<?PHP echo $dat['dir_clave_int'];?>')"><i class="fa fa-edit"></i></span>
		    			</div>
		    			<?php
		    		}	    		
	    		
	    		}
	    		else
	    		{
	    			?>
	    			<h4>No tienes registrada ninguna dirección!.<br>Crear tus direcciones para permitirnos ubicarte</h4>
	    			<?php
	    		}
	    	?>
    	</div>
    	<?php
    	echo "<style onload=INICIALIZARCONTENIDO()></style>";
    }
    else if($opcion=="EDITARDIRECCION")
    {
    	$id = $_POST['id'];
    	$sql = pg_query($dbconn,"select d.dir_clave_int,d.dir_descripcion des,d.dir_detalle as det,s.sec_clave_int as sec,b.bar_clave_int as bar,dir_nomenclatura,dir_letra,dir_numero1,dir_numero2,dir_tipo,dir_tipo_icon from tbl_direcciones d JOIN tbl_barrio b ON b.bar_clave_int = d.bar_clave_int JOIN tbl_sector s on s.sec_clave_int = b.sec_clave_int WHERE d.dir_clave_int ='".$id."'");
    	$dat = pg_fetch_array($sql);
    	$des = $dat['des'];
    	$det = br2nl($dat['det']);
    	$sec = $dat['sec'];
    	$bar = $dat['bar'];
    	$nome = $dat['dir_nomenclatura'];
    	$letra = $dat['dir_letra'];
    	$num1 = $dat['dir_numero1'];
    	$num2 = $dat['dir_numero1'];
    	$tip = $dat['dir_tipo'];
    	$ico  = $dat['dir_tipo_icon'];
    	?>
    	<div class="row">
    		<div class="col-md-12">
    			<label for="selsector">Sector:</label>
    			
    			<select name="selsector" id="selsector" class="form-control input-sm selectpicker" onchange="cargarbarrio('selbarrio','selsector')">
    				<option value="">-Seleccione-</option>
    				<?php
    				$sql = pg_query($dbconn,"SELECT s.sec_clave_int,s.sec_nombre from tbl_sector s join tbl_barrio b on b.sec_clave_int = s.sec_clave_int  where s.est_clave_int != 2  group by s.sec_clave_int order by s.sec_clave_int");
    				while($dat = pg_fetch_array($sql))
    				{
    					?>
    					<option <?php if($sec==$dat['sec_clave_int']){ echo "selected"; }?> value="<?php echo $dat['sec_clave_int'];?>"><?php echo $dat['sec_nombre'];?></option>
    					<?php
    				}
    				?>
    			</select>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-md-12">
    			<label for="selbarrio">Barrio:</label>
    			<select name="selbarrio" id="selbarrio" class="form-control input-sm selectpicker">
    				<option value="">-Seleccione-</option>
    				<?php
    				$sql = pg_query($dbconn,"select bar_clave_int,bar_nombre from tbl_barrio where est_clave_int !=2 and sec_clave_int = '".$sec."' order by bar_nombre");
			    	$num = pg_num_rows($sql);
			    	
			    	if($num>0)
			    	{
			    		for($k=0;$k<$num;$k++)
			    		{
			    			$dat = pg_fetch_array($sql);
			    			$idb = $dat['bar_clave_int'];
			    			$barrio = $dat['bar_nombre'];
			    			
			    			?>
			    			<option <?php if($idb==$bar){ echo "selected"; } ?> value="<?php echo $idb?>"><?php echo $barrio; ?></option>
			    			option
			    			<?php
			    		}
			    	}
			    	?>
    			</select>
    		</div>
    	</div>
    	<hr>
    	<div class="row">
    		<div class="col-md-4">
    			<select id="selcalle" name="selcalle" class="form-control input-sm selectpicker" title="Nomenclatura">
    				<option value="Calle" <?php if($nome=="Calle"){ echo "selected"; } ?> >Calle</option>
    				<option value="Carrera" <?php if($nome=="Carrera"){ echo "selected"; } ?> >Carrera</option>
    				<option value="Avenida" <?php if($nome=="Avenida"){ echo "selected"; } ?>>Avenida</option>
    				<option value="Avenida Carrera" <?php if($nome=="Avenida Carrera"){ echo "selected"; } ?>>Avenida Carrera</option>
    				<option value="Avenida Calle" <?php if($nome=="Avenida Calle"){ echo "selected"; } ?>>Avenida Calle</option>
    				<option value="Circular" <?php if($nome=="Circular"){ echo "selected"; } ?>>Circular</option>
    				<option value="Circunvalar" <?php if($nome=="Circunvalar"){ echo "selected"; } ?>>Circunvalar</option>
    				<option value="Diagonal" <?php if($nome=="Diagonal"){ echo "selected"; } ?>>Diagonal</option>
    				<option value="Manzana" <?php if($nome=="Manzana"){ echo "selected"; } ?>>Manzana</option>
    				<option value="Transversal" <?php if($nome=="Transversal"){ echo "selected"; } ?>>Transversal</option>
    				<option value="Via" <?php if($nome=="Via"){ echo "selected"; } ?>>Via</option>
    			</select>
    		</div>
    		
    		<div class="col-md-2"><input type="text" id="txtletdir" name="txtletdir" class="form-control input-sm" value="<?php echo $letra;?>"></div>
    		<div class="col-md-3">
    			<div class="has-feedback">
					<span class="form-control-feedback left" >#</span>
					<input type="text" id="txtnumdir1" name="txtnumdir1" class="form-control input-sm" value="<?php echo $num1;?>" >
				</div>
    		</div>
    		<div class="col-md-3">
    			<div class="has-feedback">
					<span class="form-control-feedback left" >-</span>
					<input type="text" id="txtnumdir2" name="txtnumdir2" class="form-control input-sm" value="<?php echo $num2;?>">
				</div>
    		</div>
    	</div>
    	<div class="row" style="display: none;">
    		<div class="col-md-12">
    			<label for="txtdescripcion">Descripción:</label>
    			<input type="text" id="txtdescripcion" name="txtdescripcion" class="form-control input-sm" value="<?php echo $des; ?>">
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-md-12">
    			<label for="txtdetalle">Detalle:</label>
    			<textarea name="txtdetalle" id="txtdetalle" class="form-control input-sm"><?php echo $det;?></textarea>

    		</div>
    	</div>
    	<div class="row">
    		<div class="col-md-12">
    			
				<label>¿De donde es tu dirección?</label><br>
				<div class="radio-toolbar">
				<label class="btn btn-app" for="radtipodireccion1">
				<input data-icon="fa fa-home" type="radio" name="radtipodireccion" id="radtipodireccion1" value="Casa" <?php if($tip=="" || $tip=="Casa"){ echo "checked";}?>>
				<i class="fa fa-home"></i> Casa
				</label>
				<label class="btn btn-app" for="radtipodireccion2">
					<input data-icon="fa fa-building-o" type="radio" name="radtipodireccion" id="radtipodireccion2" value="Oficina" <?php if($tip=="Oficina"){ echo "checked";}?>>
				<i class="fa fa-building-o"></i> Oficina
				</label>
				<label class="btn btn-app" for="radtipodireccion3">
					<input data-icon="fa fa-heart" type="radio" name="radtipodireccion" id="radtipodireccion3" value="Pareja" <?php if( $tip=="Pareja"){ echo "checked";}?>>
				<i class="fa fa-heart"></i> Pareja
				</label>
				<label class="btn btn-app" for="radtipodireccion4">
					<input data-icon="fa fa-plus" type="radio" name="radtipodireccion" id="radtipodireccion4" value="Otro" <?php if( $tip=="Otro"){ echo "checked";}?>>
				<i class="fa fa-plus"></i> Otro
				</label>
          		</div>
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-md-12">
    			<a class="btn btn-success" onclick="CRUDPEDIDOS('GUARDAREDITARDIRECCION','<?php echo $id;?>')">Actualizar Dirección</a>
    		</div>
    	</div>
    	<?php
    	echo "<style onload=INICIALIZARCONTENIDO()></style>";
    }
    else if($opcion=="ACTUALIZARDIRECCION")
    {
    	$dir = $_POST['dir'];
    	$tel = $_POST['tel'];
    	$upd = pg_query($dbconn,"UPDATE tbl_usuario SET dir_clave_int = '".$dir."',usu_ult_telefono = '".$tel."' WHERE usu_clave_int = '".$idUsuario."'");
    	if($upd>0)
    	{
    		$res = "ok";
    		$msn ="";
    	}
    	else
    	{
    		$res ="error";
    		$msn = "No se actualizo la dirección seleccionada al usuario. Error BD(".pg_last_error($dbconn).")";
    	}
    	//COSTO DOMICILIO y TIEMPO DE ENTREGA
    	
    	$datos[] = array('res' => $res, "msn" => $msn,"domicilio"=>number_format($domicilio,0,'.',','));
    	echo json_encode($datos);
    }
    else if($opcion=="CARGARBARRIOS")
    {
    	$sec = $_POST['sector'];
    	$sql = pg_query($dbconn,"select bar_clave_int,bar_nombre from tbl_barrio where est_clave_int !=2 and sec_clave_int = '".$sec."' order by bar_nombre");
    	$num = pg_num_rows($sql);
    	$datos = array();
    	if($num>0)
    	{
    		for($k=0;$k<$num;$k++)
    		{
    			$dat = pg_fetch_array($sql);
    			$idb = $dat['bar_clave_int'];
    			$bar = $dat['bar_nombre'];
    			$datos[] = array("id"=>$idb,"literal"=>$bar,"res"=>"si");
    		}
    	}
    	else
    	{
    		$datos[] = array("id"=>"","literal"=>"","res"=>"no");
    	}
    	echo json_encode($datos);
    }
    else if($opcion=="GUARDARDIRECCION")
    {
    	$bar = $_POST['bar'];
    	$des = $_POST['des'];
    	$det = nl2br($_POST['det']);
    	$nome = $_POST['nome'];
    	$let = $_POST['let'];
    	$num1 = $_POST['num1'];
    	$num2 = $_POST['num2'];
    	$tip = $_POST['tip'];
    	$ico = $_POST['ico'];

    	$ins = pg_query($dbconn, "INSERT INTO tbl_direcciones(usu_clave_int,bar_clave_int,dir_descripcion,dir_detalle,dir_usu_actualiz,dir_fec_actualiz,dir_nomenclatura,dir_letra,dir_numero1,dir_numero2,dir_tipo,dir_tipo_icon) VALUES('".$idUsuario."','".$bar."','".$des."','".$det."','".$usuario."','".$fecha."','".$nome."','".$let."','".$num1."','".$num2."','".$tip."','".$ico."')");
    	if($ins>0)
    	{
    		$uid = pg_fetch_row(pg_query($dbconn,"SELECT setval('dir_id_seq',nextval('dir_id_seq')-1) as id;"));
			$iddir = $uid[0];
			$upd = pg_query($dbconn,"UPDATE tbl_usuario SET dir_clave_int = '".$iddir."' WHERE usu_clave_int = '".$idUsuario."'");
			$res = "ok";
			$msn = "Dirección registrada correctamente";
    	}
    	else
    	{
    		$res = "error";
    		$msn = "Surgio un error al registrar direccion. Error BD (".pg_last_error($dbconn).")";
    	}
    	//COSTO DOMICILIO y TIEMPO DE ENTREGA
    	$condom = pg_query($dbconn,"select sec_domicilio,sec_horas,sec_minutos from tbl_sector s join tbl_barrio b on b.sec_clave_int = s.sec_clave_int join tbl_direcciones d on d.bar_clave_int = b.bar_clave_int where d.usu_clave_int = '".$idUsuario."' and d.dir_clave_int = '".$iddir."'");
    	$datdom = pg_fetch_array($condom);
    	$domicilio = $datdom['sec_domicilio'];
    	$datos[] = array('res' => $res ,"msn"=>$msn , "iddir" => $iddir, "domicilio" => number_format($domicilio,0,'.',',') );
    	echo json_encode($datos);
    }
    else if($opcion=="GUARDAREDITARDIRECCION")
    {
    	$bar = $_POST['bar'];
    	$des = $_POST['des'];
    	$det = nl2br($_POST['det']);
    	$nome = $_POST['nome'];
    	$let = $_POST['let'];
    	$num1 = $_POST['num1'];
    	$num2 = $_POST['num2'];
    	$tip = $_POST['tip'];
    	$ico = $_POST['ico'];
    	$iddir = $_POST['id'];

    	$ins = pg_query($dbconn, "UPDATE tbl_direcciones SET bar_clave_int = '".$bar."',dir_descripcion = '".$des."',dir_detalle = '".$det."',dir_usu_actualiz = '".$usuario."',dir_fec_actualiz = '".$fecha."',dir_nomenclatura ='".$nome."' ,dir_letra = '".$let."',dir_numero1 = '".$num1."',dir_numero2= '".$num2."',dir_tipo ='".$tip."' ,dir_tipo_icon = '".$ico."' WHERE dir_clave_int = '".$iddir."'");
    	if($ins>0)
    	{
    		
			$res = "ok";
			$msn = "Dirección actualizada correctamente";
    	}
    	else
    	{
    		$res = "error";
    		$msn = "Surgio un error al actualizar direccion. Error BD (".pg_last_error($dbconn).")";
    	}
    	//COSTO DOMICILIO y TIEMPO DE ENTREGA
    	$condom = pg_query($dbconn,"select sec_domicilio,sec_horas,sec_minutos from tbl_sector s join tbl_barrio b on b.sec_clave_int = s.sec_clave_int join tbl_direcciones d on d.bar_clave_int = b.bar_clave_int where d.usu_clave_int = '".$idUsuario."' and d.dir_clave_int = '".$iddir."'");
    	$datdom = pg_fetch_array($condom);
    	$domicilio = $datdom['sec_domicilio'];
    	$datos[] = array('res' => $res ,"msn"=>$msn , "iddir" => $iddir, "domicilio" => number_format($domicilio,0,'.',',') );
    	echo json_encode($datos);
    }
    else if($opcion=="CAMBIARPRECIO")
    {
    	$cla = $_POST['cla'];
    	$idp = $_POST['idp'];
    	/*$concla = pg_query($dbconn,"SELECT cla_clave_int from tbl_clasificacion WHERE cla_nombre = '".$cla."'");
		$datcla = pg_fetch_array($concla);
		$codcla = $datcla['cla_clave_int'];*/

    	$sql = pg_query($dbconn, "select prm_venta from tbl_precios_mercado where pro_clave_int ='".$idp."' and cla_clave_int = '".$cla."' and mer_clave_int = '".$idmercado."'");
    	$dat = pg_fetch_array($sql);
    	$ven = $dat['prm_venta']; if($ven<=0){ $ven = 0;}
    	$datos[]= array("venta"=>$ven);
    	echo json_encode($datos);
    }
    else if($opcion=="ANADIRPRODUCTO")
    {
    	
    	$idproducto = $_POST['idp'];
    	$pre = $_POST['pre'];
    	$can = $_POST['can'];
    	$tam = $_POST['tam'];
    	$idclasificacion = $_POST['cla']; 
    	$mad = $_POST['mad'];
    	$uni = $_POST['uni'];
    	$min = $_POST['min'];
    	//unidad de compra
    	if($uni==0)
    	{
    		$peso = 0;
    		$cantidad = $can;
    		$can = $can;
    	}
    	else if($uni==1)
    	{
    		$peso = $can;
    		$cantidad = 0;
    		$can = $can/$min;
    	}
    	//VERIFICAR SI HAY PEDIDO PENDIENTE
    	$conped = pg_query($dbconn, "SELECT ped_clave_int FROM tbl_pedidos WHERE usu_clave_int = '".$idUsuario."' and ped_estado = 0");
    	$numped = pg_num_rows($conped);
    	if($numped>0)
    	{
    		$datped = pg_fetch_array($conped);
    		$idpedido = $datped['ped_clave_int'];
    	}
    	else
    	{
    		$insertped = pg_query($dbconn,"INSERT INTO tbl_pedidos(usu_clave_int,ped_usu_actualiz,ped_fec_actualiz,ped_ip) VALUES('".$idUsuario."','".$usuario."','".$fecha."','".$IP."')");
    		$uid = pg_fetch_row(pg_query($dbconn,"SELECT setval('ped_id_seq',nextval('ped_id_seq')-1) as id;"));
			$idpedido = $uid[0];
    	}
    	$codpedido = sprintf('%010d',$idpedido);

    	//VERIFICAR EXISTENCIA DE PRODUCTO EN CARRO VALIDANDO TODAS LAS CARACTERISTICAS
    	$veridet = pg_query($dbconn,"SELECT * FROM tbl_pedidos_detalle WHERE pro_clave_int = '".$idproducto."' and mer_clave_int = '".$idmercado."' and cla_clave_int = '".$idclasificacion."' and pde_tamano = '".$tam."' and pde_estado='".$mad."' and pde_valor = '".$pre."' and pde_unidad = '".$uni."' and ped_clave_int = '".$idpedido."'");
    	$numdet = pg_num_rows($veridet);
    	if($numdet>0)
    	{
    		$datdet = pg_fetch_array($veridet);
    		$iddet = $datdet['pde_clave_int'];
    		
    		$upd = pg_query($dbconn,"UPDATE tbl_pedidos_detalle SET pde_cantidad= pde_cantidad + '".$can."',pde_usu_actualiz ='".$usuario."',pde_cantidad_unidad = pde_cantidad_unidad + '".$cantidad."', pde_cantidad_peso = pde_cantidad_peso + '".$peso."',pde_fec_actualiz = '".$fecha."' WHERE pde_clave_int = '".$iddet."'");
    	}
    	else
    	{
    		$sql = pg_query($dbconn, "INSERT INTO tbl_pedidos_detalle(mer_clave_int,cla_clave_int,pro_clave_int,pde_cantidad,pde_valor,pde_usu_actualiz,pde_fec_actualiz,pde_estado,pde_tamano,ped_clave_int,pde_cantidad_unidad,pde_cantidad_peso,pde_unidad,pde_minimo) VALUES('".$idmercado."','".$idclasificacion."','".$idproducto."','".$can."','".$pre."','".$usuario."','".$fecha."','".$mad."','".$tam."','".$idpedido."','".$cantidad."','".$peso."','".$uni."','".$min."')");
    		

    	}
    	$sql1 = "INSERT INTO tbl_pedidos_detalle(mer_clave_int,cla_clave_int,pro_clave_int,pde_cantidad,pde_valor,pde_usu_actualiz,pde_fec_actualiz,pde_estado,pde_tamano,ped_clave_int,pde_cantidad_unidad,pde_cantidad_peso,pde_unidad,pde_minimo) VALUES('".$idmercado."','".$idclasificacion."','".$idproducto."','".$can."','".$pre."','".$usuario."','".$fecha."','".$mad."','".$tam."','".$idpedido."','".$cantidad."','".$peso."','".$uni."','".$min."')";

    	if($sql>0 || $upd>0)
    	{
    		if($sql>0)
    		{
	    		$uidd = pg_fetch_row(pg_query($dbconn,"SELECT setval('pde_id_seq',nextval('pde_id_seq')-1) as id;"));
				$iddet = $uidd[0];
			}

    		$coddetalle = sprintf('%010d',$iddet);
    		$upddet = pg_query($dbconn, "UPDATE tbl_pedidos_detalle SET pde_codigo='".$coddetalle."' where pde_clave_int = '".$iddet."'");

    		$sqltot = pg_query($dbconn, "SELECT sum(pde_cantidad*pde_valor) as tot,count(pde_clave_int) AS cant from tbl_pedidos_detalle where ped_clave_int = '".$idpedido."'");
    		$dattot = pg_fetch_array($sqltot);
    		$tot = $dattot['tot'];
    		$cantactual = $dattot['cant'];
    		$sqlupd = pg_query($dbconn,"UPDATE tbl_pedidos SET ped_total = '".$tot."',ped_codigo = '".$codpedido."' WHERE ped_clave_int = '".$idpedido."'");
    		$res = "ok";
    		$msn = "";
    	}
    	else
    	{
    		$res = "error";
    		$msn = "La cantidad no se agrego. Error BD(".pg_last_error($dbconn).")";
    		$cantactual = 0;
    	}
    	$datos[] = array('res' => $res , 'msn' => $msn ,'idpedido' => $idpedido ,'idUsuario'=>$idUsuario,"can"=>$cantactual,"sql1"=>$sql1);
    	echo json_encode($datos);
    }
    else if($opcion=="DETALLEPEDIDO")
    {
    	//$idpedido = $_POST['idpedido'];
    	$condet = pg_query($dbconn, "SELECT d.pde_clave_int,p.pro_clave_int,p.pro_nombre,p.pro_descripcionbreve,c.cla_nombre,pde_estado,pde_tamano,pde_cantidad,pde_valor,pde_unidad,pro_uni_venta,pro_pes_venta,pro_mu_venta,pro_mp_venta,pde_minimo,pde_unidad from tbl_pedidos_detalle d JOIN tbl_productos p on p.pro_clave_int = d.pro_clave_int join tbl_clasificacion c on c.cla_clave_int = d.cla_clave_int where d.ped_clave_int = '".$idpedido."' order by p.pro_nombre,c.cla_nombre");
    	$numdet = pg_num_rows($condet);


    	?>
    	<div class="panel-heading" id="divdirecciones"></div>
    	<div class="panel-heading sc-cart-heading">Cant. Productos<span class="sc-cart-count badge"><?php echo $numdet;?></span></div>
    	<div class="list-group sc-cart-item-list" id="divcontenidocarro">
    	<?php 
    	if($numdet<=0)
    	{ ?>
    	<div class="list-group sc-cart-item-list"><div class="sc-cart-empty-msg">Carro esta vacio!<br>Carga tus productos</div></div>
    	<?php
    	}
    	else
    	{ 
    		$total = 0;
    		while($datdet= pg_fetch_array($condet))
    		{
    			$iddetalle = $datdet['pde_clave_int'];
    			$idp = $datdet['pro_clave_int'];
    			$nompro = $datdet['pro_nombre'];
    			$nomcla = $datdet['cla_nombre'];
    			$maduracion = $datdet['pde_estado'];
    			$descp = $datdet['pro_descripcionbreve'];
    			$precio = $datdet['pde_valor'];
    			$cantidad = $datdet['pde_cantidad'];
    			$subtotal = $precio * $cantidad;
    			$tamano = $datdet['pde_tamano'];
    			if($tamano!=""){ $tamano = '-'.$tamano;}
    			
    			$puv = $datdet['pro_uni_venta'];
    			$ppv = $datdet['pro_pes_venta'];
    			$pmuv = $datdet['pro_mu_venta'];
    			$pmpv = $datdet['pro_mp_venta'];
    			$min = $datdet['pde_minimo'];
    			$uni = $datdet['pde_unidad'];
    			if($uni==0)
    			{
    				$unimin = $pmuv ;
    				$cantidad = $cantidad;
    			}
    			else
    			{
    				$unimin = $pmpv;
    				$cantidad = $cantidad * $min;//esto debido a que el precio del producto cuando es en peso es precio por la unidad minima ejemplo:250gr = 1 unidad
    			}

				$con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idp."' and pri_activo = 1 LIMIT 1");
				$dat = pg_fetch_row($con);
				$cimg = $dat[0];
				$img = $dat[1];


    		?>	

	    	
	    		<div class="sc-cart-item list-group-item" data-unique-key="1535566924443">
	    			<button type="button" class="sc-cart-remove" data-detalle="<?php echo $iddetalle; ?>">×</button>
	    			<img class="img-responsive pull-left" src="<?php echo $img;?>" />
	    			<h4 class="list-group-item-heading"><?php echo $nompro;?></h4>
	    			<h4 class="list-group-item-heading"><span class="badge bg-purple"><?php echo $nomcla;?>-<?php echo $maduracion."".$tamano;?></span></h4><p class="list-group-item-text"><?php echo $descp;?></p>

	    			<div class="sc-cart-item-summary">
	    				<span class="sc-cart-item-price currency <?php if($uni==1){ echo 'invisible';}?>" ><?php echo $precio;?></span> × <input data-unidad='<?php echo $uni;?>' type="number" min="<?php echo $min;?>" max="99999" class="sc-cart-item-qty" step ="<?php echo $min;?>" value="<?php echo $cantidad;?>"><?php if($uni==1){ echo "GR";}else { echo "UND";} ?> = <span class="sc-cart-item-amount currency"><?php echo $subtotal;?></span></div>
	    		</div>
	    	
    	<?php
    	 		$total = $total + $subtotal;
    		}
    	}
    	?>
    	</div>
    	<div class="panel-footer sc-toolbar">
    		<div class="sc-cart-summary">Subtotal: $<?php echo number_format($total,0,'.',',');?></div>
    		<div class="sc-cart-envio">Domicilio: $<?php echo number_format($domicilio,0,'.',',');?></div>
    		<div class="sc-cart-toolbar">
    			<a class="btn btn-info sc-cart-checkout" data-pedido = '<?php echo $idpedido;?>' type="button" onclick="CRUDPEDIDOS('RESUMENPEDIDO','<?PHP echo $idpedido;?>')"><i class="fa fa-opencart"></i>Resumen</a>
    			<button  class="btn btn-danger sc-cart-clear" data-pedido = '<?php echo $idpedido;?>' type="button" onclick="CRUDPEDIDOS('CANCELARPEDIDO','<?PHP echo $idpedido;?>')">Cancelar</button>
    		</div>
    	</div>
    	<script>
    		$(document).ready(function(e) {
    			$('#divdetallepedido').on("click", '.sc-cart-remove', function (e) {
                		e.preventDefault();
                		var idd = $(this).attr('data-detalle');
                		var del = $(this);
                		$.post('funciones/pedidos/fnPedidos.php',{opcion:"DELETEPRODUCTO",iddetalle:idd},function(data){
                			var res = data[0].res;
                			var msn = data[0].msn;
                			var can = data[0].can;
                			var total = data[0].total;
                			if(res=="ok")
                			{
                				$('.sc-cart-count').html(can);
                				$('#numpedido').html(can);
                				if(can==0)
                				{
                					$('#divcontenidocarro').html('<div class="list-group sc-cart-item-list"><div class="sc-cart-empty-msg">Carro esta vacio!<br>Carga tus productos</div></div>');
                				}
                				else
                				{
                					console.info("Eliminado");
                					del.parents('.sc-cart-item').fadeOut("normal");
                				}
                				$('.sc-cart-summary').html("Subtotal:" + total);
                			}
                			else
                			{
                				error(msn);
                			}
                		},"json")               		
                		
            		});

				$('#divdetallepedido').on("change", '.sc-cart-item-qty', function (e) 
				{
					e.preventDefault();
					var cant = $(this).val();
					var min = $(this).attr('min');
					var uni = $(this).attr('data-unidad');

					if(cant<=0)
					{
						error("Indicar una cantidad mayor a cero");					
					}
					else
					{
						var iddetalle = $(this).parents('.sc-cart-item').find('.sc-cart-remove').attr('data-detalle');
						var del = $(this);

						$.post('funciones/pedidos/fnPedidos.php', {opcion: 'ACTUALIZARPRODUCTO',iddetalle:iddetalle,cant:cant,min:min,uni:uni}, function(data, textStatus, xhr) {
							 var res = data[0].res;
							 var msn = data[0].msn;
							 var total = data[0].total;
							 var subtotal = data[0].subtotal;
							 if(res=="ok")
							 {
							 	$('.sc-cart-summary').html("Subtotal:" +total);
							 	del.parents('.sc-cart-item').find('.sc-cart-item-amount').html(subtotal);
							 }
							 else
							 {
							 	error(msn);
							 }
						},"json");
						
					}
				});

    		});
    	</script>
    	<?php
    	echo "<style onload=CRUDPEDIDOS('LISTADIRECCIONES','')></style>";
    	echo "<style onload=INICIALIZARCONTENIDO()></div>";
    }
    else if($opcion=="DELETEPRODUCTO")
    {
    	$iddetalle = $_POST['iddetalle'];
    	$del = pg_query($dbconn, "DELETE FROM tbl_pedidos_detalle WHERE pde_clave_int = '".$iddetalle."'");
    	if($del>0)
    	{
    		$res = "ok";
    		$msn = "";
    	}
    	else
    	{
    		$res = "error";
    		$msn = "Surgio un error al eliminar producto del carrito. Error BD(".pg_last_error($dbconn).")";
    	}
    	$concan = pg_query($dbconn, "select COUNT(pde_clave_int) cant,sum(pde_cantidad*pde_valor) tot from tbl_pedidos_detalle where ped_clave_int = '".$idpedido."'");
    	$datcan = pg_fetch_row($concan); $can = $datcan[0]; $tot = $datcan[1];
    	$totalpedido = $tot + $domicilio;
    	$datos[] = array('res' => $res , 'msn'=>$msn, "can"=>$can ,"total" =>"$".number_format($tot,0,'.',',') ,"totalpedido" =>"$".number_format($totalpedido,0,'.',','));
    	echo json_encode($datos);
    }
    else if($opcion=="ACTUALIZARPRODUCTO")
    {
    	$iddetalle = $_POST['iddetalle'];
    	$cant = $_POST['cant'];
    	$min = $_POST['min'];
    	$uni = $_POST['uni'];
    	//unidad de compra
    	if($uni==0)
    	{
    		$peso = 0;
    		$cantidad = $cant;
    		$cant = $cant;
    	}
    	else if($uni==1)
    	{
    		$peso = $cant;
    		$cantidad = 0;
    		$cant = $cant/$min;
    	}
    	$upd = pg_query($dbconn,"UPDATE tbl_pedidos_detalle SET pde_cantidad = '".$cant."' ,pde_usu_actualiz = '".$usuario."',pde_cantidad_unidad =  '".$cantidad."',pde_cantidad_peso='".$peso."', pde_fec_actualiz = '".$fecha."' where pde_clave_int = '".$iddetalle."'");
    	if($upd>0)
    	{
    		$res = "ok";
    		$msn = "";
    	}
    	else
    	{

    		$res = "error";
    		$msn = "Surgió un error al modificar cantidad. Error BD('".pg_last_error($dbconn)."')";
    	}

    	$condet = pg_query($dbconn, "select sum(pde_cantidad*pde_valor) subtot from tbl_pedidos_detalle where pde_clave_int = '".$iddetalle."'");
    	$datdet = pg_fetch_row($condet);
    	$subtotal = $datdet[0];

    	$concan = pg_query($dbconn, "select COUNT(pde_clave_int) cant,sum(pde_cantidad*pde_valor) tot from tbl_pedidos_detalle where ped_clave_int = '".$idpedido."'");
    	$datcan = pg_fetch_row($concan); $can = $datcan[0]; $tot = $datcan[1];
    	$totalpedido = $tot + $domicilio;
    	$datos[] = array('res' => $res , 'msn'=>$msn, "can"=>$can ,"total" =>"$".number_format($tot,0,'.',',') ,"subtotal" =>"$".number_format($subtotal,0,'.',','),"totalpedido" =>"$".number_format($totalpedido,0,'.',',') );
    	echo json_encode($datos);
    }
    else if($opcion=="RESUMENPEDIDO")
   {
   	  $coninfo = pg_query($dbconn,"select u.usu_nombre,u.usu_apellido,u.usu_usuario,p.ped_fecha,u.usu_email from tbl_pedidos p JOIN tbl_usuario u ON u.usu_clave_int = p.usu_clave_int WHERE p.ped_clave_int = '".$idpedido."'");
   	  $datinfo = pg_fetch_array($coninfo);
   	  $cliente = $datinfo['usu_nombre']." ".$datinfo['usu_apellido'];
   	  $pedfecha = $datinfo['ped_fecha'];
   	  $email = $datinfo['usu_email'];

   	  $condir = pg_query($dbconn,"select s.sec_nombre,b.bar_nombre,s.sec_domicilio,d.dir_nomenclatura,d.dir_letra,d.dir_numero1,d.dir_numero2,d.dir_detalle from tbl_direcciones d join tbl_barrio b on b.bar_clave_int = d.bar_clave_int join tbl_sector s on s.sec_clave_int = b.sec_clave_int where d.dir_clave_int = '".$ultimadireccion."'");
   	  $datdir = pg_fetch_array($condir);
   	  $sector = $datdir['sec_nombre'];
   	  $barrio = $datdir['bar_nombre'];
   	  $nome = $datdir['dir_nomenclatura'];
   	  $letra = $datdir['dir_letra'];
   	  $nume1 = $datdir['dir_numero1'];
   	  $nume2 = $datdir['dir_numero2'];
   	  $direccion = $nome." ".$letra."#".$nume1."-".$nume2;
   	  $domicilio = $datdir['sec_domicilio'];

	$condet = pg_query($dbconn, "SELECT d.pde_clave_int,p.pro_clave_int,p.pro_nombre,p.pro_descripcionbreve,p.pro_codigo,c.cla_nombre,pde_estado,pde_tamano,cla_nombre,pde_cantidad,pde_valor,pde_unidad,pro_uni_venta,pro_pes_venta,pro_mu_venta,pro_mp_venta,pde_minimo,pde_unidad ,pde_codigo from tbl_pedidos_detalle d JOIN tbl_productos p on p.pro_clave_int = d.pro_clave_int join tbl_clasificacion c on c.cla_clave_int = d.cla_clave_int where d.ped_clave_int = '".$idpedido."' order by p.pro_nombre,c.cla_nombre");
	$numdet = pg_num_rows($condet);

   	  ?>
   	  <div id="divresumenpedido">
   	  	
   	  	<table width="100%">
   	  		<tr><th colspan="6"></th><th rowspan="5" width="20%"><img width="150" height="87" src="dist/img/LOGO.png"></th></tr>
   	  		<tr><th colspan="6"></th></tr>
   	  		<tr><th>Cliente:</th><th><?php  echo $cliente;?></th><th>Fecha:</th><th><?php echo $fecha;;?></th></tr>
   	  		<tr><th>Email:</th><th><?php echo $email;?></th><th>Telefono:</th><th><?php echo $ultimotelefono;?></th></tr>
   	  		<tr><th>Sector:</th><th><?php echo $sector;?></th><th>Barrio:</th><th><?php echo $barrio;?></th><th>Dirección:</th><th><?php echo $direccion;?></th></tr>
   	  		<tr><th colspan="7"><hr></th></tr>
   	  		<tr><th colspan="7">
   	  			<?php 
    	if($numdet<=0)
    	{ ?>
    	<div class="list-group sc-cart-item-list"><div class="sc-cart-empty-msg">Carro esta vacio!<br>Carga tus productos</div></div>
    	<?php
    	}
    	else
    	{ 
    		?>
   	  			<table id="tbresumen" width="100%" class="table table-bordered compact" style="font-size: 10px">
   	  				
   	  			
   	  				<tr>
   	  					<th class="bg-success text-center">Linea</th>
	   	  				<th class="bg-success text-center">Producto</th>
	   	  				<th class="bg-success text-center">Descripción</th>	
	   	  				<th class="bg-success text-center">Precio</th>   	  				
	   	  				<th class="bg-success text-center">Cantidad</th>
	   	  				<th class="bg-success text-center">Unidad Medida</th>
	   	  				<th class="bg-success text-center">Observación</th>
	   	  				<th class="bg-success text-center">Costo</th>
	   	  				<th class="bg-success text-center" width="20px"></th>
	   	  			</tr>
	   	  			<?php
	   	  			$total = 0;
		    		while($datdet= pg_fetch_array($condet))
		    		{
		    			$iddetalle = $datdet['pde_clave_int'];
		    			$idp = $datdet['pro_clave_int'];
		    			$codlinea = $datdet['pde_codigo'];
		    			$nompro = $datdet['pro_nombre'];
		    			$codpro = $datdet['pro_codigo'];
		    			$nomcla = $datdet['cla_nombre'];
		    			$maduracion = $datdet['pde_estado'];
		    			$descp = $datdet['pro_descripcionbreve'];
		    			$precio = $datdet['pde_valor'];
		    			$cantidad = $datdet['pde_cantidad'];
		    			$subtotal = $precio * $cantidad;
		    			$tamano = $datdet['pde_tamano'];
		    			$observacion = "<strong>Clasificación:</strong>".$nomcla." <br>";
		    			if($tamano!=""){$observacion.="<strong>Tamano:</strong>".$tamano."<br> ";}
		    			if($maduracion!=""){$observacion.="<strong>Maduración:</strong>".$maduracion." ";}
		    			if($tamano!=""){ $tamano = '-'.$tamano;}
		    			
		    			$puv = $datdet['pro_uni_venta'];
		    			$ppv = $datdet['pro_pes_venta'];
		    			$pmuv = $datdet['pro_mu_venta'];
		    			$pmpv = $datdet['pro_mp_venta'];
		    			$min = $datdet['pde_minimo'];
		    			$uni = $datdet['pde_unidad'];
		    			if($uni==0)
		    			{
		    				$unimin = $pmuv ;
		    				$cantidad = $cantidad;
		    			}
		    			else
		    			{
		    				$unimin = $pmpv;
		    				$cantidad = $cantidad * $min;//esto debido a que el precio del producto cuando es en peso es precio por la unidad minima ejemplo:250gr = 1 unidad
		    			}

						$con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idp."' and pri_activo = 1 LIMIT 1");
						$dat = pg_fetch_row($con);
						$cimg = $dat[0];
						$img = $dat[1];
						?>
						<tr class="sc-cart-itemr list-group-itemr">
							<td><?php echo $codlinea;?></td>
							<td><?php echo $codpro;?></td>
							<td><?php echo $nompro;?></td>
							<td><span class="sc-cart-item-pricer currency <?php if($uni==1){ echo 'invisible';}?>" ><?php echo $precio;?></span></td>
							<td><input data-unidad='<?php echo $uni;?>' type="number" min="<?php echo $min;?>" max="99999" class="form-control input-sm sc-cart-item-qtyr" step ="<?php echo $min;?>" value="<?php echo $cantidad;?>"></td>
							<td><?php if($uni==1){ echo "GR";}else { echo "UND";} ?></td>
							<td><?php echo $observacion;?></td>
							<td> <span class="sc-cart-item-amountr currency"><?php echo $subtotal;?></span></td>
							<td><button type="button" class="btn btn-xs btn-danger sc-cart-remover" data-detalle="<?php echo $iddetalle; ?>"><i class="fa fa-trash"></i></button></td>
						</tr>
						<?php
						$total = $total + $subtotal;
					}
					$totalpedido = $total + $domicilio;
					?>
					<tr><td colspan="6" rowspan="4"></td><td>Sub Total:</td><td colspan="2"><span class="sc-cart-summaryr currency"><?php echo $total;?></span></td></tr>
					<tr><td >Domicilio:</td><td colspan="2" class="currency"><?php echo $domicilio;?></td></tr>
					<tr><td>Total:</td><td colspan="2"><span id="totalpedido" class="currency"><?php echo $totalpedido;?></span></td></tr>
					<tr><td>Lista Deseos</td><td colspan="2">
						<label class='switch'><input type='checkbox' id="cklista" name="cklista" value="1"><span class='slider round'></span><span class='absolute-no'>NO</span>
</label>
					</td></tr>
   	  			</table>
   	  			<?php
   	  		}
   	  		?>
   	  		</th></tr>
   	  	</table>
   	  	<div class="row">
   	  		<div class="col-md2"></div>
   	  	</div>
   	  </div>
   	  <script>
   	  	$(document).ready(function(e) {
    			$('#tbresumen').on("click", '.sc-cart-remover', function (e) {
                		e.preventDefault();
                		var idd = $(this).attr('data-detalle');
                		var del = $(this);
                		$.post('funciones/pedidos/fnPedidos.php',{opcion:"DELETEPRODUCTO",iddetalle:idd},function(data){
                			var res = data[0].res;
                			var msn = data[0].msn;
                			var can = data[0].can;
                			var total = data[0].total;
                			var totalpedido = data[0].totalpedido;
                			if(res=="ok")
                			{

                				$('.sc-cart-countr').html(can);
                				$('#numpedido').html(can);
                				$('#totalpedido').html(totalpedido);
                				if(can==0)
                				{
                					//$('#divcontenidocarro').html('<div class="list-group sc-cart-item-list"><div class="sc-cart-empty-msg">Carro esta vacio!<br>Carga tus productos</div></div>');
                				}
                				else
                				{
                					console.info("Eliminado");
                					del.parents('.sc-cart-itemr').fadeOut("normal");
                				}
                				$('.sc-cart-summaryr').html(total);
                			}
                			else
                			{
                				error(msn);
                			}
                		},"json")               		
                		
            		});

				$('#tbresumen').on("change", '.sc-cart-item-qtyr', function (e) 
				{
					e.preventDefault();
					var cant = $(this).val();
					var min = $(this).attr('min');
					var uni = $(this).attr('data-unidad');

					if(cant<=0)
					{
						error("Indicar una cantidad mayor a cero");					
					}
					else
					{
						var iddetalle = $(this).parents('.sc-cart-itemr').find('.sc-cart-remover').attr('data-detalle');
						var del = $(this);

						$.post('funciones/pedidos/fnPedidos.php', {opcion: 'ACTUALIZARPRODUCTO',iddetalle:iddetalle,cant:cant,min:min,uni:uni}, function(data, textStatus, xhr) {
							 var res = data[0].res;
							 var msn = data[0].msn;
							 var total = data[0].total;
							 var subtotal = data[0].subtotal;
							 var totalpedido = data[0].totalpedido;
							 if(res=="ok")
							 {
							 	$('.sc-cart-summaryr').html(total);
							 	del.parents('.sc-cart-itemr').find('.sc-cart-item-amountr').html(subtotal);
							 	$('#totalpedido').html(totalpedido);

							 }
							 else
							 {
							 	error(msn);
							 }
						},"json");
						
					}
				});

    		});
   	  </script>
   	  <?php
   	  echo "<style onload=INICIALIZARCONTENIDO()></style>";
   }
   else if($opcion=="CANCELARPEDIDO")
   {
   		
   		//eliminar detalle 
   		$deldetalle = pg_query($dbconn, "DELETE FROM tbl_pedidos_detalle WHERE ped_clave_int = '".$idpedido."'");
   		if($deldetalle>0)
   		{
   			$delpedido = pg_query($dbconn,"DELETE FROM tbl_pedidos WHERE ped_clave_int = '".$idpedido."'");
   			if($delpedido>0)
   			{
   				$res = "ok";
   				$msn = "Has cancelado tu pedido";
   			}
   			else
   			{
   				$res = "error";
   				$msn = "Surgió un error al cancelar pedido. Error BD(".pg_last_error($dbconn).")";
   			}
   		}
   		else
   		{
   			$res = "error";
   			$msn = "Surgió un error al eliminar el detalle del pedido. Error BD(".pg_last_error($dbconn).")";
   		}
   		$datos[] = array("res"=>$res,"msn"=>$msn);
   		echo json_encode($datos);
   }
   else if($opcion=="PEDIDOS")
   {
   	  //PERFIL
   	  $estado = $_POST['estado'];
   	  if($perfil=="2")
   	  {
   	  	$concliente = pg_query($dbconn, "SELECT u.usu_clave_int,u.usu_nombre,usu_email FROM tbl_usuario u JOIN tbl_pedidos p on u.usu_clave_int = p.usu_clave_int join tbl_direcciones d on d.dir_clave_int = p.dir_clave_int JOIN tbl_barrio b on b.bar_clave_int = d.dir_clave_int JOIN tbl_sector s on s.sec_clave_int = b.sec_clave_int WHERE p.ped_estado = '".$estado."' and s.sec_clave_int IN(SELECT sec_clave_int FROM tbl_usuario_sector where usu_clave_int = '".$idUsuario."')  group by u.usu_clave_int");
   	  } 
   	  else if($perfil=="1")
   	  {
   	  		$concliente = pg_query($dbconn, "SELECT u.usu_clave_int,u.usu_nombre,usu_email FROM tbl_usuario u JOIN tbl_pedidos p on u.usu_clave_int = p.usu_clave_int join tbl_direcciones d on d.dir_clave_int = p.dir_clave_int JOIN tbl_barrio b on b.bar_clave_int = d.bar_clave_int JOIN tbl_sector s on s.sec_clave_int = b.sec_clave_int WHERE p.ped_estado = '".$estado."' group by u.usu_clave_int");
   	  }
   	  else if($perfil=="3")
   	  {
   	  		$concliente = pg_query($dbconn, "SELECT u.usu_clave_int,u.usu_nombre,usu_email FROM tbl_usuario u JOIN tbl_pedidos p on u.usu_clave_int = p.usu_clave_int join tbl_direcciones d on d.dir_clave_int = p.dir_clave_int JOIN tbl_barrio b on b.bar_clave_int = d.bar_clave_int JOIN tbl_sector s on s.sec_clave_int = b.sec_clave_int WHERE p.ped_estado = '".$estado."' and u.usu_clave_int=  '".$idUsuario."' group by u.usu_clave_int");
   	  }
   	  $numcliente = pg_num_rows($concliente);
   		
   	  //echo $numcliente;
   	   ?>
   	   
   	   	<div class="col-md-4 <?php if($perfil==3){ echo "hide";}?>">
   	   		<label>Cliente:</label>
   	   		<select name="selcliente" id="selcliente" class="form-control selectpicker" multiple title="Seleccionar cliente" onchange="CRUDPEDIDOS('VERPEDIDOS','')">
   	   			
   	   			<?php
   	   			while($dat = pg_fetch_array($concliente))
   	   			{
   	   				?>
   	   				<option <?php if($idUsuario==$dat['usu_clave_int']){ echo "selected";} ?> data-subtext='<?php echo $dat['usu_email'];?>' value="<?php echo $dat['usu_clave_int'];?>"><?php echo $dat['usu_nombre']; ?></option>
   	   				<?php
   	   			}
   	   			?>
   	   			
   	   		</select>
   	   	</div>
   	  
   	   <?php
   	   echo "<style onload=INICIALIZARCONTENIDO()></style>";
   	   echo "<style onload=CRUDPEDIDOS('VERPEDIDOS','')></style>";
   }
   else if($opcion=="VERPEDIDOS")
   {
   	  $estado = $_POST['estado'];
   	  $lista = $_POST['lista'];
   	  $cliente = $_POST['cliente']; $cliente = implode(', ', (array)$cliente); 
   	  if($cliente==""){$cliente1="'0'";}else {$cliente1=$cliente;}
   	  ?>
   	  <hr>
   	  <?php
   	  if($perfil=="2")
   	  {
   	  	$wh = "";
   	  	if($estado==2 || $estado==3) 
   	  	{
   	  		$wh.=" and p.ped_domiciliario = '".$idUsuario."'";
   	  	}
   	  	$concliente = pg_query($dbconn, "SELECT p.ped_clave_int,p.ped_fecha,p.ped_estado,u.usu_clave_int,u.usu_nombre,usu_email,usu_apellido,ped_total,ped_domicilio,ped_tiempo_entrega,ped_codigo,usu_imagen,d.dir_descripcion des,d.dir_detalle as det,s.sec_nombre as sec,b.bar_nombre as bar,dir_nomenclatura,dir_letra,dir_numero1,dir_numero2,dir_tipo,dir_tipo_icon,ped_lista_deseo FROM tbl_usuario u JOIN tbl_pedidos p on u.usu_clave_int = p.usu_clave_int join tbl_direcciones d on d.dir_clave_int = p.dir_clave_int JOIN tbl_barrio b on b.bar_clave_int = d.dir_clave_int JOIN tbl_sector s on s.sec_clave_int = b.sec_clave_int WHERE p.ped_estado = '".$estado."' and s.sec_clave_int IN(SELECT sec_clave_int FROM tbl_usuario_sector where usu_clave_int = '".$idUsuario."') and ( u.usu_clave_int IN(".$cliente1.") OR '".$cliente."' IS NULL OR '".$cliente."' = '') ".$wh." order by p.ped_fecha ASC");
   	  } 
   	  else if($perfil=="1")
   	  {
   	  		$concliente = pg_query($dbconn, "SELECT p.ped_clave_int,p.ped_fecha,p.ped_estado,u.usu_clave_int,u.usu_nombre,usu_email,usu_apellido,ped_total,ped_domicilio,ped_tiempo_entrega,ped_codigo,usu_imagen,d.dir_descripcion des,d.dir_detalle as det,s.sec_nombre as sec,b.bar_nombre as bar,dir_nomenclatura,dir_letra,dir_numero1,dir_numero2,dir_tipo,dir_tipo_icon,ped_lista_deseo FROM tbl_usuario u JOIN tbl_pedidos p on u.usu_clave_int = p.usu_clave_int join tbl_direcciones d on d.dir_clave_int = p.dir_clave_int JOIN tbl_barrio b on b.bar_clave_int = d.bar_clave_int JOIN tbl_sector s on s.sec_clave_int = b.sec_clave_int WHERE p.ped_estado = '".$estado."' and ( u.usu_clave_int IN(".$cliente1.") OR '".$cliente."' IS NULL OR '".$cliente."' = '') order by p.ped_fecha ASC");
   	  }
   	  else if($perfil=="3")
   	  {
   	  		if($lista==1)
   	  		{
   	  			$concliente = pg_query($dbconn, "SELECT p.ped_clave_int,p.ped_fecha,p.ped_estado,u.usu_clave_int,u.usu_nombre,usu_email,usu_apellido,ped_total,ped_domicilio,ped_tiempo_entrega,ped_codigo,usu_imagen,d.dir_descripcion des,d.dir_detalle as det,s.sec_nombre as sec,b.bar_nombre as bar,dir_nomenclatura,dir_letra,dir_numero1,dir_numero2,dir_tipo,dir_tipo_icon,ped_lista_deseo FROM tbl_usuario u JOIN tbl_pedidos p on u.usu_clave_int = p.usu_clave_int join tbl_direcciones d on d.dir_clave_int = p.dir_clave_int JOIN tbl_barrio b on b.bar_clave_int = d.bar_clave_int JOIN tbl_sector s on s.sec_clave_int = b.sec_clave_int WHERE  ( u.usu_clave_int IN(".$cliente1.") OR '".$cliente."' IS NULL OR '".$cliente."' = '') and u.usu_clave_int=  '".$idUsuario."' and p.ped_lista_deseo = 1 order by p.ped_fecha ASC");
   	  		}
   	  		else
   	  		{
   	  			$concliente = pg_query($dbconn, "SELECT p.ped_clave_int,p.ped_fecha,p.ped_estado,u.usu_clave_int,u.usu_nombre,usu_email,usu_apellido,ped_total,ped_domicilio,ped_tiempo_entrega,ped_codigo,usu_imagen,d.dir_descripcion des,d.dir_detalle as det,s.sec_nombre as sec,b.bar_nombre as bar,dir_nomenclatura,dir_letra,dir_numero1,dir_numero2,dir_tipo,dir_tipo_icon,ped_lista_deseo FROM tbl_usuario u JOIN tbl_pedidos p on u.usu_clave_int = p.usu_clave_int join tbl_direcciones d on d.dir_clave_int = p.dir_clave_int JOIN tbl_barrio b on b.bar_clave_int = d.bar_clave_int JOIN tbl_sector s on s.sec_clave_int = b.sec_clave_int WHERE p.ped_estado = '".$estado."' and ( u.usu_clave_int IN(".$cliente1.") OR '".$cliente."' IS NULL OR '".$cliente."' = '') and u.usu_clave_int=  '".$idUsuario."' order by p.ped_fecha ASC");
   	  	    }
   	  }
   	  $numcliente = pg_num_rows($concliente);
   	 
   	  $sql = "SELECT p.ped_clave_int,p.ped_fecha,p.ped_estado,u.usu_clave_int,u.usu_nombre,usu_email,usu_apellido,ped_total,ped_domicilio,ped_tiempo_entrega,ped_codigo FROM tbl_usuario u JOIN tbl_pedidos p on u.usu_clave_int = p.usu_clave_int join tbl_direcciones d on d.dir_clave_int = p.dir_clave_int JOIN tbl_barrio b on b.bar_clave_int = d.bar_clave_int JOIN tbl_sector s on s.sec_clave_int = b.sec_clave_int WHERE p.ped_estado = '".$estado."' and ( u.usu_clave_int IN(".$cliente1.") OR '".$cliente."' IS NULL OR '".$cliente."' = '') group by p.ped_fecha ASC";

   	  while($dat = pg_fetch_array($concliente))
   	  {
   	  	$ped = $dat['ped_clave_int'];
   	  	$codped = $dat['ped_codigo'];
   	  	$usucliente = $dat['usu_clave_int'];
   	  	$nomcliente = $dat['usu_nombre']." ".$dat['usu_apellido'];
   	  	$emacliente=  $dat['usu_email'];
   	  	$imacliente = $dat['usu_imagen'];
   	  	if($imacliente=="" || $imacliente==NULL)
		{
		  $imacliente = "dist/img/default-user.png";
		}
   	  	$total = $dat['ped_total'];
   	  	$fec = $dat['ped_fecha'];
   	  	$domicilio = $dat['ped_domicilio'];
   	  	$totalpedido =  $total + $domicilio; 
   	  	$est = $dat['ped_estado']; 	  	
   	  	$sec=  $dat['sec'];
		$bar = $dat['bar'];
		$des = $dat['des'];
		$lis = $dat['ped_lista_deseo'];
		$direccion = $dat['dir_nomenclatura']." ".$dat['dir_letra']." #".$dat['dir_numero1']."-".$dat['dir_numero2'].",".$bar;
		$ico = $dat['dir_tipo_icon'];

		$conpro = pg_query($dbconn, "SELECT count(pde_clave_int) cant FROM tbl_pedidos_detalle WHERE ped_clave_int = '".$ped."'");
		$datpro = pg_fetch_array($conpro);
		$cantpro = $datpro['cant']; if($cantpro<=0){ $cantpro = 0;}
   	  	?>

   	  	<div class="col-md-4">
          <!-- Widget: user widget style 1 -->
          <div class="box box-widget widget-user">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-aqua-active">
              <h3 class="widget-user-username"><?php echo $nomcliente;?></h3>
              <h5 class="widget-user-desc"><?php echo $emacliente;?></h5>
              <h6 class="widget-user-desc">Orden: <?php echo $codped;?></h5>
            </div>
            <div class="widget-user-image">
              <img style="width:100px;height:100px" class="img-circle" src="<?php echo $imacliente;?>" alt="User Avatar">
            </div>
            <div class="box-footer" >
              <div class="row">
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h5 class="description-header">$ <?php echo number_format($totalpedido,0,',',',')?></h5>
                    <span class="description-text">TOTAL</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h5 class="description-header"><?PHP echo date('Y-m-d',strtotime($fec));?></h5>
                    <span class="description-text">FECHA</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-4">
                  <div class="description-block">
                    <h5 class="description-header"><?PHP echo $cantpro;?></h5>
                    <span class="description-text">PRODUCTOS</span>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
            <?php 
            $opc = "";
            	if($est==0 || $est==3 || $est==4)
            	{ 
            		$dis = "disabled"; 
            		$vis = " hide";
            	}
            	else
            	{ 
            		$dis = "";
            		$vis = "";
            	}
            	if($perfil==2 and $est==1)
            	{
            		$txt = "Tomar pedido";
            		$bg = "success";
            		$opc = "TOMARPEDIDO";
            	}
            	else if($perfil==1 and $est==1)
            	{
            		$txt = "Asignar pedido";
            		$bg = "success";
            		$opc = "ASIGNARPEDIDO";
            	}
            	else if($est==2 and $perfil=="2")
            	{
            		$veri = pg_query($dbconn, "SELECT ped_clave_int FROM tbl_pedidos where ped_estado = 2 and ped_domiciliario = '".$idUsuario."'");
            		$numv = pg_num_rows($veri);
            		if($numv>0)
            		{
            			$opc = "CERRARPEDIDO";
            			$txt = "Terminar entrega";
            			$bg = "lime";
            		}
            		else
            		{
            			$opc = "";
            			$txt = "Pedido tomado";
            			$bg = "orange";
            			$dis = "disabled";
            		}
            		
            	}
            	else if($est==1 and $perfil=="3")
            	{
            		$txt = "Pendiente Confirmación";
            		$bg = "warning";
            		$dis = "disabled";
            	}
            	else if($est==2 and $perfil=="3")
            	{
            		$txt = "En proceso de entrega";
            		$bg = "success";
            		$dis = "disabled";
            	}
            	else if($est==2 and $perfil==1)
            	{
            		$txt = "En proceso de entrega";
            		$bg = "success";
            		$dis = "disabled";
            	}
            	else if($est=="3")
            	{
            		$txt = "Entregado";
            		$bg = "green";

            	}
            	else if($est==4)
            	{
            		$txt = "Cancelado";
            		$bg = "danger";
            	}
            ?>
            <div class="box-footer" style="padding-top: 0 !important">
            	<a class="btn btn-defautl"><i class="fa fa-map-marker" aria-hidden="true"></i></a><?php echo $direccion;?>
            </div>
            <div class="box-footer" style="padding-top: 0 !important">
            	
            	
            			<div class="btn-group">
            				<button onclick="CRUDPEDIDOS('<?PHP echo $opc;?>','<?php echo $ped;?>')"  type="button" class="btn bg-<?php echo $bg;?>"><?php echo $txt;?></button>
            	<button  type="button" class="btn btn-info" onclick="CRUDPEDIDOS('VERDETALLE','<?php echo $ped;?>')">Ver detalle</button>
            	<?php if($lis==1  || $est==2 || $est==1)
            	{ ?>
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                          <i class="fa fa-ellipsis-v"></i>
                        </button>
                        <ul class="dropdown-menu">
                          <li class="<?php if($usucliente!=$idUsuario || $lis!=1){ echo 'hide'; }?>""><a onclick="CRUDPEDIDOS('ANADIRLISTA','<?php echo $ped; ?>')" class="btn btn-default text-black"><i class="fa fa-list-ul"></i>Añadir lista a pedido</a></li>
                          <?php if($perfil==1){ ?>
                          <li><a class="btn btn-default text-red" onclick="ANULARPEDIDO('<?php echo $ped;?>')"><i class="fa fa-ban"></i>Anular Pedido</a></li>
                      	 <?php } ?>
                        </ul>	
                        <?php
            	}
            	?>
                      </div>
            	
            </div>
          </div>
          <!-- /.widget-user -->
        </div>
   	  	<?php
   	  }
   }
   else if($opcion=="VERDETALLE")
   {
		$idpedido = $_POST['idpedido'];
 		$coninfo = pg_query($dbconn,"select u.usu_nombre,u.usu_apellido,u.usu_usuario,p.ped_fecha,u.usu_email,p.dir_clave_int,p.ped_domicilio,p.ped_telefono,ped_codigo,p.ped_tiempo_entrega,p.ped_estado,ped_inicio,ped_final from tbl_pedidos p JOIN tbl_usuario u ON u.usu_clave_int = p.usu_clave_int WHERE p.ped_clave_int = '".$idpedido."'");
		$datinfo = pg_fetch_array($coninfo);
		$cliente = $datinfo['usu_nombre']." ".$datinfo['usu_apellido'];
		$pedfecha = $datinfo['ped_fecha'];
		$email = $datinfo['usu_email'];
		$dir = $datinfo['dir_clave_int'];
		$domicilio = $datinfo['ped_domicilio'];
		$telefono = $datinfo['ped_telefono'];
		$codigoped = $datinfo['ped_codigo'];
		$estped=  $datinfo['ped_estado'];
		$tiempo = $datinfo['ped_tiempo_entrega']." min";
		$inicio = $datinfo['ped_inicio'];
		$final = $datinfo['ped_final'];
		$condir = pg_query($dbconn,"select s.sec_nombre,b.bar_nombre,s.sec_domicilio,d.dir_nomenclatura,d.dir_letra,d.dir_numero1,d.dir_numero2,d.dir_detalle from tbl_direcciones d join tbl_barrio b on b.bar_clave_int = d.bar_clave_int join tbl_sector s on s.sec_clave_int = b.sec_clave_int where d.dir_clave_int = '".$dir."'");
		$datdir = pg_fetch_array($condir);
		$sector = $datdir['sec_nombre'];
		$barrio = $datdir['bar_nombre'];
		$nome = $datdir['dir_nomenclatura'];
		$letra = $datdir['dir_letra'];
		$nume1 = $datdir['dir_numero1'];
		$nume2 = $datdir['dir_numero2'];
		$direccion = $nome." ".$letra."#".$nume1."-".$nume2;
	   	 

		$condet = pg_query($dbconn, "SELECT d.pde_clave_int,p.pro_clave_int,p.pro_nombre,p.pro_descripcionbreve,p.pro_codigo,c.cla_nombre,pde_estado,pde_tamano,cla_nombre,pde_cantidad,pde_valor,pde_unidad,pro_uni_venta,pro_pes_venta,pro_mu_venta,pro_mp_venta,pde_minimo,pde_unidad ,pde_codigo from tbl_pedidos_detalle d JOIN tbl_productos p on p.pro_clave_int = d.pro_clave_int join tbl_clasificacion c on c.cla_clave_int = d.cla_clave_int where d.ped_clave_int = '".$idpedido."' order by p.pro_nombre,c.cla_nombre");
		$numdet = pg_num_rows($condet);

   	  ?>
   	 
   	  <div id="divresumenpedido" style="width: 820px;background:#FFF url('dist/img/plantillas/pestana1.jpg') no-repeat top center;font-family: calibri;font-size: 12px;padding: 10px">
   	  
   	  	<table width="100%" style="margin-top: 0px;font-size: 11px;" cellpadding="2" cellspacing="2">   	  		
   	  		<tr><th colspan="6" style="font-size: 14px;font-weight: bold; text-align: left;">Numero de orden: <?php echo $codigoped; ?></th></tr>
   	  		<tr>
   	  			<th style="text-align: left;font-weight: bold;">Cliente:</th><th style="text-align: left"><?php  echo $cliente;?></th>
   	  			<th style="text-align: left;font-weight: bold;">Fecha de pedido:</th><th style="text-align: left"><?php echo $fecha;?></th>
   	  			<th style="text-align: left;font-weight: bold;">Tiempo entrega:</th><th style="text-align: left"><?php echo $tiempo;?></th>
   	  		</tr>
   	  		<tr>
   	  			<th style="text-align: left;font-weight: bold;">Email:</th><th style="text-align: left"><?php echo $email;?></th>
   	  			<th style="text-align: left;font-weight: bold;">Telefono:</th><th style="text-align: left"><?php echo $telefono;?></th>
   	  		</tr>
   	  		<tr>
   	  			<th style="text-align: left;font-weight: bold;">Sector:</th><th style="text-align: left"><?php echo $sector;?></th>
   	  			<th style="text-align: left;font-weight: bold;">Barrio:</th><th style="text-align: left"><?php echo $barrio;?></th>
   	  			<th style="text-align: left;font-weight: bold;">Dirección:</th><th style="text-align: left"><?php echo $direccion;?></th>
   	  		</tr>
   	  		<?php if($estped==2 || $estped==3) { ?>
   	  		<tr>
   	  			<th style="text-align: left;font-weight: bold;">Hora Confirmación:</th><th style="text-align: left"><?php echo $inicio;?></th>
   	  			<th style="text-align: left;font-weight: bold;">Hora Entrega:</th><th style="text-align: left"><?php echo $final;?></th>
   	  			
   	  		</tr>
   	  	<?php } ?>
   	  		<tr><th colspan="6"><hr></th></tr>
   	  		<tr><th colspan="6">
   	  			<?php 
    	if($numdet<=0)
    	{ 
    	
    	}
    	else
    	{ 
    		?>
   	  			<table style="font-size: 10px; width: 100%;border: 1px solid #f4f4f4" cellspacing="2" cellpadding="2" border="1">  	  				
   	  			
   	  				<tr>
   	  					<th style="text-align: center; font-weight:bold;background-color: #dff0d8">Linea</th>
	   	  				<th style="text-align: center; font-weight:bold;background-color: #dff0d8">Producto</th>
	   	  				<th style="text-align: center; font-weight:bold;background-color: #dff0d8">Descripción</th>	
	   	  				<th style="text-align: center; font-weight:bold;background-color: #dff0d8">Precio</th>   	  				
	   	  				<th style="text-align: center; font-weight:bold;background-color: #dff0d8">Cantidad</th>
	   	  				<th style="text-align: center; font-weight:bold;background-color: #dff0d8">Unidad Medida</th>
	   	  				<th style="text-align: center; font-weight:bold;background-color: #dff0d8">Observación</th>
	   	  				<th style="text-align: center; font-weight:bold;background-color: #dff0d8">Costo</th>	   	  				
	   	  			</tr>
	   	  			<?php
	   	  			$total = 0;
		    		while($datdet= pg_fetch_array($condet))
		    		{
		    			$iddetalle = $datdet['pde_clave_int'];
		    			$idp = $datdet['pro_clave_int'];
		    			$codlinea = $datdet['pde_codigo'];
		    			$nompro = $datdet['pro_nombre'];
		    			$codpro = $datdet['pro_codigo'];
		    			$nomcla = $datdet['cla_nombre'];
		    			$maduracion = $datdet['pde_estado'];
		    			$descp = $datdet['pro_descripcionbreve'];
		    			$precio = $datdet['pde_valor'];
		    			$cantidad = $datdet['pde_cantidad'];
		    			$subtotal = $precio * $cantidad;
		    			$tamano = $datdet['pde_tamano'];
		    			$observacion = "<strong>Clasificación:</strong>".$nomcla." <br>";
		    			if($tamano!=""){$observacion.="<strong>Tamano:</strong>".$tamano."<br> ";}
		    			if($maduracion!=""){$observacion.="<strong>Maduración:</strong>".$maduracion." ";}
		    			if($tamano!=""){ $tamano = '-'.$tamano;}
		    			
		    			$puv = $datdet['pro_uni_venta'];
		    			$ppv = $datdet['pro_pes_venta'];
		    			$pmuv = $datdet['pro_mu_venta'];
		    			$pmpv = $datdet['pro_mp_venta'];
		    			$min = $datdet['pde_minimo'];
		    			$uni = $datdet['pde_unidad'];
		    			if($uni==0)
		    			{
		    				$unimin = $pmuv ;
		    				$cantidad = $cantidad;
		    			}
		    			else
		    			{
		    				$unimin = $pmpv;
		    				$cantidad = $cantidad * $min;//esto debido a que el precio del producto cuando es en peso es precio por la unidad minima ejemplo:250gr = 1 unidad
		    			}

						$con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idp."' and pri_activo = 1 LIMIT 1");
						$dat = pg_fetch_row($con);
						$cimg = $dat[0];
						$img = $dat[1];
						?>
						<tr>
							<td style="text-align: left"><?php echo $codlinea;?></td>
							<td style="text-align: left"><?php echo $codpro;?></td>
							<td style="text-align: left"><?php echo $nompro;?></td>
							<td style="text-align: right">	<?php echo number_format($precio,0,',',',');?></td>
							<td style="text-align: center"><?php echo $cantidad;?></td>
							<td ><?php if($uni==1){ echo "GR";}else { echo "UND";} ?></td>
							<td style="text-align: left"><?php echo $observacion;?></td>
							<td style="text-align: right; font-weight:bold"> <?php echo number_format($subtotal,0,',',',');?></td>
							
						</tr>
						<?php
						$total = $total + $subtotal;
					}
					$totalpedido = $total + $domicilio;
					?>
					<tr>
						<td colspan="6" rowspan="4"></td><td style="text-align: right; font-weight:bold">Sub Total:</td><td colspan="2" style="text-align: right"><?php echo number_format($total,0,',',',');?></td></tr>
					<tr>
						<td style="text-align: right; font-weight:bold">Domicilio:</td><td colspan="2" style="text-align: right"><?php echo number_format($domicilio,0,',',',');?></td>
					</tr>
					<tr>
						<td style="text-align: right; font-weight:bold">Total:</td><td colspan="2" style="text-align: right"><?php echo number_format($totalpedido,0,',',',');?></td>
					</tr>					
   	  			</table>
   	  			<?php
   	  		}
   	  		?>
   	  		</th>
   	  		</tr>
   	  		
   	  	</table>
   	  </div>
   	  <?php
   }
   else if($opcion=="TOMARPEDIDO")
   {
   		$id = $_POST['id'];
   		$verip = pg_query($dbconn,"SELECT * FROM tbl_pedidos where ped_estado = 2 and ped_domiciliario = '".$idUsuario."'");
   		$nump = pg_num_rows($verip);

   		if($nump>0)
   		{
   			$res = "error";
   			$msn = "Todavia tiene un pedido en proceso de entrega";
   		}
   		else
   		{
	   		$updatepedido = pg_query($dbconn, "UPDATE tbl_pedidos SET ped_estado = '2',ped_domiciliario= '".$idUsuario."',ped_inicio='".$fecha."' where ped_clave_int='".$id."'");
	   		if($updatepedido>0)
	   		{
	   			$res = "ok";
	   			$msn = "Pedido tomado correctamente";
	   		}
	   		else
	   		{
	   			$res = "error";
	   			$msn = "Surgió un error al tomar pedido. Error BD(".pg_last_error($dbconn).")";
	   		}
   		}
   		$datos[] = array("res"=>$res,"msn"=>$msn);
   		echo json_encode($datos);

   }
   else if($opcion=="CERRARPEDIDO")
   {
   		$id = $_POST['id'];   		

   		$updatepedido = pg_query($dbconn, "UPDATE tbl_pedidos SET ped_estado = '3',ped_domiciliario= '".$idUsuario."',ped_final='".$fecha."' where ped_clave_int='".$id."'");
   		if($updatepedido>0)
   		{
   			$res = "ok";
   			$msn = "Pedido entregado correctamente";
   		}
   		else
   		{
   			$res = "error";
   			$msn = "Surgió un error al cerrar entrega de pedido. Error BD(".pg_last_error($dbconn).")";
   		}
   		
   		$datos[] = array("res"=>$res,"msn"=>$msn);
   		echo json_encode($datos);

   }
   else if($opcion=="ASIGNARPEDIDO")
   {
   	$id = $_POST['idpedido'];
   	$conp = pg_query($dbconn,"select s.sec_clave_int from tbl_pedidos p join tbl_direcciones d on d.dir_clave_int = p.dir_clave_int join tbl_barrio b on b.bar_clave_int = d.bar_clave_int join tbl_sector s on s.sec_clave_int = b.sec_clave_int where p.ped_clave_int = '".$id."' limit 1");
   	$datp = pg_fetch_array($conp);
   	$sec = $datp['sec_clave_int'];
   	?>
   	<div class="row">
   		<div class="col-md-12">
   			<label for="seldomiciliario">Domiciliario:
   				
   			</label>
   			<select name="seldomiciliario" id="seldomiciliario" class="form-control selectpicker" title="Seleccionar domiciliario">
   				<?php
   				$condo = pg_query($dbconn,"SELECT u.usu_clave_int,u.usu_nombre,u.usu_email,usu_imagen,usu_apellido from tbl_usuario u join tbl_usuario_sector s on s.usu_clave_int = u.usu_clave_int where s.sec_clave_int = '".$sec."'");
   				while($datdo = pg_fetch_array($condo))
   				{
   					$img = $datdo['usu_imagen'];
   					$idu = $datdo['usu_clave_int'];
   					$ema = $datdo['usu_email'];
   					$nom = $datdo['usu_nombre']." ".$datdo['usu_apellido'];
   					
                    ?>
                        <option  value="<?php echo $idu;?>" data-content="<?php echo $nom;?><img class='pull-right' src='<?php echo $img;?>'  width='30'/>" data-subtext="<?php echo $ema;?>"><?php echo $nom;?></option>
   					<?php
   				}
   				?>

   			</select>
   		</div>
   		<div class="col-md-12"><button onclick="CRUDPEDIDOS('GUARDARASIGNACION','<?PHP echo $id;?>')" type="button" class="btn btn-success btn-block">Guardar asignación</button></div>
   	</div>
   	<?php
   	   echo "<style onload=INICIALIZARCONTENIDO()></style>";
   }
   else if($opcion=="GUARDARASIGNACION")
   {
   		$id = $_POST['id'];
   		$dom = $_POST['dom'];
   		$verip = pg_query($dbconn,"SELECT * FROM tbl_pedidos where ped_estado = 2 and ped_domiciliario = '".$dom."'");
   		$nump = pg_num_rows($verip);

   		if($nump>0)
   		{
   			$res = "error";
   			$msn = "El domiciliario seleccionado todavia tiene un pedido en proceso de entrega";
   		}
   		else
   		{
	   		$updatepedido = pg_query($dbconn, "UPDATE tbl_pedidos SET ped_estado = '2',ped_domiciliario= '".$dom."',ped_inicio='".$fecha."' where ped_clave_int='".$id."'");
	   		if($updatepedido>0)
	   		{
	   			$res = "ok";
	   			$msn = "Pedido asignado correctamente";
	   		}
	   		else
	   		{
	   			$res = "error";
	   			$msn = "Surgió un error al asignar el pedido. Error BD(".pg_last_error($dbconn).")";
	   		}
   		}
   		$datos[] = array("res"=>$res,"msn"=>$msn);
   		echo json_encode($datos);
   }
   else if($opcion=="ANADIRLISTA")
   {
   	 	$idped = $_POST['id'];
   	 	$msn = "";
   	 //VERIFICAR SI TIENE UN PEDIDO ACTIVO
   	 	$conped = pg_query($dbconn, "SELECT ped_clave_int FROM tbl_pedidos WHERE usu_clave_int = '".$idUsuario."' and ped_estado = 0");
    	$numped = pg_num_rows($conped);
    	if($numped>0)
    	{
    		$datped = pg_fetch_array($conped);
    		$idpedido = $datped['ped_clave_int'];
    	}
    	else
    	{
    		$insertped = pg_query($dbconn,"INSERT INTO tbl_pedidos(usu_clave_int,ped_usu_actualiz,ped_fec_actualiz,ped_ip) VALUES('".$idUsuario."','".$usuario."','".$fecha."','".$IP."')");
    		$uid = pg_fetch_row(pg_query($dbconn,"SELECT setval('ped_id_seq',nextval('ped_id_seq')-1) as id;"));
			$idpedido = $uid[0];
    	}
    	$codpedido = sprintf('%010d',$idpedido);

    	$conlista = pg_query($dbconn,"select * from tbl_pedidos_detalle where ped_clave_int = '".$idped."'");
    	$numlista = pg_num_rows($conlista);
    	$inser = 0;
    	for($nl=0;$nl<$numlista;$nl++)
    	{
    		$datl = pg_fetch_array($conlista);
    		$idproducto = $datl['pro_clave_int'];
    		$idmercado = $datl['mer_clave_int'];
    		$idclasificacion = $datl['cla_clave_int'];
    		$tam = $datl['pde_tamano'];
    		$mad = $datl['pde_estado'];
    		$pre = $datl['pde_valor'];
    		$uni = $datl['pde_unidad'];
    		$can = $datl['pde_cantidad'];
    		$cantidad = $datl['pde_cantidad_unidad'];
    		$peso = $datl['pde_cantidad_peso'];
    		$min = $datl['pde_minimo'];
    		$veridet = pg_query($dbconn,"SELECT * FROM tbl_pedidos_detalle WHERE pro_clave_int = '".$idproducto."' and mer_clave_int = '".$idmercado."' and cla_clave_int = '".$idclasificacion."' and pde_tamano = '".$tam."' and pde_estado='".$mad."' and pde_valor = '".$pre."' and pde_unidad = '".$uni."' and ped_clave_int = '".$idpedido."'");
	    	$numdet = pg_num_rows($veridet);
	    	if($numdet>0)
	    	{
	    		$datdet = pg_fetch_array($veridet);
	    		$iddet = $datdet['pde_clave_int'];	    		
	    		$upd = pg_query($dbconn,"UPDATE tbl_pedidos_detalle SET pde_cantidad= pde_cantidad + '".$can."',pde_usu_actualiz ='".$usuario."',pde_cantidad_unidad = pde_cantidad_unidad + '".$cantidad."', pde_cantidad_peso = pde_cantidad_peso + '".$peso."',pde_fec_actualiz = '".$fecha."' WHERE pde_clave_int = '".$iddet."'");
	    	}
	    	else
	    	{
    			$sql = pg_query($dbconn, "INSERT INTO tbl_pedidos_detalle(mer_clave_int,cla_clave_int,pro_clave_int,pde_cantidad,pde_valor,pde_usu_actualiz,pde_fec_actualiz,pde_estado,pde_tamano,ped_clave_int,pde_cantidad_unidad,pde_cantidad_peso,pde_unidad,pde_minimo) VALUES('".$idmercado."','".$idclasificacion."','".$idproducto."','".$can."','".$pre."','".$usuario."','".$fecha."','".$mad."','".$tam."','".$idpedido."','".$cantidad."','".$peso."','".$uni."','".$min."')"); 
    		}
    		//$sql1 = "INSERT INTO tbl_pedidos_detalle(mer_clave_int,cla_clave_int,pro_clave_int,pde_cantidad,pde_valor,pde_usu_actualiz,pde_fec_actualiz,pde_estado,pde_tamano,ped_clave_int,pde_cantidad_unidad,pde_cantidad_peso,pde_unidad,pde_minimo) VALUES('".$idmercado."','".$idclasificacion."','".$idproducto."','".$can."','".$pre."','".$usuario."','".$fecha."','".$mad."','".$tam."','".$idpedido."','".$cantidad."','".$peso."','".$uni."','".$min."')";

	    	if($sql>0 || $upd>0)
	    	{
	    		if($sql>0)
	    		{
		    		$uidd = pg_fetch_row(pg_query($dbconn,"SELECT setval('pde_id_seq',nextval('pde_id_seq')-1) as id;"));
					$iddet = $uidd[0];
				}

	    		$coddetalle = sprintf('%010d',$iddet);
	    		$upddet = pg_query($dbconn, "UPDATE tbl_pedidos_detalle SET pde_codigo='".$coddetalle."' where pde_clave_int = '".$iddet."'");
	    		$inser++;
	    		$msn.= "";
	    	}
	    	else
	    	{
	    		//$res = "error";
	    		$msn.= "La cantidad no se agrego. Error BD(".pg_last_error($dbconn).")";
	    		
	    	}
    	}
    	if($inser==$numlista)
    	{
			$sqltot = pg_query($dbconn, "SELECT sum(pde_cantidad*pde_valor) as tot,count(pde_clave_int) AS cant from tbl_pedidos_detalle where ped_clave_int = '".$idpedido."'");
			$dattot = pg_fetch_array($sqltot);
			$tot = $dattot['tot'];
			$cantactual = $dattot['cant'];
			$sqlupd = pg_query($dbconn,"UPDATE tbl_pedidos SET ped_total = '".$tot."',ped_codigo = '".$codpedido."' WHERE ped_clave_int = '".$idpedido."'");
    		$res = "ok";
    	}
    	else
    	{
    		$res = "error";
    		$cantactual = 0;

    	}
    	$datos[] = array('res' => $res , 'msn' => $msn ,'idpedido' => $idpedido ,'idUsuario'=>$idUsuario,"can"=>$cantactual);
    	echo json_encode($datos);

   }
   else if($opcion=="INFOPRODUCTO")
   {
   		$idproducto = $_POST['idproducto'];
   		$canp = $_POST['canp'];
   		$con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idproducto."' and pri_activo = 1 LIMIT 1");
    	$dat = pg_fetch_row($con);
    	$cimg = $dat[0];
    	$img = $dat[1];

    	$sql = "SELECT pro_clave_int,pro_nombre,pro_descripcion,pro_descripcionbreve,pro_uni_venta,pro_pes_venta,pro_mu_venta,pro_mp_venta,pro_estado FROM tbl_productos AS p JOIN tbl_categorias c ON c.cat_clave_int = p.cat_clave_int ";
		$sql.= " WHERE  (p.pro_clave_int = '".$idproducto."') order by pro_nombre ASC";
		$conpro = pg_query($dbconn,$sql);
		$datp = pg_fetch_array($conpro);
						
		//$idp = $datp['pro_clave_int'];
		$nomp = $datp['pro_nombre'];
		$desc = $datp['pro_descripcion'];
		$descp = $datp['pro_descripcionbreve'];
		$puv = $datp['pro_uni_venta'];
		$ppv = $datp['pro_pes_venta'];
		$pmuv = $datp['pro_mu_venta'];
		$pmpv = $datp['pro_mp_venta'];
		$mad = $datp['pro_estado'];

		if($puv==1)
		{
			$unidadmin = $pmuv;
			$dataunidad = 0;//por unidad
		}
		else if($ppv==1)
		{
			$unidadmin = $pmpv;
			$dataunidad = 1; //venta por peso
		}

		$con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idproducto."' and pri_activo = 1 LIMIT 1");
		$dat = pg_fetch_row($con);
		$cimg = $dat[0];
		$img = $dat[1];

		$conpre = pg_query($dbconn, "select prm_venta from tbl_precios_mercado where pro_clave_int ='".$idproducto."' and cla_clave_int in  (SELECT cla_clave_int FROM tbl_clasificacion where cla_clave_int in(".$clasificacion.") ORDER BY cla_clave_int ASC LIMIT 1) and mer_clave_int = '".$idmercado."'");
		$datpre = pg_fetch_array($conpre);
		$venta = $datpre['prm_venta'];
		if($venta<=0){ $venta = 0;}

		//VEIRIFCAR SI PRODUCTO ESTA EN EL PEDIDO ACTUAL EN PROCESO
		$conpedido = pg_query($dbconn,"select * from tbl_pedidos_detalle where ped_clave_int = '".$idpedido."' and pro_clave_int = '".$idproducto."'");
		$numpedido = pg_num_rows($conpedido);


		//informacion de la oferta
		$conoferta = pg_query($dbconn, "SELECT o.ofe_clave_int,ofe_titulo,ofe_descripcion,ofe_inicio,ofe_fin,ofe_tipo_descuento,ofe_descuento,ofe_estado,ofe_imagen FROM tbl_ofertas o join tbl_ofertas_producto op ON op.ofe_clave_int = o.ofe_clave_int   where o.ofe_estado = 1 and op.pro_clave_int = '".$idproducto."' order by ofe_inicio ASC limit 1");
		$numoferta = pg_num_rows($conoferta);
		if($numoferta>0)
		{
			$datoferta = pg_fetch_array($conoferta);
			$tipo = $datoferta['ofe_tipo_descuento'];
			$descuento = $datoferta['ofe_descuento'];
			if($tipo==1)
			{
				$ventaold = $venta;
				$ventaoferta = $descuento;
				$ahorrooferta = $venta - $ventaoferta;
				$descuentooferta = 100 - ($ventaoferta*100/$venta);
			}
			else if($tipo==2)
			{
				$ventaold = $venta;
				$ventaoferta = $venta - ($venta*$descuento/100);
				$ahorrooferta = $venta - $ventaoferta;
				$descuentooferta = $descueto;
			}
		}
		else
		{
			$ventaold  = 0;
			$ventaoferta = $venta;
		}

    	?>

    	<div class="row sc-product-item infoproducto" >
    		<div class="col-md-6 col-sm-6 col-xs-12 visorImg">
    			<figure class="visor">
    				<?php
					$con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idproducto."'  and pri_activo = 1");
					$num = pg_num_rows($con);
					//if($num>1)
					//{
						for($k=0;$k<$num;$k++)
						{
							$dat = pg_fetch_row($con);
							$cimg = $dat[0];
							$imga = $dat[1];
							?>
							<a href="#" data-image="<?php echo $imga;?>" data-zoom-image="<?php echo $imga;?>">
							<img class="img-thumbnail" id="lupa<?php echo ($k+1);?>"    src="<?php echo $imga;?>" />
							</a>
							<?php
						}
					//}
				?>

    			</figure>
    			<div class="flexslider">
    				<ul class="slides">
    				<?php
					$con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idproducto."'  and pri_activo = 1");
					$num = pg_num_rows($con);
					//if($num>1)
					//{
						for($k=0;$k<$num;$k++)
						{
							$dat = pg_fetch_row($con);
							$cimg = $dat[0];
							$imga = $dat[1];

							echo '<li>
								     	<img value="'.($k+1).'" class="img-thumbnail" src="'.$imga.'" alt="Img Producto">
								    </li>';
						}
					//}
					?>
					</ul>
				</div>
    		</div>
    		<div class="col-md-6 col-sm-6 col-xs-12">
    			<div class="col-xs-6">
    				<h6>						
						<a data-dismiss="modal" aria-label="Close" class="text-muted">	
							<i class="fa fa-reply"></i> Continuar Comprando
						</a>
					</h6>
				</div>
				<div class="clearfix"></div>
				<h3>
				<small class="pricesmalloferta currency2 <?php if($numoferta<=0){ echo ' hide';}?>" ><?php echo $ventaold;?></small>
				<small id="precio_<?php echo $idproducto;?>" style="display: inline-block;" class="pricesmall currency2" ><?php echo $ventaoferta;?></small><br>
				<span data-name="product_name"><?php echo $nomp;?></span></h3>
				<hr/>
				<div class="col-md-12">
					<div class="col-md-8">
					
					   <div class="col-md-12 <?php if(count($arrayclasificacion)<=1){ echo ' hide';}?>"">
			                    <h5>Clasificacion: </h5>
			                	
			                    <select name="product_clasificacion" id="product_clasificacion_<?php echo $idproductp;?>" class="form-control input-sm selectpicker" onchange="CRUDPEDIDOS('CAMBIARPRECIO','<?php echo $idpproducto; ?>',this.value)">
                            	<?php
                            	
                            		for($c=0;$c<count($arrayclasificacion);$c++)
                            		{ 
                            			$concla = pg_query($dbconn,"SELECT * FROM tbl_clasificacion where cla_clave_int = '".$arrayclasificacion[$c]."'");
                            			$datc = pg_fetch_array($concla)
                            			?>
                                    	<option value="<?php echo $arrayclasificacion[$c];?>"><?php echo $datc['cla_nombre'];?></option>	
                                    <?php 
                                	}
                                ?>
                                </select>
	                    </div>
					              
						<div class="col-md-12 <?php if($aplicatamano==1){ }else{ echo ' hide'; } ?>">
						<h5>Selecciona la tamaño de tu producto:</h5>
						<div class="toggle_radio">
								<input data-name="product_tam" name="product_tamano_<?php echo $idproducto;?>" type="radio" class="toggle_option grande_toggle" id="tamano1_<?php echo $idproducto;?>" name="toggle_option" value="Grande">
								<input data-name="product_tam" name="product_tamano_<?php echo $idproducto;?>" type="radio"  class="toggle_option mediano_toggle" id="tamano2_<?php echo $idproducto;?>" name="toggle_option" value="Mediano">
								<input data-name="product_tam" name="product_tamano_<?php echo $idproducto;?>" type="radio" class="toggle_option pequeno_toggle" id="tamano3_<?php echo $idproducto;?>" name="toggle_option" value="Pequeño">
								<label class="info" for="tamano1_<?php echo $idproducto;?>"><p>Grande</p></label>
								<label class="info" for="tamano2_<?php echo $idproducto;?>"><p>Mediano</p></label>
								<label class="info" for="tamano3_<?php echo $idproducto;?>"><p>Pequeño</p></label>
								<div class="toggle_option_slider">
								</div>
						</div>
					</div>
					<div class="col-md-12 <?php if($mad==0){ echo "hide";}?>">
						<h5>Selecciona la madurez de tu producto:</h5>
						<div class="toggle_radio info">
							<input data-name="product_est" name="product_estado_<?php echo $idproducto;?>" type="radio" class="toggle_option first_toggle" id="estado1_<?php echo $idproducto;?>" name="toggle_option" value="Verde">
							<input data-name="product_est" name="product_estado_<?php echo $idproducto;?>" type="radio" checked class="toggle_option second_toggle" id="estado2_<?php echo $idproducto;?>" name="toggle_option" value="Pinton">
							<input data-name="product_est" name="product_estado_<?php echo $idproducto;?>" type="radio" class="toggle_option third_toggle" id="estado3_<?php echo $idproducto;?>" name="toggle_option" value="Maduro">
							<label class="info" for="estado1_<?php echo $idproducto;?>"><p>Verde</p></label>
							<label class="info"  for="estado2_<?php echo $idproducto;?>"><p>Pintón</p></label>
							<label class="info"  for="estado3_<?php echo $idproducto;?>"><p>Maduro</p></label>
							<div class="toggle_option_slider "></div>
						</div>	
					</div>
				</div>

				<div class="nav-tabs-custom col-md-12 sel-unidad <?php if(($puv==1 and $ppv==0) || ($ppv==1 and $puv==0)){ echo " hide"; } ?>" >
					<ul class="nav nav-tabs pull-left">    
						<li class="active unidad" >
							<a  data-toggle="tab" aria-expanded="true" onclick="ckradio('product_unidad_<?php echo $idproducto;?>','0')" >
								<label>Unidad</label>
								<input style="display: none;" data-name="product_uni" name="product_unidad_<?php echo $idproducto;?>" type="radio" id="unidad1_<?php echo $idproducto;?>" value="0" <?php if($dataunidad==0){ echo "checked";}?> data-uni-min = '<?php echo $pmuv;?>' data-text-min="UND" data-idp="<?php echo $idproducto;?>">
							</a>
						</li>
						<li class="peso" onclick="ckradio('product_unidad_<?php echo $idproducto;?>','1')">
							<a data-toggle="tab" aria-expanded="false">
								<label>Peso</label>
								<input style="display: none;" data-name="product_uni" name="product_unidad_<?php echo $idproducto;?>" type="radio" id="unidad2_<?php echo $idproducto;?>" value="1" <?php if($dataunidad==1){ echo "checked";}?> data-uni-min = '<?php echo $pmpv;?>' data-text-min="GR" data-idp="<?php echo $idproducto;?>">
							</a>
						</li>
					</ul>
				</div>
				<div class="col-md-7 col-md-offset-1 col-xs-12">
					<div class="input-group pull-right" style="width: 70%" >
					<span class="input-group-btn">
					<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="product_quantity2_<?php echo $idproducto;?>">
					<span class="glyphicon glyphicon-minus"></span>
					</button>
					</span>
					<div class="has-feedback">
					<input type="text" id="product_quantity2_<?php echo $idproducto;?>" name="product_quantity" class="form-control input-number  sc-cart-item-qty" step="<?php echo $unidadmin?>" value="<?php if($canp>0){ echo $canp;} else { echo $unidadmin; } ?>" min="<?php echo $unidadmin;?>" max="9999999" data-unidad="<?php echo $datunidad; ?>" style="border-radius: 0px !important">
					<span class="form-control-feedback" id="unidadproducto_<?php echo $idproducto;?>"><?php if($dataunidad==1){ echo "GR"; }else{ echo "UND";}?></span>
					</div>
					
					<span class="input-group-btn">
					<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="product_quantity2_<?php echo $idproducto;?>">
					<span class="glyphicon glyphicon-plus"></span>									
					</button>

					</span>
					</div>
				</div>
				<div class="col-md-4 col-xs-12 pull-right">
					<?php 
					if($idUsuario>0)
					{
						?>
					<button class="sc-add-to-cart btn btn-success btn-block pull-right  btn-xs">Agregar <i class="fa fa-opencart" title="Añadir al carro"></i></button>
					<?php
					}
					else
					{
							?>
					<button type="button" class="sc-logout btn btn-success  pull-right  btn-block">Agregar <i class="fa fa-opencart" title="Añadir al carro"></i></button>
					<?php
					}
					?>
				</div>
				 <input name="product_price" id="product_price_<?php echo $idproducto;?>" value="<?php echo $ventaoferta;?>" type="hidden" />
	            <input class="sc-product-id" name="product_id" value="<?php echo $idproducto;?>" type="hidden" />
				</div>
			</div>
		</div>
	

<!--
    <div class="row sc-product-item">
    	<div class="col-md-7">
    		<div class="col-md-12">
    				<img id="zoom_01" width="500"  heigth="500" class="img-responsive" src='<?php echo $img;?>' data-zoom-image="<?php echo $img;?>"/>
   			</div>
			<div id="gal1" class="col-md-12">
				<?php
				$con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idproducto."'  and pri_activo = 1");
				$num = pg_num_rows($con);
				if($num>1)
				{
					for($k=0;$k<$num;$k++)
					{
					$dat = pg_fetch_row($con);
					$cimg = $dat[0];
					$imga = $dat[1];
				?>
					<a href="#" data-image="<?php echo $imga;?>" data-zoom-image="<?php echo $imga;?>">
					<img class="zoom"  width="80"  src="<?php echo $imga;?>" />
					</a>
					<?php
					}
				}
				?>

			

			</div>
		</div>
		<div class="col-md-5 " >
			<h3>
			<small class="pricesmalloferta currency2 <?php if($numoferta<=0){ echo ' hide';}?>" ><?php echo $ventaold;?></small>
			<small id="precio_<?php echo $idproducto;?>" style="display: inline-block;" class="pricesmall currency2" ><?php echo $ventaoferta;?></small><br>
			<span data-name="product_name"><?php echo $nomp;?></span></h3>
				<hr/>
				<div class="col-md-12">
					<div class="col-md-8">
					
					   <div class="col-md-12 <?php if(count($arrayclasificacion)<=1){ echo ' hide';}?>"">
			                    <h5>Clasificacion: </h5>
			                	
			                    <select name="product_clasificacion" id="product_clasificacion_<?php echo $idproductp;?>" class="form-control input-sm selectpicker" onchange="CRUDPEDIDOS('CAMBIARPRECIO','<?php echo $idpproducto; ?>',this.value)">
                            	<?php
                            	
                            		for($c=0;$c<count($arrayclasificacion);$c++)
                            		{ 
                            			$concla = pg_query($dbconn,"SELECT * FROM tbl_clasificacion where cla_clave_int = '".$arrayclasificacion[$c]."'");
                            			$datc = pg_fetch_array($concla)
                            			?>
                                    	<option value="<?php echo $arrayclasificacion[$c];?>"><?php echo $datc['cla_nombre'];?></option>	
                                    <?php 
                                	}
                                ?>
                                </select>
	                    </div>
					              
						<div class="col-md-12 <?php if($aplicatamano==1){ }else{ echo ' hide'; } ?>">
						<h5>Selecciona la tamaño de tu producto:</h5>
						<div class="toggle_radio">
								<input data-name="product_tam" name="product_tamano_<?php echo $idproducto;?>" type="radio" class="toggle_option grande_toggle" id="tamano1_<?php echo $idproducto;?>" name="toggle_option" value="Grande">
								<input data-name="product_tam" name="product_tamano_<?php echo $idproducto;?>" type="radio"  class="toggle_option mediano_toggle" id="tamano2_<?php echo $idproducto;?>" name="toggle_option" value="Mediano">
								<input data-name="product_tam" name="product_tamano_<?php echo $idproducto;?>" type="radio" class="toggle_option pequeno_toggle" id="tamano3_<?php echo $idproducto;?>" name="toggle_option" value="Pequeño">
								<label class="info" for="tamano1_<?php echo $idproducto;?>"><p>Grande</p></label>
								<label class="info" for="tamano2_<?php echo $idproducto;?>"><p>Mediano</p></label>
								<label class="info" for="tamano3_<?php echo $idproducto;?>"><p>Pequeño</p></label>
								<div class="toggle_option_slider">
								</div>
						</div>
					</div>
					<div class="col-md-12 <?php if($mad==0){ echo "hide";}?>">
						<h5>Selecciona la madurez de tu producto:</h5>
						<div class="toggle_radio info">
							<input data-name="product_est" name="product_estado_<?php echo $idproducto;?>" type="radio" class="toggle_option first_toggle" id="estado1_<?php echo $idproducto;?>" name="toggle_option" value="Verde">
							<input data-name="product_est" name="product_estado_<?php echo $idproducto;?>" type="radio" checked class="toggle_option second_toggle" id="estado2_<?php echo $idproducto;?>" name="toggle_option" value="Pinton">
							<input data-name="product_est" name="product_estado_<?php echo $idproducto;?>" type="radio" class="toggle_option third_toggle" id="estado3_<?php echo $idproducto;?>" name="toggle_option" value="Maduro">
							<label class="info" for="estado1_<?php echo $idproducto;?>"><p>Verde</p></label>
							<label class="info"  for="estado2_<?php echo $idproducto;?>"><p>Pintón</p></label>
							<label class="info"  for="estado3_<?php echo $idproducto;?>"><p>Maduro</p></label>
							<div class="toggle_option_slider "></div>
						</div>	
					</div>
				</div>

				<div class="nav-tabs-custom col-md-12 sel-unidad <?php if(($puv==1 and $ppv==0) || ($ppv==1 and $puv==0)){ echo " hide"; } ?>" >
					<ul class="nav nav-tabs pull-left">    <li class="active unidad" ><a  data-toggle="tab" aria-expanded="true" onclick="ckradio('product_unidad_<?php echo $idproducto;?>','0')" ><label>Unidad</label>
						<input style="display: none;" data-name="product_uni" name="product_unidad_<?php echo $idproducto;?>" type="radio" id="unidad1_<?php echo $idproducto;?>" value="0" <?php if($dataunidad==0){ echo "checked";}?> data-uni-min = '<?php echo $pmuv;?>' data-text-min="UND" data-idp="<?php echo $idproducto;?>">
						</a>
						</li>
						<li class="peso" onclick="ckradio('product_unidad_<?php echo $idproducto;?>','1')">
							<a data-toggle="tab" aria-expanded="false"><label>Peso</label>
								<input style="display: none;" data-name="product_uni" name="product_unidad_<?php echo $idproducto;?>" type="radio" id="unidad2_<?php echo $idproducto;?>" value="1" <?php if($dataunidad==1){ echo "checked";}?> data-uni-min = '<?php echo $pmpv;?>' data-text-min="GR" data-idp="<?php echo $idproducto;?>">
							</a>
						</li>
					</ul>
				</div>
				<div class="col-md-7 col-md-offset-1 col-xs-12">
					<div class="input-group pull-right" style="width: 70%" >
					<span class="input-group-btn">
					<button type="button" class="btn btn-default btn-number" disabled="disabled" data-type="minus" data-field="product_quantity2_<?php echo $idproducto;?>">
					<span class="glyphicon glyphicon-minus"></span>
					</button>
					</span>
					<div class="has-feedback">
					<input type="text" id="product_quantity2_<?php echo $idproducto;?>" name="product_quantity" class="form-control input-number  sc-cart-item-qty" step="<?php echo $unidadmin?>" value="<?php if($canp>0){ echo $canp;} else { echo $unidadmin; } ?>" min="<?php echo $unidadmin;?>" max="9999999" data-unidad="<?php echo $datunidad; ?>" style="border-radius: 0px !important">
					<span class="form-control-feedback" id="unidadproducto_<?php echo $idproducto;?>"><?php if($dataunidad==1){ echo "GR"; }else{ echo "UND";}?></span>
					</div>
					
					<span class="input-group-btn">
					<button type="button" class="btn btn-default btn-number" data-type="plus" data-field="product_quantity2_<?php echo $idproducto;?>">
					<span class="glyphicon glyphicon-plus"></span>									
					</button>

					</span>
					</div>
				</div>
				<div class="col-md-4 col-xs-12 pull-right">
					<?php 
					if($idUsuario>0)
					{
						?>
					<button class="sc-add-to-cart btn btn-success btn-block pull-right  btn-xs">Agregar <i class="fa fa-opencart" title="Añadir al carro"></i></button>
					<?php
					}
					else
					{
							?>
					<button type="button" class="sc-logout btn btn-success  pull-right  btn-block">Agregar <i class="fa fa-opencart" title="Añadir al carro"></i></button>
					<?php
					}
					?>
				</div>
				 <input name="product_price" id="product_price_<?php echo $idproducto;?>" value="<?php echo $ventaoferta;?>" type="hidden" />
	                                <input class="sc-product-id" name="product_id" value="<?php echo $idproducto;?>" type="hidden" />

			</div>
	</div>-->
    	<script>
    		jQuery(document).ready(function(e) 
    		{
    			//$.removeData(image, 'elevateZoom');//remove zoom instance from image

    			$(".flexslider").flexslider({

					animation: "slide",
				    controlNav: true,
				    animationLoop: false,
				    slideshow: false,
				    itemWidth: 100,
				    itemMargin: 5

				});

				$(".flexslider ul li img").click(function(){

					var capturaIndice = $(this).attr("value");

					$(".infoproducto figure.visor img").hide();

					$("#lupa"+capturaIndice).show();
				})

				/*=============================================
				EFECTO LUPA
				=============================================*/
				$(".infoproducto figure.visor img").mouseover(function(event){

					var capturaImg = $(this).attr("src");

					$(".lupa img").attr("src", capturaImg);

					$(".lupa").fadeIn("fast");

					$(".lupa").css({

						"height":$(".visorImg").height()+"px",
						"background":"#eee",
						"width":"100%"

					})

				})

				$(".infoproducto figure.visor img").mouseout(function(event){

					$(".lupa").fadeOut("fast");

				})

				$(".infoproducto figure.visor img").mousemove(function(event){

					var posX = event.offsetX;
					var posY = event.offsetY;

					$(".lupa img").css({

						"margin-left":-posX+"px",
						"margin-top":-posY+"px"

					})

				})

    			/*if($('.zoomContainer').length>0)
    			{
        			$('.zoomContainer').remove();// remove zoom container from DOM
        		}
    			$("#zoom_01").elevateZoom({gallery:'gal1', cursor: 'pointer', galleryActiveClass: 'active', imageCrossfade: true, loadingIcon: 'dist/img/spinner.gif',
    				  zoomWindowWidth:300,
        			  zoomWindowHeight:200
    			}); 
				//pass the images to Fancybox
				$(".zomm").bind("click", function(e) {  
				  var ez =   $('#zoom_01').data('elevateZoom');	
					$.fancybox(ez.getGalleryList());
				  return false;
				});*/

				
		    $('.sc-logout').on('click',function(e){
		    	e.preventDefault();
		    	$('#titlecustom').html("Hola, usuario");
		    	$('#modalcustom').modal('show');
		    	$('#contenidomodalcustom').html("<h3 style='margin:0px'>Debes iniciar sesión primero</h3><a class='btn btn-success btn-block' href='login.php'>Ingresar</a>")
		    });
			//FUNCION AÑADIR AL CARRITO
			$('.sc-add-to-cart').on('click',function(e){
	    	e.preventDefault();
	    	
	    	var pad = $(this).parents('.sc-product-item');
	    	//var idp = $(this).parents('.sc-product-item').attr('data-idp');
	    	var idp = pad.find("input[name='product_id']").val();
	    	var pre = pad.find("input[name='product_price']").val();
	    	var can = pad.find("input[name='product_quantity']").val();
	    	var mad = pad.find("[data-name='product_est']:checked").val();
	    	var tam = pad.find("[data-name='product_tam']:checked").val();
	    	var cla = pad.find("select[name='product_clasificacion']").val();	    	
	    	var uni = pad.find("[data-name='product_uni']:checked").val();
	    	var min = pad.find("[data-name='product_uni']:checked").attr('data-uni-min');


	    	if(tam==undefined){ tam = "";}
	    	if(can<=0 || can==null)
	    	{
	    		error("La cantidad debe ser mayor a cero");
	    	}
	    	else if(cla=="" || cla==undefined)
	    	{
	    		error("No hay clasificacion del producto asociado");
	    	}
	    	else
	    	{
	    		$.post('funciones/pedidos/fnPedidos.php', {opcion: 'ANADIRPRODUCTO',idp:idp,pre:pre,can:can,mad:mad,tam:tam,cla:cla,uni:uni,min:min}, function(data, textStatus, xhr) {
	    			
	    			var res = data[0].res;
	    			var msn = data[0].msn;
	    			var idpedido = data[0].idpedido;
	    			var can = data[0].can;
	    			if(res=="ok")
	    			{
	    				$(this).parents('.sc-product-item').addClass('sc-added-item');
	    				$('#producto-item-' + idp).addClass('sc-added-item');
	    				$('#numpedido').html(can);
	    				ok(msn);	    				
	    				//setTimeout(CRUDPEDIDOS('DETALLEPEDIDO',idpedido),1000);

	    				//funciones alerta pedido en proceso
	    			}
	    			else
	    			{
	    				error(msn);
	    			}
	    		},"json");
	    	}
	    	
	    	//console.info(idp + " - " + pre + ' - ' + can + ' - ' +est + ' - '+cla + '-' + tam); 
	    })
    		});
		
</script>
    	<?php
    	echo "<style onload=IncDec()></style>";
    	echo "<style onload=INICIALIZARCONTENIDO()></style>";
   }