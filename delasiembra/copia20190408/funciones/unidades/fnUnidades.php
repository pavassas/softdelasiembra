<?php
	include ("../../data/Conexion.php");
  session_start();
	error_reporting(0);
	// variable login que almacena el login o nombre de usuario de la persona logueada
  $login= isset($_SESSION['persona']);
  // cookie que almacena el numero de identificacion de la persona logueada
  $usuario= $_COOKIE['usuario'];
  $idUsuario= $_SESSION["idusuario"];
  $clave= $_COOKIE["clave"];
  $identificacion = $_COOKIE["usIdentificacion"];
  date_default_timezone_set('America/Bogota');
  $fecha=date("Y/m/d H:i:s");

  $opcion = $_POST['opcion'];
	
  if($opcion=="NUEVO")
  {
  ?>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <form name="form1" id="form1" class="form-horizontal">
      <div class="form-group">
           <div class="col-md-6"><strong>Código:</strong>
           <div class="ui corner labeled input">
           <input type="text" placeholder="Ingrese el código" name="txtcodigo" id="txtcodigo" class="form-control input-sm">
           <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
           </div> 
           <div class="col-md-6"><strong>Nombre:</strong>
           <div class="ui corner labeled input">
           <input  name="txtnombre" id="txtnombre" class="form-control input-sm" type="text" autocomplete="off" placeholder="Ingrese nombre">
           <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
          
           </div>
      </div>
   
    </form>
  <?php  
  }
  else if($opcion=="UNIFICAR")
  {
	 ?>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <form name="form1" id="form1" class="form-horizontal">
    <div class="row">
        <div class="col-md-8">
        <strong>Unidades a Unificar:</strong>
        <select name="unidadunificar" id="unidadunificar" class="form-control input-sm selectpicker" multiple="multiple" data-live-search="true" data-live-search-placeholder="Buscar Unidad" style="width:100%">
         <?php 
		 $con = pg_query($dbconn,"select uni_clave_int,uni_codigo,uni_nombre from tbl_unidades where est_clave_int = 1 order by uni_codigo");
		 while($dat = pg_fetch_array($con))
		 {
			 $idu = $dat['uni_clave_int'];
			 $nom = $dat['uni_nombre'];
			 $cod = $dat['uni_codigo'];
		    ?>
            <option value="<?Php echo $idu;?>"><?php echo $nom."(".$cod.")";?></option>
            <?php
		 }
		 ?>		 
        </select>
        </div>
        <div class="col-md-4">
         <strong>Unidad Base:</strong>
        <select name="unidadpor" id="unidadpor" class="form-control input-sm selectpicker" style="width:100%">
         <?php 
		 $con = pg_query($dbconn,"select uni_clave_int,uni_codigo,uni_nombre from tbl_unidades where est_clave_int = 1 order by uni_codigo");
		 while($dat = pg_fetch_array($con))
		 {
			 $idu = $dat['uni_clave_int'];
			 $nom = $dat['uni_nombre'];
			 $cod = $dat['uni_codigo'];
		    ?>
            <option value="<?php echo $idu;?>"><?php echo $nom."(".$cod.")";?></option>
            <?php
		 }
		 ?>		 
        </select>
        </div>
    </div>
    </form>
      
  <?php
	  echo "<script>INICIALIZARCONTENIDO();</script>";
  }
else if($opcion=="GUARDARUNIFICAR")
{
  $uniu = $_POST['uniu']; $uniu = implode(', ', (array)$uniu);
  $unip = $_POST['unip'];
  
  $conu = pg_query($dbconn,"select uni_clave_int,uni_codigo,uni_nombre from tbl_unidades where uni_clave_int in(".$uniu.")");
  $cantu = pg_num_rows($conu);
  if($cantu>0)
  {
	  for($k=0;$k<$cantu;$k++)
	  {
		  $datu = pg_fetch_array($conu);
		  $idu = $datu['uni_clave_int'];
		  $update = pg_query($dbconn,"update tbl_productos set pro_uni_consumo = '".$unip."' where pro_uni_consumo ='".$idu."'");
      $update = pg_query($dbconn,"update tbl_productos set pro_uni_compra = '".$unip."' where pro_uni_compra ='".$idu."'");
		  
		  if($idu!=$unip)
		  {
		  	$update3 = pg_query($dbconn,"update tbl_unidades set est_clave_int = 2 where uni_clave_int = '".$idu."'");
		  }
	  }
	  $res = 1;
	  $msn = 'Unidades Unificadas correctamente';
  }
  else
  {
	 $res = 2;
	$msn = 'No ha seleccionado ninguna unidad a unificar';
  }
  $datos[] = array("res"=>$res,"msn"=>$msn);
  echo json_encode($datos);
}
else if($opcion=="EDITAR" )
{
	  $id = $_POST['id'];
	  $coninfo = pg_query($dbconn,"select u.uni_clave_int idu,u.uni_codigo as cod,u.uni_nombre as nom,u.est_clave_int as est from tbl_unidades u where u.uni_clave_int = '".$id."' limit 1");
	  $datinfo = pg_fetch_array($coninfo);	
	  $idu = $datinfo['idu'];
	  $cod = $datinfo['cod'];
	  $nom = $datinfo['nom'];
	//  $est = $datinfo['est'];
  ?>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">
  <input type="hidden" id="idedicion" value="<?php echo $id;?>">
    <div class="form-group">
    <div class="col-md-6"><strong>Código:</strong>
    <div class="ui corner labeled input">
    <input name="txtcodigo" id="txtcodigo" type="text" class="form-control input-sm" value="<?php echo $cod;?>" placeholder="Ingrese el código">
    <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>
    </div><div class="col-md-6"><strong>Nombre:</strong>
    <div class="ui corner labeled input">
    <input  name="txtnombre" id="txtnombre" class="form-control input-sm" type="text" autocomplete="off" placeholder="Ingrese el nombre" value="<?php echo $nom;?>">
    <div class="ui corner label"> <i class="asterisk icon"></i> </div></div>    
    </div>
    </div>
  
  </form>
  <?php  
}
else if($opcion=="GUARDAR")
{
  $codigo = $_POST['codigo'];
  $nombre  = $_POST['nombre'];
  $veri = pg_query($dbconn,"select * from tbl_unidades where UPPER(uni_codigo) = UPPER('".$codigo."') and (est_clave_int = 1 or uni_clave_int in(".$unidades."))");
  $numv = pg_num_rows($veri);
  if($numv>0)
  {
    echo 2;
  }
  else 
  {
    $ins = pg_query($dbconn,"insert into tbl_unidades(uni_codigo,uni_nombre,uni_usu_actualiz,uni_fec_actualiz) values('".$codigo."','".$nombre."','".$usuario."','".$fecha."')");
    if($ins>0)
    {
      echo 1;
    }
    else
    {
      echo 3;
    }	
	} 	 
} 
else if($opcion=="GUARDAREDICION" )
{
  $ide =$_POST['id'];
  $codigo = $_POST['codigo'];
  $nombre  = $_POST['nombre'];
  $veri = pg_query($dbconn,"SELECT * FROM tbl_unidades WHERE UPPER(uni_codigo  = '".$codigo."')) and uni_clave_int!='".$ide."' and (est_clave_int = 1)");
  $numv = pg_num_rows($veri);
  if($numv>0)
  {
    echo 2;
  }
  else 
  {
    $upd = pg_query($dbconn,"update tbl_unidades set uni_codigo='".$codigo."',uni_nombre='".$nombre."',uni_usu_actualiz='".$usuario."',uni_fec_actualiz='".$fecha."' where uni_clave_int = '".$ide."'");
    if($upd>0)
    {
      echo 1;
    }
    else
    {
      echo 3;
    }	
	}	 
}
else if($opcion=="LISTAUNIDADES")
{
	?> 
  <script src="jsdatatable/unidades/jsunidades.js"></script>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <div>
    <table id="tbunidades" class="table table-striped" >
      <thead>
      <tr>
        <th class="dt-head-center" style="width:20px"></th>
        <th class="dt-head-center" style="width:20px"></th>
        <th class="dt-head-center" style="width:40px">CÓDIGO</th>
        <th class="dt-head-center">NOMBRE</th>
        <th class="dt-head-center" style="width:40px">ESTADO</th>
      </tr>
      </thead>
        <tfoot>
      <tr>
      <th></th>
      <th></th>
      <th class="dt-head-center">CÓDIGO</th>
      <th class="dt-head-center">NOMBRE</th>
      <th class="dt-head-center">ESTADO</th>
      </tr>
      </tfoot>           
      </tbody>
    </table>
  </div>
<?php
}
else if($opcion=="ELIMINAR")
{
  $id = $_POST['id'];
	$update = pg_query($dbconn,"update tbl_unidades set est_clave_int = 2 where uni_clave_int = '".$id."'");
	if($update>0){ $res = "ok"; $mns = "Unidad eliminada";  }else { $res = "error"; $msn = "Error BD. No se elimino la unidad"; }
  $datos[] = array("res"=>$res,"msn"=>$msn);
  echo json_encode($datos);
} 
?>