
<?php
include ("../../data/Conexion.php");
session_start();
error_reporting(0);
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_SESSION["idusuario"];
$clave= $_COOKIE["clave"];
$identificacion = $_COOKIE["usIdentificacion"];
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//Vvariable GET
//$Socios = $_GET['Socios'];

// DB table to use
$table = 'tbl_proveedor';
// Table's primary key
$primaryKey = 'p.prv_clave_int';

$nom = $_POST['nom'];
$tip = $_POST['tip']; $tip = implode("', '", (array)$tip); if($tip==""){$tip1="''";}else {$tip1=$tip;}
$num = $_POST['num'];
$tel1 = $_POST['tel1'];
$tel2 = $_POST['tel2'];
$ema = $_POST['ema'];
$dir = $_POST['dir'];
$obs = $_POST['obs'];
$act = $_POST['act']; $act = implode("', '", (array)$act); if($act==""){$act1="'0'";}else {$act1=$act;}

$prov = $_POST['prov']; $prov = implode("', '", (array)$prov); if($prov==""){$prov1="'0'";}else {$prov1=$prov;}
// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object
// parameter names
$columns = array(
        array(
                'db' => 'p.prv_clave_int',
                'dt' => 'DT_RowId', 'field' => 'prv_clave_int',
                'formatter' => function( $d, $row ) {
                // Technically a DOM id cannot start with an integer, so we prefix
                // a string. This can also be useful if you have multiple tables
                // to ensure that the id is unique with a different prefix
                return 'rowp_'.$d;
                }
        ),	        
        array( 'db' => 'p.prv_clave_int', 'dt' => 'Eliminar', 'field' => 'prv_clave_int','formatter'=>function($d,$row){
        return "<a class='btn btn-circle btn-block btn-danger btn-xs' onclick=CRUDPROVEEDORES('ELIMINARPROVEEDOR','".$d."') title='Eliminar producto' style='heigth:22px; width:22px'><i class='fa fa-trash'></i></a>";
        }),
        array( 'db' => 'p.prv_clave_int', 'dt' => 'Editar', 'field' => 'prv_clave_int','formatter'=>function($d,$row){
                return "<table><tr><td><a class='btn btn-circle btn-block btn-danger btn-xs' onclick=CRUDPROVEEDORES('ELIMINARPROVEEDOR','".$d."') title='Eliminar producto' style='heigth:22px; width:22px'><i class='fa fa-trash'></i></a></td><td><a class='btn btn-circle btn-block btn-warning btn-xs' onClick=CRUDPROVEEDORES('EDITAR','".$d."') title='Editar producto' style='heigth:22px; width:22px' data-toggle='modal' data-target='#modalregistro'><i class='fa fa-pencil'></i></a></td></tr></table>";        
        }),		
        array( 'db' => 'p.prv_nombre', 'dt' => 'Nombre', 'field' => 'prv_nombre'),
        array( 'db' => 'p.prv_nro_docum', 'dt' => 'Documento', 'field' => 'prv_nro_docum' ),
        array( 'db' => 'p.prv_tipo_docum', 'dt' => 'Tipo', 'field' => 'prv_tipo_docum' ),
        array( 'db' => 'p.prv_email', 'dt' => 'Email', 'field' => 'prv_email'),
        array( 'db' => 'p.prv_direccion', 'dt' => 'Direccion', 'field' => 'prv_direccion'),
        array( 'db' => 'p.prv_telefono1', 'dt' => 'Telefono', 'field' => 'prv_telefono1','formatter'=>function($d,$row){ 
        	return $d."/".$row[9];
        }),
        array( 'db' => 'p.prv_telefono2', 'dt' => 'Telefono2', 'field' => 'prv_telefono2'),
        array( 'db' => 'p.prv_clave_int', 'dt' => 'Clave', 'field' => 'prv_clave_int'),
        array( 'db' => 'p.prv_observacion', 'dt' => 'Observacion', 'field' => 'prv_observacion'),
        array( 'db' => 'p.prv_activo', 'dt' => 'Estado', 'field' => 'prv_activo','formatter'=>function($d,$row){
        	if($d==0){ $d = "Inactivo";}else if($d==1){ $d="Activo";}
        	return $d;
        })
);

$sql_details = array(
	'user' => 'usrpavas',
	'pass' => '9A12)WHFy$2p4v4s',
	'db'   => 'dbdelasiembra',
	'port'   => '5432',
	'host' => '127.0.0.1'
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );
$whereAll = "";// customerid =".$customerid." AND date( orderdate ) >= '".$startdate."' AND date( orderdate ) <= '".$enddate."'";
$groupBy = '';//'p.pro_clave_int';
$with = '';
$joinQuery = "FROM tbl_proveedor AS p";
$extraWhere =" ( p.prv_nombre LIKE REPLACE('".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '' ) and ( p.prv_nro_docum LIKE REPLACE('".$num."%',' ','%') OR '".$num."' IS NULL OR '".$num."' = '' ) and ( p.prv_email LIKE REPLACE('".$ema."%',' ','%') OR '".$ema."' IS NULL OR '".$ema."' = '' ) and (prv_telefono1 LIKE REPLACE('".$tel1."%',' ','%') OR '".$tel1."' IS NULL OR '".$tel1."' = '') and (prv_telefono2 LIKE REPLACE('".$tel2."%',' ','%') OR '".$tel2."' IS NULL OR '".$tel2."' = '') and (prv_direccion LIKE REPLACE('".$dir."%',' ','%')  OR '".$dir."' IS NULL OR '".$dir."' = '') and (prv_observacion LIKE REPLACE('%".$obs."%',' ','%')  OR '".$obs."' IS NULL OR '".$obs."' = '') and ( p.prv_tipo_docum IN(".$tip1.") OR '".$tip."' IS NULL OR '".$tip."' = '') and ( p.prv_activo IN(".$act1.") OR '".$act."' IS NULL OR '".$act."' = '') and ( p.prv_clave_int IN(".$prov1.") OR '".$prov."' IS NULL OR '".$prov."' = '') and prv_activo!=2";


echo json_encode(
	SSP::simple($_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy,$with )

);