<?php
  include ("../../data/Conexion.php");
  session_start();
  error_reporting(0);
  // variable login que almacena el login o nombre de usuario de la persona logueada
  $login= isset($_SESSION['persona']);
  // cookie que almacena el numero de identificacion de la persona logueada
  $usuario= $_COOKIE['usuario'];
  $idUsuario= $_SESSION["idusuario"];
  $clave= $_COOKIE["clave"];
  $identificacion = $_COOKIE["usIdentificacion"];
  date_default_timezone_set('America/Bogota');
  $fecha=date("Y/m/d H:i:s");
  $conusu= pg_query($dbconn,"SELECT mer_clave_int,dir_clave_int,usu_ult_telefono,usu_imagen,usu_modo from tbl_usuario where usu_clave_int = '".$idUsuario."'");
  $datusu = pg_fetch_array($conusu);
  $idmercado = $datusu['mer_clave_int'];
  $ultimadireccion = $datusu['dir_clave_int'];
  $ultimotelefono = $datusu['usu_ult_telefono'];
  $imagen = $datusu['usu_imagen'];
  $modo = $datusu['usu_modo'];

  //echo "<script>CRUDCUENTA('AJUSTECUENTA','');</script>";
  ?>
  <section class="content-header">
      <h1>
        Mi cuenta
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Mi Cuenta</a></li>
        <li class="active">Ajuste de Cuenta</li>          
      </ol>
    </section>  
    <section class="content">
      <form id="formcuenta" name="formcuenta" class="form-horizontal" method="post" action="_" enctype="multipart/form-data">
      <div class="row">
        <aside class="col-md-3">
          <div class="box box-success">
              <div class="box-body box-profile">
                <input type="file" id="imgcuenta" name="imgcuenta" data-allowed-file-extensions="png jpg gif jpge" class="dropify profile-user-img img-responsive img-circle" data-default-file="<?php echo $imagen;?>" data-height="128"  data-min-width="128"  data-show-remove="false" onChange="setpreview('rutaimagen','imgcuenta','formcuenta')" <?php if($modo=="facebook"){ echo "disabled"; }?>>
                <span id="rutaimagen"></span>

                <h3 class="profile-username text-center"><?php echo $usuario;?></h3>               

                <ul class="list-group list-group-unbordered">
                  <li class="list-group-item">
                    <b>Cambiar Avatar</a>
                  </li>               
                 
                </ul>

                
              </div>
              <!-- /.box-body -->
            </div>
          
        </aside>
        <div class="col-md-9">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active" id="licuenta"><a data-toggle="tab" ui-sref="Ajustes({ Op: 'Cue' })" ui-sref-opts="{reload: true}">Cuenta</a></li>
                <li id="lidireccion"><a data-toggle="tab" ui-sref="Ajustes({ Op: 'Dir' })" ui-sref-opts="{reload: true}">Direcciones</a></li>
                <li><a data-toggle="tab">Notificaciones</a></li>
              </ul>
              <div class="tab-content">
                <div class="active tab-pane" id="acount">                  
                </div>              
              </div>
            </div>
        </div>
    </div>
       </form>
              <iframe src="about:blank" name="null" style="display:none"></iframe>

    </section>
    <script>
      $(document).ready(function(e) {
          $('#imgcuenta').dropify();
      });
    </script>
    <?php
   
    ?>