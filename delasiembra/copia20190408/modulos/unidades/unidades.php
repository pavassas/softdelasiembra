<?php
include ("../../data/Conexion.php");
  session_start();
  error_reporting(0);
  // variable login que almacena el login o nombre de usuario de la persona logueada
  $login= isset($_SESSION['persona']);
  // cookie que almacena el numero de identificacion de la persona logueada
  $usuario= $_COOKIE['usuario'];
  $idUsuario= $_SESSION["idusuario"];
  $clave= $_COOKIE["clave"];
  $identificacion = $_COOKIE["usIdentificacion"];
  date_default_timezone_set('America/Bogota');
  $fecha=date("Y/m/d H:i:s");
echo "<script>CRUDUNIDADES('LISTAUNIDADES','');</script>";
?>

<section class="content-header">
      <h1>
        Unidades
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Configuración</a></li>
        <li class="active">Unidades</li>        
      </ol><a class="btn btn-flat margin btn-success btn-xs"  title="Añadir Unidad" tabindex="-1" data-toggle="modal" data-target="#modalregistro" onclick="CRUDUNIDADES('NUEVO','')">   
            <i class="fa fa-plus-circle"></i>NUEVO
        </a>
        <a class="btn btn-flat margin btn-info btn-xs" onClick="CRUDUNIDADES('UNIFICAR','')" role="button" data-toggle="modal" data-target="#modalregistro"><i class="glyphicon glyphicon glyphicon-refresh"></i>Unificar</a>
    </section>

 <section class="content">
  <div class="row">
      <div class="col-md-12">            
        <div class="input-group">
          <span class="input-group-addon" onclick="CRUDUNIDADES('LISTAUNIDADES','')"><i class="fa fa-search"></i></span>
          <input type="text" class="form-control" placeholder="busque por codigo o nombre" id="filtrounidad">
        </div>    
    </div>
  </div>
  <div class="row">
    <div class="col-md-12" id="tabladatos"></div>
  </div>
 </section>




