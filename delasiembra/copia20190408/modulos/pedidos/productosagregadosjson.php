
<?php
include ("../../data/Conexion.php");
session_start();
error_reporting(0);
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_SESSION["idusuario"];
$clave= $_COOKIE["clave"];
$identificacion = $_COOKIE["usIdentificacion"];
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");

 
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */
// DB table to use
$table = 'tbl_productos';
// Table's primary key
$primaryKey = 'p.pro_clave_int';
$idpedido = $_POST['idpedido'];
$idcalidad = $_POST['idcalidad'];

$pro = $_POST['producto'];
// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object
// parameter names
$columns = array(
	array(
		'db' => 'p.pro_clave_int',
		'dt' => 'DT_RowId', 'field' => 'pro_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'rowpa_'.$d;
		}
	),	
   
	array( 'db' => 'p.pro_nombre', 'dt' => 'Nombre', 'field' => 'pro_nombre'),
    array( 'db' => 'p.pro_codigo', 'dt' => 'Codigo', 'field' => 'pro_codigo'),
    array( 'db' => 'c.cat_nombre', 'dt' => 'Categoria', 'field' => 'cat_nombre'),
    array( 'db' => 'p.pro_clave_int', 'dt' => 'Clave', 'field' => 'pro_clave_int'),  
    array( 'db' => 'p.pro_clave_int', 'dt' => 'Compra', 'field' => 'pro_clave_int','formatter'=>function($d,$row){
        global $dbconn,$idcalidad;
        return "0";
        /*$veri = pg_query($dbconn, "SELECT ppr_compra FROM tbl_proveedor_productos WHERE prv_clave_int = '".$_POST['idproveedor']."' and pro_clave_int = '".$d."' and cla_clave_int = '".$idcalidad."'");
        $num = pg_num_rows($veri);
        $datv = pg_fetch_array($veri);
        $compra = $datv['ppr_compra'];
        if($num>0){ $dis="";}else{$dis="disabled";}
        return "<input id='valor_".$d."' name='valor_".$d."' class='form-control input-sm' type='text' value='".$compra."' ".$dis." data-old='".$compra."' onkeypress=return validar_texto(event) onkeyup=CRUDPROVEEDORES('ASIGNARPRODUCTO','".$d."','".$idcalidad."') >";*/
    
    }), 
    array( 'db' => 'p.pro_clave_int', 'dt' => 'Boton', 'field' => 'pro_clave_int','formatter'=>function($d,$row){
           //if($d==1){ $ck = "checked";}else{$ck="";}
        global $idcalidad;
        return "<button id='btn_".$d."' class='btn btn-sm btn-success' type='button' onclick=CRUDPEDIDOS('ASIGNARPRODUCTO','".$d."','".$idcalidad."') ><i class='fa fa-plus'></i></button>";
    })
    ,array( 'db' => 'p.pro_clave_int', 'dt' => 'Cantidad', 'field' => 'pro_clave_int')
);

$sql_details = array(
	'user' => 'usrpavas',
	'pass' => '9A12)WHFy$2p4v4s',
	'db'   => 'dbdelasiembra',
	'port'   => '5432',
	'host' => '127.0.0.1'
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );
$whereAll = "";// customerid =".$customerid." AND date( orderdate ) >= '".$startdate."' AND date( orderdate ) <= '".$enddate."'";
$groupBy = 'p.pro_clave_int,c.cat_clave_int';
$with = '';
$extraWhere =" pro_activo!=2 and ( p.pro_nombre ILIKE REPLACE('".$pro."%',' ','%') OR '".$pro."' IS NULL OR '".$pro."' = '' )";
$joinQuery = "FROM tbl_productos AS p JOIN tbl_categorias c ON c.cat_clave_int = p.cat_clave_int";
//$extraWhere.=" and pp.cla_clave_int>0 and pp.cla_clave_int = '".$idcalidad."'";


echo json_encode(
	SSP::simple($_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy,$with )

);