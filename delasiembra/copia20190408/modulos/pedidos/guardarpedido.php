<?php
$IP = $_SERVER['REMOTE_ADDR'];
include ("../../data/Conexion.php");
session_start();
error_reporting(0);
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_SESSION["idusuario"];
$clave= $_COOKIE["clave"];
$identificacion = $_COOKIE["usIdentificacion"];
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
$conusu= pg_query($dbconn,"SELECT mer_clave_int from tbl_usuario where usu_clave_int = '".$idUsuario."'");
$datusu = pg_fetch_array($conusu);
$idmercado = $datusu['mer_clave_int'];


if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
// Get the results as JSON string
	$product_list = $_POST['cart_list'];// filter_input(INPUT_POST, 'cart_list');
	$dir = $_POST['dirpedido'];
	$tel = $_POST['txttelefono'];
	// Convert JSON to array
	$product_list_array = json_decode($product_list);
	$result_html = "";
	if($dir=="" || $dir==NULL)
	{
		echo "Seleccionar la dirección a la cual se debe enviar el pedido o cree una nueva";
	}
	else if($tel=="" || $tel==NULL)
	{
		echo "Diligenciar el telefono de envio del pedido";
	}
	else
	if($product_list_array) 
	{
		$sqlped = pg_query($dbconn, "INSERT INTO tbl_pedidos(usu_clave_int,dir_clave_int,ped_fecha,ped_usu_actualiz,ped_fec_actualiz,ped_ip,ped_telefono) VALUES('".$idUsuario."','".$dir."','".$fecha."','".$usuario."','".$fecha."','".$IP."','".$tel."')");
		$sql = "INSERT INTO tbl_pedidos(usu_clave_int,dir_clave_int,ped_fecha,ped_usu_actualiz,ped_fec_actualiz,ped_ip,ped_telefono) VALUES('".$idUsuario."','".$dir."','".$fecha."','".$usuario."','".$fecha."','".$IP."','".$tel."')";
		if($sqlped>0)
		{
			$uid = pg_fetch_row(pg_query($dbconn,"SELECT setval('ped_id_seq',nextval('ped_id_seq')-1) as id;"));
			$idpedido = $uid[0];
			$total = 0;
			$totali = 0;
			$sql1 = "";
		    foreach($product_list_array as $p)
		    {
		    	//$idp = $p['product_id'];
		    	//$result_html.="ok ";//Codpro:".$idp;
		    	$array = json_decode(json_encode($p),true); 
		    	$idp =$array['product_id'];
		    	$cant = $array['product_quantity'];
		    	$val = $array['product_price'];
		    	$val = str_replace("$", "", $val);
		    	$val = str_replace(",","", $val);		    	
		    	$pedkey = $array['unique_key'];
		    	$est = $array['product_estado_'.$idp];
		    	$cla = $array['product_clasificacion'];
		    	$concla = pg_query($dbconn,"SELECT cla_clave_int from tbl_clasificacion WHERE cla_nombre = '".$cla."'");
		    	$datcla = pg_fetch_array($concla);
		    	$codcla = $datcla['cla_clave_int'];

		    	$insdet = pg_query($dbconn,"INSERT INTO tbl_pedidos_detalle(ped_clave_int,mer_clave_int,cla_clave_int,pro_clave_int,pde_cantidad,pde_valor,pde_usu_actualiz,pde_fec_actualiz,pde_key) VALUES('".$idpedido."','".$idmercado."','".$codcla."','".$idp."','".$cant."','".$val."','".$usuario."','".$fecha."','".$pedkey."')");
		    	$sql1.= "INSERT INTO tbl_pedidos_detalle(ped_clave_int,mer_clave_int,cla_clave_int,pro_clave_int,pde_cantidad,pde_valor,pde_usu_actualiz,pde_fec_actualiz,pde_key) VALUES('".$idpedido."','".$idmercado."','".$codcla."','".$idp."','".$cant."','".$val."','".$usuario."','".$fecha."','".$pedkey."')";
		    	if($insdet)
		    	{
		    		$totali++;
		    	}

		    	$sub = $cant * $val;
		    	$total = $total + $sub;
		      
		        
		    }
		    if($totali<=0)
		    {
		    	$del = pg_query($dbconn,"DELETE FROM tbl_pedidos where ped_clave_int = '".$idpedido."'");
		    	echo "No se inserto ningun producto al pedido. (".$sql1.")";

		    }
		    else
		    {
			    $updped = pg_query($dbconn,"UPDATE tbl_pedidos SET ped_total = '".$total."' where ped_clave_int = '".$idpedido."'");
			    $updusu = pg_query($dbconn,"UPDATE tbl_usuario SET dir_clave_int = '".$dir."' WHERE usu_clave_int = '".$idUsuario."'");

			    echo "ok";
			}
		}
		else
		{
			echo "Surgió un error al guardar pedido. Error BD(".pg_last_error($dbconn).")".$sql;
		}
	} 
	else 
	{	
		$res = "Carro de compras vacio";
		echo $res;
	}

}
else
{
    throw new Exception("Error Processing Request", 1);
}