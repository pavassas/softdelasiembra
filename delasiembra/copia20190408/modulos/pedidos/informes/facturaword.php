<?php 
error_reporting(E_ALL);
include('../../../data/Conexion.php');
require_once '../../../clases/phpword/Autoloader.php';
\PhpOffice\PhpWord\Autoloader::register();
session_start();
$idpedido = $_GET['clave'];	
setlocale(LC_ALL,"es_ES.utf8","es_ES","esp");
            
$coninfo = pg_query($dbconn,"select u.usu_nombre,u.usu_apellido,u.usu_usuario,p.ped_fecha,u.usu_email,p.dir_clave_int,p.ped_domicilio,p.ped_telefono,ped_codigo,ped_tiempo_entrega,ped_nota,ped_fec_programada,ped_hor_programada from tbl_pedidos p JOIN tbl_usuario u ON u.usu_clave_int = p.usu_clave_int WHERE p.ped_clave_int = '".$idpedido."'");
$datinfo = pg_fetch_array($coninfo);
$cliente = $datinfo['usu_nombre']." ".$datinfo['usu_apellido'];
$pedfecha = $datinfo['ped_fecha'];
$email = $datinfo['usu_email'];
$dir = $datinfo['dir_clave_int'];
$domicilio = $datinfo['ped_domicilio'];
$telefono = $datinfo['ped_telefono'];
$codigoped = $datinfo['ped_codigo'];
$tiempo = $datinfo['ped_tiempo_entrega']." min";
$an = strftime("%Y",strtotime($pedfecha));
$dia = strftime("%a",strtotime($pedfecha));
$dia1 = strftime("%d",strtotime($pedfecha));
$mes = strftime("%B",strtotime($pedfecha));
$hi = strftime("%H:%M",strtotime($pedfecha));
$pedfecha = $dia.", ".$dia1." de ".$mes." de ".$an;
$pedfecha = $pedfecha . ", ".$hi;
$nota = $datinfo['ped_nota'];

$fecent = $datinfo['ped_fec_programada'];
$horent = $datinfo['ped_hor_programada'];
$an = strftime("%Y",strtotime($fecent));
$dia = strftime("%a",strtotime($fecent));
$dia1 = strftime("%d",strtotime($fecent));
$mes = strftime("%B",strtotime($fecent));

$fecent = $dia.", ".$dia1." de ".$mes." de ".$an;
$fecent = $fecent . ", ".date("g:i a",strtotime($horent));

$condir = pg_query($dbconn,"select s.sec_nombre,b.bar_nombre,s.sec_domicilio,d.dir_nomenclatura,d.dir_letra,d.dir_numero1,d.dir_numero2,d.dir_detalle,dir_real,dir_corta from tbl_direcciones d join tbl_barrio b on b.bar_clave_int = d.bar_clave_int join tbl_sector s on s.sec_clave_int = b.sec_clave_int where d.dir_clave_int = '".$dir."'");
$datdir = pg_fetch_array($condir);
$sector = $datdir['sec_nombre'];
$barrio = $datdir['bar_nombre'];
$nome = $datdir['dir_nomenclatura'];
$letra = $datdir['dir_letra'];
$nume1 = $datdir['dir_numero1'];
$nume2 = $datdir['dir_numero2'];

if($nome=="")
{
    $direccion = $datdir['dir_corta'];
}
else
{
    $direccion = $nome." ".$letra."#".$nume1."-".$nume2;
}
//$direccion = $nome." ".$letra."#".$nume1."-".$nume2;
$dirdetalle = $datdir['dir_detalle'];
 

$condet = pg_query($dbconn, "SELECT d.pde_clave_int,p.pro_clave_int,p.pro_nombre,p.pro_descripcionbreve,p.pro_codigo,c.cla_nombre,pde_estado,pde_tamano,cla_nombre,pde_cantidad,pde_valor,pde_unidad,pro_uni_venta,pro_pes_venta,pro_mu_venta,pro_mp_venta,pde_minimo,pde_unidad ,pde_codigo from tbl_pedidos_detalle d JOIN tbl_productos p on p.pro_clave_int = d.pro_clave_int join tbl_clasificacion c on c.cla_clave_int = d.cla_clave_int where d.ped_clave_int = '".$idpedido."' and pde_confirmado = 2 order by p.pro_nombre,c.cla_nombre");
$numdet = pg_num_rows($condet);

use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Style\Font;

//Instancia phpWord.
//$phpWord = new \PhpOffice\PhpWord\PhpWord();
$documento = new PhpWord();
// Nueva seccion
$seccion = $documento->addSection();

$header = $seccion->addHeader();
$header->addWatermark('../../../dist/img/plantillas/hojaA4N.jpg', 

    array( 'marginTop'=>10, 'marginRight' => -40,
        'width' => 800,
        'height' =>1000, 
        'wrappingStyle' => 'behind',
        'positioning' => 'absolute',
        'posHorizontal'    => \PhpOffice\PhpWord\Style\Image::POSITION_HORIZONTAL_RIGHT,
        'posHorizontalRel' => 'margin',
        'posVerticalRel' => 'line',

    ));

$estilo = array('width' => 75 * 75, 'unit' => 'pct', 'align' => 'center', 'borderSize' => '5');

$titulo = array('name' => 'Arial', 'size' => '12', 'bold' => 'true');
$texto = array('name' => 'Arial', 'size' => '10');
$left = array('align'=> 'left');
$center = array('align'=> 'center');
$right = array('align'=> 'right');

$seccion->addTextBreak(1);
$tabla1 = $seccion->addTable($estilo);
$tabla1->addRow();
$tabla1->addCell(100)->addText("Codigo",$titulo,$center);
$tabla1->addCell(300)->addText("Producto",$titulo,$center);
$tabla1->addCell(100)->addText("Precio Unidad",$titulo,$center);
$tabla1->addCell(100)->addText("Cantidad",$titulo,$center);
$tabla1->addCell(100)->addText("Precio Neto",$titulo,$center);

 $total = 0;
    while($datdet= pg_fetch_array($condet))
    {

    	$tabla1->addRow();
        $iddetalle = $datdet['pde_clave_int'];
        $idp = $datdet['pro_clave_int'];
        $codlinea = $datdet['pde_codigo'];
        $nompro = $datdet['pro_nombre'];
        $codpro = $datdet['pro_codigo'];
        $nomcla = $datdet['cla_nombre'];
        $maduracion = $datdet['pde_estado'];
        $descp = $datdet['pro_descripcionbreve'];
        $precio = $datdet['pde_valor'];
        $cantidad = $datdet['pde_cantidad'];
        $subtotal = $precio * $cantidad;
        $tamano = $datdet['pde_tamano'];

        $observacion = "Clasificación:".$nomcla." <br>";

        if($tamano!=""){$observacion.="Tamano:".$tamano."<br> ";}
        if($maduracion!=""){$observacion.="Maduración:".$maduracion." ";}
        if($tamano!=""){ $tamano = '-'.$tamano;}
        
        $puv = $datdet['pro_uni_venta'];
        $ppv = $datdet['pro_pes_venta'];
        $pmuv = $datdet['pro_mu_venta'];
        $pmpv = $datdet['pro_mp_venta'];
        $min = $datdet['pde_minimo'];
        $uni = $datdet['pde_unidad'];

        if($uni==0)
        {
            $unimin = $pmuv ;
            $cantidad = $cantidad;
            $vis = $cantidad." UND";
        }
        else
        {
            $unimin = $pmpv;
            $cantidad = $cantidad * $min;//esto debido a que el precio del producto cuando es en peso es precio por la unidad minima ejemplo:250gr = 1 unidad
            if($cantidad>=1000)
            {
                $vis = $cantidad/1000 ." KG";
            }
            else 
            {
                $vis = $cantidad." GR";
            }
        }
        
        
        $con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idp."' and pri_activo = 1 LIMIT 1");
        $dat = pg_fetch_row($con);
        $cimg = $dat[0];
        $img = $dat[1];

       $tabla1->addCell(100)->addText($codpro,$texto,$left);
       $tabla1->addCell(300)->addText($nompro."(".$maduracion.$tamano.")",$texto,$left);
       $tabla1->addCell(100)->addText("$".number_format($precio,0,',',','),$texto,$right);
       $tabla1->addCell(100)->addText($vis,$texto,$center);
       $tabla1->addCell(100)->addText("$".number_format($subtotal,0,',',','),$texto,$right);
    }

// Add footer
$footer = $seccion->addFooter();
$footer->addPreserveText('Page {PAGE} of {NUMPAGES}.', null, array('align' => "center"));
$footer->addLink('https://delasiembra.com', 'DELASIEMBRA.COM S.A.S / Dir. Cre 58 N°77 – 41, interior 1051,Itagüí/ Cel: 310 861 10 64');

//Guardando documento
$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($documento, 'Word2007');
$objWriter->save('PED'.$codigoped.'.docx');
header("Content-Disposition: attachment; filename='PED".$codigoped.".docx'");
echo file_get_contents('PED'.$codigoped.'.docx');
