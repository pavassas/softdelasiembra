<?php 
  include ("../../data/Conexion.php");
  session_start();
  error_reporting(0);
  // variable login que almacena el login o nombre de usuario de la persona logueada
  $login= isset($_SESSION['persona']);
  // cookie que almacena el numero de identificacion de la persona logueada
  $usuario= $_COOKIE['usuario'];
  $idUsuario= $_SESSION["idusuario"];
  $clave= $_COOKIE["clave"];
  $identificacion = $_COOKIE["usIdentificacion"];
  date_default_timezone_set('America/Bogota');
  $fecha=date("Y/m/d H:i:s");

  $conusu= pg_query($dbconn,"SELECT mer_clave_int,dir_clave_int,usu_ult_telefono,prf_clave_int,usu_telefono from tbl_usuario where usu_clave_int = '".$idUsuario."'");
  $datusu = pg_fetch_array($conusu);
  $idmercado = $datusu['mer_clave_int']; if($idmercado<=0 || $idmercado==null){ $idmercado = 3;}

  //echo "<script>INICIALIZAR('Pedidos');</script>";
 
//echo "<script>CRUDPRODUCTOS('LISTAPRODUCTOS','');</script>"; 
?>
<script>
  INICIALIZAR('Pedidos');
</script>
<input id="idcategoria" value="" type="hidden">
<section class="content-header">
      <h4>
        <a ui-sref="Pedidos({ Idestado: ''})" ui-sref-opts="{reload: true}">Inicio</a>
        <small>/ <span id="gadcategoria"></span></small>
      </h4>
      <?php
         

            $conmer = pg_query($dbconn, "SELECT mer_tamano,mer_clasificacion,mer_cla_default FROM tbl_mercado WHERE mer_clave_int = '".$idmercado."'");
            $datmer = pg_fetch_array($conmer);
            //$aplicatamano = $datmer['mer_tamano'];
            $clasificacion = $datmer['mer_clasificacion'];
            $cladef = $datmer['mer_cla_default'];
            $arrayclasificacion = explode(",", $clasificacion);
            ?>
      <ol class="breadcrumb <?php if(count($arrayclasificacion)<=1){ echo "hide" ;} ?>">
      	<li>  
          Tipo Producto
          <select title="<?php echo $cladef;?>" class="select-filter" id="selcla" name="selcla" onchange="CRUDPEDIDOS('LISTAPRODUCTOS','','')">
           
           <?php
            for($c=0;$c<count($arrayclasificacion);$c++)
            {
              $concla = pg_query($dbconn,"SELECT * FROM tbl_clasificacion where cla_clave_int = '".$arrayclasificacion[$c]."'");
              $datc = pg_fetch_array($concla);
             
              ?>
              <option <?php if($cladef==$arrayclasificacion[$c]){ echo "selected"; }else if($c==0){ echo "selected"; }?> value="<?php echo $datc['cla_clave_int'];?>"><?php echo $datc['cla_nombre'];?></option>
              option
              <?php
            }
            ?>
          </select>
        
        </a></li>              
      </ol>
</section>
<section class="content">
  
  <div class="row">
    <div class="col-md-12" id="tabladatos"></div>
   <!-- <aside class="col-md-4">
     
      
    </aside>-->
    <div class="col-md-12">
      <em><strong><span class="text-red">IMPORTANTE:</span></strong>El muestrario de productos aquí descritos está sujeto a disponibilidad y stock</em>
    </div>
  </div>
 </section>