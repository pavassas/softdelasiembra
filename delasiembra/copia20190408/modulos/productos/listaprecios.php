<?php 
 include ("../../data/Conexion.php");
  session_start();
  error_reporting(0);
  // variable login que almacena el login o nombre de usuario de la persona logueada
  $login= isset($_SESSION['persona']);
  // cookie que almacena el numero de identificacion de la persona logueada
  $usuario= $_COOKIE['usuario'];
  $idUsuario= $_SESSION["idusuario"];
  $clave= $_COOKIE["clave"];
  $identificacion = $_COOKIE["usIdentificacion"];
  date_default_timezone_set('America/Bogota');
  $fecha=date("Y/m/d H:i:s");
echo "<script>INICIALIZAR('ListaPrecios');</script>";
//echo "<script>CRUDPRODUCTOS('LISTAPRECIOS','');</script>"; 
?>  
<section class="content-header">
      <h1>
        Lista de Precios
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
      	<li><a href="#"><i class="fa fa-dashboard"></i> Configuración</a></li>
        <li class="active">Lista Precios</li>
         
      </ol>
    </section>
 <section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="input-group">
                <span class="input-group-addon" data-toggle="modal" data-target="#modalleft"><i class="fa fa-filter"></i></span>
                <select id="busproducto" onchange="CRUDPRODUCTOS('LISTAPRECIOS','')" class="form-control selectpicker" placeholder="seleccione un producto" data-header="Buscar por codigo, nombre" multiple data-actions-box="true" data-live-search="true" data-selected-text-format="count > 3">
                  <?php
                  $concate = pg_query($dbconn,"select cat_clave_int,cat_nombre from tbl_categorias where cat_activo = 1");
                  $numcate = pg_num_rows($concate);
                  for($nc=0;$nc<$numcate;$nc++)
                  {
                    $datc = pg_fetch_array($concate);
                    ?>

                     <optgroup label="<?php echo $datc['cat_nombre'];?>">  
                      <?php
                    $conpro = pg_query($dbconn,"select pro_clave_int,pro_nombre from tbl_productos where cat_clave_int = '".$datc['cat_clave_int']."' and pro_activo=1 order by LOWER(pro_nombre) ASC");
                    $numpro = pg_num_rows($conpro);
                      for($np=0;$np<$numpro;$np++)
                      {
                        $datp = pg_fetch_array($conpro);
                        $idp = $datp['pro_clave_int'];
                        $nomp = $datp['pro_nombre'];
                        $con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idp."' and pri_activo = 1 LIMIT 1");
                        $dat = pg_fetch_row($con);
                        $cimg = $dat[0];
                        $img = $dat[1];
                        if($img=="" || $img==NULL)
                        {
                          $img= "dist/img/nofoto.png";

                        }
                        else
                        {
                          $img = $urlweb.$img;
                        }
                        $imagen = '<img  src="'.$img.'" alt="message user image">';
                        ?>
                        <option  value="<?php echo $idp;?>" data-content="<?php echo $nomp;?><img  class='pull-right' src='<?php echo $img;?>'  width='30'/>"><?php echo $nomp;?></option>
                        <?php
                      }
                    ?>
                     
                    </optgroup>
                    <?php
                  }
                  ?>

                </select>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12" id="tabladatos"></div>
  </div>
 </section>