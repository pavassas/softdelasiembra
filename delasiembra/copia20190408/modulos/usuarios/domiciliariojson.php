<?php
include ("../../data/Conexion.php");
error_reporting(0);
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//variable GET
$tipo  =$_POST['tipo'];
$nom = $_POST['nom'];
$ema = $_POST['ema'];
$usu = $_POST['usu'];
// DB table to use
$table = 'usuario';
// Table's primary key
$primaryKey = 'u.usu_clave_int';

// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object
// parameter names
$columns = array(
		array(
			'db' => 'u.usu_clave_int',
			'dt' => 'DT_RowId', 'field' => 'usu_clave_int',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array( 'db' => 'u.usu_clave_int', 'dt' => 'Codigo', 'field' => 'usu_clave_int' ),		
		array( 'db' => 'prf.prf_descripcion', 'dt' => 'Perfil', 'field' => 'prf_descripcion' ),
		array( 'db' => 'u.usu_email', 'dt' => 'E-Mail', 'field' => 'usu_email' ),	
		array( 'db' => 'u.usu_telefono','dt' => 'Celular', 'field' => 'usu_telefono' ),		
		array('db'  => 'u.usu_color','dt' => 'Color', 'field' => 'usu_color'),		
	    array('db'  => 'u.usu_clave_int','dt' => 'Editar', 'field' => 'usu_clave_int', 'formatter' => function( $d, $row ) {
	    	global $dbconn;
	        $idUsuario = $_COOKIE["usIdentificacion"];	   
	        return "<a class='btn btn-block btn-warning btn-xs' data-toggle='modal' data-target='#modalregistro'  onclick=CRUDUSUARIOS('CONFIRMAR','".$d."','') title='Confirmar Domiciliario' style='heigth:22px; width:22px'><i class='fa fa-check'></i></a>"; 
	    }),
	    array('db'  => 'u.usu_clave_int','dt' => 'Descartar', 'field' => 'usu_clave_int', 'formatter' => function( $d, $row ) {
	    	global $dbconn;
	        $idUsuario = $_COOKIE["usIdentificacion"];	   
	        return "<a class='btn btn-block btn-danger btn-xs'  onclick=CRUDUSUARIOS('DESCARTAR','".$d."','') title='Descartar Domiciliario' style='heigth:22px; width:22px'><i class='fa fa-trash'></i></a>"; 
	    }),
	    array( 'db' => 'u.usu_fec_registro', 'dt' => 'FechaRegistro', 'field' => 'usu_fec_registro' ),
	    array( 'db' => 'u.usu_ip', 'dt' => 'Ip', 'field' => 'usu_ip' ),
	
);

$sql_details = array(
	'user' => 'usrpavas',
	'pass' => '9A12)WHFy$2p4v4s',
	'db'   => 'dbdelasiembra',
	'port'   => '5432',
	'host' => '127.0.0.1'
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
//require('../../data/ssp.class.pg.php');
require( '../../data/ssp.class.php' );
$whereAll = "";// customerid =".$customerid." AND date( orderdate ) >= '".$startdate."' AND date( orderdate ) <= '".$enddate."'";
$groupBy = '';// u.usu_clave_int';
$with = '';

$joinQuery = "FROM  tbl_usuario AS  u inner join tbl_perfil AS prf ON (prf.prf_clave_int = u.prf_clave_int) join tbl_estados  AS es on es.est_clave_int = u.est_clave_int";

$extraWhere = "  (u.usu_nombre LIKE REPLACE('".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (u.usu_email LIKE REPLACE('".$ema."%',' ','%')  OR '".$ema."' IS NULL OR '".$ema."' = '') and (u.usu_usuario LIKE '".$usu."%' OR '".$usu."' IS NULL OR '".$usu."' = '') and u.est_clave_int!=2 and prf.prf_clave_int = '2' and usu_confirmar=0";


echo json_encode(SSP::simple($_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy,$with));

//echo json_encode(SSP::simple( $_GET, $pg_details, $table, $primaryKey, $columns,$filtroAdd ));