<?php
include ("../../data/Conexion.php");
error_reporting(0);
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//Vvariable GET
$tipo  =$_POST['tipo'];
$nom = $_POST['nom'];
$ema = $_POST['ema'];
$usu = $_POST['usu'];
// DB table to use
$table = 'usuario';
// Table's primary key
$primaryKey = 'u.usu_clave_int';

// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object


// parameter names
$columns = array(
		array(
			'db' => 'u.usu_clave_int',
			'dt' => 'DT_RowId', 'field' => 'usu_clave_int',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'row_'.$d;
			}
		),
		array( 'db' => 'u.usu_clave_int', 'dt' => 'Codigo', 'field' => 'usu_clave_int' ),
		array( 'db' => 'u.usu_nombre', 'dt' => 'Nombre', 'field' => 'usu_nombre' ),
		array( 'db' => 'prf.prf_descripcion', 'dt' => 'Perfil', 'field' => 'prf_descripcion' ),
		array( 'db' => 'u.usu_email', 'dt' => 'E-Mail', 'field' => 'usu_email' ),	
		array( 'db' => 'u.usu_usuario','dt' => 'Usuario', 'field' => 'usu_usuario' ),
		array( 'db' => 'u.usu_usu_actualiz', 'dt' => 'Usuarioa', 'field' => 'usu_usu_actualiz' ),
		array( 'db' => 'u.usu_fec_actualiz', 'dt' => 'Fecha', 'field' => 'usu_fec_actualiz' ),
		array('db'  => 'u.usu_color','dt' => 'Color', 'field' => 'usu_color'),
		array('db'  => 'es.est_clave_int','dt' => 'Activo', 'field' => 'est_clave_int','formatter' => function( $d, $row ) {
				   if($d==0)
				   {
            			return '<input type="checkbox" disabled value="">';
				   }
				   else
				   {
				   		return '<input type="checkbox" disabled checked value="">';
				   }
        		}),		
		array('db'  => 'u.usu_clave_int','dt' => 'Permisos', 'field' => 'usu_clave_int', 'formatter' => function( $d, $row ) {
			global $dbconn;
            $idUsuario = $_COOKIE["usIdentificacion"];
            //$conectar = mysqli_connect("localhost", "usrpavas", "9A12)WHFy$2p4v4s", "helpdesktesting");
            $conp1 = pg_query($dbconn,"select * from permisos_usuario where usu_clave_int = '".$idUsuario."' and pea_sigla='P41'");
            $P41 = 1;// mysqli_num_rows($conp1);
            if($P41>0) {
		      return "<a class='btn btn-xs btn-primary' data-toggle='modal' data-target='#myModal'  onclick=CRUDUSUARIOS('PERMISOS','".$d."','') title='Asignar Permisos' style='heigth:22px; width:22px'><i class='fa fa-expeditedssl'></i></a>"; } else { return "";}
		}),
	    array('db'  => 'u.usu_clave_int','dt' => 'Editar', 'field' => 'usu_clave_int', 'formatter' => function( $d, $row ) {
	    	global $dbconn;
	        $idUsuario = $_COOKIE["usIdentificacion"];
	        //$conectar = mysqli_connect("localhost", "usrpavas", "9A12)WHFy$2p4v4s", "helpdesktesting");
	        $conp1 = pg_query($dbconn,"select * from permisos_usuario where usu_clave_int = '".$idUsuario."' and pea_sigla='P37'");
	        $P37 = 1;//mysqli_num_rows($conp1);
	        if($P37>0)
	        {
	        	return "<a class='btn btn-block btn-warning btn-xs' data-toggle='modal' data-target='#modalregistro'  onclick=CRUDUSUARIOS('EDITAR','".$d."','') title='Editar Usuario' style='heigth:22px; width:22px'><i class='fa fa-edit'></i></a>"; 
	    	} else {return "";}

	    }),
	    array('db'  => 'u.usu_clave_int','dt' => 'Eliminar', 'field' => 'usu_clave_int', 'formatter' => function( $d, $row ) {
	    	global $dbconn;
	       
	        return "<a class='btn btn-block btn-danger btn-xs' data-toggle='tooltip' data-placement='bottom'  onclick=CRUDUSUARIOS('ELIMINAR','".$d."','') title='Eliminar Usuario' style='heigth:22px; width:22px'><i class='fa fa-trash'></i></a>"; 
	    	

	    }),
	    array( 'db' => 'u.usu_telefono', 'dt' => 'Telefono', 'field' => 'usu_telefono' ),
	    array( 'db' => 'u.usu_fec_registro', 'dt' => 'Registro', 'field' => 'usu_fec_registro' ),
	    array( 'db' => 'm.mer_nombre', 'dt' => 'Mercado', 'field' => 'mer_nombre' ),
	
);

$sql_details = array(
	'user' => 'usrpavas',
	'pass' => '9A12)WHFy$2p4v4s',
	'db'   => 'dbdelasiembra',
	'port'   => '5432',
	'host' => '127.0.0.1'
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */
//require('../../data/ssp.class.pg.php');
require( '../../data/ssp.class.php' );
$whereAll = "";// customerid =".$customerid." AND date( orderdate ) >= '".$startdate."' AND date( orderdate ) <= '".$enddate."'";
$groupBy = '';// u.usu_clave_int';
$with = '';

$joinQuery = "FROM  tbl_usuario AS  u inner join tbl_perfil AS prf ON (prf.prf_clave_int = u.prf_clave_int) join tbl_estados  AS es on es.est_clave_int = u.est_clave_int left outer join tbl_mercado m on m.mer_clave_int = u.mer_clave_int";

if($tipo=="Todos")
{
	$extraWhere = "  (u.usu_nombre LIKE REPLACE('".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (u.usu_email LIKE REPLACE('".$ema."%',' ','%')  OR '".$ema."' IS NULL OR '".$ema."' = '')  and (u.usu_usuario LIKE '".$usu."%' OR '".$usu."' IS NULL OR '".$usu."' = '') and u.est_clave_int!=2 and usu_confirmar=1";
}
else 
{
	$extraWhere = "  (u.usu_nombre LIKE REPLACE('".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '') and (u.usu_email LIKE REPLACE('".$ema."%',' ','%')  OR '".$ema."' IS NULL OR '".$ema."' = '') and (u.usu_usuario LIKE '".$usu."%' OR '".$usu."' IS NULL OR '".$usu."' = '') and u.est_clave_int!=2 and prf.prf_clave_int = '".$tipo."' and usu_confirmar=1";
}

echo json_encode(SSP::simple($_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy,$with));

//echo json_encode(SSP::simple( $_POST, $pg_details, $table, $primaryKey, $columns,$filtroAdd ));