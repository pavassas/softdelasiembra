<?php
$idUsuario = $_COOKIE["usIdentificacion"];
include('../../data/Conexion.php');
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//Vvariable GET
$ide = $_GET['ide'];

// DB table to use
$table = 'obra';
//act_clave_int id,act_nombre as nom,t.tpp_nombre tip,u.uni_codigo as cod,u.uni_nombre nomu,act_analisis as ana, act_sw_activo as est,p.pai_nombre as pai,d.dep_nombre as depa,c.ciu_nombre as ciu
// Table's primary key
$primaryKey = 'o.obr_clave_int';

// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object

// parameter names
$columns = array(
	array(
		'db' => 'o.obr_clave_int',
		'dt' => 'DT_RowId', 'field' => 'obr_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'rowo_'.$d;
		}
	),
		array( 'db' => 'o.obr_clave_int', 'dt' => 'Codigo', 'field' => 'obr_clave_int' ),
		array( 'db' => 'o.obr_nombre', 'dt' => 'Nombre', 'field' => 'obr_nombre','formatter'=>function($d,$row){
		    return "<input class='form-control input-sm'  type='text' value='".$d."' id='obr_".$row[1]."' onkeyup=CRUDEMPRESA('ACTUALIZAROBRA','".$row[1]."')>";

        } ),

		array( 'db' => 'o.obr_usu_actualiz', 'dt' => 'Usuario', 'field' => 'obr_usu_actualiz' ),
		array( 'db' => 'o.obr_fec_actualiz', 'dt' => 'Fecha', 'field' => 'obr_fec_actualiz' ),
		array( 'db' => 'o.obr_clave_int', 'dt' => 'Quitar', 'field' => 'obr_clave_int',"formatter"=>function($d,$row)
		{
				return "<a class='btn btn-block btn-danger btn-xs' onClick=CRUDEMPRESA('QUITAROBRA','".$d."') style='width:20px; height:20px,cursor:pointer'  title='Eliminar Obra'><i class='fa fa-trash'></i></a>";

		}),
		array( 'db' => 'o.obr_correo', 'dt' => 'Correo', 'field' => 'obr_correo','formatter'=>function($d,$row){
		    return "<input class='form-control input-sm'  type='text' value='".$d."' id='obrcorreo_".$row[1]."' onkeyup=CRUDEMPRESA('ACTUALIZARCORREO','".$row[1]."')>";

        } ),
	
);

$sql_details = array(
	'user' => 'usrpavas',
	'pass' => '9A12)WHFy$2p4v4s',
	'db'   => 'helpdesktesting',
	'host' => '127.0.0.1'
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );
$whereAll = "";// customerid =".$customerid." AND date( orderdate ) >= '".$startdate."' AND date( orderdate ) <= '".$enddate."'";
$groupBy = ' o.obr_clave_int ';
$joinQuery = "FROM  obra AS o";
$with = '';
$extraWhere = " o.epr_clave_int = '".$ide."'";
//u.salary >= 90000";  
//$whereAll = "i.act_clave_int not in(select act_clave_int from pre_cap_actividad where prc_clave_int='".$idca."')";// customerid =".$customerid." AND date( orderdate ) >= '".$startdate."' AND date( orderdate ) <= '".$enddate."'";

echo json_encode(
//SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy,$with )
);

