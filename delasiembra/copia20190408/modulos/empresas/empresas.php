<?php
error_reporting(0);
$idUsuario = $_COOKIE["usIdentificacion"];
include('../../data/Conexion.php');
include ("../../data/permisos.php");
echo "<script>CRUDEMPRESA('CARGARLISTAEMPRESAS','');</script>";
?>
<section>
<div class="row">
<div class="col-md-7">
<h4><i class="fa fa-dashboard"></i> Administrador</a> <small class="active">/ Empresas</small></h4>
</div>
    <div class="col-md-5">
        <table>
        <tr>
        <td <?php if($P48<=0){ echo 'style="display:none"';}?> class="auto-style1"><a class="btn btn-success  btn-md" id="activar" onclick="activarempresas()"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;Activar</a></td>
        <td class="auto-style1" <?php if($P49<=0){ echo 'style="display:none"';}?>><a class="btn btn-warning  btn-md" id="inactivar" onclick="inactivaempresas()"><i class="glyphicon glyphicon-ban-circle"></i>&nbsp;Inactivar</a></td>
        <td class="auto-style1" <?php if($P50<=0){ echo 'style="display:none"';}?>><a class="btn btn-danger  btn-md" id="eliminar" onclick="eliminarempresas()"><i class="glyphicon glyphicon-trash"></i>&nbsp;Eliminar</a></td>
        <td <?php if($P46<=0){ echo 'style="display:none"';}?>> <a class="btn btn-block btn-primary btn-md" onClick="CRUDEMPRESA('NUEVO','')" role="button" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;Nuevo</a></td>
        </tr>
        </table>
    </div>
</div>
 <div class="row">
<div class="col-xs-12">
	<div class="box">
            <div class="box-body">
            <table style="width: 100%">
            <tr>
            	
                 <td>
                <select class="form-control input-sm" onchange="CRUDEMPRESA('CARGARLISTAEMPRESAS','')" name="tipo2" id="tipo2">
                    <option value="">--Tipo--</option>
                    <option value="1">Cliente</option>
                    <option value="2">Proveedor</option>
                </select>
                </td>
               
				<td>
				<input class="form-control input-sm" onkeyup="CRUDEMPRESA('CARGARLISTAEMPRESAS','')" name="empresa2" id="empresa2" maxlength="70" type="text" value="" placeholder="Empresa" style="width: 150px" /></td>
				<td>
				<input class="form-control input-sm" onkeyup="CRUDEMPRESA('CARGARLISTAEMPRESAS','')" name="documento2" id="documento2" maxlength="12" type="text" value="" placeholder="Documento" style="width: 150px" /></td>
				<td>
				<input class="form-control input-sm" onkeyup="CRUDEMPRESA('CARGARLISTAEMPRESAS','')" name="telefono2" id="telefono2" maxlength="15" type="text" value="" placeholder="Telefono" style="width: 150px" /></td>
				<td>
				<input class="form-control input-sm" onkeyup="CRUDEMPRESA('CARGARLISTAEMPRESAS','')" name="direccion2" id="direccion2" maxlength="50" type="text" value="" placeholder="Dirección" style="width: 150px" /></td>
				<td>
				<input class="form-control input-sm" onkeyup="CRUDEMPRESA('CARGARLISTAEMPRESAS','')" name="email2" id="email2" maxlength="50" type="text" value="" placeholder="E-Mail" style="width: 150px" /></td>
				
			</tr>
            <tr>
                <td>
                <input class="form-control input-sm" onkeyup="CRUDEMPRESA('CARGARLISTAEMPRESAS','')" name="cuenta2" id="cuenta2" maxlength="70" type="text" value="" placeholder="N° Cuenta" style="width: 150px" /></td>
               <td>
                <input class="form-control input-sm" onkeyup="CRUDEMPRESA('CARGARLISTAEMPRESAS','')" name="ciudad2" id="ciudad2" maxlength="20" type="text" value="" placeholder="Ciudad" style="width: 150px" /></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
		</table>
       

                </div>
        <!-- /.box-body -->
            </div>
        <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
    <div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-body">
            <span id="msn1"></span>
                <div id="tabladatos"></div>
            </div>
        </div>
    </div>
    </div>
</div>

         
</section>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
      <span id="msn"></span>
        <div id="contenido"></div>        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <a role="button" name="" class="btn btn-primary" id="btnguardar" rel="" onClick="CRUDEMPRESA(this.name,'')">Guardar Cambios</a>
      </div>
    </div>
  </div>
</div>
