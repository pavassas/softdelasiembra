/* Formatting function for row details - modify as you need */

// JavaScript Document
$(document).ready(function(e) {

    var selected = [];
    var nom = $('#busnombre').val();
    var ema = $('#buscorreo').val();	
    var usu = "";// $('#bususuario').val();
    var tip = $('#tipo').val();				
    
    var table2 = $('#tbCompras').DataTable( {       
       "dom": '<"top"i>rt<"bottom"lp><"clear">',
    "ordering": true,
    "info": true,
    "autoWidth": true,
    "pagingType": "simple_numbers",
    "lengthMenu": [[10,15,20,-1], [10,15,20,"Todos"]],
    "language": {
    "lengthMenu": "Ver _MENU_ registros",
    "zeroRecords": "No se encontraron datos",
    "info": "Resultado _START_ - _END_ de _TOTAL_ registros",
    "infoEmpty": "No se encontraron datos",
    "infoFiltered": "",
    "paginate": {"previous": "Anterior","next":"siguiente"},
    "search":"",
    "sSearchPlaceholder":"Busqueda"
    },	
    "processing": true,
    "serverSide": true,
    "ajax": {
                "url": "modulos/informes/informecomprasjson.php",
                "data": {},
                "type":"POST"
            },
    "columns": [					
        { "data" : "Nombre",   "className" : "dt-left" },
        { "data" : "Mercado",   "className" : "dt-left" },
        { "data" : "Perfil",   "className" : "dt-left" },
        { "data" : "E-Mail" ,  "className" : "dt-left" },			
        { "data" : "Telefono", "className" : "dt-center" },
        { "data" : "Numdpedi", "className" : "dt-center" },
        { "data" : "Ultimafc",   "className" : "dt-center" },			
        { "data" : "Numdias",   "className" : "dt-center"},
    ],
    "order": [[0, 'asc']]
    
    
} );
 
 $('#tbCompras tbody').on('click', 'tr', function () {
    var id = this.id;
    var index = $.inArray(id, selected);

    if ( index === -1 ) {
        selected.push( id );
    } else {
        selected.splice( index, 1 );
    }

    $(this).toggleClass('selected');
} );	

 var table2 = $('#tbCompras').DataTable();

// Apply the search
 table2.columns().every( function () {
    var that = this;

    $( 'input', this.footer() ).on( 'keyup change', function () {
        that
            .search( this.value )
            .draw();
    } );
} );

});


