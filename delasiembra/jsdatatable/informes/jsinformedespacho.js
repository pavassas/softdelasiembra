// JavaScript Document
$(document).ready(function(e) {

		var selected = [];
		//var Socios = $('#idsocios').val();
		var desde = $('#busdesde').val();
		var hasta = $('#bushasta').val();
		
		var table2 = $('#tbDespachos').DataTable( { 
		"columnDefs": 
		 [ 
		    //{ "visible": false, "targets": 0 }	       
         ],	      
       	"dom": '<"top">rt<"bottom"l>p<"clear">',
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,15,20,-1], [10,15,20,"Todos"]],
		"language": {
		"lengthMenu": "Ver _MENU_",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},		
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/informes/informedespachojson.php",
                    "data": { desde:desde,hasta:hasta },//uni:uni,	
                    "type": "POST"
				},
		"columns": [			
				
			{ "data" : "FechaProgramada", "className": "dt-left" },			
			{ "data" : "Producto", "className": "dt-left" },
			{ "data" : "Calidad", "className": "dt-left" },
			{ "data" : "Tamano", "className": "dt-left" },
			{ "data" : "Estado", "className": "dt-left" },
			{ "data" : "Total", "className": "dt-right"//,render: $.fn.dataTable.render.number( ',', '.', 0, '$' ) 
			},					
			{ "data" : "Estimado", "className": "dt-right"//,render: $.fn.dataTable.render.number( ',', '.', 0, '$' ) 
			},
			{ "data" : "Verificacion", "className": "dt-center","orderable":false},
		],
		"order": [[0, 'asc'],[1, 'asc']],	
			
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
               // $(row).addClass('selected');
            }  
           

      		$('td:eq(7)>label>input',row).on('ifClicked', function (e) {
      			console.log("Prueba");
     			$(this).trigger("onclick", e);
			}).iCheck({ checkboxClass: 'icheckbox_flat-green',
      		radioClass: 'iradio_flat-green' });	

      		
        },
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows({page: 'current'}).nodes();
            var last = null;

            api.column(0, {page: 'current'}).data().each(function (group, i) {
                if (last !== group) {
                    $(rows).eq(i).before(
                        '<tr class="group"><td colspan="8" class="bg-info"><strong>Fecha Programada:' + group + '</strong></td></tr>'
                    );

                    last = group;
                }
            });
        }
		
    } );

	$('a.group-by').on( 'click', function (e) {
        e.preventDefault();
 
        table2.rowGroup().dataSrc( $(this).data('column') );
    } );


    $('#tbDespachos tbody').on( 'click', 'tr.group', function () {
        var currentOrder = table.order()[0];
        if ( currentOrder[0] === 1 && currentOrder[1] === 'asc' ) {
            table.order( [ 1, 'desc' ] ).draw();
        }
        else {
            table.order( [ 1, 'asc' ] ).draw();
        }
    } );
     
	 /*$('#tbproductos tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );*/
	
	var table2 = $('#tbDespachos').DataTable();
 
    // Apply the search
    table2.columns().every( function () {
        var that = this;



 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
});


