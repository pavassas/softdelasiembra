/* Formatting function for row details - modify as you need */

// JavaScript Document
$(document).ready(function(e) {

		var selected = [];
		var epr = $('#empresa2').val();
		var doc = $('#documento2').val();
		var tel = $('#telefono2').val();
		var dir = $('#direccion2').val();
		var ema = $('#email2').val();
		var ciu = $('#ciudad2').val();
		
		var table2 = $('#tbEmpresas').DataTable( {       
        "dom": '<"top"i>rt<"bottom"flp><"clear">',
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,15,20,30,-1], [10,15,20,30,"Todas" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "anterior","next":"siguiente"},
        "search":"",
        "sSearchPlaceholder":"Busqueda"
		},		
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/empresas/empresajson.php",
                    "data": {epr:epr, doc:doc, tel:tel, dir:dir, ema:ema, ciu:ciu}
				},
		"columns": [					
			{ "data" : "Nombre", "className" : "dt-left"},
			{ "data" : "Documento", "className" : "dt-left" },
			{ "data" : "Telefono", "className" : "dt-left" },
			{ "data" : "Fax", "className" : "dt-left" },
			{ "data" : "Direccion" , "className" : "dt-left" },
			{ "data" : "Ciudad", "className" : "dt-left" },			
			{ "data" : "Email", "className": "dt-left" },			
			{ "data" : "Usuario", "className" : "dt-center" },
			{ "data" : "Fecha", "className" : "dt-center" },
			{ "data" : "Editar", "orderable" : false },	
		],
		"order": [[0, 'asc']],
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
            $('td:eq(0)', row).css('background-color',data.Color);
            $('td:eq(1)', row).css('background-color',data.Color);
            $('td:eq(2)', row).css('background-color',data.Color);
            $('td:eq(3)', row).css('background-color',data.Color);
            $('td:eq(4)', row).css('background-color',data.Color);
            $('td:eq(5)', row).css('background-color',data.Color);
            $('td:eq(6)', row).css('background-color',data.Color);
            $('td:eq(7)', row).css('background-color',data.Color);
            $('td:eq(8)', row).css('background-color',data.Color);
        }
		
    } );
     
	 $('#tbEmpresas tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
	
	   var table2 = $('#tbEmpresas').DataTable();
 
    // Apply the search
    table2.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );

	$('#activar').click( function() 
	{
		var table = $('#tbEmpresas').DataTable();
		var col = table.rows('.selected').data().length;
		if(col <=0)		
		{
			$('#msn').html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4><i class="icon fa fa-ban"></i>Alerta!</h4>No ha seleccionado ninguna empresa</div>'); return false;
		}
		else
		{		
			for(var i = 0; i<selected.length;i++)
			{
				
				if(selected[i]=="" || selected[i]==null)
				{
				}
				else
				{
					var id = selected[i];
					var n=id.split("row_");
			      
					CRUDEMPRESA('ACTIVAR',n[1]);	
				}
			}
			$('#msn').html('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4><i class="icon fa fa-check"></i>Alerta!</h4>Empresas activadas Correctamente</div>');
			setTimeout(CRUDEMPRESA('CARGARLISTAEMPRESAS',''),1000);	
			
		}
    } ); 
	
	$('#inactivar').click( function() 
	{
		var table = $('#tbEmpresas').DataTable();
		var col = table.rows('.selected').data().length;
		if(col <=0)		
		{
		$('#msn').html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4><i class="icon fa fa-ban"></i>Alerta!</h4>No ha seleccionado ninguna empresa</div>'); return false;
		}
		else
		{		
			for(var i = 0; i<selected.length;i++)
			{				
				if(selected[i]=="" || selected[i]==null)
				{
				}
				else
				{
					var id = selected[i];
					var n=id.split("row_");			      
					CRUDEMPRESA('INACTIVAR',n[1]);	
				}
			}
			$('#msn').html('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4><i class="icon fa fa-check"></i>Alerta!</h4>Empresas Inactivada Correctamente</div>');
			setTimeout(CRUDEMPRESA('CARGARLISTAEMPRESAS',''),1000);				
		}
    } ); 
	
	$('#eliminar').click( function() 
	{
		var table = $('#tbEmpresas').DataTable();
		var col = table.rows('.selected').data().length;
		if(col <=0)		
		{
		    $('#msn').html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4><i class="icon fa fa-ban"></i>Alerta!</h4>No ha seleccionado ninguna empresa</div>'); return false;
		}
		else
		{
			jConfirm('Realmente desee eliminar las empresas seleccionadas?', 'Dialogo de Confirmación HD',null, function(r) {
		    //var conf = confirm("Realmente desee eliminar las empresas seleccionadas");
				if(r==true)
				{
					for(var i = 0; i<selected.length;i++)
					{					
						if(selected[i]=="" || selected[i]==null)
						{
						}
						else
						{
							var id = selected[i];
							var n=id.split("row_");			      	
							CRUDEMPRESA('ELIMINAR',n[1]);	
						}
					}
					$('#msn').html('<div class="alert alert-success"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4><i class="icon fa fa-ban"></i>Alerta!</h4>Empresa Eliminada Correctamente</div>'); 
					
				}
				else
				{
					return false;
				}
			});
		}
 	} );
});


