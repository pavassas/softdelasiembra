/* Formatting function for row details - modify as you need */

// JavaScript Document
$(document).ready(function(e) {

		var selected = [];
		//var Socios = $('#idsocios').val();
		var arcjs = document.getElementById('jsproductosagregar');
		var idpedido=arcjs.getAttribute('data-pedido');
		var idcalidad = arcjs.getAttribute('data-calidad');
		var producto = $('#txtproductopedido').val();
		

		var table2 = $('#tbProductosPedido').DataTable( {   
		 "columnDefs": 
		 [ 
			//{ "targets": [4], "visible":false },
			
			       
         ],    
       	"dom": '<"top"f>rt<"bottom"lp><"clear">',
       	"searching":false,
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[5,10,15,-1], [5,10,15,"Todos"]],
		"language": {
		"lengthMenu": "Ver _MENU_",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},		
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/pedidos/productosagregadosjson.php",
                    "data": { idpedido:idpedido, idcalidad: idcalidad,producto:producto  },
                    "type": "POST"
				},
		"columns": [
			
			{ "data" : "Codigo", "className" : "dt-center" },
			{ "data" : "Nombre", "className" : "dt-left" },				
			{ "data" : "Compra", "className" : "dt-left", "orderable" : false },
			{ "data" : "Cantidad", "className" : "dt-left", "orderable" : false },				
			{ "data" : "Boton", "className" : "dt-center", "orderable" : false }
		],
		"order": [[1, 'asc']],
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
               // $(row).addClass('selected');
            }  
        }		
    } );
     
	 /*$('#tbproductos tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );*/
	
	   var table2 = $('#tbProductosPedido').DataTable();
 
    // Apply the search
    table2.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
});


