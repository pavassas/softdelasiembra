/* Formatting function for row details - modify as you need */

// JavaScript Document
$(document).ready(function(e) {

		var selected = [];
		//var Socios = $('#idsocios').val();
		var nom = $('#busnombre').val();
		var mar = $('#busmarca').val();
		var cat = $('#buscategoria').val();
		var cod = $('#buscodigo').val();
		//var uni = $('#busunidad').val();
		var pro = $('#busproducto').val();
		
		var table2 = $('#tbsalidas').DataTable( {       
       	"dom": '<"top">rt<"bottom"lp><"clear">',
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,15,20,-1], [10,15,20,"Todos"]],
		"language": {
		"lengthMenu": "Ver _MENU_",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},		
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/inventario/salidasjson.php",
                    "data": { nom:nom,cod:cod,mar:mar,cat:cat,pro:pro },//uni:uni,	
                    "type": "POST"
				},
		"columns": [
			{
    			"class":          "details-control",
    			"orderable":      false,
    			"data":           null,
    			"defaultContent": ""
			},				
			
			{ "data" : "Codigo", "className": "dt-left" },
			{ "data" : "Nombre", "className": "dt-left" },
			{ "data" : "Categoria", "className": "dt-left" },			
			//{ "data" : "UniCompra", "className": "dt-center" },
			//{ "data" : "UniConsumo", "className": "dt-center" },
			{ "data" : "Cantidad", "className": "dt-center" },

		],
		"order": [[2, 'asc']],
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
               // $(row).addClass('selected');
            }  
        }
		
    } );
     
	     $('#tbsalidas tbody').on('click', 'td.details-control', function () {
      
		var tr = $(this).closest('tr');
        var row = table2.row( tr );
		var dat = row.data();
		var id = dat.Clave;
		//alert(id);
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else 
		{
            // Open this row
			var table1 = "<div id='divsal_"+id+"'  class='col-xs-12'>"+
			"<table  class='table table-condensed table-striped' id='tbsal_"+id+"'>";
			table1+="<thead><tr>"+
			"<th class='dt-head-center'></th>"+
			"<th class='dt-head-center'></th>"+
			"<th class='dt-head-center'>Fecha</th>"+
			"<th class='dt-head-center'>Motivo</th>"+
			"<th class='dt-head-center'>Calidad</th>"+
			"<th class='dt-head-center'>Cant</th>"+
			"<th class='dt-head-center'>Eq.Peso</th>"+
			//"<th class='dt-head-center'>Vr.Unit</th>"+
			//"<th class='dt-head-center'>Total</th>"+
			"<th class='dt-head-center'>Creada por</th>"+
			"<th class='dt-head-center'>Fecha Registro</th>"+			
			"</tr></thead></div>";
			row.child(table1).show();
			convertirsalidastb(id);				
			tr.addClass('shown');
		}
	} );
	
	
	   var table2 = $('#tbsalidas').DataTable();
 
    // Apply the search
    table2.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
});


function convertirsalidastb(id)
{
	var tableent = $('#tbsal_' + id).DataTable( {       
   	"dom": '<"top">rt<"bottom"lp><"clear">',
	"ordering": true,
	"info": true,
	"autoWidth": true,
	"pagingType": "simple_numbers",
	"lengthMenu": [[10,15,20,-1], [10,15,20,"Todos"]],
	"language": {
	"lengthMenu": "Ver _MENU_",
	"zeroRecords": "No se encontraron datos",
	"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
	"infoEmpty": "No se encontraron datos",
	"infoFiltered": "",
	"paginate": {"previous": "&#9668;","next":"&#9658;"}
	},		
	"processing": true,
    "serverSide": true,
    "ajax": {
                "url": "modulos/inventario/detallesalidasjson.php",
                "data": { id:id},//fi, ff	
                "type": "POST"
			},
	"columns": [				
		{ "data" : "Eliminar", "className": "dt-center" },
		{ "data" : "Editar", "className": "dt-center" },
		{ "data" : "Fecha", "className": "dt-left" },
		{ "data" : "Motivo", "className": "dt-left" },
		{ "data" : "Calidad", "className": "dt-left" },		
		{ "data" : "Cantidad", "className": "dt-left" },
		{ "data" : "Peso", "className": "dt-left" },			
		//{ "data" : "Valor", "className": "dt-center" },
		//{ "data" : "Total", "className": "dt-center" },
		{ "data" : "Usuario", "className": "dt-center" },
		{ "data" : "FechaAct", "className": "dt-center" },

	],
	"order": [[0, 'asc']]			
	} );
}


