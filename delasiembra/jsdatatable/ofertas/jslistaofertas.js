/* Formatting function for row details - modify as you need */

// JavaScript Document
$(document).ready(function(e) {

		var selected = [];
		//var Socios = $('#idsocios').val();
		var nom = $('#busnombre').val();
		var mar = $('#busmarca').val();
		var cat = $('#buscategoria').val();
		var cod = $('#buscodigo').val();
		var uni = $('#busunidad').val();
		var pro = $('#busproducto').val();
		var est = $('#busestadooferta').val();
		var ofe = $('#busoferta').val();
		
		var table2 = $('#tblistaofertas').DataTable( {       
       	"dom": '<"top">rt<"bottom"lp><"clear">',
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,15,20,-1], [10,15,20,"Todos"]],
		"language": {
		"lengthMenu": "Ver _MENU_",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},		
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/ofertas/ofertasjson.php",
                    "data": { nom:nom,cod:cod,mar:mar,cat:cat,uni:uni,pro:pro,est:est,ofe:ofe },	
                    "type": "POST"
				},
		"columns": [
			{
				"class":          "details-control",
				"orderable":      false,
				"data":           null,
				"defaultContent": ""
			},       
			{ "data" : "Eliminar", "className": "dt-left","orderable":false },
			{ "data" : "Editar", "className": "dt-center","orderable":false },				
			{ "data" : "Nombre", "className": "dt-left" },
			{ "data" : "Inicio", "className": "dt-left" },
			{ "data" : "Fin", "className": "dt-left" },
			{ "data" : "Tipo", "className": "dt-left" },
			{ "data" : "Descuento", "className": "dt-left" },
			{ "data" : "Estado", "className": "dt-left" },			
		],
		"order": [[3, 'asc']],
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
               // $(row).addClass('selected');
            }  
        }
		
    } );
     
	 /*$('#tbproductos tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );*/

     $('#tblistaofertas tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table2.row( tr );
        var idx = $.inArray( tr.attr('id'), detailRows );
 
        if ( row.child.isShown() ) {
            tr.removeClass( 'details' );
            row.child.hide();
 
            // Remove from the 'open' array
            detailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'details' );
            row.child( format( row.data() ) ).show();
 
            // Add to the 'open' array
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
        }
    } );
	
	   var table2 = $('#tblistaofertas').DataTable();
 
    // Apply the search
    table2.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
});


