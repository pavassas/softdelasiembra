/* Formatting function for row details - modify as you need */
function format ( d ){   
	
		return '<div class="row" ><div class="col-md-12" id="precio_'+d.Clave+'"></div></div>';

        
}
function PRECIOS ( d ) {
	$.post('funciones/productos/fnProductos.php', {opcion: 'PRECIOS',id:d}, function(data, textStatus, xhr) {
     	$('#precio_'+d).html(data);
     });
	}
// JavaScript Document
$(document).ready(function(e) {

		var selected = [];
		//var Socios = $('#idsocios').val();
		var nom = $('#busnombre').val();
		var mar = $('#busmarca').val();
		var cat = $('#buscategoria').val();
		var cod = $('#buscodigo').val();
       // var uni = $('#busunidad').val();
        var pro = $('#busproducto').val();
        
		
		var table2 = $('#tbproductos').DataTable( {       
       	"dom": '<"top">rt<"bottom"lp><"clear">',
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,15,20,-1], [10,15,20,"Todos"]],
		"language": {
		"lengthMenu": "Ver _MENU_",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},		
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/productos/productospreciosjson.php",
                    "data": { nom:nom,cod:cod,mar:mar,cat:cat,pro:pro},//uni:uni,	
                    "type": "POST"
				},
		
			"columns": [
			{
    			"class":          "details-control",
    			"orderable":      false,
    			"data":           null,
    			"defaultContent": ""
			},		
			{ "data" : "Imagen", "className": "dt-left" },		
			{ "data" : "Nombre", "className": "dt-left" },
			{ "data" : "Codigo", "className": "dt-left" },
			{ "data" : "Categoria", "className": "dt-left" },
			{ "data" : "Descripcion", "className": "dt-left" },
			//{ "data" : "Marca", "className": "dt-left" },
            //{ "data" : "UniCompra", "className": "dt-left" },
            //{ "data" : "UniConsumo", "className": "dt-left" },
		],
		"order": [[2, 'asc']],
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
               // $(row).addClass('selected');
            }  
        }
		
    } );
     
	 /*$('#tbproductos tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );*/


	
	var table2 = $('#tbproductos').DataTable();

	var detailRows = [];
 
    $('#tbproductos tbody').on( 'click', 'tr td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table2.row( tr );
        var idx = $.inArray( tr.attr('id'), detailRows );
 
        if ( row.child.isShown() ) {
            tr.removeClass( 'shown' );
            row.child.hide();
 
            // Remove from the 'open' array
            detailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'shown' );
            row.child( '<div class="row" ><div class="col-md-11 col-md-offset-1" id="precio_'+row.data().Clave+'"></div></div>' ).show();
            setTimeout(PRECIOS(row.data().Clave),10);
 
            // Add to the 'open' array
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
        }
    } );
 
    // Apply the search
    table2.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
});


