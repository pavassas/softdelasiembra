<?php
include('../../data/Conexion.php');
session_start();
error_reporting(0);
$IP = $_SERVER['REMOTE_ADDR'];
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_SESSION["idusuario"];
$identificacion = $_COOKIE["usIdentificacion"];
date_default_timezone_set('America/Bogota');
setlocale(LC_ALL,"es_ES.utf8","es_ES","esp");
$fecha=date("Y/m/d H:i:s");
$horaa = date("H:i");
$conusu= pg_query($dbconn,"SELECT mer_clave_int,dir_clave_int,usu_ult_telefono,prf_clave_int,usu_telefono from tbl_usuario where usu_clave_int = '".$idUsuario."'");
$datusu = pg_fetch_array($conusu);
$idmercado = $datusu['mer_clave_int']; if($idmercado<=0 || $idmercado==null){ $idmercado = 3;}
$ultimadireccion = $datusu['dir_clave_int'];
$ultimotelefono = $datusu['usu_ult_telefono'];
$usutelefono = $datusu['usu_telefono'];
if($ultimotelefono==""){
	$ultimotelefono = $usutelefono;
}
$perfil = $datusu['prf_clave_int'];

$opcion = $_POST['opcion'];
if($opcion=="INFORMEDESPACHOS")
{
	$con = pg_query($dbconn, "SELECT min(ped_fec_programada) fmin,max(ped_fec_programada) fmax FROM tbl_pedidos WHERE ped_estado in( 1,2) ");
	$dat = pg_fetch_array($con);
	$fmin = $dat['fmin'];
	$fmax = $dat['fmax'];
?>
<div class="row">
	<div class="col-md-2">
		<label>Desde</label>
		<input type="date" id="busdesde" name="busdesde" class="form-control" onchange="CRUDINFORMES('LISTAINFORMEDESPACHOS')" value="<?php echo $fmin;?>" min="<?php echo $fmin;?>" max="<?php echo $fmax;?>">
	</div>
	<div class="col-md-2">
		<label>Hasta</label>
		<input type="date" id="bushasta" name="bushasta" class="form-control" onchange="CRUDINFORMES('LISTAINFORMEDESPACHOS')" value="<?php echo $fmax;?>" max="<?php echo $fmax;?>">
	</div>
	<div class="col-md-2">
	<br>
	<a class="btn btn-success" onclick="CRUDINFORMES('DESCARGARINFORMESPDF','','')"><i class="fa fa-file-pdf-o"></i>Exportar informe</a>
	</div>
</div>
<div class="row">
	<div class="col-md-12" id="divlistadespacho"></div>
</div>
<?php

echo "<script>CRUDINFORMES('LISTAINFORMEDESPACHOS');</script>";
}
else if($opcion=="LISTAINFORMEDESPACHOS")
{
	?>
	<script src="jsdatatable/informes/jsinformedespacho.js"></script>
	<table class="table table-bordered table-striped compact responsive" id="tbDespachos" style="width: 100%">
		<thead>
			<tr>
				<th></th>								
				<th>Producto</th>
				<th>Calidad</th>
				<th>Tamaño</th>
				<th>Maduracion</th>
				<th>Cant.Total</th>
				<th>Peso.Estimado</th>
				<th>Verificación</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th></th>							
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
			</tr>
		</tfoot>
	</table>
	<?php
}
else if($opcion=="GUARDARVERIFICACION")
{
	$fec = $_POST['fec'];
	$idp = $_POST['idp'];
	$idc = $_POST['idc'];
	$tam = $_POST['tam'];
	$mad = $_POST['mad'];

	$veri = pg_query($dbconn,"SELECT * FROM tbl_pedidos_verificar WHERE pro_clave_int = '".$idp."' and cla_clave_int = '".$idc."' and pev_tamano = '".$tam."' and pev_maduracion = '".$mad."' and pev_fecha = '".$fec."' LIMIT 1");
	
	$numv = pg_num_rows($veri);
	if($numv>0)
	{
		$datv = pg_fetch_array($veri);
		$pev = $datv['pev_clave_int'];
		$del = pg_query($dbconn, "DELETE FROM tbl_pedidos_verificar WHERE pev_clave_int = '".$pev."'");
		if($del>0)
		{
			$res = "ok";
			$msn = "Producto deseleccionado";
		}
		else
		{
			$res = "error";
			$msn = "Surgió un error al deseleccionar producto. Error BD(".pg_last_error($dbconn).")";
		}
	}
	else
	{
		$ins = pg_query($dbconn,"INSERT INTO tbl_pedidos_verificar(pro_clave_int,cla_clave_int,pev_tamano,pev_maduracion,pev_fecha,pev_usu_actualiz,pev_fec_actualiz) VALUES('".$idp."','".$idc."','".$tam."','".$mad."','".$fec."','".$usuario."','".$fecha."')");
		if($ins>0)
		{
			$res = "ok";
			$msn = "Verificación de producto guardada correctamente";
		}
		else
		{
			$res = "error";
			$msn = "Surgió un error al guardar Verificación de producto. Error BD(".pg_last_error($dbconn).")";
		}
	}
	$datos[] = array("res"=>$res, "msn"=>$msn);
	echo json_encode($datos);
}else if ($opcion == "INFORMECOMPRADOR") {
	?> 
	<div class="row">
		<div class="col-md-2">
			<a class="btn btn-success" onclick="CRUDINFORMES('DESCARGARINFORMESCOMPRASPDF','','')"><i class="fa fa-file-pdf-o"></i>Exportar informe</a>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12" id="divlistacompras"></div>
	</div>
	<?php
	echo "<script>CRUDINFORMES('LISTAINFORCOMPRAS');</script>";
}else if ($opcion == "LISTAINFORCOMPRAS") {


	?>
	<script src="jsdatatable/informes/jsinformecompras.js"></script>
	<table class="table table-bordered table-striped compact responsive" id="tbCompras" style="width: 100%">
		<thead>
			<tr>
				<th>Nombre</th>	
				<th>Mercado</th>	
				<th>Perfil</th>	
				<th>E-mail</th>	
				<th>Celular</th>								
				<th>Cantidad de pedidos</th>
				<th>Ultima fecha de pedido</th>
				<th>Días trascurrido del ultimo pedido</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th></th>							
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
			</tr>
		</tfoot>
	</table>
	<?php
}