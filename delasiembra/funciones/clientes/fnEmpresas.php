<?php 
include ("../data/Conexion.php");
error_reporting(0);
session_start();
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
	//$idUsuario= $_SESSION["idusuario"];
$idUsuario = $_COOKIE["usIdentificacion"];
$clave= $_COOKIE["clave"];
$fecha=date("Y/m/d H:i:s");
$con = mysqli_query($conectar,"select * from usuario u inner join perfil p on (p.prf_clave_int = u.prf_clave_int) where u.usu_usuario = '".$usuario."'");
$dato = mysqli_fetch_array($con);
$perfil = $dato['prf_descripcion'];
$percla = $dato['prf_clave-int'];
$claveusuario = $dato['usu_clave_int'];
$nombre = $dato['usu_nombre'];
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
include ("../data/permisos.php");
$opcion = $_POST['opcion'];
if($opcion=="NUEVO")
{
	?>
	 <form name="form1" id="form1" class="form-horizontal">
			   <div class="row">
                   <div class="col-md-6">
                       <strong>Tipo:</strong>
                       <select id="tipo" name="tipo" onchange="CRUDEMPRESA('TIPOEMPRESA','')" class="form-control input-sm selectpicker">
                           <option value="1">Cliente</option>
                           <option value="2">Proveedor</option>
                       </select>

                   </div>
               </div>
    <div class="row">
        <div class="col-md-6"><strong>Nombre:</strong><input class="form-control input-sm" name="empresa" id="empresa" maxlength="70" type="text" /></div>
        <div class="col-md-6"><strong>Documento:</strong>
        <input class="form-control input-sm" name="documento" id="documento" maxlength="20" type="text" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-6"><strong>Teléfono:</strong>
        <input class="form-control input-sm" name="telefono" id="telefono" maxlength="15" type="text" />
        </div>
        <div class="col-md-6"><strong>Celular:</strong>
        <input class="form-control input-sm" name="fax" id="fax" maxlength="15" type="text" />
        </div>
    </div>
    <div class="row">
        <div class="col-md-6"><strong>Dirección:</strong>
        <input class="form-control input-sm" name="direccion" id="direccion" maxlength="50" type="text" /></div>			
        <div class="col-md-6"><strong>Ciudad:</strong>
        <input class="form-control input-sm" name="ciudad" id="ciudad" maxlength="20" type="text" />
        </div>
    </div>			
    <div class="row">
    	<div class="col-md-6"><strong>E-Mail:</strong>
    	<input class="form-control input-sm" name="email" id="email" maxlength="50" type="text"  /></div>
        <div class="col-md-3"> <strong>Contrato:</strong><br>


        <input name="ckcontrato" type="checkbox"  value="1" /></div>
        <div class="col-md-3"> <strong>En Mora:</strong><br>


            <input name="ckmora" id="ckmora" type="checkbox"  value="1" /></div>
    </div>
    <div class="row" id="divdatosbanco">
    	
    	<div class="col-md-6"><strong>Nombre Banco:</strong>
    	<input class="form-control input-sm" name="nombanco" id="nombanco" maxlength="50" type="text"  /></div>
    	<div class="col-md-3"><strong>Tipo Cuenta:</strong>
    	<input class="form-control input-sm" name="tipbanco" id="tipbanco" maxlength="50" type="text" placeholder="ejemplo: cuenta ahorros, credito.." /></div>
    	<div class="col-md-3"><strong>Numero cuenta:</strong>
    	<input class="form-control input-sm" name="numbanco" id="numbanco" maxlength="50" type="text"  /></div>
    </div>
			
                 
    </form>
    <?php
    echo "<script>CRUDEMPRESA('TIPOEMPRESA','');</script>";
    echo "<script>INICIALIZARLISTAS();</script>";
}
else if($opcion=="EDITAR")
{
	    $epredi = $_POST['id'];
		$con = mysqli_query($conectar,"select * from empresa where epr_clave_int = '".$epredi."'"); 
		$dato = mysqli_fetch_array($con); 
		$epr = $dato['epr_nombre'];
		$doc = $dato['epr_nro_docum'];
		$tel = $dato['epr_telefono'];
		$dir = $dato['epr_direccion'];
		$ema = $dato['epr_email'];
		$fax = $dato['epr_fax'];
		$ciu = $dato['epr_ciudad'];
		$cont = $dato['epr_contrato'];
		$mora = $dato['epr_mora'];
		$tip = $dato['epr_tipo'];
		$numbanco = $dato['epr_num_banco'];
		$nombanco = $dato['epr_nom_banco'];
		$tipbanco = $dato['epr_tipo_cuenta'];
  ?>
  <form name="form1" id="form1" class="form-horizontal">
			   <input type="hidden" id="idedicion" value="<?php echo $epredi;?>">
      <div class="row">
          <div class="col-md-6">
              <strong>Tipo:</strong>
              <select id="tipo" onchange="CRUDEMPRESA('TIPOEMPRESA','')" name="tipo" class="form-control input-sm selectpicker">
                  <option value="1" <?php if($tip==1){ echo "selected";}?>>Cliente</option>
                  <option value="2" <?php if($tip==2){ echo "selected";}?>>Proveedor</option>
              </select>

          </div>
      </div>
		<div class="row">
                 <div class="col-md-6"><strong>Nombre:</strong><input class="form-control input-sm" name="empresa" id="empresa" maxlength="70" type="text" value="<?php echo $epr;?>" /></div>
                 <div class="col-md-6"><strong>Documento:</strong>
                 <input class="form-control input-sm" name="documento" id="documento" maxlength="20" type="text" value="<?php echo $doc;?>" />
            </div></div>
			<div class="row">
                 <div class="col-md-6"><strong>Teléfono:</strong>
			<input class="form-control input-sm" name="telefono" id="telefono" maxlength="15" type="text" value="<?php echo $tel;?>" />
            </div>
            
            <div class="col-md-6"><strong>Celular:</strong>
			<input class="form-control input-sm" name="fax" id="fax" maxlength="15" type="text"  value="<?php echo $fax;?>"/>
            </div>
            </div>
            <div class="row">
            <div class="col-md-6"><strong>Dirección:</strong>
            <input class="form-control input-sm" name="direccion" id="direccion" maxlength="50" type="text" value="<?php echo $dir;?>" />
            </div>
            <div class="col-md-6"><strong>Ciudad:</strong>
            <input class="form-control input-sm" name="ciudad" id="ciudad" maxlength="20" type="text" value="<?php echo $ciu;?>" />
            </div>            
            </div>
            <div class="row">
            <div class="col-md-6"><strong>E-Mail:</strong>
            <input class="form-control input-sm" name="email" id="email" maxlength="50" type="text" value="<?php echo $ema;?>"   />
            </div>
                <div class="col-md-3">


                    <strong>Contrato:</strong><br>

                    <input <?php if($cont == 1){ echo 'checked="checked"'; } ?> name="ckcontrato" id="ckcontrato" type="checkbox" value="1" />

                </div>
                <div class="col-md-3">


                    <strong>En Mora:</strong><br>

                    <input <?php if($mora == 1){ echo 'checked="checked"'; } ?> name="ckmora" id="ckmora" type="checkbox" value="1" />

                </div>
            </div>
			<div class="row" id="divdatosbanco">
			
			<div class="col-md-6"><strong>Nombre Banco:</strong>
			<input class="form-control input-sm" name="nombanco" id="nombanco" maxlength="50" type="text" value="<?php echo $nombanco;?>"  /></div>
			<div class="col-md-3"><strong>Tipo Cuenta:</strong>
			<input class="form-control input-sm" name="tipbanco" id="tipbanco" maxlength="50" type="text" value="<?php echo $tipbanco;?>" placeholder="ejemplo: cuenta ahorros, credito.." /></div>
			<div class="col-md-3"><strong>Numero cuenta:</strong>
			<input class="form-control input-sm" name="numbanco" id="numbanco" maxlength="50" type="text" value="<?php echo $numbanco;?>" /></div>
			</div>
            <div class="row">
                <div class="col-md-5"><strong>Nueva Obra:</strong>
                    <input class="form-control input-sm" name="obra" id="obra" maxlength="50" type="text" value=""   />
                </div>

                <div class="col-md-5"><strong>Correo Obra:</strong>
                    <input class="form-control input-sm" name="correoobra" id="correoobra" maxlength="80" type="text" value=""   />
                </div>
                <div class="col-md-1"><br>
                    <a class="btn btn-sm btn-success" title="Agregar Obra" ONCLICK="CRUDEMPRESA('AGREGAROBRA','')"><i class="fa fa-plus-circle"></i> </a>
                </div>
            </div>
      <div class="row">
          <div class="col-md-12">
              <script src="js2/jslistaobras.js" type="application/javascript"></script>
              <table id="tbObras" class="table table-bordered compact" style="font-size:11px ">
                  <thead><tr><th></th><th>Nombre de la Obra</th><th>Correo</th></tr></thead>
                  <tfoot><tr><th></th><th></th></tr></tfoot>
              </table>

          </div>

      </div>
			
                 
    </form>
  <?Php
    echo "<script>CRUDEMPRESA('TIPOEMPRESA','');</script>";
    echo "<script>INICIALIZARLISTAS();</script>";
}
else
if($opcion=="CARGARLISTAEMPRESAS")
{
   ?>
<script type="text/javascript" src="js2/jslistaempresa.js"></script>
<table id="tbEmpresas" class="table table-striped table-bordered compact" cellspacing="0" width="100%" style="font-size:11px">
<thead>
<tr>
 <th class="dt-head-center"><strong>Tipo</strong></th>
<th class="dt-head-center"><strong>Empresa</strong></th>
<th class="dt-head-center"><strong>Documento</strong></th>
<th class="dt-head-center"><strong>Teléfono</strong></th>
<th class="dt-head-center"><strong>Celular</strong></th>
<th class="dt-head-center"><strong>Dirección</strong></th>
<th class="dt-head-center"><strong>Ciudad</strong></th>
<th class="dt-head-center"><strong>E-Mail</strong></th>
<th class="dt-head-center"><strong>Cuenta</strong></th>
<th class="dt-head-center"></th>
<th class="dt-head-center"></th>
</tr>
<thead>
	<tfoot>
<tr>
<th class="dt-head-center"><strong></strong></th>
<th class="dt-head-center"><strong></strong></th>
<th class="dt-head-center"><strong></strong></th>
<th class="dt-head-center"><strong></strong></th>
<th class="dt-head-center"><strong></strong></th>
<th class="dt-head-center"><strong></strong></th>
<th class="dt-head-center"><strong></strong></th>
<th class="dt-head-center"><strong></strong></th>
<th class="dt-head-center"><strong></strong></th>
<th class="dt-head-center"><strong></strong></th>
<th class="dt-head-center"></th>
</tr>
<tfoot>

</table>
   <?PHP
}
else if($opcion=="ACTIVAR")
{
   $id = $_POST['id'];
   $con = mysqli_query($conectar,"update empresa set est_clave_int = '1',epr_usu_actualiz='".$usuario."',epr_fec_actualiz= '".$fecha."' where epr_clave_int = '".$id."'");
   if($con>0)
   {
	   echo 1;
   }
   else 
   {
      echo 2;
   }
}
else if($opcion=="INACTIVAR")
{
   $id = $_POST['id'];
   $con = mysqli_query($conectar,"update empresa set est_clave_int = '0',epr_usu_actualiz='".$usuario."',epr_fec_actualiz= '".$fecha."' where epr_clave_int = '".$id."'");
   if($con>0)
   {
	   echo 1;
   }
   else 
   {
      echo 2;
   }
}
else if($opcion=="ELIMINAR")
{
   $id = $_POST['id'];
   $con = mysqli_query($conectar,"update empresa set est_clave_int = '2',epr_usu_actualiz='".$usuario."',epr_fec_actualiz= '".$fecha."' where epr_clave_int = '".$id."'");
   if($con>0)
   {
	   echo 1;
   }
   else 
   {
       echo 2;
   }
}
else if($opcion=="TODOS")
{
	$con = mysqli_query($conectar,"select COUNT(*) cant from empresa where est_clave_int<>2");
	$dato = mysqli_fetch_array($con);
	echo "Todos ".$dato['cant'];
}
else if($opcion=="GUARDAR")
{
    	$epr = $_POST['epr'];
		$doc = $_POST['doc'];
		$tel = $_POST['tel'];
		$dir = $_POST['dir'];
		$ema = $_POST['ema'];
		$fax = $_POST['fax'];
		$ciu = $_POST['ciu'];
		$le = $_POST['le'];
		$cont = $_POST['cont'];
		$mora = $_POST['mora'];
		$tip = $_POST['tip'];
		$numbanco = $_POST['numb'];
		$nombanco = $_POST['nomb'];
		$tipbanco = $_POST['tipb'];
       if($cont == '' || $cont==NULL){ $swcont = 0; }else{ $swcont = 1; }
    if($mora == '' || $mora==NULL){ $swmora = 0; }else{ $swmora = 1; }
		
		$sql = mysqli_query($conectar,"select * from empresa where epr_nro_docum = '".$doc."' OR UPPER(epr_email) = UPPER('".$ema."')");
		$dato = mysqli_fetch_array($sql);
		$condoc = $dato['epr_nro_docum'];
		$conema = $dato['epr_email'];
		if($P46<=0)
		{
			$res  = 2;
			$msn  = 'No posee permisos para crear nuevas empresas. Verificar';
		}
		else if($tip=="")
        {
            $res = 2;
            $msn = "Debe seleccionar el tipo de cliente";
        }
        else

		if($epr == '')
		{
			$res  = 2;
			$msn  = 'Debe ingresar el nombre de la Empresa. Verificar';
		}
		else
		if($le < 3)
		{   
		    $res  = 2;
			$msn =  'El nombre de la empresa debe ser mí­nimo de 3 dijitos. Verificar';
		}
		else
		if($doc == '')
		{
			$res  = 2;
			$msn =  'Debe ingresar el Documento de la empresa';
		}
		else
		if(STRTOUPPER($conema) != '' and STRTOUPPER($ema) != '' and STRTOUPPER($conema) == STRTOUPPER($ema))
		{
			$res  = 2;
			$msn =  'El E-mail ingresado ya existe';
		}
		else
		{
			if($_POST["ema"] != '')
			{
				if(STRTOUPPER($ema) == STRTOUPPER($conema))
				{
					$res  = 2;
					$msn =  'La empresa ingresada ya existe';
				}
				else
				if (preg_match('/^[A-Za-z0-9-_.+%]+@[A-Za-z0-9-.]+\.[A-Za-z]{2,4}$/', $_POST["ema"])) 
				{	
					$con = mysqli_query($conectar,"insert into empresa (epr_nombre,epr_nro_docum,epr_telefono,epr_direccion,epr_email,epr_fax,epr_ciudad,epr_usu_actualiz,epr_fec_actualiz,epr_contrato,epr_mora,epr_tipo,epr_num_banco,epr_nom_banco,epr_tipo_cuenta) values('".$epr."','".$doc."','".$tel."','".$dir."','".$ema."','".$fax."','".$ciu."','".$usuario."','".$fecha."','".$swcont."','".$swmora."','".$tip."','".$numbanco."','".$nombanco."','".$tipbanco."')");
					
					if($con > 0)
					{
						$res  = 1;
						$msn =  'Datos grabados correctamente';
					}
					else
					{
						$res  = 2;
						$msn =  'No se han podido guardar los datos';
					}
				}
				else
				{
					$res  = 2;
					$msn =  'Debe ingresar un e-mail válido';
				}
			}
			else
			{
				$con = mysqli_query($conectar,"insert into empresa (epr_nombre,epr_nro_docum,epr_telefono,epr_direccion,epr_email,epr_fax,epr_ciudad,epr_usu_actualiz,epr_fec_actualiz,epr_contrato,epr_mora,epr_tipo,epr_num_banco,epr_nom_banco,epr_tipo_cuenta) values('".$epr."','".$doc."','".$tel."','".$dir."','".$ema."','".$fax."','".$ciu."','".$usuario."','".$fecha."','".$swcont."','".$swmora."','".$tip."','".$numbanco."','".$nombanco."','".$tipbanco."')");
				
				if($con >0)
				{
					$res  = 1;
					$msn =  'Datos grabados correctamente';
				}
				else
				{
					$res  = 2;
					$msn = 'No se han podido guardar los datos';
				}
			}
		}
   $datos[] = array("res"=>$res,"msn"=>$msn);
   echo json_encode($datos);
	
}
else if($opcion=="GUARDAREDICION")
{
	

		//sleep(1);
		$epr = $_POST['epr'];
		$doc = $_POST['doc'];
		$tel = $_POST['tel'];
		$dir = $_POST['dir'];
		$ema = $_POST['ema'];
		$fax = $_POST['fax'];
		$ciu = $_POST['ciu'];
		$le = $_POST['le'];
		$e = $_POST['ide'];
		$cont = $_POST['cont'];
		$tip = $_POST['tip'];
		$numbanco = $_POST['numb'];
		$nombanco = $_POST['nomb'];
		$tipbanco = $_POST['tipb'];
    if($cont == '' || $cont==NULL){ $swcont = 0; }else{ $swcont = 1; }
    $mora = $_POST['mora'];
    if($mora == '' || $mora==NULL){ $swmora = 0; }else{ $swmora = 1; }
		
		$sql = mysqli_query($conectar,"select * from empresa where (UPPER(epr_nombre) = UPPER('".$epr."')) AND epr_clave_int <> '".$e."'"); //OR UPPER(epr_email) = UPPER('".$ema."')
		$dato = mysqli_fetch_array($sql);
		$conepr = $dato['epr_nombre'];
		$conema = $dato['epr_email'];
		
		if($P47<=0)
		{
			$res  = 2;
			$msn  = 'No posee permisos para editar una empresa. Verificar';
		}
        else if($tip=="")
        {
            $res = 2;
            $msn = "Debe seleccionar el tipo de cliente";
        }
		else

		if($epr == '')
		{
			$res  = 2;
			$msn  = 'Debe ingresar el nombre de la Empresa. Verificar';
		}
		else
		if($le < 3)
		{   
		    $res  = 2;
			$msn =  'El nombre de la empresa debe ser mí­nimo de 3 dijitos. Verificar';
		}
		else
		if($doc == '')
		{
			$res  = 2;
			$msn =  'Debe ingresar el Documento de la empresa';
		}
		else
		if(STRTOUPPER($conema) != '' and STRTOUPPER($ema) != '' and STRTOUPPER($conema) == STRTOUPPER($ema))
		{
			$res  = 2;
			$msn =  'El E-mail ingresado ya existe';
		}
		else
		{
			if($_POST["ema"] != '')
			{
				if(STRTOUPPER($ema) == STRTOUPPER($conema))
				{
					$res  = 2;
					$msn =  'La empresa ingresada ya existe';
				}
				else
				if (preg_match('/^[A-Za-z0-9-_.+%]+@[A-Za-z0-9-.]+\.[A-Za-z]{2,4}$/', $_POST["ema"])) 
				{	
					$con = mysqli_query($conectar,"update empresa set epr_nombre = '".$epr."',epr_nro_docum = '".$doc."',epr_telefono = '".$tel."',epr_direccion = '".$dir."',epr_email = '".$ema."',epr_fax = '".$fax."',epr_ciudad = '".$ciu."',epr_usu_actualiz = '".$usuario."',epr_fec_actualiz = '".$fecha."',epr_contrato = '".$swcont."',epr_mora = '".$mora."',epr_tipo= '".$tip."' ,epr_num_banco = '".$numbanco."',epr_nom_banco = '".$nombanco."',epr_tipo_cuenta = '".$tipbanco."' where epr_clave_int = '".$e."'");
				if($con > 0)
					{
						$res  = 1;
						$msn =  'Datos actualizados correctamente';
					}
					else
					{
						$res  = 2;
						$msn =  'No se han podido actualizar los datos';
					}
				}
				else
				{
					$res  = 2;
					$msn =  'Debe ingresar un e-mail válido';
				}
			}
			else
			{
				$con = mysqli_query($conectar,"update empresa set epr_nombre = '".$epr."',epr_nro_docum = '".$doc."',epr_telefono = '".$tel."',epr_direccion = '".$dir."',epr_email = '".$ema."',epr_fax = '".$fax."',epr_ciudad = '".$ciu."',epr_usu_actualiz = '".$usuario."',epr_fec_actualiz = '".$fecha."',epr_contrato = '".$swcont."',epr_mora = '".$mora."',epr_tipo = '".$tip."',epr_num_banco = '".$numbanco."',epr_nom_banco = '".$nombanco."',epr_tipo_cuenta = '".$tipbanco."' where epr_clave_int = '".$e."'");
			if($con >0)
				{
					$res  = 1;
					$msn =  'Datos Actualizados correctamente';
				}
				else
				{
					$res  = 2;
					$msn = 'No se han podido actualizar los datos';
				}
			}
		}
   $datos[] = array("res"=>$res,"msn"=>$msn);
   echo json_encode($datos);
	
}
else if($opcion=="AGREGAROBRA")
{
    $obra = $_POST['obra'];
	$correo = $_POST['correo'];
    $ide = $_POST['ide'];

    $veri = mysqli_query($conectar,"select * from obra where epr_clave_int = '".$ide."' and UPPER(obr_nombre) = '".$obra."'");
    $numv = mysqli_num_rows($veri);
    if($numv>0)
        {
            $res = "error1";
        }
		$veri1 = mysqli_query($conectar,"select * from obra where epr_clave_int = '".$ide."' and UPPER(obr_correo) = '".$correo."'");
    $numv1 = mysqli_num_rows($veri1);
    if($numv1>0)
        {
            $res = "error3";
        }
        else
        {
            $ins = mysqli_query($conectar,"insert into obra(epr_clave_int,obr_nombre,obr_usu_actualiz,obr_fec_actualiz,obr_correo) VALUES('".$ide."','".$obra."','".$usuario."','".$fecha."','".$correo."')");
            if($ins>0)
            {
                $res = "ok";
            }
            else{
                $res = "error2";
            }
        }
        $datos[] = array("res"=>$res);
    echo json_encode($datos);
}
else if($opcion=="ACTUALIZAROBRA")
{
    $ido = $_POST['ido'];
    $ide = $_POST['ide'];
    $obra = $_POST['obra'];
    $veri = mysqli_query($conectar,"select * from obra where epr_clave_int = '".$ide."' and UPPER(obr_nombre) = '".$obra."' and obr_clave_int != '".$ido."'");
    $numv = mysqli_num_rows($veri);
    if($numv) {
        $res = "error1";
    }
    else
    {
        $upd = mysqli_query($conectar,"UPDATE obra SET obr_nombre = '".$obra."',obr_usu_actualiz = '".$usuario."',obr_fec_actualiz = '".$fecha."' where obr_clave_int = '".$ido."'");
        if($upd>0)
        {
           $res = "ok";
        }
        else
        {
            $res = "error2";
        }
    }
    $datos[] = array("res"=>$res);
    echo json_encode($datos);
}
else if($opcion=="QUITAROBRA")
{
    $ido = $_POST['ido'];
    $del = mysqli_query($conectar, "delete  from obra where obr_clave_int = '".$ido."'");
    if($del>0)
    {
        $res = "ok";
    }
    else{
        $res = "error";
    }
    $datos[] = array("res"=>$res);
    echo json_encode($datos);
}
else if($opcion=="ACTUALIZARCORREO")
{
	
    $ido = $_POST['ido'];
    $ide = $_POST['ide'];
    $correo = $_POST['correo'];
    $veri = mysqli_query($conectar,"select * from obra where epr_clave_int = '".$ide."' and UPPER(obr_correo) = '".$obra."' and obr_clave_int != '".$ido."'");
    $numv = mysqli_num_rows($veri);
    if($numv) {
        $res = "error1";
    }
    else
    {
        $upd = mysqli_query($conectar,"UPDATE obra SET obr_correo = '".$correo."',obr_usu_actualiz = '".$usuario."',obr_fec_actualiz = '".$fecha."' where obr_clave_int = '".$ido."'");
        if($upd>0)
        {
           $res = "ok";
        }
        else
        {
            $res = "error2";
        }
    }
    $datos[] = array("res"=>$res);
    echo json_encode($datos);

	
}
else if($opcion=="AGREGARCORREOOBRA")
{
	
    $obra = $_POST['obra'];
    $ide = $_POST['ide'];

    $veri = mysqli_query($conectar,"select * from obra where epr_clave_int = '".$ide."' and UPPER(obr_correo) = '".$obra."'");
    $numv = mysqli_num_rows($veri);
    if($numv>0)
        {
            $res = "error1";
        }
        else
        {
            $ins = mysqli_query($conectar,"insert into obra(epr_clave_int,obr_correo,obr_usu_actualiz,obr_fec_actualiz) VALUES('".$ide."','".$obra."','".$usuario."','".$fecha."')");
            if($ins>0)
            {
                $res = "ok";
            }
            else{
                $res = "error2";
            }
        }
        $datos[] = array("res"=>$res);
    echo json_encode($datos);

}