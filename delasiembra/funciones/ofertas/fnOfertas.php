<?php
include('../../data/Conexion.php');
session_start();
error_reporting(0);
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_SESSION["idusuario"];
$clave= $_COOKIE["clave"];
$identificacion = $_COOKIE["usIdentificacion"];
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");

 
$opcion = $_POST['opcion'];
if($opcion=="NUEVO")
{

   	?>
   	<form class="form-horizontal" id="form3" name="form3" action="_" method="post" enctype="multipart/form-data">
             
          
   	<div class="col-xs-12 col-md-6">
   		<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Datos generales de la oferta</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: block;">
            	<div class="form-group row">
            		<label class="col-xs-12 col-md-2 ">Nombre:</label>
            		<div class="col-xs-12 col-md-6"><input id="txttitulo" name="txttitulo" class="form-control input-sm" autocomplete="off"></div>
            		<label class="col-xs-12 col-md-2 ">Estado</label>
            		<div class="col-xs-12 col-md-2">
						<label class="bs-switch">
						<input type="checkbox" id="ckestado" name="ckestado" value="1" checked>
						<span class="slider round"></span>
						</label>
            		</div>
            	</div>
            	<div class="form-group row">
            		<label class="col-xs-12 col-md-2" for="">Descripción:</label>
            		<div class="col-xs-12 col-md-10">
            			<textarea class="form-control input-sm" name="txtdescripcion" id="txtdescripcion"></textarea>
            		</div>
            	</div>
            	<div class="form-group row">
            		<label class="col-xs-12 col-md-2">Mercado:</label>
            		<div class="col-xs-12 col-md-10">
            			<select name="selmercado" id="selmercado" class="form-control input-sm selectpicker" title="Seleccione mercado">
            				<?php
            				$conm = pg_query($dbconn, "SELECT mer_clave_int,mer_nombre FROM tbl_mercado WHERE mer_activo = 1");
            				while($datm = pg_fetch_array($conm))
            				{
            				?>
							<option value="<?php echo $datm['mer_clave_int'];?>"><?php echo $datm['mer_nombre'];?></option>
							<?php
							}
							?>
						</select>
            		</div>
            	</div>
            	<h4 class="divider">Validez</h4>
            	<div class="form-group row">
            		<label class="col-xs-12 col-md-3 ">Fecha Inicio:</label>
            		<div class="col-xs-12 col-md-4"><input id="txtfechainicio" name="txtfechainicio" class="form-control input-sm " data-date-format="yyyy-mm-dd" autocomplete="off" type="date">            			
            		</div>
            		
            		<div class="col-xs-12 col-md-4 input-group bootstrap-timepicker">
						<input type="text" id="txthorainicio" name="txthorainicio" class="form-control input-sm timepicker">
						<div class="input-group-addon">
						<i class="fa fa-clock-o"></i>
						</div>
					</div>
            	</div>

            	<div class="form-group row">
            		<label class="col-xs-12  col-md-3 ">Fecha Fin:</label>
            		<div class="col-xs-12 col-md-4"><input id="txtfechafin" name="txtfechafin" class="form-control input-sm" data-date-format="yyyy-mm-dd" autocomplete="off" type="date"></div>
            		
					<div class="col-xs-12  col-md-4 input-group bootstrap-timepicker">
						<input type="text" id="txthorafin" name="txthorafin" class="form-control input-sm timepicker">
						<div class="input-group-addon">
						<i class="fa fa-clock-o"></i>
						</div>
					</div>
            	</div>
            	<div class="form-group row">
            		<label class="col-xs-12  col-md-12">
            			¿Cuál es el tipo y el valor del descuento?
            		</label>
					<div class="col-xs-12 col-md-12">
						<div class="col-xs-12  col-md-6">
						<select name="seltipodescuento" id="seltipodescuento" class="form-control input-sm selectpicker" title="Seleccione tipo descuento" onchange="CRUDOFERTAS('TIPODESCUENTO','')" >
							<option value="1">Valor</option>
							<option value="2">Porcentaje</option>
						</select></div>
						<div class=" col-xs-12 col-md-6 input-group">						
							<input type="text" id="txtvalor" name="txtvalor" class="form-control input-sm">
							<div class="input-group-addon" id="divtipodescuento">
							%
							</div>
						</div>
					</div>
            	</div>
            	<div class="form-group row">
            		<div class="col-md-12">
            			<label>Imagen de la oferta(Opcional);</label>
            			<input type="file" id="imgoferta" name="imgoferta" class="form-control dropify" onChange="setpreview('rutaoferta','imgoferta','form3')">
				<span id="rutaoferta" style="display: none;"></span>
            		</div>
            	</div>
        
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center" style="display: block;">
              <a class="btn btn-primary" onclick="guardaroferta()">Guardar</a>
            </div>
            <!-- /.box-footer -->
          </div>
   	</div>
   	<div class="col-xs-12 col-md-6">
   		<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Productos</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: block;">
            	<script src="jsdatatable/ofertas/jsproductosoferta.js" type="text/javascript"></script>
            		<table id="tblistaproductosofertas" class="table table-striped">
            		<thead>
            			<tr>
            				<th> <input name="select_all" value="1" type="checkbox"></th>
            				<th>Nombre</th>
            				<th>Codigo</th>
            			</tr>
            		</thead>
            		<tfoot>
            			<tr>
            				<th></th>
            				<th></th>
            				<th></th>
            			</tr>
            		</tfoot>
            	</table>
            </div>
            <!-- /.box-body -->
            
            <!-- /.box-footer -->
          </div>
   		
   	</div>
   	  </form>
   	   <iframe src="about:blank" name="null" style="display:none"></iframe>
   	<?php
   	echo "<script>INICIALIZARCONTENIDO();</script>";
}
else if($opcion=="VISTAMOSAICO")
{

	$nom = $_POST['nom'];
	$cod = $_POST['cod'];
	$cat = $_POST['cat']; $cat = implode(', ', (array)$cat); if($cat==""){$cat1="'0'";}else {$cat1=$cat;}
	$mar = $_POST['mar'];
	//$uni = $_POST['uni']; $uni = implode(', ', (array)$uni); if($uni==""){$uni1="'0'";}else {$uni1=$uni;}
	$pro = $_POST['pro']; $pro = implode(', ', (array)$pro); if($pro==""){$pro1="'0'";}else {$pro1=$pro;}
	$est = $_POST['est']; $est = implode(', ', (array)$est); if($est==""){$est1="'0'";}else {$est1=$est;}
	$ofe = $_POST['ofe']; $ofe = implode(', ', (array)$ofe); if($ofe==""){$ofe1="'0'";}else {$ofe1=$ofe;}

	$sql = "SELECT p.pro_clave_int,pro_nombre,pro_descripcionbreve,o.ofe_titulo,ofe_descripcion,ofe_estado,ofe_tipo_descuento,ofe_descuento,ofe_inicio,ofe_fin,mer_clave_int,pro_kilo,pro_und_kilo FROM tbl_productos AS p JOIN tbl_categorias c ON c.cat_clave_int = p.cat_clave_int join tbl_ofertas_producto AS op on op.pro_clave_int = p.pro_clave_int join tbl_ofertas AS o on o.ofe_clave_int = op.ofe_clave_int";
	$sql.= " WHERE ( p.pro_nombre LIKE REPLACE('".$nom."%',' ','%') OR p.pro_palabra LIKE REPLACE('".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '' ) and ( p.pro_codigo LIKE REPLACE('".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '' ) and ( p.pro_marca LIKE REPLACE('".$mar."%',' ','%') OR '".$mar."' IS NULL OR '".$mar."' = '' ) and ( p.cat_clave_int IN(".$cat1.") OR '".$cat."' IS NULL OR '".$cat."' = '')  and ( p.pro_clave_int IN(".$pro1.") OR '".$pro."' IS NULL OR '".$pro."' = '') and ( o.ofe_clave_int IN(".$ofe1.") OR '".$ofe."' IS NULL OR '".$ofe."' = '') and ( o.ofe_estado IN(".$est1.") OR '".$est."' IS NULL OR '".$est."' = '') and pro_activo!=2";//and ( p.pro_uni_compra IN(".$uni1.") OR '".$uni."' IS NULL OR '".$uni."' = '') and ( p.pro_uni_consumo IN(".$uni1.") OR '".$uni."' IS NULL OR '".$uni."' = '')

    	$conpro = pg_query($dbconn,$sql);
    	$numpro = pg_num_rows($conpro);
	?>
	

				<div class="row paging-container tablePagingLista"> </div>
				<div class="row" id="divlistaproductos">
					<?php
					while($datp = pg_fetch_array($conpro))
					{
						$idp = $datp['pro_clave_int'];
						$nomp = $datp['pro_nombre'];
						$descp = $datp['pro_descripcionbreve'];
						$mer = $datp['mer_clave_int'];
						$kil = $datp['pro_kilo'];
						$undkilo = $datp['pro_und_kilo'];

						
						 $con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idp."' and pri_activo = 1 LIMIT 1");
                        $dat = pg_fetch_row($con);
                        $cimg = $dat[0];
                        $img = $dat[1];

						if($img=="" || $img==NULL)
						{
						$img= "dist/img/nofoto.png";
						$bn = "bn";
						}
						else
						{
						$img = $urlweb.$img;
						}

                        $nomo = $datp['ofe_titulo'];
                        $est = $datp['ofe_estado'];
                        $inicio = $datp['ofe_inicio'];
                        $fin = $datp['ofe_fin'];
                        $m1 = date('M',strtotime($inicio));
                        $m2 = date('M',strtotime($fin));
                        $d1 = date('d',strtotime($inicio));
                        $d2 = date('d',strtotime($fin));
                        $y1 = date('Y',strtotime($inicio));
                        $y2 = date('Y',strtotime($fin));
                        if($est==0)
                        {
                        	$offer = "default";
                        	$toffer = "Inactivo";
                        }
                        else if($est==1)
                        {
                        	$offer = "success";
                        	$toffer = "Activo";
                        }

                        $tipo = $datp['ofe_tipo_descuento'];
						$descuento = $datp['ofe_descuento'];




						$conpre = pg_query($dbconn, "select prm_venta,prm_peso_unidad from tbl_precios_mercado where pro_clave_int ='".$idp."' and cla_clave_int in  (SELECT cla_clave_int FROM tbl_clasificacion where cla_clave_int = '1' ORDER BY cla_clave_int ASC LIMIT 1) and mer_clave_int = '".$mer."'");
						$datpre = pg_fetch_array($conpre);
						$venta = $datpre['prm_venta'];
						if($venta<=0){ $venta = 0;}

						$pesounidad = $datpre['prm_peso_unidad']; if($pesounidad<=0 || $pesounidad==NULL){ $pesounidad = 0; }
						$pesounidadkilo = $pesounidad/1000;
						$pesototalkilo = $pesounidadkilo * $unidadmin;
						
						if($tipo==1)
						{
							$ventaold = $venta;
							$ventaoferta = $descuento;
							$ahorrooferta = $venta - $ventaoferta;
							$descuentooferta = 100 - ($ventaoferta*100/$venta);
						}
						else if($tipo==2)
						{
							$ventaold = $venta;
							$ventaoferta = $venta - ($venta*$descuento/100);
							$ahorrooferta = $venta - $ventaoferta;
							$descuentooferta = $descuento;
						}


						if($kil==1)
						{
						//$undkilo = 1000/$pesounidad;
						//$ventaoferta = $ventaoferta/$undkilo;
							if($pesounidad<=0){ $undkilo = 0; $ventaoferta = $ventaoferta/1; $ventaold = $ventaold/1; }
							else { $undkilo = 1000/$pesounidad; $ventaoferta = $ventaoferta/$undkilo; $ventaold = $ventaold/$undkilo; }
						}

						$ventaoferta = ceil($ventaoferta/50)*50;
						$ventaold = ceil($ventaold/50)*50;


						?>
						<!--
		    		 <div class="col-md-3 col-sm-6 data-row" data-toggle="modal" data-target="#modalinfo" onclick="CRUDPRODUCTOS('ZOOMPRODUCTOS','<?PHP echo $idp;?>','','')">
	                    <div class="sc-product-item thumbnail">
						<div class="ih-item circle colored effect3  bottom_to_top"><a href="#">
						<div class="img"><img data-name="product_image" src="<?php echo $img;?>" alt="img"></div>
						<div class="info">
						<h3><?php echo $nomp;?></h3>
						<p><?php echo $descp;?></p>
						</div></a></div>

	                       
	                        <div class="caption">
	                            <h4 style="display: none;" data-name="product_name"><?php echo $nomp;?></h4>
	                            <p style="display: none;" data-name="product_desc"><?php echo $descp;?></p>
	                           
	                            
	                            <div>
								

	                            </div>
	                            <div class="clearfix"></div>
	                        </div>
	                    </div>
	                </div>-->

						<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3 data-row">
							<div class="offer offer-<?php echo $offer;?>">
								<div class="shape">
									<div class="shape-text">
									<?php echo $toffer;?>								
									</div>
								</div>
								<div class="offer-content">
									<h1>
									<?php echo $descuentooferta;?> %	
									</h1>

									<p>
										<img class="direct-chat-img" src="<?php echo $img;?>">
									<h4><?php echo $nomp;?></h4>
									<br> 
									<span data-toggle="tooltip" title="" class="col-md-5 badge bg-default currency" data-original-title="Precio actual"  style="text-decoration: line-through;"><?php echo $ventaold;?></span>
									<span data-toggle="tooltip" title="" class="col-md-5 badge bg-orange currency" data-original-title="Precio nuevo con descuento" ><?php echo $ventaoferta;?></span>
									</p>
									
								</div>
								<div class="offer-content bg-gray " style="vertical-align: middle;"><h3 class="text-center"><?php echo $nomo;?></h3></div>
								<div class="offer-content">
									<h1 class="col-md-6">
										<?php echo $d1;?><div style="font-size: 12px; width: 40px;float: right;"><?php echo $m1;?><hr style="height: 2px; margin: 0 !important"><?php echo $y1;?></div>
									</h1>
									<h1 class="col-md-6">
										<?php echo $d2;?><div style="font-size: 12px; width: 40px;float: right;"><?php echo $m2;?><hr style="height: 2px; margin: 0 !important"><?php echo $y2;?></div>
									</h1>
								</div>
							</div>
						</div>
            		<?php 
        			}
        			?>
    		</div>
    		<div class="row paging-container tablePagingLista"> </div>
    	
	<script type="text/javascript">
	$(document).ready(function(){
		load = function() 
		{
				window.tp = new Pagination('.tablePagingLista', {
					itemsCount: <?php echo $numpro;?>,
					onPageSizeChange: function (ps) {
						console.log('changed to ' + ps);
					},
					onPageChange: function (paging) {
						//custom paging logic here
						console.log(paging);
						var start = paging.pageSize * (paging.currentPage - 1),
							end = start + paging.pageSize,
							$rows = $('#divlistaproductos').find('.data-row');

						$rows.hide();

						for (var i = start; i < end; i++) {
							$rows.eq(i).show();
						}
					}
				});


			}

		load();
	    // Initialize Smart Cart    	
	    //$('#smartcart').smartCart();
	});
	INICIALIZARCONTENIDO();
	</script>
    	<?php
	
}
else if($opcion=="LISTAOFERTAS")
{
	?>
	<script src="jsdatatable/ofertas/jslistaofertas.js" type="text/javascript" ></script>
	<table id="tblistaofertas" class="table table-striped">		
		<thead>
			<tr>
				<th></th>
				<th></th>
				<th></th>
				<th>Titulo</th>
				<th>Inicio</th>
				<th>Fin</th>
				<th>Tipo Descuento</th>
				<th>Descuento</th>
				<th>Estado</th>				
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>			
			</tr>
		</tfoot>
	</table>
	<?php
}
else if($opcion=="GUARDAR")
{
	$pro = $_POST['pro'];
    $pro = str_replace("-", ",", $pro);//por facturar
    $tit = $_POST['tit'];
    $des = nl2br($_POST['des']);
    $est = $_POST['est'];
    $fi = $_POST['fi'];
    $ff = $_POST['ff'];
    $hi = $_POST['hi'];
    $hf = $_POST['hf'];
    $td = $_POST['td'];
    $vd = $_POST['vd'];
    $ruta = $_POST['ruta'];
    $mer = $_POST['mer'];

	$trozos = explode(".", $ruta); 
	$rutaold = "../../".$ruta;
	$extension = end($trozos);

    $ins = pg_query($dbconn,"INSERT INTO tbl_ofertas(ofe_titulo,ofe_descripcion,ofe_inicio,ofe_fin,ofe_tipo_descuento,ofe_descuento,ofe_estado,ofe_hor_inicio,ofe_hor_fin,ofe_usu_actualiz,ofe_fec_actualiz,mer_clave_int) VALUES('".$tit."','".$des."','".$fi."','".$ff."','".$td."','".$vd."','".$est."','".$hi."','".$hf."','".$usuario."','".$fecha."','".$mer."')");
    if($ins>0)
    {
    	$uid = pg_fetch_row(pg_query($dbconn,"SELECT setval('ofe_id_seq',nextval('ofe_id_seq')-1) as id;"));
		$ido = $uid[0];
		$carpetaa2 = '../../modulos/ofertas/imagenes/COD'.$ido;

		$inspro = pg_query($dbconn,"INSERT INTO tbl_ofertas_producto (pro_clave_int,ofp_usu_actualiz,ofp_fec_actualiz,ofe_clave_int) SELECT pro_clave_int,'".$usuario."','".$fecha."','".$ido."' FROM tbl_productos where pro_clave_int in(".$pro.")");
		//CREACION DE CARPETA PARA LAS IMAGENES DEL PRODUCTOS
		if(!file_exists($carpetaa2)){  
		if(mkdir($carpetaa2, 0777, true)){ $insim = 1; } else { $insim = 0; } }
		else{ $insim = 1;}

		if($_POST['ruta']!="")
		{		
				//SI EXISTE LA CARPETA DE LAS IMAGENES PARA PRODUCTOS SE MUEVE LA IMAGEN
			if($insim==1)
			{
				$rutan = "../../modulos/ofertas/imagenes/COD".$ido."/".$ido.".".$extension;
				if(rename($rutaold,$rutan) and $_POST['ruta']!="")
				{
					$rutanew = "modulos/ofertas/imagenes/COD".$ido."/".$ido.".".$extension;	
					$update = pg_query($dbconn,"update tbl_ofertas set ofe_imagen='".$rutanew."' where ofe_clave_int='".$ido."'");
					unlink($rutalold);
				}
			}
			
		}
		$res = "ok";
		$msn = "Oferta guardada correctamente";	
    }
    else
    {
    	$res = "error";
    	$msn = "Surgió un error al guardar oferta(".pg_last_error($dbconn).")";
    }
    $datos[] =   array('res' => $res ,'msn' => $msn ,"insim"=>$insim,"RUTA"=>$_POST['ruta'],"rutan"=>$rutan);
    echo json_encode($datos);
}

else if($opcion=="EDITAROFERTA")
{
	$id = $_POST['id'];
	$cono = pg_query($dbconn, "SELECT ofe_titulo,ofe_descripcion,ofe_inicio,ofe_fin,ofe_hor_inicio,ofe_hor_fin,ofe_tipo_descuento,ofe_descuento,ofe_imagen,mer_clave_int,ofe_estado FROM tbl_ofertas WHERE ofe_clave_int = '".$id."'");
	$dato = pg_fetch_array($cono);
	$tit = $dato['ofe_titulo'];
	$des = $dato['ofe_descripcion'];
	$ini = $dato['ofe_inicio'];
	$fin = $dato['ofe_fin'];
	$hini = $dato['ofe_hor_inicio'];
	$hfin = $dato['ofe_hor_fin'];
	$tipo = $dato['ofe_tipo_descuento'];
	$descuento = $dato['ofe_descuento'];
	$mer = $dato['mer_clave_int'];
	$est = $dato['ofe_estado'];
	$rut = $dato['ofe_imagen'];



	?>
	<form class="form-horizontal" id="form4" name="form4" action="_" method="post" enctype="multipart/form-data">
     <div class="row">        
          
   	<div class="col-xs-12 col-md-6">
   		<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Datos generales de la oferta</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
              
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: block;">
            	<div class="form-group row">
            		<label class="col-xs-12 col-md-2 ">Nombre:</label>
            		<div class="col-xs-12 col-md-6"><input id="txttitulo" name="txttitulo" class="form-control input-sm" autocomplete="off" value="<?php echo $tit;?>"></div>
            		<label class="col-xs-12 col-md-2 ">Estado</label>
            		<div class="col-xs-12 col-md-2">
						<label class="bs-switch">
						<input type="checkbox" id="ckestado" name="ckestado" value="1" <?php if($est==1){ echo "checked"; }?>>
						<span class="slider round"></span>
						</label>
            		</div>
            	</div>
            	<div class="form-group row">
            		<label class="col-xs-12 col-md-2" for="">Descripción:</label>
            		<div class="col-xs-12 col-md-10">
            			<textarea class="form-control input-sm" name="txtdescripcion" id="txtdescripcion"><?php echo nl2br($des);?></textarea>
            		</div>
            	</div>
            	<div class="form-group row">
            		<label class="col-xs-12 col-md-2">Mercado:</label>
            		<div class="col-xs-12 col-md-10">
            			<select name="selmercado" id="selmercado" class="form-control input-sm selectpicker" title="Seleccione mercado">
            				<?php
            				$conm = pg_query($dbconn, "SELECT mer_clave_int,mer_nombre FROM tbl_mercado WHERE mer_activo = 1");
            				while($datm = pg_fetch_array($conm))
            				{
            				?>
							<option <?php if($mer==$datm['mer_clave_int']){ echo "selected";} ?> value="<?php echo $datm['mer_clave_int'];?>"><?php echo $datm['mer_nombre'];?></option>
							<?php
							}
							?>
						</select>
            		</div>
            	</div>
            	<h4 class="divider">Validez</h4>
            	<div class="form-group row">
            		<label class="col-xs-12 col-md-3 ">Fecha Inicio:</label>
            		<div class="col-xs-12 col-md-4"><input id="txtfechainicio" name="txtfechainicio" class="form-control input-sm " data-date-format="yyyy-mm-dd" autocomplete="off" type="date" value="<?php echo $ini;?>">            			
            		</div>
            		
            		<div class="col-xs-12 col-md-4 input-group bootstrap-timepicker">
						<input type="text" id="txthorainicio" name="txthorainicio" class="form-control input-sm timepicker" value="<?php echo $hini;?>">
						<div class="input-group-addon">
						<i class="fa fa-clock-o"></i>
						</div>
					</div>
            	</div>

            	<div class="form-group row">
            		<label class="col-xs-12  col-md-3 ">Fecha Fin:</label>
            		<div class="col-xs-12 col-md-4"><input id="txtfechafin" name="txtfechafin" class="form-control input-sm" data-date-format="yyyy-mm-dd" autocomplete="off" type="date" value="<?php echo $fin;?>"></div>
            		
					<div class="col-xs-12  col-md-4 input-group bootstrap-timepicker">
						<input type="text" id="txthorafin" name="txthorafin" class="form-control input-sm timepicker" value="<?php echo $hfin;?>">
						<div class="input-group-addon">
						<i class="fa fa-clock-o"></i>
						</div>
					</div>
            	</div>
            	<div class="form-group row">
            		<label class="col-xs-12  col-md-12">
            			¿Cuál es el tipo y el valor del descuento?
            		</label>
					<div class="col-xs-12 col-md-12">
						<div class="col-xs-12  col-md-6">
						<select name="seltipodescuento" id="seltipodescuento" class="form-control input-sm selectpicker" title="Seleccione tipo descuento" onchange="CRUDOFERTAS('TIPODESCUENTO','')" >
							<option value="1" <?php if($tipo==1){ echo "selected"; }?> >Valor</option>
							<option value="2" <?php if($tipo==2){ echo "selected"; }?> >Porcentaje</option>
						</select></div>
						<div class=" col-xs-12 col-md-6 input-group">						
							<input type="text" id="txtvalor" name="txtvalor" class="form-control input-sm" value="<?php echo $descuento;?>">
							<div class="input-group-addon" id="divtipodescuento">
							%
							</div>
						</div>
					</div>
            	</div>
            	<div class="form-group row">
            		<div class="col-md-12">
            			<label>Imagen de la oferta(Opcional);</label>
            			<input type="file" id="imgoferta" name="imgoferta" class="form-control dropify" onChange="setpreview('rutaoferta','imgoferta','form3')"  data-default-file="<?php echo $rut;?>">
				<span id="rutaoferta" style="display: none;"></span>
            		</div>
            	</div>
        
            </div>
       
          </div>
   	</div>
   	<div class="col-xs-12 col-md-6">
   		<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Productos</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
               
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body" style="display: block;">
            	<script src="jsdatatable/ofertas/jsproductosofertaedicion.js" type="text/javascript"></script>
            		<table data-id="<?php echo $id;?>" id="tblistaproductosofertasedicion" class="table table-striped" width="100%">
            		<thead>
            			<tr>
            				<th></th>
            				<th>Nombre</th>
            				<th>Codigo</th>
            			</tr>
            		</thead>
            		<tfoot>
            			<tr>
            				<th></th>
            				<th></th>
            				<th></th>
            			</tr>
            		</tfoot>
            	</table>
            </div>
            <!-- /.box-body -->
            
            <!-- /.box-footer -->
          </div>
   	</div>
   </div>
   </form>
   <script>INICIALIZARCONTENIDO();</script>
	<?php	
}
else if($opcion=="ASOCIARPRODUCTO")
{
	$idp  =$_POST['idp'];
	$ido = $_POST['ido'];
	$con = pg_query($dbconn, "SELECT * FROM tbl_ofertas_producto where ofe_clave_int  = '".$ido."' and pro_clave_int = '".$idp."'");
	$num = pg_num_rows($con);
	if($num>0)
	{
		$sql = pg_query($dbconn, "DELETE FROM tbl_ofertas_producto WHERE ofe_clave_int = '".$ido."' and pro_clave_int = '".$idp."'");
	}
	else
	{
		$sql = pg_query($dbconn,"INSERT INTO tbl_ofertas_producto (pro_clave_int,ofp_usu_actualiz,ofp_fec_actualiz,ofe_clave_int) VALUES('".$idp."','".$usuario."','".$fecha."','".$ido."')");
	}
	if($sql>0)
	{
		$res = "ok";
		$msn = "";
	}
	else
	{
		$res = "error";
		$msn = "Surgió un error BD(".pg_last_error($dbconn).")";
	}
	$datos[] = array("res"=>$res, "msn"=>$msn);
	echo json_encode($datos);
 
}
else if($opcion=="GUARDAREDICION")
{
	$ido = $_POST['id'];
    $tit = $_POST['tit'];
    $des = nl2br($_POST['des']);
    $est = $_POST['est'];
    $fi = $_POST['fi'];
    $ff = $_POST['ff'];
    $hi = $_POST['hi'];
    $hf = $_POST['hf'];
    $td = $_POST['td'];
    $vd = $_POST['vd'];
    $ruta = $_POST['ruta'];
    $mer = $_POST['mer'];

	$trozos = explode(".", $ruta); 
	$rutaold = "../../".$ruta;
	$extension = end($trozos);

	$veri = pg_query($dbconn, "SELECT * FROM tbl_ofertas_producto WHERE ofe_clave_int = '".$ido."'");
	$numv = pg_num_rows($veri);
	if($numv<=0)
	{
		$res = "error";
		$msn = "No ha seleccionado productos para asociar a la oferta. Verificar";
	}
	else
	{
	    $ins = pg_query($dbconn,"UPDATE  tbl_ofertas SET ofe_titulo = '".$tit."',ofe_descripcion = '".$des."' ,ofe_inicio = '".$fi."',ofe_fin = '".$ff."',ofe_tipo_descuento = '".$td."',ofe_descuento = '".$vd."',ofe_estado = '".$est."',ofe_hor_inicio = '".$hi."',ofe_hor_fin = '".$hf."',ofe_usu_actualiz = '".$usuario."',ofe_fec_actualiz = '".$fecha."',mer_clave_int = '".$mer."' WHERE ofe_clave_int = '".$ido."'");

	    if($ins>0)
	    {
			$carpetaa2 = '../../modulos/ofertas/imagenes/COD'.$ido;
			//CREACION DE CARPETA PARA LAS IMAGENES DEL PRODUCTOS
			if(!file_exists($carpetaa2)){  
			if(mkdir($carpetaa2, 0777, true)){ $insim = 1; } else { $insim = 0; } }
			else{ $insim = 1;}

			if($_POST['ruta']!="")
			{		
					//SI EXISTE LA CARPETA DE LAS IMAGENES PARA PRODUCTOS SE MUEVE LA IMAGEN
				if($insim==1)
				{
					$rutan = "../../modulos/ofertas/imagenes/COD".$ido."/".$ido.".".$extension;
					if(rename($rutaold,$rutan) and $_POST['ruta']!="")
					{
						$rutanew = "modulos/ofertas/imagenes/COD".$ido."/".$ido.".".$extension;	
						$update = pg_query($dbconn,"update tbl_ofertas set ofe_imagen='".$rutanew."' where ofe_clave_int='".$ido."'");
						unlink($rutalold);
					}
				}
				
			}
			$res = "ok";
			$msn = "Oferta modificada correctamente";	
	    }
	    else
	    {
	    	$res = "error";
	    	$msn = "Surgió un error al guardar oferta(".pg_last_error($dbconn).")";
	    }
	}
    $datos[] =   array('res' => $res ,'msn' => $msn ,"insim"=>$insim,"RUTA"=>$_POST['ruta'],"rutan"=>$rutan);
    echo json_encode($datos);
}
else if($opcion=="ELIMINAROFERTA")
{
	$ido = $_POST['id'];
	$del = pg_query($dbconn,"UPDATE  tbl_ofertas SET ofe_estado = '3',ofe_usu_actualiz = '".$usuario."',ofe_fec_actualiz = '".$fecha."' WHERE ofe_clave_int = '".$ido."'");
	if($del>0)
	{
		$res = "ok";
		$msn = "Oferta eliminada correctamente";
	}
	else
	{
		$res = "error";
		$msn = "Surgió un error al eliminar oferta. Error BD(".pg_last_error($dbconn).")";
	}
	$datos[] = array("res"=>$res, "msn"=>$msn);
	echo json_encode($datos);
}