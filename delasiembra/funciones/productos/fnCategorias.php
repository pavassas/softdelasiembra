<?php
	include('../../data/Conexion.php');
	session_start();
    error_reporting(0);
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_COOKIE['usuario'];
	$idUsuario= $_SESSION["idusuario"];
	$clave= $_COOKIE["clave"];
	$identificacion = $_COOKIE["usIdentificacion"];
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");

     
    $opcion = $_POST['opcion'];
    if($opcion=="NUEVO")
    {
    	?>
    	<form  id="form1" name="form1" action="_" method="post" enctype="multipart/form-data" class="form-horizontal">
    		<div class="row">
				<div class="col-md-12">
				<label for="txtnombre">Nombre:</label>
				<input type="text" class="form-control" id="txtnombre" name="txtnombre">
				</div>
				<div class="col-md-12" style="display:none;">
				<label for="txtpalabra">Palabra Sustituta:</label>
				<input type="text" class="form-control" id="txtpalabra" name="txtpalabra">
				</div>
				<div class="col-md-12">
				<label for="txtdescripcion">Descripción:</label>
				<textarea class="form-control" id="txtdescripcion" name="txtdescripcion"></textarea>
				</div>
				<div class="col-md-12">
				<label for="selpadre">Categoria Padre:</label>
				<select class="form-control selectpicker" id="selpadre" name="selpadre">
					<option value="0">--Seleccione--</option>
					<?php 
					$concat = pg_query($dbconn, "select * from tbl_categorias WHERE cat_activo in(0,1)");
					while($datcat = pg_fetch_array($concat))
					{
						$imgc = $datcat['cat_imagen'];
						$nomc = $datcat['cat_nombre'];
						?>
						 <option data-content="<img src='<?php echo $imgc;?>' width='20'/> <?php echo $nomc;?>" value='<?php echo $datcat['cat_clave_int'];?>'><?php echo $nomc;?></option>
						<?php
					}
					?>
				</select>
				</div>
				<div class="col-md-12">
				<label for="imgcategoria">Imagen:</label>
				<input type="file" class="form-control dropify" id="imgcategoria" name="imgcategoria" onChange="setpreview('rutacategoria','imgcategoria','form1')">
				<span id="rutacategoria"></span>
				</div>
				<div class="col-md-12">
					<div class="checkbox">
					<label><input type="checkbox" id="ckactivo"  name="ckactivo" value="1" checked> Activo</label>
					</div>
				</div>
			</div>
		</form>
		 <iframe src="about:blank" name="null" style="display:none"></iframe>

    	<?php
    	echo "<script>INICIALIZARCONTENIDO();</script>";
    }
    else if($opcion=="EDITAR")
    {
    	$id = $_POST['id'];
    	$con = pg_query($dbconn, "select cat_nombre,cat_descripcion,cat_padre,cat_activo,cat_imagen,cat_palabra from tbl_categorias where cat_clave_int ='".$id."'");
    	$dat = pg_fetch_array($con);
    	$nom = $dat['cat_nombre'];
    	$desc = br2nl($dat['cat_descripcion']);
    	$pad = $dat['cat_padre'];
    	$act = $dat['cat_activo'];
    	$rut = $dat['cat_imagen'];
    	$pal = $dat['cat_palabra'];
    	?>
    	<form  id="form1" name="form1" action="_" method="post" enctype="multipart/form-data" class="form-horizontal">
    		<div class="row">
				<div class="col-md-12">
				<label for="txtnombre">Nombre:</label>
				<input type="text" class="form-control" id="txtnombre" name="txtnombre" value="<?php echo $nom;?>">
				</div>
				<div class="col-md-12" style="display: none;">
				<label for="txtpalabra">Palabra Sustituta:</label>
				<input type="text" class="form-control" id="txtpalabra" name="txtpalabra" value="<?php echo $pal;?>">
				</div>
				<div class="col-md-12">
				<label for="txtdescripcion">Descripción:</label>
				<textarea class="form-control" id="txtdescripcion" name="txtdescripcion"><?php echo $desc;?></textarea>
				</div>
				<div class="col-md-12">
				<label for="selpadre">Categoria Padre:</label>
				<select class="form-control selectpicker" id="selpadre" name="selpadre">
					<option value="0">--Seleccione--</option>
					<?php 
					$concat = pg_query($dbconn, "select * from tbl_categorias WHERE cat_activo in(0,1)");
					while($datcat = pg_fetch_array($concat))
					{
						$idc = $datcat['cat_clave_int'];
						$imgc = $datcat['cat_imagen'];
						$nomc = $datcat['cat_nombre'];
						?>
						 <option data-content="<img src='<?php echo $imgc;?>' width='20'/> <?php echo $nomc;?>" <?php if($pad==$idc){ echo "selected"; } ?>  value="<?php echo $idc;?>"><?php echo $nomc;?></option>
						<?php
					}
					?>
				</select>
				</div>
				<div class="col-md-12">
				<label for="imgcategoria">Imagen:</label>
				<input type="file" class="form-control dropify" id="imgcategoria" name="imgcategoria" onChange="setpreview('rutacategoria','imgcategoria','form1')" data-default-file="<?php echo $rut;?>">
				<span id="rutacategoria"><?php echo $rut;?></span>
				</div>
				<div class="col-md-12">
					<div class="checkbox">
					<label><input type="checkbox" id="ckactivo"  name="ckactivo" value="1" <?php if($act==1){echo "checked";}?>> Activo</label>
					</div>
				</div>
			</div>
		</form>
		 <iframe src="about:blank" name="null" style="display:none"></iframe>

    	<?php
    	echo "<script>INICIALIZARCONTENIDO();</script>";
    	

    }
    else if($opcion=="GUARDAR")
    {
    	$nom = $_POST['nom'];
    	$desc = nl2br($_POST['desc']);
    	$pal = $_POST['pal'];
    	$pad = $_POST['pad'];
    	$act = $_POST['act'];
    	$ruta = $_POST['ruta'];
    	$trozos = explode(".", $ruta); 
		$rutaold = "../../".$ruta;
		$extension = end($trozos);

		$veri = pg_query($dbconn, "SELECT * FROM tbl_categorias where cat_nombre = '".$nom."' and cat_activo!=2");
		$numv = pg_num_rows($veri);

		if($nom=="" || $nom==NULL)
		{
			$res = "error";
			$msn = "Diligenciar el nombre de la categoria. Verificar";
		}
		else if($desc =="" || $desc==NULL)
		{
			$res = "error";
			$msn = "Diligenciar la descripción de la categoria. Verificar";
		}
		else if($numv>0)
		{
			$res = "error";
			$msn = "Ya hay una categoria con el nombre ingresado. Verificar";
		}
		else
		{
			if($pad<=0){$pad = 0;}
			$ins = pg_query($dbconn,"INSERT INTO tbl_categorias(cat_nombre,cat_descripcion,cat_palabra,cat_padre,cat_activo,cat_usu_actualiz,cat_fec_actualiz) VALUES('".$nom."','".$desc."','".$pal."','".$pad."','".$act."','".$usuario."','".$fecha."')");
			if($ins>0)
			{
				$uid = pg_fetch_row(pg_query($dbconn,"SELECT setval('cat_id_seq',nextval('cat_id_seq')-1) as id;"));
				$idc = $uid[0];
				//$idc = pg_insert_id($dbconn);

				$rutan = "../../modulos/categorias/imagenes/".$idc.".".$extension;
				if(rename($rutaold,$rutan) and $_POST['ruta']!="")
				{
					$rutanew = "modulos/categorias/imagenes/".$idc.".".$extension;
					$update = pg_query($dbconn,"update tbl_categorias set cat_imagen='".$rutanew."' where cat_clave_int='".$idc."'");
					unlink($rutalold);
				}
					
				$res ="ok";
				$msn = "Categoria guardar con exito";
			}
			else
			{
				$res = "error";
				$msn = "Error de BD(".pg_last_error($dbconn)."). No se guardo la categoria. Verificar";

			}
			$ql  ="INSERT INTO tbl_categorias(cat_nombre,cat_descripcion,cat_palabra,cat_padre,cat_activo,cat_usu_actualiz,cat_fec_actualiz) VALUES('".$nom."','".$desc."','".$pal."','".$pad."','".$act."','".$usuario."','".$fecha."')";
		}
		$datos[] = array('res' => $res , 'msn' =>$msn,"ql"=>$ql );
		echo json_encode($datos);
    }
    else if($opcion=="GUARDAREDICION")
    {
    	$id = $_POST['id'];
    	$nom = $_POST['nom'];
    	$desc = nl2br($_POST['desc']);
    	$pal = $_POST['pal'];
    	$pad = $_POST['pad'];
    	$act = $_POST['act'];
    	$ruta = $_POST['ruta'];
    	$trozos = explode(".", $ruta); 
		$rutaold = "../../".$ruta;
		$extension = end($trozos);

		$veri = pg_query($dbconn, "SELECT * FROM tbl_categorias where cat_nombre = '".$nom."' and cat_activo!=2 and cat_clave_int!='".$id."'");
		$numv = pg_num_rows($veri);

		$infoa = pg_query($dbconn, "select cat_imagen from tbl_categorias WHERE cat_clave_int = '".$id."'");
		$dati = pg_fetch_array($infoa);
		$rutaa = $dati['cat_imagen'];

		if($nom=="" || $nom==NULL)
		{
			$res = "error";
			$msn = "Diligenciar el nombre de la categoria. Verificar";
		}
		else if($desc =="" || $desc==NULL)
		{
			$res = "error";
			$msn = "Diligenciar la descripción de la categoria. Verificar";
		}
		else if($numv>0)
		{
			$res = "error";
			$msn = "Ya hay una categoria con el nombre ingresado. Verificar";
		}
		else
		{
			$ins = pg_query($dbconn,"UPDATE tbl_categorias SET cat_nombre = '".$nom."',cat_descripcion = '".$desc."',cat_palabra = '".$pal."',cat_padre = '".$pad."', cat_activo= '".$act."',cat_usu_actualiz = '".$usuario."',cat_fec_actualiz = '".$fecha."' WHERE cat_clave_int = '".$id."'");
			if($ins>0)
			{				

				$rutan = "../../modulos/categorias/imagenes/".$id.".".$extension;
				if(rename($rutaold,$rutan) and $_POST['ruta']!="" and $ruta!=$rutaa)
				{
					$rutanew = "modulos/categorias/imagenes/".$id.".".$extension;
					$update = pg_query($dbconn,"update tbl_categorias set cat_imagen='".$rutanew."' where cat_clave_int='".$id."'");
					unlink($rutalold);
				}
					
				$res ="ok";
				$msn = "Categoria actualizada con exito";
			}
			else
			{
				$res = "error";
				$msn = "Error de BD(".pg_last_error($dbconn)."). No se actualizo la categoria. Verificar";

			}
		}
		$datos[] = array('res' => $res , 'msn' =>$msn );
		echo json_encode($datos);
    }
    else if($opcion=="LISTACATEGORIAS")
    {
    	$bus = $_POST['bus'];
    	$concategorias = pg_query($dbconn, "SELECT * FROM tbl_categorias WHERE (cat_nombre  LIKE REPLACE('%".$bus."%',' ','%') OR '".$bus."' IS NULL OR '".$bus."' = '' OR cat_descripcion LIKE REPLACE('%".$bus."%',' ','%')) and cat_activo in(0,1) ORDER BY cat_nombre ");
    	while($datc = pg_fetch_array($concategorias)){
    		$nomc = $datc['cat_nombre'];
    		$desc = $datc['cat_descripcion'];
    		$act = $datc['cat_activo'];
    		$img = $datc['cat_imagen'];
    		$idc = $datc['cat_clave_int'];
    		$pal = $datc['cat_palabra'];

    		if($act=="0"){
    			$act = "Inactiva";
    		}
    		else
    		{
    			$act = "Activa";
    		}
    		$conpro = pg_query($dbconn,"select count(pro_clave_int) np from tbl_productos WHERE cat_clave_int = '".$idc."' and pro_activo!=2");
    		$datpro = pg_fetch_array($conpro);
    		$numpro = $datpro['np']; if($numpro<=0){ $numpro = 0;}
    		?>
    		<div class="col-md-4" id="cat_<?php echo $idc;?>">
          <!-- Widget: user widget style 1 -->
          	<div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-yellow">
              <div class="widget-user-image">
                <img class="img-circle" src="<?php echo $img."?".time();?>" alt="Img Categoria">
              </div>
              <!-- /.widget-user-image -->
              <h3 class="widget-user-username"><?php echo $nomc;?></h3>
              <h5 class="widget-user-desc"><?php echo $desc;?></h5>
            </div>
            <div class="box-footer no-padding">
              <ul class="nav nav-stacked">
                <li><a href="#">Productos <span class="pull-right badge bg-blue"><?php echo $numpro;?></span></a></li>
               
    		   <li style="display: none;"><a href="#">Palabra sustituta <span class="pull-right badge bg-aqua"><?php echo $pal;?></span></a></li>
                		
                <li><a href="#">Estado <span class="pull-right badge bg-aqua"><?php echo $act;?></span></a></li>
                <li><a href="#"> 
                	<i class="fa fa-pencil-square-o fa-2x" title="Editar Categoria" data-toggle="modal" data-target="#modalregistro" onclick="CRUDCATEGORIAS('EDITAR','<?php echo $idc;?>')"></i>
                	<i class="fa fa-trash fa-2x" title="Eliminar Categoria" onclick="CRUDCATEGORIAS('ELIMINARCATEGORIA','<?php echo $idc;?>')"></i>
                 </a></li>               
              </ul>
            </div>
          </div>
          <!-- /.widget-user -->
        </div>
    		<?php
    	}
    	?>

    	<?php
    }
    else if($opcion=="ELIMINARCATEGORIA")
    {
    	$id = $_POST['id'];
    	$del = pg_query($dbconn, "UPDATE tbl_categorias SET cat_activo = 2,cat_usu_actualiz = '".$usuario."',cat_fec_actualiz = '".$fecha."' where cat_clave_int = '".$id."'");
    	if($del>0)
    	{
    		$msn = "Categoria eliminada";
    		$res = "ok";
    	}
    	else
    	{
    		$res = "error";
    		$msn = "Error BD. No se elimino la categoria";
    	}
    	$datos[] = array("res"=>$res,"msn"=>$msn);
    	echo json_encode($datos);
    }
    

?>