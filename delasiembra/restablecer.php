<?php
  include('data/Conexion.php');
  $codigo = $_GET['codigo'];
  if($codigo=="")
  {

      echo '<script>alert("No hay codigo de verificación"); window.location.href="index.php";</script>';
  }
  else
  {
  
    $con = pg_query($dbconn,"select * from tbl_recuperar where rec_codigo = '".$codigo."' and rec_estado = 1");
    $num = pg_num_rows($con);
    
    if($num > 0)
    {
      echo '<script>alert("Este codigo ya fue usado anteriormente"); window.location.href="index.php";</script>';
    }
    else
    {
      $condatos = pg_query($dbconn,"select * from tbl_recuperar where rec_codigo = '".$codigo."' limit 1");
      $num = pg_num_rows($condatos);
      $datos = pg_fetch_array($condatos);

      $idusuario = $datos['usu_clave_int'];
      $conusu= pg_query($dbconn,"SELECT usu_usuario,dir_clave_int,usu_imagen from tbl_usuario where usu_clave_int = '".$idusuario."'");
      $datusu = pg_fetch_array($conusu);
      $usuario = $datusu['usu_usuario'];
      $imagen = $datusu['usu_imagen'];
      if($imagen=="" || $imagen==NULL)
      {
        $imagen = "dist/img/default-user.png";
      }
      //echo $idusuario;
    }
  } 
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>DELASIEMBRA | Restablecer</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.css?<?php echo time();?>">

    <link rel="stylesheet" href="bootstrap/css/bootstrapds.css?<?php echo time();?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="dist/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css?<?php echo time();?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.css?<?php echo time();?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css?<?php echo time();?>">
  <link rel="stylesheet" href="dist/sweetalert/sweetalert2.css?<?php echo time ();?>">
</head>
<body class="hold-transition lockscreen">
<!-- Automatic element centering -->
<div class="lockscreen-wrapper">
  <div class="lockscreen-logo">
    <a href="index.php"> <img src="dist/img/LOGO.png" height="80" class="img-login"></a>
  </div>
  <!-- User name -->
  <div class="lockscreen-name">Hola, <?php echo $usuario;?></div>

  <!-- START LOCK SCREEN ITEM -->
  <div class="lockscreen-item">
    <!-- lockscreen image -->
    <div class="lockscreen-image">
      <img src="<?php echo $imagen;?>" width="100"  height="100" alt="User Image">
    </div>
    <!-- /.lockscreen-image -->

    <!-- lockscreen credentials (contains the form) -->
    <form class="lockscreen-credentials">
      <div class="input-group">
        <input type="password" class="form-control" placeholder="Contraseña nueva" id="contrasena1" name="contrasena1">
        <hr>
         <input type="password" class="form-control" placeholder="Repetir contraseña" id="contrasena2" name="contrasena2">

        <div class="input-group-btn">
          <button type="button" class="btn btn-success" onclick="RESTABLECER('<?php echo $codigo;?>')"><i class="fa fa-arrow-right text-muted"></i></button>
        </div>
      </div>
    </form>
    <!-- /.lockscreen credentials -->

  </div>
  <!-- /.lockscreen-item -->
  <div class="help-block text-center">
    Ingresa la nueva contraseña para restablecer tu sesión
  </div>
  <div class="text-center">
    <a href="index.php">O inicia sesión como un usuario diferente</a>
  </div>
  <div class="lockscreen-footer text-center">
   <small> PAVAS S.A.S.<br>
Copyright © Todos los derechos reservados</small>
  </div>
</div>
<!-- /.center -->

<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js?<?php echo time();?>"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.js?<?php echo time();?>"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js?<?php echo time();?>"></script>
<script  type="text/javascript" src="dist/sweetalert/sweetalert2.js?<?php echo time();?>"></script>
<script src="llamadas.js?<?php echo time();?>" type="text/javascript"></script>
</body>
<?php

?>
</html>
