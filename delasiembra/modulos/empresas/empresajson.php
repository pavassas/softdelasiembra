<?php
$idUsuario = $_COOKIE["usIdentificacion"];
include('../../data/Conexion.php');
//error_reporting(E_ALL);
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//Vvariable GET
$epr = $_GET['epr'];
$doc = $_GET['doc'];
$tel = $_GET['tel'];
$dir = $_GET['dir'];
$ema = $_GET['ema'];
$ciu = $_GET['ciu'];
$tip = $_GET['tip'];
$cue = $_GET['cue'];
// DB table to use
$table = 'empresa';
//act_clave_int id,act_nombre as nom,t.tpp_nombre tip,u.uni_codigo as cod,u.uni_nombre nomu,act_analisis as ana, act_sw_activo as est,p.pai_nombre as pai,d.dep_nombre as depa,c.ciu_nombre as ciu
// Table's primary key
$primaryKey = 'e.epr_clave_int';

// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object

// parameter names
$columns = array(
	array(
		'db' => 'e.epr_clave_int',
		'dt' => 'DT_RowId', 'field' => 'epr_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'row_'.$d;
		}
	),
		array( 'db' => 'e.epr_clave_int', 'dt' => 'Codigo', 'field' => 'epr_clave_int' ),
		array( 'db' => 'e.epr_nombre', 'dt' => 'Nombre', 'field' => 'epr_nombre' ),
		array( 'db' => 'e.epr_nro_docum', 'dt' => 'Documento', 'field' => 'epr_nro_docum' ),
		array( 'db' => 'e.epr_telefono', 'dt' => 'Telefono', 'field' => 'epr_telefono' ),
		array( 'db' => "e.epr_direccion", 'dt' => 'Direccion', 'field' => 'epr_direccion'  ),	
		array( 'db' => 'e.epr_email','dt' => 'Email', 'field' => 'epr_email' ),
		array( 'db' => 'e.epr_fax','dt' => 'Fax', 'field' => 'epr_fax' ),
		array( 'db' => 'e.epr_ciudad', 'dt' => 'Ciudad', 'field' => 'epr_ciudad' ),
		array( 'db' => 'e.epr_usu_actualiz', 'dt' => 'Usuario', 'field' => 'epr_usu_actualiz' ),
		array( 'db' => 'e.epr_fec_actualiz', 'dt' => 'Fecha', 'field' => 'epr_fec_actualiz' ),
		array( 'db' => 'e.epr_clave_int', 'dt' => 'Editar', 'field' => 'epr_clave_int',"formatter"=>function($d,$row)
		{
			$conectar = mysqli_connect("localhost", "usrpavas", "9A12)WHFy$2p4v4s", "helpdesktesting");
			$idUsuario = $_COOKIE["usIdentificacion"];
			$conp1 = mysqli_query($conectar,"select * from permisos_usuario where usu_clave_int = '".$idUsuario."' and pea_sigla='P47'");
			$P47 = mysqli_num_rows($conp1);
			if($P47>0)
			{	
				return "<a class='btn btn-block btn-warning btn-xs' onClick=CRUDEMPRESA('EDITAR','".$d."') style='width:20px; height:20px,cursor:pointer' data-toggle='modal' data-target='#myModal'  title='Editar Empresa'><i class='fa fa-edit'    ></i></a>";
			}
		}),
        array('db'=>'e.est_clave_int','dt'=>'Color','field'=>'est_clave_int','formatter'=>function($d,$row){
            if($d==1)
            {
                $col = "rgba(106,168,79, 0.5)";
                return $col;
            }
            else if($d==0){
               $col = "rgba(230,145,56, 0.5)";
                 return $col;
            }

        }),
         array( 'db' => 'e.epr_tipo','dt' => 'Tipo', 'field' => 'epr_tipo','formatter'=>function($d, $row){
             if($d==1){ return "Cliente";}
             else if($d==2){ return "Proveedor"; }
         } ),
         array('db' => 'e.epr_clave_int', 'dt'=> 'CantUsuarios','field' => 'epr_clave_int','formatter'=>function($d, $row){
         	$conectar = mysqli_connect("localhost", "usrpavas", "9A12)WHFy$2p4v4s", "helpdesktesting");
         	$con = mysqli_query($conectar, "select emu_usuario, emu_correo FROM empresa_usuario where epr_clave_int = '".$d."'");
         	$num = mysqli_num_rows($con);
         	return "<a class='btn btn-sm btn-info'>".$num."<i class='fa fa-user'></i></a>";
         }),
         array('db' => 'e.epr_clave_int', 'dt'=> 'Usuarios','field' => 'epr_clave_int','formatter'=>function($d, $row){
         	$conectar = mysqli_connect("localhost", "usrpavas", "9A12)WHFy$2p4v4s", "helpdesktesting");
         	$con = mysqli_query($conectar, "select emu_usuario, emu_correo FROM empresa_usuario where epr_clave_int = '".$d."'");
         	$num = mysqli_num_rows($con);
         	if($num>0)
         	{
         		$table = "<table class='table table-bordered' style='width:'>";
         		$table.="<tr><th>USUARIO</th><th>CORREO</td></tr>";
         		for($n=0;$n<$num;$n++)
         		{
         			$datn = mysqli_fetch_array($con);
         			$usu = $datn['emu_usuario'];
         			$cor = $datn['emu_correo'];
         			$table.="<tr><td>".$usu."</td><td>".$cor."</td></tr>";

         		}
         		$table .= "</table>";
         		return $table;
         	}
         	else
         	{
         		return "No hay usuarios asignados";
         	}

         }),
         array( 'db' => 'e.epr_num_banco', 'dt' => 'Cuenta', 'field' => 'epr_num_banco',"formatter"=>function($d,$row){
            if($row[13]==1){ return "";}
            else{ return "".$row[18]." ".$row[17]. " Nro.".$row[16]; }
         }),
         array( 'db' => 'e.epr_nom_banco', 'dt' => 'Banco', 'field' => 'epr_nom_banco'),
         array( 'db' => 'e.epr_tipo_cuenta','dt' => 'TipoCuenta', 'field' => 'epr_tipo_cuenta' ),
	
);

$sql_details = array(
	'user' => 'usrpavas',
	'pass' => '9A12)WHFy$2p4v4s',
	'db'   => 'helpdesktesting',
	'host' => '127.0.0.1'
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );
$whereAll = "";// customerid =".$customerid." AND date( orderdate ) >= '".$startdate."' AND date( orderdate ) <= '".$enddate."'";
$groupBy = ' e.epr_clave_int ';
$joinQuery = "FROM  empresa AS e";
$with = '';
$extraWhere = " (e.epr_nombre LIKE REPLACE('".$epr."%',' ','%') OR '".$epr."' IS NULL OR '".$epr."' = '') and (e.epr_nro_docum LIKE '".$doc."%' OR '".$doc."' IS NULL OR '".$doc."' = '') and (e.epr_telefono LIKE '".$tel."%' OR '".$tel."' IS NULL OR '".$tel."' = '') and (e.epr_direccion LIKE REPLACE('".$dir."%',' ','%')  OR '".$dir."' IS NULL OR '".$dir."' = '') and (e.epr_email LIKE REPLACE('".$ema."%',' ','%')  OR '".$ema."' IS NULL OR '".$ema."' = '') and (e.epr_ciudad LIKE REPLACE('".$ciu."%',' ','%')  OR '".$ciu."' IS NULL OR '".$ciu."' = '') and e.est_clave_int <> 2 and  (e.epr_tipo LIKE REPLACE('".$tip."%',' ','%') OR '".$tip."' IS NULL OR '".$tip."' = '') and  (e.epr_num_banco LIKE REPLACE('".$cue."%',' ','%') OR '".$cue."' IS NULL OR '".$cue."' = '')";
//u.salary >= 90000";  
//$whereAll = "i.act_clave_int not in(select act_clave_int from pre_cap_actividad where prc_clave_int='".$idca."')";// customerid =".$customerid." AND date( orderdate ) >= '".$startdate."' AND date( orderdate ) <= '".$enddate."'";

echo json_encode(
//SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy,$with )
);

