 
<?php
echo "<script>INICIALIZAR('Home');</script>";
?>
 <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3 id="diventregados">0</h3>

              <p>Pedidos Entregados</p>
            </div>
            <div class="icon">
              <i class="ion  ion-android-checkmark-circle"></i>
            </div>
            <a ui-sref="Pendientes({ Idestado: '3' })" ui-sref-opts="{reload: true}" class="small-box-footer">Mas info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6" >
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h3 id="divproceso"></h3>

              <p>Pedidos en proceso</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a ui-sref="Pendientes({ Idestado: '2' })" ui-sref-opts="{reload: true}" class="small-box-footer">Mas info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3 id="divpendientes"></h3>

              <p >Entregas pendientes</p>
            </div>
            <div class="icon">
              <i class="ion ion-android-notifications"></i>
            </div>
            <a ui-sref="Pendientes({ Idestado: '1' })" ui-sref-opts="{reload: true}" class="small-box-footer">Mas info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3 id="divcancelados"></h3>

              <p >Pedidos Cancelados</p>
            </div>
            <div class="icon">
              <i class="ion ion-minus-circled"></i>
            </div>
            <a ui-sref="Pendientes({ Idestado: '4' })" ui-sref-opts="{reload: true}" class="small-box-footer">Mas info <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-12 connectedSortable">
          <!-- Custom tabs (Charts with tabs)-->
          <div class="nav-tabs-custom">
            <!-- Tabs within a box -->
            <ul class="nav nav-tabs pull-right">
              <li class="active"><a href="#revenue-chart" data-toggle="tab">Frutas</a></li>
              <!--<li><a href="#sales-chart" data-toggle="tab">Donut</a></li>-->
              <li class="pull-left header"><i class="fa fa-inbox"></i> Ventas</li>
            </ul>
            <div class="tab-content no-padding">
              <!-- Morris chart - Sales -->
              <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;"></div>
              <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
             
            </div>
          </div>
          <!-- /.nav-tabs-custom -->         

        </section>
        <!-- /.Left col -->
      
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->