<?php
/**
 * PHPExcel
 *
 * Copyright (c) 2006 - 2015 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2015 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    ##VERSION##, ##DATE##
 */

/** Error reporting */

include ("../../data/Conexion.php");
error_reporting(0);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('memory_limit','500M');
ini_set('max_execution_time', 600);
ini_set('upload_max_filesize', '300M');
ini_set('safe_mode', 0);

define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
date_default_timezone_set("America/Bogota");
setlocale (LC_TIME,"spanish", "es_ES@euro", "es_ES", "es");

function convert_htmlentities($data)
{
    //$result = str_replace(
    //array("&aacute;","&eacute;","&iacute;","&oacute;","&uacute;","&ntilde;",
    //"&Aacute;","&Eacute;","&Iacute;","&Oacute;","&Uacute;","&Ntilde;"),array("á","é","í","ó","ú","ñ","Á","É","Í","Ó","Ú","Ñ") ,$data);
    $result = str_replace("á",htmlentities("á"),$data);
    $result = str_replace("é",htmlentities("é"),$result);
    $result = str_replace("í",htmlentities("í"),$result);
    $result = str_replace("ó",htmlentities("ó"),$result);
    $result = str_replace("ú",htmlentities("ú"),$result);
    $result = str_replace("Á",htmlentities("Á"),$result);
    $result = str_replace("É",htmlentities("É"),$result);
    $result = str_replace("Í",htmlentities("Í"),$result);
    $result = str_replace("Ó",htmlentities("Ó"),$result);
    $result = str_replace("Ú",htmlentities("Ú"),$result);
    $result = str_replace("ñ",htmlentities("ñ"),$result);
    $result = str_replace("Ñ",htmlentities("Ñ"),$result);
    $result = html_entity_decode($result, ENT_QUOTES, "ISO-8859-1");
    return $result;
}
function numeros_a_letras($numero) 
{
    // Convierte un numero en una letra de la A a la Z en el alfabeto latin
    // Utilizado para las columnas de excel
    // A = 0, si queremos que A = 1, modificaremos 26 por 27 (en los 2 sitios que esta)
    // Si le pasamos el valor 0 nos devolvera A, si pasamos 27 nos devolvera AB
    $res = "";
    while ($numero > -1) {
        // Cargaremos la letra actual
        $letter = $numero % 26;
        $res = chr(65 + $letter) . $res;  // A 65 en ASCII (A) le sumaremos el valor de la letra y lo convertiremos a texto (65 + 0 = A)
        $numero = intval($numero / 26) - 1; // Le quitamos la letra para ir a la siguiente y le restamos 1 si no se saltara una serie
    }
    return $res;
}

/** Include PHPExcel */
require_once '../../clases/PHPExcel.php';
date_default_timezone_set('UTC');
$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_sqlite;
if (PHPExcel_Settings::setCacheStorageMethod($cacheMethod)) {
    // echo date('H:i:s') , " Habilitar el almacenamiento en caché de la celda utilizando " , $cacheMethod , " metodo" , EOL;
} else {
    //echo date('H:i:s') , " No se puede establecer el almacenamiento en caché de la celda utilizando " , $cacheMethod , " método, volviendo a la memoria" , EOL;
}


PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_AdvancedValueBinder() );

function cellColor($cells,$color){
    global $objPHPExcel;

    $objPHPExcel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'startcolor' => array(
            'rgb' => $color
        )
    ));
}
// Set thin black border outline around column
//echo date('H:i:s') , " Set thin black border outline around column" , EOL;
$styleThinBlackBorderOutline = array(
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => 'FF000000'),
        ),
    ),
);
// Set thick brown border outline around "Total"
//echo date('H:i:s') , " Set thick brown border outline around Total" , EOL;
$styleThickBrownBorderOutline = array(
    'borders' => array(
        'outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THICK,
            'color' => array('argb' => 'FF993300'),
        ),
    ),
);

// Create new PHPExcel object
//echo date('H:i:s') , " Crear nuevo objeto PHPExcel" , EOL;
$objPHPExcel = new PHPExcel();

// Set document properties
//echo date('H:i:s') , " Establecer propiedades" , EOL;
$objPHPExcel->getProperties()->setCreator("Pavas.co")
    ->setLastModifiedBy("Pavas.co")
    ->setTitle("Informe Productos")
    ->setSubject("Informe Productos")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Informes");

$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()
    ->setCellValue('A1' , "LISTADO DE PRODUCTOS")   
    ->setCellValue('A2' , "CÓDIGO")
    ->setCellValue('B2' , "DESCRIPCIÓN")
    ->setCellValue('C2' , "CATEGORIA");

$nl = 3;
$conmer = pg_query($dbconn, "SELECT mer_clave_int,mer_nombre,mer_clasificacion,mer_porcentaje from tbl_mercado where mer_activo = 1  order by mer_clave_int = 3 ASC");
$nlc = $nl;
while($datm = pg_fetch_array($conmer))
{
    $l1 = numeros_a_letras($nl);  
    //echo $l1;         
    $idm = $datm['mer_clave_int'];
    $nom = $datm['mer_nombre'];
    $clas = $datm['mer_clasificacion'];
    $porc = $datm['mer_porcentaje'];
    $concla = pg_query($dbconn,"SELECT * FROM tbl_clasificacion where cla_clave_int in(".$clas.")");
    $numcla = pg_num_rows($concla);
    $nul = $nl + $numcla - 1;
    $l2 = numeros_a_letras($nul);
    $objPHPExcel->getActiveSheet()->setCellValue($l1.'2' , $nom)
     ->mergeCells($l1.'2:'.$l2.'2');

   
    for($n=0;$n<$numcla;$n++)
    {

        $lc = numeros_a_letras($nlc);
        $datc = pg_fetch_array($concla);
        $idc = $datc['cla_clave_int'];
        $nomc = $datc['cla_nombre'];
        $objPHPExcel->getActiveSheet()->setCellValue($lc.'3' , $nomc);
        $nlc++;
    }
    $nl = $nl + $numcla;
}

$ultf = $nl - 1;
$l3 = numeros_a_letras($ultf);
cellColor('A1:'.$l3.'3', '92D050');
$objPHPExcel->getActiveSheet()->mergeCells('A1:'.$l3.'1');
$objPHPExcel->getActiveSheet()->getStyle('A1:'.$l3.'3')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
$objPHPExcel->getActiveSheet()->getStyle('A1:'.$l3.'3')->getFont()->setBold(true);
$objPHPExcel->getActiveSheet()->getStyle('A1:'.$l3.'3')->getAlignment()->applyFromArray(array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER));
$objPHPExcel->getActiveSheet()->getStyle('A1:'.$l3.'3')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

//echo date('H:i:s') , " Set column height" , EOL;
$objPHPExcel->getActiveSheet()->getRowDimension('1')->setRowHeight(14);
$objPHPExcel->getActiveSheet()->getRowDimension('2')->setRowHeight(12.75);

$conpro = pg_query($dbconn,"SELECT  p.pro_clave_int, p.pro_clave_int, p.pro_clave_int, p.pro_nombre, p.pro_descripcion, p.pro_marca, p.pro_codigo, c.cat_nombre, p.pro_clave_int, p.pro_clave_int, p.pro_uni_compra, p.pro_pes_compra, p.pro_mu_compra, p.pro_mp_compra, p.pro_uni_venta, p.pro_pes_venta, p.pro_mu_venta, p.pro_mp_venta, p.pro_reserva, p.pro_estado, p.pro_activo, p.pro_tamano,p.pro_kilo FROM tbl_productos AS p JOIN tbl_categorias c ON c.cat_clave_int = p.cat_clave_int  WHERE pro_activo=1 ORDER BY p.pro_nombre ASC");

$numpro = pg_num_rows($conpro);
$hasta = $numpro + 3;
$acum = $hasta;
$filc = 4;
for ($i = 4; $i <= $hasta; $i++)
{
    $dat = pg_fetch_array($conpro);
    $idp = $dat['pro_clave_int'];
    $cod = $dat['pro_codigo'];
    $nom = $dat['pro_nombre'];
    $cat = $dat['cat_nombre'];
    $kilo = $dat['pro_kilo'];

    $objPHPExcel->getActiveSheet()//->setCellValue('A' . $filc, $cod)
        ->setCellValue('B' . $filc, $nom)
        ->setCellValue('C' . $filc, $cat)
        //->setCellValue('E' . $filc, $cant)
        //->setCellValue('F' . $filc, $valor)
        //->setCellValue('G' . $filc, "=E".$filc."*F".$filc)
        ;
    $objPHPExcel->getActiveSheet()->getStyle('A'.$filc.':C'.$filc)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

    $nlp = 3;
    $conmer = pg_query($dbconn, "SELECT mer_clave_int,mer_nombre,mer_clasificacion,mer_porcentaje from tbl_mercado where mer_activo = 1  order by mer_clave_int = 3 ASC");
    $nlcp = $nlp;
    while($datm = pg_fetch_array($conmer))
    {         
        //echo $l1;         
        $idm = $datm['mer_clave_int'];
        $nom = $datm['mer_nombre'];
        $clas = $datm['mer_clasificacion'];
        $porc = $datm['mer_porcentaje'];
        $concla = pg_query($dbconn,"SELECT * FROM tbl_clasificacion where cla_clave_int in(".$clas.")");
        $numcla = pg_num_rows($concla);       
       
        for($n=0;$n<$numcla;$n++)
        {
            $lcp = numeros_a_letras($nlcp);
            $datc = pg_fetch_array($concla);
            $idc = $datc['cla_clave_int'];
            $nomc = $datc['cla_nombre'];
            $convalact = pg_query($dbconn,"SELECT  prm_costo,prm_venta,prm_peso_unidad,pro_kilo,pro_und_kilo FROM tbl_precios_mercado m JOIN tbl_productos p on p.pro_clave_int = m.pro_clave_int WHERE m.pro_clave_int = '".$idp."' and mer_clave_int ='".$idm."' and cla_clave_int = '".$idc."' limit 1");
            $datval = pg_fetch_array($convalact);
            $valcos = $datval['prm_costo'];
            $valact = $datval['prm_venta'];
            $pesund = $datval['prm_peso_unidad'];
          
            if($kil==1)
            {

                if($pesund<=0){ $undkilo = 0; $costund = $valact/1; }
                else { $undkilo = 1000/$pesund; $costund = $valact/$undkilo; }
            }
            else
            {
                $undkilo = 1;
                $costund = "";
            }


            $objPHPExcel->getActiveSheet()->setCellValue($lcp.$filc , $valact);
            $objPHPExcel->getActiveSheet()->getStyle($lcp.$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0;[Red][<0]("$"* #,##0);"$"* #,##0');
            $objPHPExcel->getActiveSheet()->getStyle($lcp.$filc)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
            $objPHPExcel->getActiveSheet()->getColumnDimension($lcp)->setWidth(11);
            $nlcp++;
        }        
    }    
        /*
        ->setCellValue('I' . $filc, $valor)
        ->setCellValue('J' . $filc, "=H".$filc."*I".$filc)
        ->setCellValue('K' . $filc, $totaladj)
        ->setCellValue('L' . $filc, '=IF(K'.$filc.'>0,G'.$filc.'-K'.$filc.',G'.$filc.'-J'.$filc.')')
    ;*/
    //cellColor('A' . $filc.':L'.$filc, 'FFFFFF');
    //FORMATO MONEDA
    //$objPHPExcel->getActiveSheet()->getStyle('F'.$filc.':G'.$filc)->getNumberFormat()->setFormatCode('[Black][>=0]"$"* #,##0.00;[Red][<0]("$"* #,##0.00);"$"* #,##0.00');
    $objPHPExcel->getActiveSheet()->getCell('A' . $filc)->setValueExplicit($cod,PHPExcel_Cell_DataType::TYPE_STRING);
    //$objPHPExcel->getActiveSheet()->getCell('E' . $filc)->setValueExplicit($cantidad,PHPExcel_Cell_DataType::TYPE_NUMERIC);
    $objPHPExcel->getActiveSheet()->getRowDimension($filc)->setRowHeight(12.75);
    //FIN CONSULTAS
    $filc = $filc + 1;
}
$filc1 = $filc;
$filcu = $filc-1;
// $filc2 = $filc + 2;
//echo date('H:i:s') , " Set column widths" , EOL;
$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(17);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(60);
$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
//$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(17);
//$objPHPExcel->getActiveSheet()->getStyle('E2:E'.$filc1)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);
//$objPHPExcel->getActiveSheet()->getStyle('B8:L'.$filc1)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
//zoom de la pagina
$objPHPExcel->getActiveSheet()->getSheetView()->setZoomScale(90);
//titulos de la hoja
$objPHPExcel->getActiveSheet()->setTitle('PRODUCTOS');
//$objPHPExcel->getActiveSheet()->setSheetState(PHPExcel_Worksheet::SHEETSTATE_HIDDEN);
$callStartTime = microtime(true);
$archivo =  date('Ymd').' PRODUCTOS.xlsx';
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
//$objWriter->save(str_replace(__FILE__,'descargas/'.$archivo,__FILE__));

$callEndTime = microtime(true);
$callTime = $callEndTime - $callStartTime;
$arc = str_replace('.php', '.xlsx', pathinfo(__FILE__, PATHINFO_BASENAME));
// Set password for readonly activesheet
/*
$objPHPExcel->getSecurity()->setLockWindows(true);
$objPHPExcel->getSecurity()->setLockStructure(true);
$objPHPExcel->getSecurity()->setWorkbookPassword("P4v4s2017.*");*/
// Set password for readonly data
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$archivo.'"');
header('Cache-Control: max-age=0');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');