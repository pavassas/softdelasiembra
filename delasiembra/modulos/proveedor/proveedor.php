<?php 
  include ("../../data/Conexion.php");
  session_start();
  error_reporting(0);
  // variable login que almacena el login o nombre de usuario de la persona logueada
  $login= isset($_SESSION['persona']);
  // cookie que almacena el numero de identificacion de la persona logueada
  $usuario= $_COOKIE['usuario'];
  $idUsuario= $_SESSION["idusuario"];
  $clave= $_COOKIE["clave"];
  $identificacion = $_COOKIE["usIdentificacion"];
  date_default_timezone_set('America/Bogota');
  $fecha=date("Y/m/d H:i:s");

echo "<script>INICIALIZAR('Proveedor');</script>";
//echo "<script>CRUDPRODUCTOS('LISTAPRODUCTOS','');</script>"; 
?>
<section class="content-header">
      <h1>
        Proveedores
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
      	<li><a href="#"><i class="fa fa-dashboard"></i> Configuración</a></li>
        <li class="active">Proveedores</li>
        <li>
          <a class="text-green"  title="Añadir Producto" tabindex="-1" data-toggle="modal" data-target="#modalregistro" onclick="CRUDPROVEEDORES('NUEVO','')">    
            <span class="fa fa-plus-circle fa-2x"></span>    
        </a>
        </li>       
      </ol>
    </section>
 <section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="input-group">
                <span class="input-group-addon" data-toggle="modal" data-target="#modalleft"><i class="fa fa-filter"></i></span>
                <select id="busproveedor" onchange="CRUDPROVEEDORES('LISTAPROVEEDORES','')" class="form-control selectpicker" placeholder="seleccione un proveedor" data-header="Buscar por documento, nombre, correo" multiple data-actions-box="true" data-live-search="true" data-selected-text-format="count > 6">
                  <?php
                 
                    $conpro = pg_query($dbconn,"select prv_clave_int,prv_nombre,prv_email,prv_nro_docum from tbl_proveedor where prv_activo!=2");
                    $numpro = pg_num_rows($conpro);
                      for($np=0;$np<$numpro;$np++)
                      {
                        $datp = pg_fetch_array($conpro);
                        $idp = $datp['prv_clave_int'];
                        $nomp = $datp['prv_nombre'];
                        $corp = $datp['prv_email'];
                        $docp = $datp['prv_nro_docum'];
                        
                        ?>
                        <option  value="<?php echo $idp;?>" data-subtext="<?php echo $corp;?>"><?php echo $docp."-".$nomp;?></option>
                        <?php
                      }
                    
                  ?>

                </select>
                <span id="searchvista"  data-toggle="tooltip" title="Buscar"  class="input-group-addon" onclick="CRUDPROVEEDORES('LISTAPROVEEDORES','')"><i class="fa fa-search"></i></span>
                
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12" id="tabladatos"></div>
    
  </div>
 </section>