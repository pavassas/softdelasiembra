<?php
include ("../../data/Conexion.php");
session_start();
error_reporting(0);
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_SESSION["idusuario"];
$clave= $_COOKIE["clave"];
$identificacion = $_COOKIE["usIdentificacion"];
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DB table to use
$table = 'tbl_salidas';
// Table's primary key
$primaryKey = 's.sal_clave_int';
$id = $_POST['id'];
// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object


// parameter names
$columns = array(
	array(
		'db' => 's.sal_clave_int',
		'dt' => 'DT_RowId', 'field' => 'sal_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'rows_'.$d;
		}
	),
    array( 'db' => 's.sal_clave_int', 'dt' => 'Eliminar', 'field' => 'sal_clave_int','formatter'=>function($d,$row){
    return "<a class='btn btn-circle btn-block btn-danger btn-xs' onclick=CRUDINVENTARIO('ELIMINARSALIDA','".$d."') title='Eliminar Salida' style='heigth:22px; width:22px'><i class='fa fa-trash'></i></a>";

    }),
    array( 'db' => 's.sal_clave_int', 'dt' => 'Editar', 'field' => 'sal_clave_int','formatter'=>function($d,$row){
    return "<a class='btn btn-circle btn-block btn-warning btn-xs' onClick=CRUDINVENTARIO('EDITARSALIDA','".$d."') title='Editar Salida' style='heigth:22px; width:22px' data-toggle='modal' data-target='#modalregistro'><i class='fa fa-pencil'></i></a>";

    }), 
	array( 'db' => 's.sal_fecha', 'dt' => 'Fecha', 'field' => 'sal_fecha'),
	array( 'db' => 'm.mos_nombre', 'dt' => 'Motivo', 'field' => 'mos_nombre' ),
    array( 'db' => 's.sal_cantidad', 'dt' => 'Cantidad', 'field' => 'sal_cantidad' ),
    array( 'db' => 's.sal_peso', 'dt' => 'Peso', 'field' => 'sal_peso','formatter'=>function($d,$row){
    	if($d>=1000)
    	{
    		$d = number_format($d/1000,2,',',',')."KG";

    	}
    	else
    	{
    		$d = $d."GR";
    	}
    	return $d;
    } ),
   // array( 'db' => 's.ent_valor', 'dt' => 'Valor', 'field' => 'ent_valor'),
   /* array( 'db' => 'e.ent_clave_int', 'dt' => 'Total', 'field' => 'ent_clave_int','formatter'=>function($d,$row){
        $tot = $row[5] * $row[7];
        return number_format($tot,2,'.',',');
    }),*/
    array( 'db' => 's.sal_usu_actualiz', 'dt' => 'Usuario', 'field' => 'sal_usu_actualiz'),
    array( 'db' => 's.sal_fec_actualiz', 'dt' => 'FechaAct', 'field' => 'sal_fec_actualiz'),      
    array( 'db' => 's.sal_clave_int', 'dt' => 'Clave', 'field' => 'sal_clave_int'),
    array( 'db' => 'cl.cla_nombre', 'dt' => 'Calidad', 'field' => 'cla_nombre' ),
    
);

$sql_details = array(
	'user' => 'usrpavas',
	'pass' => '9A12)WHFy$2p4v4s',
	'db'   => 'dbdelasiembra',
	'port'   => '5432',
	'host' => '127.0.0.1'
);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );
$whereAll = "";
$groupBy = '';//'p.pro_clave_int';
$with = '';
$joinQuery = "FROM tbl_salidas AS s JOIN tbl_motivo_salida  m ON m.mos_clave_int = s.mos_clave_int  left outer join tbl_clasificacion cl on cl.cla_clave_int  = s.cla_clave_int";
$extraWhere =" s.pro_clave_int = '".$id."'";

echo json_encode(
	SSP::simple($_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy,$with )

);