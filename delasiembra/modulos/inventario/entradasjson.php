<?php
include ("../../data/Conexion.php");
session_start();
error_reporting(0);
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_SESSION["idusuario"];
$clave= $_COOKIE["clave"];
$identificacion = $_COOKIE["usIdentificacion"];
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */

// DB table to use
$table = 'tbl_productos';
// Table's primary key
$primaryKey = 'p.pro_clave_int';
$nom = $_POST['nom'];
$cod = $_POST['cod'];
$cat = $_POST['cat']; $cat = implode(', ', (array)$cat); if($cat==""){$cat1="'0'";}else {$cat1=$cat;}
$mar = $_POST['mar'];
//$uni = $_POST['uni']; $uni = implode(', ', (array)$uni); if($uni==""){$uni1="'0'";}else {$uni1=$uni;}
$pro = $_POST['pro']; $pro = implode(', ', (array)$pro); if($pro==""){$pro1="'0'";}else {$pro1=$pro;}
// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object
// parameter names
$columns = array(
	array(
		'db' => 'p.pro_clave_int',
		'dt' => 'DT_RowId', 'field' => 'pro_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'rowe_'.$d;
		}
	),			
	array( 'db' => 'p.pro_nombre', 'dt' => 'Nombre', 'field' => 'pro_nombre'),
	array( 'db' => 'p.pro_descripcion', 'dt' => 'Descripcion', 'field' => 'pro_descripcion' ),
    array( 'db' => 'p.pro_marca', 'dt' => 'Marca', 'field' => 'pro_marca' ),
    array( 'db' => 'p.pro_codigo', 'dt' => 'Codigo', 'field' => 'pro_codigo'),
    array( 'db' => 'c.cat_nombre', 'dt' => 'Categoria', 'field' => 'cat_nombre'),       
    array( 'db' => 'p.pro_clave_int', 'dt' => 'Clave', 'field' => 'pro_clave_int'),
   /* array( 'db' => 'p.pro_uni_compra', 'dt' => 'UniCompra', 'field' => 'pro_uni_compra','formatter'=>function($d,$row){
    	global $dbconn;
    	$sql = pg_query($dbconn,"select uni_codigo from tbl_unidades where uni_clave_int = '".$d."'");
    	$dat = pg_fetch_array($sql);
    	$uni = $dat['uni_codigo'];
    	return $uni;
    }),
    array( 'db' => 'p.pro_uni_consumo', 'dt' => 'UniConsumo', 'field' => 'pro_uni_consumo','formatter'=>function($d,$row){
    	global $dbconn;
    	$sql = pg_query($dbconn,"select uni_codigo from tbl_unidades where uni_clave_int = '".$d."'");
    	$dat = pg_fetch_array($sql);
    	$uni = $dat['uni_codigo'];
    	return $uni;
    }),*/
    array( 'db' => 'p.pro_clave_int','dt'=>'Cantidad','field'=> 'pro_clave_int','formatter'=> function($d,$row){
    	global $dbconn;
    	$sql = pg_query($dbconn, "select sum(ent_cantidad) tot from tbl_entradas where pro_clave_int = '".$d."'");
    	$dat = pg_fetch_array($sql);
    	$cant = $dat['tot']; if($cant<=0){ $cant = 0; } 
    	return $cant;
    })
);

$sql_details = array(
	'user' => 'usrpavas',
	'pass' => '9A12)WHFy$2p4v4s',
	'db'   => 'dbdelasiembra',
	'port'   => '5432',
	'host' => '127.0.0.1'
);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );
$whereAll = "";
$groupBy = '';//'p.pro_clave_int';
$with = '';
$joinQuery = "FROM tbl_productos AS p JOIN tbl_categorias c ON c.cat_clave_int = p.cat_clave_int";
$extraWhere =" ( p.pro_nombre LIKE REPLACE('".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '' ) and ( p.pro_codigo LIKE REPLACE('".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '' ) and ( p.pro_marca LIKE REPLACE('".$mar."%',' ','%') OR '".$mar."' IS NULL OR '".$mar."' = '' ) and ( p.cat_clave_int IN(".$cat1.") OR '".$cat."' IS NULL OR '".$cat."' = '')  and ( p.pro_clave_int IN(".$pro1.") OR '".$pro."' IS NULL OR '".$pro."' = '') and pro_activo!=2";//and ( p.pro_uni_compra IN(".$uni1.") OR '".$uni."' IS NULL OR '".$uni."' = '') and ( p.pro_uni_consumo IN(".$uni1.") OR '".$uni."' IS NULL OR '".$uni."' = '')

echo json_encode(
	SSP::simple($_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy,$with )

);