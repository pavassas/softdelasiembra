<?php

  include ("../../data/Conexion.php");
  session_start();
  error_reporting(0);
  // variable login que almacena el login o nombre de usuario de la persona logueada
  $login= isset($_SESSION['persona']);
  // cookie que almacena el numero de identificacion de la persona logueada
  $usuario= $_COOKIE['usuario'];
  $idUsuario= $_SESSION["idusuario"];
  $clave= $_COOKIE["clave"];
  $identificacion = $_COOKIE["usIdentificacion"];
  date_default_timezone_set('America/Bogota');
  $fecha=date("Y/m/d H:i:s");
	echo "<script>CRUDMERCADO('LISTAMERCADOS','');</script>";
?>

<section class="content-header">
      <h1>
        Mercado
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Configuración</a></li>
        <li class="active">Mercado</li>        
      </ol><a class="btn b btn-success btn-xs"  title="Añadir Mercado" tabindex="-1" data-toggle="modal" data-target="#modalregistro" onclick="CRUDMERCADO('NUEVO','')">   
            <i class="fa fa-plus-circle"></i>NUEVO
        </a>
        
    </section>

 <section class="content">
  <div class="row">
      <div class="col-md-12">            
        <div class="input-group">
          <span class="input-group-addon" onclick="CRUDMERCADO('LISTAMERCADOS','')"><i class="fa fa-search"></i></span>
          <input type="text" class="form-control" placeholder="busque por nombre" id="filtromercado">
        </div>    
    </div>
  </div>
  <div class="row">
    <div class="col-md-12" id="tabladatos"></div>
  </div>
 </section>




