<?php
include ("../../data/Conexion.php");
error_reporting(0);
session_start();
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_SESSION["idusuario"];
$clave= $_COOKIE["clave"];
$identificacion = $_COOKIE["usIdentificacion"];
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
$fil = $_POST['fil'];
// DB table to use
$table = 'tbl_mercado';

// Table's primary key
$primaryKey = 'm.mer_clave_int';
// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object
// parameter names
$columns = array(
	array(
		'db' => 'm.mer_clave_int',
		'dt' => 'DT_RowId', 'field' => 'mer_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'row_mer'.$d;
		}
	),
	array( 'db' => 'm.mer_clave_int', 'dt' => 'Editar' ,'field' => 'mer_clave_int','formatter'=>function($d,$row){	   		  
			return "<a class='btn btn-block btn-default btn-xs' onClick=CRUDMERCADO('EDITAR','".$d."') data-toggle='modal' data-target='#modalregistro' style='width:20px; height:20px'><i class='fa fa-edit'></i></a>";
		} ),
	array( 'db' => 'm.mer_clave_int', 'dt' => 'Eliminar' ,'field' => 'mer_clave_int','formatter'=>function($d,$row){	   
				return 	"<a class='btn btn-block btn-danger btn-xs' onClick=CRUDMERCADO('ELIMINAR','".$d."') style='width:20px; height:20px'><i class='fa fa-trash'></i></a>";		  
		 } ),
	array( 'db' => 'm.mer_clasificacion', 'dt' => 'Clasificacion','field' => 'mer_clasificacion','formatter'=>function($d,$row){
		global $dbconn;
		if($d!="" and $d!=NULL){
			$sql = pg_query($dbconn, "select cla_nombre,cla_clave_int from tbl_clasificacion where cla_clave_int in(".$d.")");
			$num = pg_num_rows($sql);
			$ul = "<ul>";
			for($k=0;$k<$num;$k++)
			{
				
				$dat = pg_fetch_array($sql);
				if($row[8]==$dat['cla_clave_int']){ $td = "(Default)";}else{ $td = "";}
				$ul.= "<li>".$dat['cla_nombre']."".$td."</li>";
			}
			$ul.="</ul>";
			return $ul;
		}
		else
		{
			return "";
		}
	} ),
	array( 'db' => 'm.mer_nombre', 'dt' => 'Nombre','field' => 'mer_nombre' ),	
	array( 'db' => 'm.mer_activo', 'dt' => 'Estado' ,'field' => 'mer_activo'),
	array( 'db' => 'm.mer_porcentaje', 'dt' => 'Porcentaje' ,'field' => 'mer_porcentaje'),
	array( 'db'  => 'm.mer_tamano','dt' => 'Tamano' ,'field' => 'mer_tamano','formatter'=>function($d,$row){
		if($d=="1")
		{
			return "<label><input disabled checked type='checkbox' class='flat-red'>Si</label>";
		}
		else
		{
			return "<label><input disabled  type='checkbox' class='flat-red'>No</label>";
		}
	}),
	array( 'db' => 'm.mer_cla_default','dt'=>'Default', 'field'=> 'mer_cla_default')
);

$sql_details = array(
	'user' => 'usrpavas',
	'pass' => '9A12)WHFy$2p4v4s',
	'db'   => 'dbdelasiembra',
	'port'   => '8584',
	'host' => '127.0.0.1'
);
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );

$whereAll = "";
$groupBy = '';
$joinQuery =  " FROM tbl_mercado m ";
$extraWhere = " m.mer_activo in(0,1) and (m.mer_nombre ILIKE REPLACE('".$fil."%',' ','%')  OR '".$fil."' IS NULL OR '".$fil."' = '')  ";  
echo json_encode(
	SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy )
);

