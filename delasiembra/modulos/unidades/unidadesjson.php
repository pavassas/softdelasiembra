<?php
include ("../../data/Conexion.php");
error_reporting(0);
session_start();
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_SESSION["idusuario"];
$clave= $_COOKIE["clave"];
$identificacion = $_COOKIE["usIdentificacion"];
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
$fil = $_POST['fil'];
// DB table to use
$table = 'tbl_unidades';

// Table's primary key
$primaryKey = 'u.uni_clave_int';
// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object
// parameter names
$columns = array(
	array(
		'db' => 'u.uni_clave_int',
		'dt' => 'DT_RowId', 'field' => 'uni_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'row_uni'.$d;
		}
	),
	array( 'db' => 'u.uni_clave_int', 'dt' => 'Editar' ,'field' => 'uni_clave_int','formatter'=>function($d,$row){	   
		  
			return "<a class='btn btn-block btn-default btn-xs' onClick=CRUDUNIDADES('EDITAR','".$d."') data-toggle='modal' data-target='#modalregistro' style='width:20px; height:20px'><i class='glyphicon glyphicon-pencil'></i></a>";		   

		} ),
	array( 'db' => 'u.uni_clave_int', 'dt' => 'Eliminar' ,'field' => 'uni_clave_int','formatter'=>function($d,$row){		
		   
				return 	"<a class='btn btn-block btn-danger btn-xs' onClick=CRUDUNIDADES('ELIMINAR','".$d."') style='width:20px; height:20px'><i class='glyphicon glyphicon-trash'></i></a>";		  
		 } ),
	array( 'db' => 'u.uni_clave_int', 'dt' => 'Codigo','field' => 'uni_clave_int' ),
	array( 'db' => 'u.uni_nombre', 'dt' => 'Nombre','field' => 'uni_nombre' ),
	array( 'db' => 'u.uni_codigo', 'dt' => 'Cod' ,'field' => 'uni_codigo' ),
	array( 'db' => 'u.uni_nombre', 'dt' => 'Unidad' ,'field' => 'uni_nombre'),
	array( 'db'  => 'e.est_nombre','dt' => 'Estado' ,'field' => 'est_nombre')
);


$sql_details = array(
	'user' => 'usrpavas',
	'pass' => '9A12)WHFy$2p4v4s',
	'db'   => 'dbdelasiembra',
	'port'   => '8584',
	'host' => '127.0.0.1'
);

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );

$whereAll = "";
$groupBy = '';
$joinQuery =  " FROM tbl_unidades u join tbl_estados AS e on e.est_clave_int = u.est_clave_int ";
$extraWhere = " e.est_clave_int in(0,1) and (u.uni_nombre LIKE REPLACE('".$fil."%',' ','%')  or u.uni_codigo LIKE REPLACE('".$fil."%',' ','%') OR '".$fil."' IS NULL OR '".$fil."' = '')  ";  
echo json_encode(
	SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy )
);

