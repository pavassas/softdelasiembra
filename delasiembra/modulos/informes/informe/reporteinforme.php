<?php


$fec = $_GET['busdesde'];
$fech = $_GET['bushasta'];

setlocale(LC_ALL,"es_ES.utf8","es_ES","esp");
$sql = pg_query($dbconn , "SELECT 
p.ped_fec_programada,
pr.pro_codigo,
pr.pro_nombre,
pd.pde_tamano,
pd.pde_estado,
pd.cla_clave_int,
pr.pro_kilo,
pr.pro_clave_int
FROM tbl_pedidos p 
JOIN tbl_pedidos_detalle pd ON pd.ped_clave_int = p .ped_clave_int
JOIN tbl_productos pr ON pr.pro_clave_int = pd.pro_clave_int
WHERE p.ped_estado in(1,2) 
and ((p.ped_fec_programada BETWEEN '".$fec."' AND '".$fech."') or ('".$fec."' Is Null and '".$fech."' Is Null) or ('".$fec."' = '' and '".$fech."' = ''))"); 
?>
<page backcolor="#FEFEFE" backimg="../../../dist/img/plantillas/hojaA4N.jpg" backimgx="left" backimgy="top" backimgw="100%" backtop="0" backbottom="30mm" footer="date;time;page" style="font-size: 14px">
    <!--cellpadding="4" cellspacing="4"-->
    <style>
        table {
            border: 1px solid #000;
        }
        th, td {
            text-align: left;
            vertical-align: top;
            border: 1px solid #000;
        }
    </style>
    <h3>Despachos del día <?php echo $fec ?></h3>
    <table style="width:100%;border-collapse:collapse; border-width:1px" border="0" cellpadding="7" cellspacing="7">
    
        <tr>
            <th>Fecha<br>Programada</th>
            <th>Producto</th>
            <th>Calidad</th>
            <th>Tamaño</th>
            <th>Maduración</th>
            <th>Cant<br>Total</th>
            <th>Peso.<br>Estimado</th>
        </tr>
        
           <?php while($data = pg_fetch_array($sql)){ 
               $ped_fec_programada = $data['ped_fec_programada'];
               $pro_codigo = $data['pro_codigo'];
               $pro_nombre = $data['pro_nombre'];
               $pde_tamano = $data['pde_tamano'];
               $pde_estado = $data['pde_estado'];
               $cla_clave_int = $data['cla_clave_int'];
               $pro_kilo = $data['pro_kilo'];
               $pro_clave_int = $data['pro_clave_int'];
            ?>
           <tr>
           <td><?php echo $ped_fec_programada;  ?></td>
           <td><?php echo $pro_nombre;  ?></td>
           <td>
               <?php
                    $con = pg_query($dbconn, "select cla_nombre from tbl_clasificacion where cla_clave_int= '".$cla_clave_int."'");
                    $dat = pg_fetch_array($con);
                    $nomcla = $dat['cla_nombre'];
                    echo $nomcla; 
               ?>
           </td>
           <td><?php echo $pde_tamano;  ?></td>
           <td><?php echo $pde_estado;  ?></td>
           <td>
               <?php
                    $con = pg_query($dbconn ,"SELECT SUM(pde_cantidad) cant FROM tbl_pedidos_detalle pd join tbl_pedidos p on p.ped_clave_int = pd.ped_clave_int WHERE pde_unidad = 0 and pd.pro_clave_int = '".$pro_clave_int."' and p.ped_estado  !='4' and p.ped_estado!='0' and  p.ped_fec_programada = '".$ped_fec_programada."' and pde_tamano = '".$pde_tamano."' and pd.pde_estado = '".$pde_estado."' and pd.cla_clave_int = '".$cla_clave_int."'");
                    $dat = pg_fetch_array($con);
                    $cant1 = $dat['cant'];
            
                    $con = pg_query($dbconn ,"SELECT SUM(pde_cantidad*pde_unidad) cant FROM tbl_pedidos_detalle pd join tbl_pedidos p on p.ped_clave_int = pd.ped_clave_int WHERE pde_unidad = 1 and pd.pro_clave_int = '".$pro_clave_int."' and p.ped_estado  IN(1,2) and p.ped_fec_programada = '".$ped_fec_programada."'  and pde_tamano = '".$pde_tamano."' and pd.pde_estado = '".$pde_estado."' and pd.cla_clave_int = '".$cla_clave_int."'");
                    $dat = pg_fetch_array($con);
                    $cant2 = $dat['cant'];
            
                    $cantidad = $cant1 + $cant2;
                    echo  round($cantidad,2);
               ?>
            </td>
           <td>
               <?php
                   $concos = pg_query($dbconn, "SELECT prm_costo,prm_peso_unidad FROM tbl_precios_mercado WHERE pro_clave_int = '".$pro_clave_int."' and cla_clave_int = '".$cla_clave_int."' ORDER BY prm_costo DESC LIMIT 1");
                   $datcos = pg_fetch_array($concos);
                   $costo = $datcos['prm_costo'];
                   $pesund = $datcos['prm_peso_unidad'];
                   
                    $con = pg_query($dbconn ,"SELECT SUM(pde_cantidad) cant FROM tbl_pedidos_detalle pd join tbl_pedidos p on p.ped_clave_int = pd.ped_clave_int WHERE pde_unidad = 0 and pd.pro_clave_int = '".$pro_clave_int."' and p.ped_estado  !='4' and p.ped_estado!='0' and p.ped_fec_programada = '".$ped_fec_programada."'  and pde_tamano = '".$pde_tamano."' and pd.pde_estado = '".$pde_estado."' and pd.cla_clave_int = '".$cla_clave_int."'");
                    $dat = pg_fetch_array($con);
                    $cant1 = $dat['cant'];
           
                    $con = pg_query($dbconn ,"SELECT SUM(pde_cantidad*pde_unidad) cant FROM tbl_pedidos_detalle pd join tbl_pedidos p on p.ped_clave_int = pd.ped_clave_int WHERE pde_unidad = 1 and pd.pro_clave_int = '".$pro_clave_int."' and p.ped_estado  !='4' and p.ped_estado!='0' and  p.ped_fec_programada = '".$ped_fec_programada."' and pde_tamano = '".$pde_tamano ."' and pd.pde_estado = '".$pde_estado."' and pd.cla_clave_int = '".$cla_clave_int."'");
                    $dat = pg_fetch_array($con);
                    $cant2 = $dat['cant'];
           
                    $cantidad = $cant1 + $cant2;
                    $pesoestimado = 0;
                    $pesoestimado  =  ($cantidad*$pesund)/1000;              
                    echo round($pesoestimado,2)."kG";
                ?>
           </td>
           </tr>
           <?php }?>
    </table>
</page>