<?php
//include ("../../../data/Conexion.php");
setlocale(LC_ALL,"es_ES.utf8","es_ES","esp");
$sql = pg_query($dbconn , "SELECT 
u.usu_nombre,
u.usu_apellido,
m.mer_nombre,
prf.prf_descripcion,
u.usu_email,
u.usu_telefono,
u.usu_clave_int,
count(*) as numpe
FROM  tbl_usuario AS  u 
inner join tbl_perfil AS prf ON (prf.prf_clave_int = u.prf_clave_int) 
left outer join tbl_mercado m on m.mer_clave_int = u.mer_clave_int
join tbl_pedidos pe ON u.usu_clave_int = pe.usu_clave_int
where pe.ped_estado not in (4,0)
GROUP BY u.usu_clave_int,u.usu_nombre,u.usu_apellido,m.mer_nombre,prf.prf_descripcion,u.usu_email,u.usu_telefono");
?>
<page backcolor="#FEFEFE" backimg="../../../dist/img/plantillas/hojaCorreo.jpg" backimgx="left" backimgy="top" backimgw="144%" backtop="0" backbottom="30mm" footer="date;time;page" style="font-size: 14px" orientation="landscape">
    <!--cellpadding="4" cellspacing="4"-->
    <style>
        table {
            border: 1px solid #000;
        }
        th, td {
            text-align: left;
            vertical-align: top;
            border: 1px solid #000;
        }
    </style>
    <h3>Informe de compras</h3>
    <table style="width:100%;border-collapse:collapse; border-width:1px" border="0" cellpadding="7" cellspacing="7">
        <tr>
            <th>Nombre</th>	
            <th>Mercado</th>	
            <th>Perfil</th>	
            <th>E-mail</th>	
            <th>Celular</th>								
            <th>Numero<br>pedidos</th>
            <th>Ultimo<br>pedido</th>
            <th>Numero<br>dias</th>
        </tr>
        
           <?php while($data = pg_fetch_array($sql)){ 
               $usu_nombre    = $data['usu_nombre'];
               $usu_apellido  = $data['usu_apellido'];
               $mer_nombre    = $data['mer_nombre'];
               $prf_descripcion    = $data['prf_descripcion'];
               $usu_email    = $data['usu_email'];
               $usu_telefono = $data['usu_telefono'];
               $usu_clave_int      = $data['usu_clave_int'];
               $numpedi = $data['numpe'];

               $con2 = pg_query($dbconn, "SELECT
               DATE_PART('day', current_date::timestamp - pe.ped_fecha::timestamp)as numdi,
               to_char(pe.ped_fecha , 'YYYY-MM-DD')fechaulti
               FROM  tbl_pedidos pe
               where pe.usu_clave_int = '".$usu_clave_int."'
               and pe.ped_fecha is not null
               ORDER BY pe.ped_fecha desc
               LIMIT 1");
               $dat2 = pg_fetch_array($con2);
               $numdia = $dat2['numdi'];
               $ultgec = $dat2['fechaulti'];
           ?>
           <tr>
                <td><?php echo $usu_nombre." ".$usu_apellido  ?></td>
                <td><?php echo $mer_nombre ?></td>
                <td><?php echo $prf_descripcion ?></td>
                <td><?php echo $usu_email ?></td>
                <td><?php echo $usu_telefono ?></td>
                <td>
                    <?php
                        echo $numpedi;
                    ?>
                 </td>
                <td>
                    <?php
                        echo $ultgec; 
                    ?>
                </td>
                <td>
                    <?php
                       
                        echo $numdia;
                    ?>
                 </td>
           </tr>
           <?php }?>
    </table>
</page>