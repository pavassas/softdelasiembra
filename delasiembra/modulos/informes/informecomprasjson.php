<?php
include ("../../data/Conexion.php");
error_reporting(0);
$table = 'usuario';
$primaryKey = 'u.usu_clave_int';
$columns = array(
		array(
			'db' => 'u.usu_clave_int',
			'dt' => 'DT_RowId', 'field' => 'usu_clave_int',
			'formatter' => function( $d, $row ) {
				return 'row_'.$d;
			}
		),
		array( 'db' => 'u.usu_clave_int',     'dt' => 'Codigo',   'field' => 'usu_clave_int' ),
        array( 'db' => 'u.usu_nombre',        'dt' => 'Nombre',   'field' => 'usu_nombre' ),
		array( 'db' => 'prf.prf_descripcion', 'dt' => 'Perfil',   'field' => 'prf_descripcion' ),
		array( 'db' => 'u.usu_email',         'dt' => 'E-Mail',   'field' => 'usu_email' ),	
		array( 'db' => 'u.usu_usuario',       'dt' => 'Usuario',  'field' => 'usu_usuario' ),
		array( 'db' => 'u.usu_usu_actualiz',  'dt' => 'Usuarioa', 'field' => 'usu_usu_actualiz' ),
		array( 'db' => 'u.usu_fec_actualiz',  'dt' => 'Fecha',    'field' => 'usu_fec_actualiz' ),
		array('db'  => 'u.usu_color',         'dt' => 'Color',    'field' => 'usu_color'),	
	    array( 'db' => 'u.usu_telefono',      'dt' => 'Telefono', 'field' => 'usu_telefono' ),
	    array( 'db' => 'u.usu_fec_registro',  'dt' => 'Registro', 'field' => 'usu_fec_registro' ),
        array( 'db' => 'm.mer_nombre',        'dt' => 'Mercado',  'field' => 'mer_nombre' ),
        array( 'db' => "u.usu_clave_int",     'dt' => 'Ultimafc',  'field' => 'usu_clave_int','formatter'=>function($d, $row){
            global $dbconn;
            $con = pg_query($dbconn, "SELECT 
            pe.ped_fecha
            FROM  tbl_usuario AS  u 
            inner join tbl_perfil AS prf ON (prf.prf_clave_int = u.prf_clave_int) 
            join tbl_estados  AS es on es.est_clave_int = u.est_clave_int 
            left outer join tbl_mercado m on m.mer_clave_int = u.mer_clave_int
            join tbl_pedidos pe ON u.usu_clave_int = pe.usu_clave_int 
            where u.usu_clave_int = '".$d."'
            and pe.ped_fecha is not null
            ORDER BY pe.ped_fecha DESC
            LIMIT 1");
            $dat = pg_fetch_array($con);
            $ultgec = $dat['ped_fecha'];
            return $ultgec;
        }), 
        array( 'db' => "u.usu_clave_int",     'dt' => 'Numdias',  'field' => 'usu_clave_int','formatter'=>function($d, $row){
            global $dbconn;
            $con = pg_query($dbconn, "SELECT
            DATE_PART('day', current_date::timestamp - pe.ped_fecha::timestamp)as numdi 
            FROM  tbl_pedidos pe
            where pe.usu_clave_int = '".$d."'
            and pe.ped_fecha is not null
            ORDER BY pe.ped_fecha desc
            LIMIT 1");
            $dat = pg_fetch_array($con);
            $numdia = $dat['numdi'];
            return $numdia;
        }),
        array( 'db' => "u.usu_clave_int",     'dt' => 'Numdpedi',  'field' => 'usu_clave_int','formatter'=>function($d, $row){
            global $dbconn;
            $con = pg_query($dbconn, "SELECT
            count(*) as numpe
            FROM  tbl_pedidos pe
            where pe.usu_clave_int = '".$d."'
            and pe.ped_fecha is not null");
            $dat = pg_fetch_array($con);
            $numpedi = $dat['numpe'];
            return $numpedi;
        }),  
);

$sql_details = array(
	'user' => 'usrpavas',
	'pass' => '9A12)WHFy$2p4v4s',
	'db'   => 'bddelasiembra2',
	'port'   => '5432',
	'host' => '104.36.166.121'
);

require( '../../data/ssp.class.php' );
$whereAll = "";
$groupBy = '';
$with = '';

$joinQuery = "FROM  tbl_usuario AS  u 
inner join tbl_perfil AS prf ON (prf.prf_clave_int = u.prf_clave_int) 
join tbl_estados  AS es on es.est_clave_int = u.est_clave_int 
left outer join tbl_mercado m on m.mer_clave_int = u.mer_clave_int
join tbl_pedidos pe ON u.usu_clave_int = pe.usu_clave_int";

$extraWhere = "";
echo json_encode(SSP::simple($_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy,$with));
