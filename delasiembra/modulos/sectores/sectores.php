<?php
include('../../data/Conexion.php');
echo "<script>CRUDSECTOR('','LISTASECTORES','');</script>";
?>

<section class="content-header">
      <h1>
        Sectores
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Configuración</a></li>
        <li class="active">Sectores</li>        
      </ol>
    </section>
<section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
	       <div class="box">
            <div class="box-body">
                <div ng-controller="TabsDemoCtrl">
                  <uib-tabset>
                     <uib-tab heading="Departamentos" select="alertSector('LISTADEPARTAMENTOS')">
                     <div class="form-group">
                     <div class="col-md-2 col-xs-6">
                    <a class="btn btn-block btn-primary" onClick="CRUDSECTOR('DEPARTAMENTO','NUEVO','')" role="button" data-toggle="modal" data-target="#modalregistro"><i class="glyphicon glyphicon-plus"></i>Nuevo</a>
                    </div>
                    </div>

           
                    </uib-tab>
                     <uib-tab heading="Municipios" select="alertSector('LISTAMUNICIPIOS')">
                     <div class="form-group">
                     <div class="col-md-2 col-xs-6">
                    <a class="btn btn-block btn-primary" onClick="CRUDSECTOR('MUNICIPIO','NUEVO','')" role="button" data-toggle="modal" data-target="#modalregistro"><i class="glyphicon glyphicon-plus"></i>Nuevo</a>
                    </div>
                    </div>
                    
           
                    </uib-tab>
                    <uib-tab heading="Sectores" select="alertSector('LISTASECTORES')">
                     <div class="form-group">
                     <div class="col-md-2 col-xs-6">
                    <a class="btn btn-block btn-primary" onClick="CRUDSECTOR('SECTOR','NUEVO','')" role="button" data-toggle="modal" data-target="#modalregistro"><i class="glyphicon glyphicon-plus"></i>Nuevo</a>
                    </div>
                    </div>
           
                    </uib-tab>
                    <uib-tab heading="Barrios" select="alertSector('LISTABARRIOS')">
                    <div class="form-group">
                     <div class="col-md-2 col-xs-6">
                    <a class="btn btn-block btn-primary" onClick="CRUDSECTOR('BARRIO','NUEVO','')" role="button" data-toggle="modal" data-target="#modalregistro"><i class="glyphicon glyphicon-plus"></i>Nuevo</a>
                    </div>
                    </div>
                     
                    </uib-tab>
                     
                  </uib-tabset>

                </div>
        <!-- /.box-body -->
            </div>
        <!-- /.box -->
        </div>
        <!-- /.col -->
    </div>
  
</div> 
<div class="row">
    <div class="col-md-12" id="tabladatos">
        
    </div>
</div>
</section>
