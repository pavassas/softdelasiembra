<?php
setlocale(LC_ALL,"es_ES.utf8","es_ES","esp");
            
    $coninfo = pg_query($dbconn,"select u.usu_nombre,u.usu_apellido,u.usu_usuario,p.ped_fecha,u.usu_email,p.dir_clave_int,p.ped_domicilio,p.ped_telefono,ped_codigo,ped_tiempo_entrega,ped_nota,ped_fec_programada,ped_hor_programada from tbl_pedidos p JOIN tbl_usuario u ON u.usu_clave_int = p.usu_clave_int WHERE p.ped_clave_int = '".$idpedido."'");
    $datinfo = pg_fetch_array($coninfo);
    $cliente = $datinfo['usu_nombre']." ".$datinfo['usu_apellido'];
    $pedfecha = $datinfo['ped_fecha'];
    $email = $datinfo['usu_email'];
    $dir = $datinfo['dir_clave_int'];
    $domicilio = $datinfo['ped_domicilio'];
    $telefono = $datinfo['ped_telefono'];
    $codigoped = $datinfo['ped_codigo'];
    $tiempo = $datinfo['ped_tiempo_entrega']." min";
    $an = strftime("%Y",strtotime($pedfecha));
    $dia = strftime("%a",strtotime($pedfecha));
    $dia1 = strftime("%d",strtotime($pedfecha));
    $mes = strftime("%B",strtotime($pedfecha));
    $hi = strftime("%H:%M",strtotime($pedfecha));
    $pedfecha = $dia.", ".$dia1." de ".$mes." de ".$an;
    $pedfecha = $pedfecha . ", ".$hi;
    $nota = $datinfo['ped_nota'];

    $fecent = $datinfo['ped_fec_programada'];
    $horent = $datinfo['ped_hor_programada'];
    $an = strftime("%Y",strtotime($fecent));
    $dia = strftime("%a",strtotime($fecent));
    $dia1 = strftime("%d",strtotime($fecent));
    $mes = strftime("%B",strtotime($fecent));
    
    $fecent = $dia.", ".$dia1." de ".$mes." de ".$an;
    $fecent = $fecent . ", ".date("g:i a",strtotime($horent));




    $condir = pg_query($dbconn,"select s.sec_nombre,b.bar_nombre,s.sec_domicilio,d.dir_nomenclatura,d.dir_letra,d.dir_numero1,d.dir_numero2,d.dir_detalle,dir_real,dir_corta from tbl_direcciones d join tbl_barrio b on b.bar_clave_int = d.bar_clave_int join tbl_sector s on s.sec_clave_int = b.sec_clave_int where d.dir_clave_int = '".$dir."'");
    $datdir = pg_fetch_array($condir);
    $sector = $datdir['sec_nombre'];
    $barrio = $datdir['bar_nombre'];
    $nome = $datdir['dir_nomenclatura'];
    $letra = $datdir['dir_letra'];
    $nume1 = $datdir['dir_numero1'];
    $nume2 = $datdir['dir_numero2'];

    if($nome=="")
    {
        $direccion = $datdir['dir_corta'];
    }
    else
    {
        $direccion = $nome." ".$letra."#".$nume1."-".$nume2;
    }
    //$direccion = $nome." ".$letra."#".$nume1."-".$nume2;
    $dirdetalle = $datdir['dir_detalle'];
     

    $condet = pg_query($dbconn, "SELECT d.pde_clave_int,p.pro_clave_int,p.pro_nombre,p.pro_descripcionbreve,p.pro_codigo,c.cla_nombre,pde_estado,pde_tamano,cla_nombre,pde_cantidad,pde_valor,pde_unidad,pro_uni_venta,pro_pes_venta,pro_mu_venta,pro_mp_venta,pde_minimo,pde_unidad ,pde_codigo from tbl_pedidos_detalle d JOIN tbl_productos p on p.pro_clave_int = d.pro_clave_int join tbl_clasificacion c on c.cla_clave_int = d.cla_clave_int where d.ped_clave_int = '".$idpedido."' and pde_confirmado = 2 order by p.pro_nombre,c.cla_nombre");
    $numdet = pg_num_rows($condet);

?>

<style type="text/css">
<!--
table { vertical-align: top; }
tr    { vertical-align: top; }
td    { vertical-align: top; }
-->
</style>
<page backcolor="#FEFEFE" backimg="../../../dist/img/plantillas/hojaA4N.jpg" backimgx="left" backimgy="top" backimgw="100%" backtop="0" backbottom="30mm" footer="date;time;page" style="font-size: 14px">
    <bookmark title="Orden de Pedido" level="0" ></bookmark>
     <table style="width:100%;border-collapse:collapse; border-width:1px" border="0" cellpadding="2" cellspacing="2">
       <tr>
        <td>
        <table cellspacing="0" style="width: 100%; text-align: center; font-size: 20px">
            <tr>
                <td style="width: 100%; text-align: left; font-weight: bold;"><strong>Tu Pedido</strong></td>
               
            </tr>
        </table>
        <br>
        <br>
        <table cellspacing="0" style="width: 100%; text-align: left; font-size: 11pt;">
        <tr>
            <td style="width:25%;">Cliente :</td>
            <td style="width:75%;"><?php echo $cliente;?></td>  
        </tr>
        <tr> 
            <td style="width:25%; ">Dirección :</td>
            <td style="width:75%;"> 
                <?php echo $direccion;?><br>
                <?php echo $dirdetalle;?><br>
                <?php echo $sector;?> -<?php echo $barrio;?><br>
            </td>
           
            
        </tr>
        <tr>
            <td style="width:25%; ">Email :</td>
            <td style="width:75%;"><?php echo $email;?></td>           
        </tr>
        <tr>
            <td style="width:25%;">Fecha Pedido:</td>
            <td style="width:75%;"><?php echo $pedfecha;?></td>  
        </tr>
        <tr>
            <td style="width:25%;">Fecha Entrega:</td>
            <td style="width:75%;"><?php echo $fecent;?></td>  
        </tr>
        <tr>
            <td style="width:25%;">N° de Pedido:</td>
            <td style="width:75%;"><?php echo $codigoped;?></td>  
        </tr>
    </table>
    <br>    
    <br>   
    <table cellspacing="2" cellpadding="2" style="width: 100%; border: solid 0px black; background: #F7F7F7; font-size: 10pt;">
        <thead>
       
       <tr><th colspan="5" style="padding: 5px;background-color: #8BC34A; color:#FFF;font-size: 14px;">Detalles del pedido</th></tr>
        
        <tr>

        <th style="background-color: #cfe2ba; text-align: center;padding: 3px">Codigo</th>
        <th style="background-color: #cfe2ba;text-align: center;padding: 3px">Producto</th>
        <th style="background-color: #cfe2ba;text-align: center;padding: 3px">Precio Unidad</th>
        <th style="background-color: #cfe2ba;text-align: center;padding: 3px">Cantidad</th>
        <th style="background-color: #cfe2ba;text-align: center;padding: 3px">Precio Neto</th>
        </tr>
    </thead>
        <tbody>
        <?php
            $total = 0;
            while($datdet= pg_fetch_array($condet))
            {
                $iddetalle = $datdet['pde_clave_int'];
                $idp = $datdet['pro_clave_int'];
                $codlinea = $datdet['pde_codigo'];
                $nompro = $datdet['pro_nombre'];
                $codpro = $datdet['pro_codigo'];
                $nomcla = $datdet['cla_nombre'];
                $maduracion = $datdet['pde_estado'];
                $descp = $datdet['pro_descripcionbreve'];
                $precio = $datdet['pde_valor'];
                $cantidad = $datdet['pde_cantidad'];
                $subtotal = $precio * $cantidad;
                $tamano = $datdet['pde_tamano'];

                $observacion = "<strong>Clasificación:</strong>".$nomcla." <br>";
                if($tamano!=""){$observacion.="<strong>Tamano:</strong>".$tamano."<br> ";}
                if($maduracion!=""){$observacion.="<strong>Maduración:</strong>".$maduracion." ";}
                if($tamano!=""){ $tamano = '-'.$tamano;}
                
                $puv = $datdet['pro_uni_venta'];
                $ppv = $datdet['pro_pes_venta'];
                $pmuv = $datdet['pro_mu_venta'];
                $pmpv = $datdet['pro_mp_venta'];
                $min = $datdet['pde_minimo'];
                $uni = $datdet['pde_unidad'];

                if($uni==0)
                {
                    $unimin = $pmuv ;
                    $cantidad = $cantidad;
                    $vis = $cantidad." UND";
                }
                else
                {
                    $unimin = $pmpv;
                    $cantidad = $cantidad * $min;//esto debido a que el precio del producto cuando es en peso es precio por la unidad minima ejemplo:250gr = 1 unidad
                    if($cantidad>=1000)
                    {
                        $vis = $cantidad/1000 ." KG";
                    }
                    else 
                    {
                        $vis = $cantidad." GR";
                    }
                }
                
                
                $con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idp."' and pri_activo = 1 LIMIT 1");
                $dat = pg_fetch_row($con);
                $cimg = $dat[0];
                $img = $dat[1];
                ?>

                <tr>
                <td><?php echo $codpro;?></td>
                <td><?php echo $nompro."(".$maduracion.$tamano.")";?></td>
                <td style="text-align: right;">$<?php echo number_format($precio,0,',',',');?></td>
                <td style="text-align: center;"><?php echo $vis;?></td>
                <td style="text-align: right;">$<?php echo number_format($subtotal,0,',',',');?></td>
                </tr>
                <tr><td colspan="5" style="border-color:#636; border-style:solid; border-top: solid; text-align:right;  border-width:1px 1px 0px 1px;"></td></tr>
            <?php
                $total = $total + $subtotal;
            }
            $totalpedido = $total + $domicilio;
            ?>

        <tr>        
        <td colspan="4" style="background-color: #d1cece;text-align: right;">Subtotal</td>     
        <td style="background-color: #d1cece;text-align: right;">$<?php echo number_format($total,0,',',',');?></td>
        </tr>
        <tr>        
        <td colspan="4" style="background-color: #d1cece;text-align: right;">Domicilio</td>    
        <td style="background-color: #d1cece;text-align: right;">$<?php echo number_format($domicilio,0,',',',');?></td>
        </tr>
        <tr>
        
        <td colspan="4" style="background-color: #a3c977; color:#FFF;font-size: 16px; font-weight: bold; text-align: right;">TOTAL</td>     
        <td style="background-color: #a3c977; color:#FFF;font-size: 16px; font-weight: bold; text-align: right;">$<?php echo number_format($totalpedido,0,',',',');?></td>
        </tr>
        
        </tbody>    
    </table>
    </td>
</tr>
</table>
     <page_footer>
                        <table border="0" style="width:650px">
                            <tr>
                                <td style="width:60%">&nbsp;</td>
                                <td style="width:40%">&nbsp;</td>  
                            </tr>                        

                            <tr>
                                <td style="width:60%;  font-weight:bold; font-size:11px">Firma de Aceptación cliente</td>
                                <td style="width:40%;  font-weight:bold; font-size:11px"></td>
                            </tr>
                           
                            <tr style="height:12px">
                                <td style="width:60%">&nbsp;</td>
                                <td style="width:40%">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2" style="width:60%;font-weight:bold; font-size:11px">
                                    <p>

                                       <!-- EL CLIENTE DEBE REVISAR EL ESTADO DE SU PEDIDO
                                        DEL EQUIPO ANTES DE FIRMAR ESTE INFORME.-->
                                    </p></td>

                            </tr>

                            <tr>
                                <td colspan="2"  style="text-align:center; font-weight:bold;  font-size:12px; border-width:1px">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="2"  style="text-align:center; font-weight:bold;  font-size:12px; border-width:1px"><em>DELASIEMBRA.COM S.A.S / Dir. Cre 58 N°77 – 41, interior 1051,Itagüí/ Cel: 301 370 88 56</em></td>
                            </tr>
                        </table>
                    </page_footer>
</page>