<?php
 include('../../data/Conexion.php');
 session_start();
 error_reporting(0);
 use  PHPMailer\PHPMailer\PHPMailer;
 use  PHPMailer\PHPMailer\Exception;
 require ('../../PHPMailer-master/src/PHPMailer.php');
 require ('../../PHPMailer-master/src/Exception.php');
 //require ('../../PHPMailer-master/src/SMTP.php');
 require_once('../../clases/pdf/html2pdf.class.php');
 //variable login que almacena el login o nombre de usuario de la persona logueada
 $login = isset($_SESSION['persona']);
 //cookie que almacena el numero de identificacion de la persona logueada
 $usuario = $_COOKIE['usuario'];
 $idUsuario = $_SESSION["idusuario"];
 $identificacion = $_COOKIE["usIdentificacion"];
 $idsession =$_SESSION["idusuario"];
 date_default_timezone_set('America/Bogota');
 $fecha = date("Y/m/d H:i:s");

 $ApiKey = "4Vj8eK4rloUd272L48hsrarnUA";
 $merchant_id = $_POST['merchant_id'];
 $reference_sale = $_POST['reference_sale'];//codigo del pedido
 $value = $_POST['value'];
 $New_value = number_format($value, 1, '.', '');
 $currency = $_POST['currency'];
 $state_pol = $_POST['state_pol'];
 $reference_pol = $_POST['reference_pol'];
 $cus = $_POST['cus'];
 $extra1 = $_POST['extra1'];
 $pse_bank = $_POST['pse_bank'];
 $transaction_id = $_POST['transaction_id'];
 $tipopago = $_POST['payment_method'];
 $res = "error";
 $conped = pg_query($dbconn, "SELECT ped_clave_int,ped_codigo FROM tbl_pedidos WHERE ped_codigo = '".$reference_sale."'");
 $numped = pg_num_rows($conped);
 $datped = pg_fetch_array($conped);
 $idpedido = $datped['ped_clave_int'];

 if ($_REQUEST['state_pol'] == 4 ) {
   $estadoTx = "Transacción aprobada";
    $res = "ok";
   $sqlmodi = pg_query($dbconn,"UPDATE tbl_pedidos SET ped_estado='1',ped_fec_actualiz='".$fecha."',ped_transaccion='4',ped_orderid = '".$reference_pol."' WHERE ped_clave_int = '".$idpedido."'");
   //insertar novedad del pedido
   $insertnov = pg_query($dbconn, "INSERT INTO tbl_pedidos_novedad(ped_clave_int,pen_estado,pen_descripcion,pen_fecha) VALUES('".$idpedido."','4','".$estadoTx."','".$fecha."')");
 }

 else if ($_REQUEST['state_pol'] == 6 ) {
   $estadoTx = "Transacción rechazada";   
   $sqlmodi = pg_query($dbconn,"UPDATE tbl_pedidos SET ped_estado='0',ped_fec_actualiz='".$fecha."',ped_transaccion='6',ped_orderid = '".$reference_pol."' WHERE ped_clave_int = '".$idpedido."'");
    //insertar novedad del pedido
    $insertnov = pg_query($dbconn, "INSERT INTO tbl_pedidos_novedad(ped_clave_int,pen_estado,pen_descripcion,pen_fecha) VALUES('".$idpedido."','6','".$estadoTx."','".$fecha."')");
 }
 else if ($_REQUEST['state_pol'] == 104 ) {
   $estadoTx = "Error";
   $sqlmodi = pg_query($dbconn,"UPDATE tbl_pedidos SET ,ped_estado='0',ped_fec_actualiz='".$fecha."',ped_transaccion='104' WHERE ped_clave_int = '".$idpedido."'");
       //insertar novedad del pedido
    $insertnov = pg_query($dbconn, "INSERT INTO tbl_pedidos_novedad(ped_clave_int,pen_estado,pen_descripcion,pen_fecha) VALUES('".$idpedido."','104','".$estadoTx."','".$fecha."')");
 }
 else if ($_REQUEST['state_pol'] == 7 ) {
   $estadoTx = "Transacción pendiente";
   $res = "ok";
   $sqlmodi = pg_query($dbconn,"UPDATE tbl_pedidos SET ped_estado='1',ped_fec_actualiz='".$fecha."',ped_transaccion='7',ped_orderid = '".$reference_pol."' WHERE ped_clave_int = '".$idpedido."'");
    //insertar novedad del pedido
    $insertnov = pg_query($dbconn, "INSERT INTO tbl_pedidos_novedad(ped_clave_int,pen_estado,pen_descripcion,pen_fecha) VALUES('".$idpedido."','7','".$estadoTx."','".$fecha."')");
 }
 else {
     
   $estadoTx=$_REQUEST['response_message_pol'];
   $sqlmodi = pg_query($dbconn,"UPDATE tbl_pedidos SET ped_estado='0',ped_fec_actualiz='".$fecha."',ped_transaccion='' WHERE ped_clave_int = '".$idpedido."'");
   //insertar novedad del pedido
   $insertnov = pg_query($dbconn, "INSERT INTO tbl_pedidos_novedad(ped_clave_int,pen_estado,pen_descripcion,pen_fecha) VALUES('".$idpedido."','0','".$estadoTx."','".$fecha."')");
 }  

 if($res=="ok"){

     
        //seleccionamos los domiciliarios a que apliquen al sector del pedido informandoles que se genero un nuevo pedido
        //detalle del pedido
        ob_start();
        include('detallepedido.php');		
        $content = ob_get_clean();
        $asunto1 = "Nuevo Pedido delasiembra";
        //enviar correo a los administradores de un nuevo pedido
        //
        $envios = 0;
        $msn = "";
        $conu = pg_query($dbconn, "SELECT usu_usuario,usu_email,usu_nombre,usu_apellido from tbl_usuario where prf_clave_int = 1 and est_clave_int = 1");
        $numu = pg_num_rows($conu);
        if($numu>0)
        {
            for($n=0;$n<$numu;$n++)
            {
                $datu = pg_fetch_array($conu);
                $usu = $datu['usu_usuario'];
                $ema = $datu['usu_email'];
                $nom = $datu['usu_nombre']." ".$datu['usu_apellido'];
                $mail = new PHPMailer(true);
                $mail->isHTML(true);  // Set email format to HTML
                $mail->CharSet = "UTF-8";
                //ENVIOS LOCALES
                
                //$mail->isSMTP();
                //$mail->SMTPAuth = true;
                //$mail->SMTPDebug   = 1;
                //$mail->SMTPSecure = "tls";
                //$mail->Host = "smtp.gmail.com";
                //$mail->Port = 587;
                //$mail->SMTPOptions = array(
                //    'ssl' => array(
                //        'verify_peer' => false,
                //        'verify_peer_name' => false,
                //        'allow_self_signed' => true
                //    )
                //);
                
                    //Nuestra cuenta
                //$mail->Username ='andres.199207@gmail.com';
                //$mail->Password = 'Bayron.1214'; //Su password

                //$mail->From = "adminpavas@pavas.com.co";
                // Establecer de quién se va a enviar el mensaje
                //$mail->setFrom("andres.199207@gmail.com", "delasiembra.com");


                $mail->SetFrom("admin@delasiembra.com", "DELASIEMBRA.COM");
                //Usamos el AddReplyTo para decirle al script a quien tiene que responder el correo
                $mail->AddReplyTo("admin@delasiembra.com","DELASIEMBRA.COM");

                //Usamos el AddAddress para agregar un destinatario
                $mail->addAddress($ema, "Usuario: " . $usu);					
                    // Establecer a quién se enviará el mensaje
                
                $mail->Subject = utf8_decode($asunto1);
                $mail->msgHTML($content);
                if (!$mail->send())
                {
                    $insertnov = pg_query($dbconn, "INSERT INTO tbl_pedidos_novedad(ped_clave_int,pen_estado,pen_descripcion,pen_fecha) VALUES('".$idpedido."','0','No se envio correo a administrador','".$fecha."')");
                    //    $msn.= 'No se envio mensaje al siguiente email<strong>(' . $ema . ')</strong>' . $mail->ErrorInfo . '<br>';
                }
                else
                {
                    $insertnov = pg_query($dbconn, "INSERT INTO tbl_pedidos_novedad(ped_clave_int,pen_estado,pen_descripcion,pen_fecha) VALUES('".$idpedido."','0','Se envio correo a administrador ".$ema."','".$fecha."')");
                    $envios++;
                }
                //$mail->AltBody = $sincontenido1;// cuando no admite html
            }
            if($envios>0)
            {
               
            } 
           
            //$msn.= "Hemos enviado tu pedido a DELASIEMBRA.COM y en breve estaremos programando tu entrega.";
        } 
        else
        {
            //$res = "error";
            $insertnov = pg_query($dbconn, "INSERT INTO tbl_pedidos_novedad(ped_clave_int,pen_estado,pen_descripcion,pen_fecha) VALUES('".$idpedido."','0','No hay administradores para envio confirmación de nuevos pedidos','".$fecha."')");
            //$msn = "No hay administradores para envio confirmación de nuevos pedidos";
        } 
 }

