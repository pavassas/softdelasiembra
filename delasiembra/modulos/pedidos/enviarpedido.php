<?php
include('../../data/Conexion.php');
session_start();
error_reporting(0);
$IP = $_SERVER['REMOTE_ADDR'];
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_SESSION["idusuario"];
//$clave= $_COOKIE["clave"];
$identificacion = $_COOKIE["usIdentificacion"];
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");

$conusu= pg_query($dbconn,"SELECT mer_clave_int,dir_clave_int,usu_ult_telefono,usu_telefono from tbl_usuario where usu_clave_int = '".$idUsuario."'");
$datusu = pg_fetch_array($conusu);
$idmercado = $datusu['mer_clave_int'];
$ultimadireccion = $datusu['dir_clave_int'];
$ultimotelefono = $datusu['usu_ult_telefono'];
$telefono = $datusu['usu_telefono'];
if($ultimotelefono==""){
	$ultimotelefono = $telefono;
}
$condom = pg_query($dbconn,"select sec_domicilio,sec_horas,sec_minutos,sec_monto from tbl_sector s join tbl_barrio b on b.sec_clave_int = s.sec_clave_int join tbl_direcciones d on d.bar_clave_int = b.bar_clave_int where d.usu_clave_int = '".$idUsuario."' and d.dir_clave_int = '".$ultimadireccion."'");
$datdom = pg_fetch_array($condom);
$domicilio = $datdom['sec_domicilio'];
//$monto = $datdom['sec_monto'];

$conr = pg_query($dbconn, "SELECT reg_monto,to_char(reg_horaped,'HH24:MI:SS') hp FROM tbl_reglas LIMIT 1");
$datr = pg_fetch_array($conr);
$monto = $datr['reg_monto'];
$hormaxped = $datr['hp'];
$fecmaxped = date("Y/m/d ".$hormaxped);
$nunmax = 0;
if(strtotime($fecha)>strtotime($fecmaxped)){ $nunmax = 1; }//fecha y hora es mayor a la fecha y hora permitida para hacer pedidos desde la plataforma

$minutos = 0;
$horas = $datdom['sec_horas']; if($horas>0){ $minutos = $horas*60; }
$min = $datdom['sec_horas'];
$tiempoentrega = $minutos + $min;
if($tiempoentrega<=0)
{
	$tiempoentrega = 60;
}
$idpedido = $_GET['idpedido'];
$lista = $_GET['lista'];
$nombrelista = $_GET['nombrelista'];

$fecent = $_POST['fechaentrega'];
$horent = $_POST['horaentrega'];
$ruta = $_POST['rutarecibo'];

$trozos = explode(".", $ruta); 
$rutaold = "../../".$ruta;
$extension = end($trozos);

  use  PHPMailer\PHPMailer\PHPMailer;
  use  PHPMailer\PHPMailer\Exception;
  require ('../../PHPMailer-master/src/PHPMailer.php');
  require ('../../PHPMailer-master/src/Exception.php');
  //require ('../../PHPMailer-master/src/SMTP.php');
  require_once('../../clases/pdf/html2pdf.class.php');

//actualizar estado del pedido con el costo del domicilio

  //SELECCIONAR LAS FECHAS A OMITIT

  $confechas = pg_query($dbconn, "SELECT ref_fecha FROM tbl_reglas_fechas where ref_fecha>='".$fecent."'");
  $numfechas = pg_num_rows($confechas);
  $fechas[] = array();
  if($numfechas>0)
  {
  	for($f=0;$f<$numfechas;$f++)
  	{
  		$datf = pg_fetch_array($confechas);
		$fech = $datf['ref_fecha'];
		  
  		$fechas[$f] = $fech;
  	}
  }

		$notapedido = nl2br($_POST['notapedido']);

		//validar la hora y la fecha en la misma hora

		$validarhora = pg_query($dbconn, "SELECT ped_clave_int FROM tbl_pedidos WHERE ped_fec_programada = '".$fecent."' and ped_hor_programada = '".$horent."' and ped_clave_int != '".$idpedido."' and ped_estado not in (0,4)  ");
		$numhora = pg_num_rows($validarhora);


		$concan = pg_query($dbconn, "select COUNT(pde_clave_int) cant,sum(pde_cantidad*pde_valor) tot from tbl_pedidos_detalle where ped_clave_int = '".$idpedido."'");
		$datcan = pg_fetch_row($concan); $can = $datcan[0]; $tot = $datcan[1];

		$limmonto  = $monto - $tot;
		if($tot>=$monto and $monto>0)
		{
			$domicilio = 0;
			$titdom = "<strong>¡A buena hora!</strong>. Tu costo de envío es <strong>GRATIS</strong>";
		}
		else
		{
			$titdom = "<strong>Faltan $ ".number_format($limmonto,0,',',',')."</strong> y tu costo de envío será <strong>GRATIS</strong>";
		}

		$veri = pg_query($dbconn, "SELECT * FROM tbl_pedidos_detalle WHERE ped_clave_int = '".$idpedido."'");
		$numpro = pg_num_rows($veri);
		if($numpro<=0)
		{
			$res = "error";
			$msn = "Tu carrito de compras esta vacio!";
		}
		else if($ultimadireccion<=0)
		{
			$res = "error1";
			$msn = "Indica una dirección para permitirnos ubicarte";
		}
		else if($ultimotelefono=="")
		{
			$res = "error";
			$msn = "Indica un telefono donde comunicarnos contigo";
		}
		else if(strtotime($fecent)==strtotime(date('Y-m-d')))
		{
			$res = "error";
			$msn = "No es posible programar pedidos para el dia de hoy";
		}
		else if(strtotime($fecent)<strtotime(date('Y-m-d')))
		{
			$res = "error";
			$msn = "No es posible programar pedidos para fechas menores a la fecha actual";
		}
		else if($numfechas>0 and in_array($fecent, $fechas))
		{
			$res = "error2";
			$msn = "<h4>Estimado cliente, delasiembra.com le informa que en los siguientes días no se realizaran despachos:</h4><ul>";
			for($k=0;$k<$numfechas;$k++)
			{
				$msn.="<li>".$fechas[$k]."</li>";

			}
			$msn.="</ul><br>Cualquier inquietud comunicarse al +57 301 370 88 56";
		}
		else if($nunmax>0){
			$res = "error";
			$msn = "Haz excedido la hora máxima para realizar pedidos. Hora Max para realizar pedidos: ".$hormaxped;
		}
		else if ($numhora > 0 )
		{ 
			$res = "error" ; 
			$msn = "No es posible programar su pedido para la hora seleccionada. Por favor seleccionar otra hora dentro del listado disponible";
		}
	  	else
	  	{
			$codpedido = sprintf('%010d',$idpedido);
	  		$updatepedido = pg_query($dbconn, "UPDATE tbl_pedidos SET ped_lista_deseo = '".$lista."',ped_estado = '1',dir_clave_int= '".$ultimadireccion."',ped_telefono='".$ultimotelefono."',ped_ip='".$IP."',ped_domicilio='".$domicilio."',ped_fecha = '".$fecha."',ped_tiempo_entrega = '".$tiempoentrega."',ped_lista_nombre = '".$nombrelista."',ped_nota = '".$notapedido."',ped_fec_programada = '".$fecent."',ped_hor_programada = '".$horent."',ped_transaccion = 5 where ped_clave_int='".$idpedido."'");
	  	if($updatepedido>0)
	  	{
			$time = time();
			$rutan = "../../modulos/pedidos/recibos/".$idpedido."-".$time.".".$extension;
				
			if(rename($rutaold,$rutan))
			{
				$rutanew = "modulos/pedidos/recibos/".$idpedido."-".$time.".".$extension;
				$update = pg_query($dbconn,"update tbl_pedidos set ped_recibo='".$rutanew."' where ped_clave_int='".$idpedido."'");		
			}

			$insertnov = pg_query($dbconn, "INSERT INTO tbl_pedidos_novedad(ped_clave_int,pen_estado,pen_descripcion,pen_fecha) VALUES('".$idpedido."','5','Pago por transferencia sucursal virtual','".$fecha."')");
	  	//seleccionamos los domiciliarios a que apliquen al sector del pedido informandoles que se genero un nuevo pedido
	  		//detalle del pedido
			ob_start();
			include('detallepedido.php');		
			$content = ob_get_clean();
			$asunto1 = "Nuevo Pedido delasiembra";
			//enviar correo a los administradores de un nuevo pedido
			//
			$envios = 0;
			$msn = "";
			$conu = pg_query($dbconn, "SELECT usu_usuario,usu_email,usu_nombre,usu_apellido from tbl_usuario where prf_clave_int = 1 and est_clave_int = 1");
			$numu = pg_num_rows($conu);
			if($numu>0)
			{
				for($n=0;$n<$numu;$n++)
				{
					$datu = pg_fetch_array($conu);
					$usu = $datu['usu_usuario'];
					$ema = $datu['usu_email'];
					$nom = $datu['usu_nombre']." ".$datu['usu_apellido'];
			 		$mail = new PHPMailer(true);
			 		$mail->isHTML(true);  // Set email format to HTML
					$mail->CharSet = "UTF-8";
			 		//ENVIOS LOCALES
	                /*$mail->isSMTP();
	                $mail->SMTPAuth = true;
	                $maul->SMTPDebug   = 1;
	                $mail->SMTPSecure = "tls";
	                $mail->Host = "smtp.gmail.com";
	                $mail->Port = 587;
	                $mail->SMTPOptions = array(
	                    'ssl' => array(
	                        'verify_peer' => false,
	                        'verify_peer_name' => false,
	                        'allow_self_signed' => true
	                    )
	                );*/
	                 //Nuestra cuenta
					//$mail->Username ='andres.199207@gmail.com';
					//$mail->Password = 'Bayron.1214'; //Su password

					//$mail->From = "adminpavas@pavas.com.co";
					// Establecer de quién se va a enviar el mensaje
					//$mail->setFrom("andres.199207@gmail.com", "delasiembra.com");


					$mail->SetFrom("admin@delasiembra.com", "DELASIEMBRA.COM");
					//Usamos el AddReplyTo para decirle al script a quien tiene que responder el correo
					$mail->AddReplyTo("admin@delasiembra.com","DELASIEMBRA.COM");

					//Usamos el AddAddress para agregar un destinatario
					$mail->addAddress($ema, "Usuario: " . $usu);					
					 // Establecer a quién se enviará el mensaje
	                
	                $mail->Subject = utf8_decode($asunto1);
					$mail->msgHTML($content);
					
					$array_nombre = explode('.',$rutanew);
					$cuenta_arr_nombre = count($array_nombre);
					$extension = strtolower($array_nombre[--$cuenta_arr_nombre]);
					if($rutanew!="" and $rutanew!=NULL)
					{
						$url = 'https://www.delasiembra.com/'.$rutanew;
						$fichero = file_get_contents($url);
						$mail->addStringAttachment($fichero, 'Recibo pago pedido'.$codpedido.'.'.$extension);
					}
	                if (!$mail->send())
	                {
	                	 $msn.= 'No se envio mensaje al siguiente email<strong>(' . $ema . ')</strong>' . $mail->ErrorInfo . '<br>';
	                }
	                else
	                {
	                	$envios++;
	                }
	                //$mail->AltBody = $sincontenido1;// cuando no admite html
				}
				if($envios>0)
				{
					$res = "ok";
					$msn.= "Hemos enviado tu pedido a DELASIEMBRA.COM y en breve estaremos programando tu entrega.";
				}
			} 
			else
			{
				$res = "error";
				$msn = "No hay administradores para envio confirmación de nuevos pedidos";
			}
	  	}
	  	else
	  	{
	  		$res = "error";
	  		$msn.= "El pedido no se ha enviado.";
	  	}
	}

  	$datos[] = array("res"=>$res,"msn"=>$msn,"idpedido"=>$idpedido,"nunmax"=>$nunmax);
  	echo json_encode($datos);