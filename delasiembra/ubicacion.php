<html>

<head>


  <!--[if lte IE 8]>
  <link rel="stylesheet" href="leaflet.ie.css" type="text/css" >
  <![endif]-->
 <link rel="stylesheet" href="https://unpkg.com/leaflet@1.2.0/dist/leaflet.css" />
  <link rel="stylesheet" href="https://unpkg.com/leaflet-routing-machine@latest/dist/leaflet-routing-machine.css" />


  <script src="ubicacion/leaflet.js"></script>
  <script src="ubicacion/L.GeoSearch-master/src/js/l.control.geosearch.js?<?php echo time();?>"></script>
  <link rel="stylesheet" href="ubicacion/l.geosearch.css" />
  <script src="ubicacion/L.GeoSearch-master/src/js/l.geosearch.provider.openstreetmap.js"></script>
  <script src="ubicacion/L.GeoSearch-master/src/js/l.geosearch.provider.google.js"></script>
  <!--<script src="l.geosearch.provider.gisfile.js"></script> -->

</head>

<body>

  <div id="coords">
    <div id="map"></div>

    <input type="text" id="latitud" />
    <input type="text" id="longitud" />
</div>

  <script type="text/javascript">

   /* var map = L.map('map').setView([51.505, -0.09], 13);

    L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', { attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors' }).addTo(map);

    new L.Control.GeoSearch({
      country: 'COL',
      searchLabel: 'Buscar Direccion',
      notFoundMessage: 'Not found',
      position: 'topcenter',
      showMarker: true,
      selected: 0,
      providers: [
      {name:'Google', provider: new L.GeoSearch.Provider.Google()},
      //{name:'OpenStreetMap', provider: new L.GeoSearch.Provider.OpenStreetMap()}
      ]
    }).addTo(map);
*/
    window.onload = function(){
    localizar();
};

function localizar() {
    if (window.navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(mapa, error);
    } else {
        return;
    }
}

function mapa(posicion) {

    var latitud = posicion.coords.latitude;
    var longitud = posicion.coords.longitude;

    var contenedor = document.getElementById("map");

    var centro = new google.maps.LatLng(latitud, longitud);

    var options = {
        center: centro,
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
    };

    var map = new google.maps.Map(contenedor, options);

    //Marcador
    var mkr1 = new google.maps.Marker({
        draggable: true,
        position: centro,
        map: map,

    });

    var info = new google.maps.InfoWindow({
        content: "Mueve este marcador hasta donde se encuentre tu negocio."

        //content:"<video src='http://v2v.cc/~j/theora_testsuite/320x240.ogg' controls>Tu navegador no implementa el elemento <code>video</code></video>"

        //content:"<video controls width='300' height='200'><source src='VIDEO1_INTRODUCCION.mp4'type='video/mp4'>Tu navegador no implementa el elemento <code>video</code></video>"
    });

    //agregamos un evento al marcador junto con la funcion callback al igual que el evento dragend que indica 
    //cuando el usuario a soltado el marcador
    mkr1.addListener('click', function () {
        info.open(map, mkr1);
    });

    google.maps.event.addListenerOnce(map, 'tilesloaded', function () {
        info.open(map, mkr1);
    });

    mkr1.addListener('dragend', function (event) {
        //escribimos las coordenadas de la posicion actual del marcador dentro del input #coords

        document.getElementById("latitud").value = this.getPosition().lat();

        document.getElementById("longitud").value = this.getPosition().lng();

        //marker.setAnimation(google.maps.Animation.BOUNCE)
    });

}

function error(errorC) {
    if (errorC.code == 0)
        alert("Error Desconocido");
    else if (errorC.code == 1)
        alert("No me dejaste ubicarte :(");
    else if (errorC.code == 2)
        alert("No hay una ubicacion disponible");
    else if (errorC.code == 3)
        alert("Tiempo agotado");
    else
        alert("Error Desconocido");
}

  </script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?key=AIzaSyCLQTtkGTPtDiq1fAEa7A6HEKj4B_sTekM"></script>
</body>

</html>

