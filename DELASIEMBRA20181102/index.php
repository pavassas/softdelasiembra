﻿<?php
include('data/Conexion.php');
session_start();
error_reporting(0);
$IP = $_SERVER['REMOTE_ADDR'];
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_SESSION["idusuario"];
$clave= $_COOKIE["clave"];
$identificacion = $_COOKIE["usIdentificacion"];
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
$conusu= pg_query($dbconn,"SELECT mer_clave_int,dir_clave_int,prf_clave_int,usu_imagen from tbl_usuario where usu_clave_int = '".$idUsuario."'");
$datusu = pg_fetch_array($conusu);
$idmercado = $datusu['mer_clave_int'];
$ultimadireccion = $datusu['dir_clave_int'];
$perfil = $datusu['prf_clave_int'];
$imgperfil = $datusu['usu_imagen'];
if($imgperfil=="" || $imgperfil==NULL)
{
  $imgperfil = "dist/img/default-user.png";
}

if($idUsuario<=0)
{
  $idmercado = 3;
}
$conped = pg_query($dbconn, "SELECT ped_clave_int FROM tbl_pedidos WHERE usu_clave_int = '".$idUsuario."' and ped_estado = 0");
$numped = pg_num_rows($conped);
$datped = pg_fetch_array($conped);
$idpedido = $datped['ped_clave_int'];
?>
<!DOCTYPE html>
<html>
<head> 
 
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="dist/img/favicon.ico?<?php echo time();?>" rel="shortcut icon"> 
  <title>DELASIEMBRA.COM | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <meta name="title" content="delasiembra.co">

  <meta name="description" content="Sistema de venta y control de inventario">

  <meta name="keyword" content="Frutas,Verduras">
    <!-- Chrome, Firefox OS and Opera -->
 
  
  <meta name="theme-color" content="#4285f4">
  <!-- Windows Phone -->
  <meta name="msapplication-navbutton-color" content="#4285f4">
  <!-- iOS Safari -->
  <meta name="apple-mobile-web-app-status-bar-style" content="#4285f4">

  <script type="text/javascript" src="dist/cdn/jquery.min.js"></script>
  <script src="dist/cdn/angular.min.js"></script>
  <script src="dist/js/angular-ui-router.js"></script> 
  <script src="dist/js/ui-bootstrap-tpls-0.14.3.min.js"></script>
  <script src="dist/js/ocLazyLoad.js"></script>  
  <script src="dist/js/rutas.js?<?php echo time(); ?>"></script>
 
  <!--=====================================
  Marcado de Open Graph FACEBOOK
  ======================================-->
<!--
  <meta property="og:title"   content="<?php echo $cabeceras['titulo'];?>">
  <meta property="og:url" content="<?php echo $url.$cabeceras['ruta'];?>">
  <meta property="og:description" content="<?php echo $cabeceras['descripcion'];?>">
  <meta property="og:image"  content="<?php echo $servidor.$cabeceras['portada'];?>">
  <meta property="og:type"  content="website">  
  <meta property="og:site_name" content="Tu logo">
  <meta property="og:locale" content="es_CO">-->
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.css?<?php echo time();?>">
   <link rel="stylesheet" href="bootstrap/css/bootstrapds.css?<?php echo time();?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="dist/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.css?<?php echo time();?>">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.css?<?php echo time();?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css?<?php echo time();?>">
  <!-- Morris chart -->
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css?<?php echo time();?>">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker-bs3.css?<?php echo time();?>">

   <link rel="stylesheet" href="plugins/timepicker/bootstrap-timepicker.css?<?php echo time();?>">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">


  <link rel="stylesheet" type="text/css" href="dist/DataTables-1.10.10/media/css/dataTables.bootstrap.css?<?php echo time(); ?>">
  <link rel="stylesheet" type="text/css" href="dist/DataTables-1.10.10/extensions/Select/css/select.dataTables.css?<?php echo time();?>">
  <link rel="stylesheet" type="text/css" href="dist/DataTables-1.10.10/extensions/Responsive/css/responsive.dataTables.css?<?php echo time();?>">
  <link rel="stylesheet" type="text/css" href="dist/DataTables-1.10.10/extensions/ColReorder/css/colReorder.dataTables.min.css?<?php echo time();?>">
  <link rel="stylesheet" type="text/css" href="dist/DataTables-1.10.10/extensions/FixedColumns/css/fixedColumns.dataTables.min.css?<?php echo time();?>">
  <link rel="stylesheet" type="text/css" href="dist/DataTables-1.10.10/extensions/FixedHeader/css/fixedHeader.dataTables.min.css?<?php echo time();?>">
   <link rel="stylesheet" type="text/css" href="dist/DataTables-1.10.10/extensions/RowGroup/rowGroup.dataTables.min.css?<?php echo time();?>">


  <link rel="stylesheet" type="text/css" href="bootstrap/bootstrapselect/dist/css/bootstrap-select.css?<?php echo time(); ?>">

  <link rel="stylesheet" href="dist/dropify-master/dist/css/dropify.css?<?php echo time();?>"/>

  <link rel="stylesheet" href="dist/sweetalert/sweetalert2.css?<?php echo time ();?>">
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

  <![endif]-->
  <link rel="stylesheet" href="dist/cart/css/smart_cart.css?<?php echo time();?>"/>
  <link rel="stylesheet" href="dist/ihover/ihover.css?<?php echo time();?>"/>

  <link type="text/css" rel="stylesheet" href="dist/simplepagination/simplePagination.css?<?php echo time();?>"/>

   <link type="text/css" rel="stylesheet" href="dist/radios-to-slider-master/css/radios-to-slider.css?<?php echo time();?>"/>
   <link type="text/css" rel="stylesheet" href="dist/css/flexslider.css?<?php echo time();?>">
</head>
<body class="fixed hold-transition skin-green-light sidebar-mini" ng-app="myApp">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a ui-sref="<?php if($perfil==1){ echo 'Home';}else if($perfil==2){ echo "Pendientes({ Idestado: '1' })"; }else  { echo "Pedidos({ Idcategoria: '', Nomcategoria:'Todos los productos' })";}?>" ui-sref-opts="{reload: true}" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>D</b>S</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>DELASIEMBRA.COM</b> </span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <?php //infopedido actual
    

    $concan = pg_query($dbconn, "SELECT count(ped_clave_int) cant FROM tbl_pedidos_detalle where ped_clave_int = '".$idpedido."'");
    $datcan = pg_fetch_array($concan);
    $cantped = $datcan['cant'];
      ?>
      <div class="navbar-custom-menu">
         <form class="navbar-form navbar-left hide" role="search" id="divfiltropedido">
          <div class="input-group">
             <span class="input-group-btn hide">
                  <button  class="btn btn-flat" type="button" data-toggle="modal" data-target="#modalleft"><i class="fa fa-filter"></i>
                </button>
              </span>
             

          <input type="text" class="form-control typehead" name="bustxtproducto" id="bustxtproducto" placeholder="Busqueda" data-provide="typeahead" autocomplete="off" >
              <span class="input-group-btn btn-success">
                <button type="button" name="search" id="search-btn" class="btn btn-flat"  data-toggle="tooltip" data-placement="bottom" title="Buscar productos"  style="background-color: transparent;"> <i class="fa fa-search"></i>
                </button>
              </span>
         </div>
         <!--
           <div class="form-group">
              <div class="input-group">
               
               

                <select id="busproductopedido" onchange="CRUDPEDIDOS('LISTAPRODUCTOS','')" class="form-control hide" placeholder="seleccione un producto" data-header="Buscar por codigo, nombre" multiple data-actions-box="true" data-live-search="true" data-selected-text-format="count > 6">
                  <?php
                  $concate = pg_query($dbconn,"select cat_clave_int,cat_nombre from tbl_categorias where cat_activo = 1");
                  $numcate = pg_num_rows($concate);
                  for($nc=0;$nc<$numcate;$nc++)
                  {
                    $datc = pg_fetch_array($concate);
                    ?>

                     <optgroup label="<?php echo $datc['cat_nombre'];?>">  
                      <?php
                    $conpro = pg_query($dbconn,"select pro_clave_int,pro_nombre from tbl_productos where cat_clave_int = '".$datc['cat_clave_int']."' and pro_activo =1  and (pro_uni_venta = 1 or pro_pes_venta = 1) order by pro_nombre ASC");
                    $numpro = pg_num_rows($conpro);
                      for($np=0;$np<$numpro;$np++)
                      {
                        $datp = pg_fetch_array($conpro);
                        $idp = $datp['pro_clave_int'];
                        $nomp = $datp['pro_nombre'];
                        $con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idp."' and pri_activo = 1 LIMIT 1");
                        $dat = pg_fetch_row($con);
                        $cimg = $dat[0];
                        $img = $dat[1];
                        $imagen = '<img  src="'.$img.'" alt="message user image">';
                        ?>
                        <option  value="<?php echo $idp;?>" data-content="<?php echo $nomp;?><img class='pull-right' src='<?php echo $img;?>'  width='30'/>"><?php echo $nomp;?></option>
                        <?php
                      }
                    ?>
                     
                    </optgroup>
                    <?php
                  }
                  ?>

                </select>
                <span id="searchpedido"  data-toggle="tooltip" title="Buscar productos" class="input-group-btn" data-placement="bottom">
                 
                  <button type="button" class="btn btn-flat" onclick="CRUDPEDIDOS('LISTAPRODUCTOS','')"><i class="fa fa-search"></i>
                </button>

                </span>
                
      </div>
            </div>-->

        </form>
        <?php 
        $condom = pg_query($dbconn, "SELECT * FROM tbl_usuario WHERE prf_clave_int = 2 and usu_confirmar = 0 and est_clave_int!=2");
                
        $numdom = pg_num_rows($condom); 

        $con1 = pg_query($dbconn, "SELECT count(ped_clave_int) cant FROM tbl_pedidos WHERE ped_estado = 1");
         $dat1 = pg_fetch_array($con1);
        $pendientes = $dat1['cant'];

         $con1 = pg_query($dbconn, "SELECT count(ped_clave_int) cant FROM tbl_pedidos WHERE ped_estado = 2");
         $dat1 = pg_fetch_array($con1);
        $proceso = $dat1['cant'];
        ?>
        <ul class="nav navbar-nav icons-3d-shadow">
          <!-- Messages: style can be found in dropdown.less-->
          <li style="width: 100px" class="hide">
            <a data-toggle="tooltip" data-placement="bottom" title="Prueba piloto de aplicativo delasiembra.com"  class="link-black">
            <i class="fa"></i>
              <span class="label label-success" style="font-size:14px; font-weight:normal; border-radius: 25px; right: 0 !important">Prueba Piloto</span>
           </a>
          </li>

         <li class="messages-menu <?php if($perfil!=1){ echo 'hide';}?>"> <a data-toggle="tooltip" data-placement="bottom" title="Tienes <?php echo $proceso;?> pedidos en proceso de entrega" ui-sref="Pendientes({ Idestado: '2' })" ui-sref-opts="{reload: true}" class="link-black">
            <i class="fa  fa-motorcycle fa-2x"></i>
              <span class="label label-success" style="font-size:14px; font-weight:normal; border-radius: 25px; right: 0 !important" id="numpro"><?php echo $proceso;?></span>
           </a>
         </li>

           <li class="messages-menu <?php if($perfil!=1){ echo 'hide';}?>"> <a data-toggle="tooltip" data-placement="bottom" title="Tienes <?php echo $pendientes;?> pedidos pendientes por despachar" ui-sref="Pendientes({ Idestado: '1' })" ui-sref-opts="{reload: true}" class="link-black">

            
            <i class="fa  fa-bell fa-2x"></i>
              <span class="label label-info" style="font-size:14px; font-weight:normal; border-radius: 25px; right: 0 !important" id="numped"><?php echo $pendientes;?></span>
           </a>
         </li>
          <li class="messages-menu <?php if($perfil!=1){ echo 'hide';}?>"> <a data-toggle="tooltip" data-placement="bottom" title="Tienes <?php echo $numdom;?> domiciliarios por confirmar" ui-sref="Usuarios({ Conf: '1' })" ui-sref-opts="{reload: true}">
            <img  class="img-cart" src="dist/img/domi30x30w.png" />
              <span class="label label-danger" style="font-size:14px; font-weight:normal; border-radius: 25px; right: 0 !important" id="numpedi"><?php echo $numdom;?></span>
           </a>
         </li>
          <li class="messages-menu text-center"> <a data-target="#modalpedido" class="dropdown-toggle" data-toggle="modal" onclick="CRUDPEDIDOS('DETALLEPEDIDO','')" style="padding: 0px 10px 4px 2px;">
              <img  class="img-cart-2" src="dist/img/cart6.png" />
            
              <span class="label label-warning" style="font-size:14px; font-weight:normal; border-radius: 25px; right: 0 !important" id="numpedido"><?php echo $cantped;?></span>
            </a></li>
          <li class="dropdown messages-menu" style="display: none;">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <img src="dist/img/user8-128x128.jpg" class="img-circle" alt="User Image"/>
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message -->
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="dist/img/user8-128x128.jpg" class="img-circle" alt="User Image"/>
                      </div>
                      <h4>
                      DELASIEMBREA.COM
                        <small><i class="fa fa-clock-o"></i> 2 hours</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="dist/img/user8-128x128.jpg" class="img-circle" alt="User Image"/>
                      </div>
                      <h4>
                        Developers
                        <small><i class="fa fa-clock-o"></i> Today</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="dist/img/user8-128x128.jpg" class="img-circle" alt="User Image"/>
                      </div>
                      <h4>
                        Sales Department
                        <small><i class="fa fa-clock-o"></i> Yesterday</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <div class="pull-left">
                        <img src="dist/img/user8-128x128.jpg" class="img-circle" alt="User Image"/>
                      </div>
                      <h4>
                        Reviewers
                        <small><i class="fa fa-clock-o"></i> 2 days</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu" style="display: none;">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">10</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 10 notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-aqua"></i> 5 new members joined today
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-warning text-yellow"></i> Very long description here that may not fit into the
                      page and may cause design problems
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-users text-red"></i> 5 new members joined
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-shopping-cart text-green"></i> 25 sales made
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <i class="fa fa-user text-red"></i> You changed your username
                    </a>
                  </li>
                </ul>
              </li>
              <li class="footer"><a href="#">View all</a></li>
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu" style="display: none;">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Create a nice theme
                        <small class="pull-right">40%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">40% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Some task I need to do
                        <small class="pull-right">60%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Make beautiful transitions
                        <small class="pull-right">80%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">80% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo $imgperfil;?>" class="user-image" alt="User Image"/>
              <span class="hidden-xs"><?php echo $usuario;?>
              </span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo $imgperfil;?>" class="img-circle" alt="User Image"/>

                <p>
                   <?php echo $usuario;?>
                  <small><?php echo date('d.M.Y'); ?></small>
                </p>
              </li>
              <!-- Menu Body -->
              <?php 
              if($idUsuario>0)
              { ?>
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-6 text-center">
                    <a ui-sref="Ajustes" ui-sref-opts="{reload: true}">Mi cuenta</a>
                  </div>
                  
                  <div class="col-xs-6 text-center text-red">
                    <a href="data/logout.php" <?php //if($_SESSION["modo"]=="facebook"){ onclick="LOGOUT();"?><?php //}else{ ?> <?php //} ?>class="text-red">Cerrar Sesión</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
            <?php 
            } 
            else
            {
              ?>
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-6 text-center">
                    <a data-toggle="modal" data-target="#modallogin" class="text-red" onclick="CRUDUSUARIOS('REGISTRONUEVO','','')">Registrate</a>
                  </div>
                  
                  <div class="col-xs-6 text-center text-red">
                    <a data-toggle="modal" data-target="#modallogin" class="text-red" onclick="CRUDUSUARIOS('LOGIN','','')">Iniciar Sesión</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <?php
            }
            ?>

              <!-- Menu Footer-->
              
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <!--<li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>-->
        </ul>
       
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left">
          <a ui-sref="<?php if($perfil==1){ echo 'Home';}else if($perfil==2){ echo "Pendientes({ Idestado: '1' })"; }else { echo "Pedidos({ Idcategoria: '', Nomcategoria:'Todos los productos' })";}?>" ui-sref-opts="{reload: true}" >
           <img src="dist/img/LOGO.png" id="imglogo" class="logoweb" width="100%" height="100%"  alt="User Image"/></a>        
         </div>
       
      </div>
      <!-- search form -->
      <div  class="sidebar-form hide" id="divbusquedageneral">
        
          <input type="text" name="busquedageneral" id="busquedageneral" class="form-control typehead" placeholder="Buscar..." data-provide="typeahead" autocomplete="off">
         
             
        
      </div>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MENU PRINCIPAL</li>
        
         <li class="active treeview itemmenu dos">
          <a ui-sref="<?php if($perfil==1){ echo 'Home';}else if($perfil==2){ echo "Pendientes({ Idestado: '1' })"; }else { echo "Pedidos({ Idcategoria: '', Nomcategoria:'Todos los productos' })";}?>" ui-sref-opts="{reload: true}">
            <i class="fa fa-home "></i> <span>Inicio</span> 
          </a>
        </li>
         <?php if($perfil==3 || $idUsuario<=0) { 
          $concate = pg_query($dbconn,"select cat_clave_int,cat_nombre,cat_imagen from tbl_categorias where cat_activo = 1");
          ?>
        <li class="treeview active"><a ui-sref="Pedidos({ Idcategoria: '', Nomcategoria:'Todos los productos' })" ui-sref-opts="{reload: true}"><i class="fa  fa-cart-arrow-down "></i>
          <span>Productos</span>  <i class="fa fa-angle-left   pull-right"></i>
        </a>
             <ul class="treeview-menu">
              <?php
              while($datc = pg_fetch_array($concate))
              {
              ?>
              <li><a ui-sref="Pedidos({ Idcategoria: <?php echo $datc['cat_clave_int'];?>,Nomcategoria:'<?php echo $datc['cat_nombre'];?>' })" ui-sref-opts="{reload: true}"> <img src="<?php echo $datc['cat_imagen'];?>" class="img-cart-icon-left"><?php echo $datc['cat_nombre'];?></a></li>
              <?php
              }
              ?>
              
            </ul>
        </li>
      <?php 
      } 
      ?>
        <?php

        if($idUsuario>0)
        {
        ?>
         <li class=" treeview">
          <a href="#">
           <i class="fa fa-user-circle-o  "></i>  <span>Mi cuenta</span> <i class="fa fa-angle-left   pull-right"></i>
          </a>
          <ul class="treeview-menu">
            
            <li class="itemmenu dos"><a ui-sref="Ajustes({ Op: 'Cue' })" ui-sref-opts="{reload: true}"><i class="fa fa-cog  "></i>Ajustes</a></li>
            <li class="itemmenu dos hide"><a data-toggle="modal" data-target="#modalright" onclick="CRUDPEDIDOS('NUEVADIRECCION','')"><i class="fa  fa-street-view "></i>Direcciones</a></li>
            <li class="hide"><a href="#"><i class="fa fa-globe "></i>Idioma</a></li>
            <li class="hide"><a href="#"><i class="fa fa-bell-o  "></i>Centro de notificaciones</a></li>
            <li><a ui-sref="Pqr" ui-sref-opts="{reload: true}"><i class="fa fa-info-circle "></i>(PQR)</a></li>
          </ul>
        </li>
        <li class="treeview <?php if($perfil=="1"){ echo "hide";}?>">
          <a href="#">
            <i class="fa fa-info-circle  "></i> <span>Historial de pedidos</span> <i class="fa fa-angle-left   pull-right"></i>
          </a>
          <ul class="treeview-menu">
                <?php if($perfil=="1"){}?>
                <li class="itemmenu dos"><a ui-sref="Pendientes({ Idestado: '1', Lista: '0' })" ui-sref-opts="{reload: true}"><i class="fa fa-bell  "></i> Pedidos pendientes</a></li>
                <li class="itemmenu dos"><a ui-sref="Pendientes({ Idestado: '2', Lista: '0' })" ui-sref-opts="{reload: true}"><i class="fa fa-motorcycle  "></i>Proceso de entrega</a></li>
                <li class="itemmenu dos"><a ui-sref="Pendientes({ Idestado: '3', Lista: '0' })" ui-sref-opts="{reload: true}"><i class="fa fa-check  "></i> Pedidos completados</a></li>
                <li class="itemmenu dos"><a ui-sref="Pendientes({ Idestado: '4', Lista: '0' })" ui-sref-opts="{reload: true}"><i class="fa fa-ban  "></i> Pedidos Cancelados</a></li>
                 <li class="itemmenu dos <?php if($perfil!=3){ echo 'hide';}?>"><a ui-sref="Pendientes({ Idestado: '', Lista: '1' })" ui-sref-opts="{reload: true}"><i class="fa fa-list-ul  "></i> Lista deseos</a></li>
              </ul>
        </li>
        
       
        <?php if($perfil=="1"){ ?>

        <li class="active treeview">
          <a href="#">
             <i class="fa fa-shield  "></i><span>Seguridad</span> <i class="fa fa-angle-left    pull-right"></i>
          </a>
           <ul class="treeview-menu">           
            <li class="itemmenu dos"><a ui-sref="Perfiles" ui-sref-opts="{reload: true}"><i class="fa fa-user  "></i>Perfiles</a></li>
            <li class="itemmenu dos"><a ui-sref="Usuarios({ Conf: '0' })" ui-sref-opts="{reload: true}"><i class="fa fa-users  "></i>Usuarios</a></li>
            <li class="itemmenu dos hide"><a href="#"><i class="fa fa fa-expeditedssl  "></i>Permisos</a></li>
          </ul>
        </li>
      <?php } ?>
      <?php if($perfil=="1"){ ?>
        <li class="active treeview">
          <a href="#">
             <i class="fa fa-wrench "></i><span>Configuración</span> <i class="fa fa-angle-left   pull-right"></i>
          </a>
           <ul class="treeview-menu">
            <!--<li><a ui-sref="Unidades" ui-sref-opts="{reload: true}"><i class="fa fa-cog"></i>Unidades</a></li>-->
             <li class="itemmenu dos"><a ui-sref="Mercados" ui-sref-opts="{reload: true}"><i class="fa fa-cog  "></i>Mercados</a></li>
            <li class="itemmenu dos"><a ui-sref="Categorias" ui-sref-opts="{reload: true}"><i class="fa fa-leanpu  "></i>Categorias</a></li>
            <li class="itemmenu dos"><a ui-sref="Productos" ui-sref-opts="{reload: true}"><i class="fa fa-cog  "></i>Productos</a></li>
            <li class="itemmenu dos hide"><a  ui-sref="ListaPrecios" ui-sref-opts="{reload: true}"><i class="fa fa-cog  "></i>Lista de precios</a></li>
            <li class="itemmenu dos"><a ui-sref="Proveedores" ui-sref-opts="{reload: true}"><i class="fa fa-building  "></i>Proveedores</a></li>
            <li class="itemmenu dos"><a ui-sref="Inventario" ui-sref-opts="{reload: true}"><i class="fa fa-folder-open  "></i>Inventario</a></li>
            <li class="itemmenu dos hide"><a ui-sref="Pedidos({ Idcategoria: 1 })" ui-sref-opts="{reload: true}"><i class="fa  fa-cart-arrow-down  "></i>Pedidos</a></li>
            <li class="itemmenu dos"><a ui-sref="Ofertas" ui-sref-opts="{reload: true}"><i class="fa  fa-ticket  "></i>Ofertas</a></li>
            <li class="hide"><a href="#"><i class="fa fa-globe  "></i>Clientes</a></li>
            <li class="hide"><a href="#"><i class="fa fa-bell-o  "></i>Domiciliarios</a></li>
            <li class="itemmenu dos"><a ui-sref="Sectores" ui-sref-opts="{reload: true}"><i class="fa fa-cog  "></i>Sectores</a></li>
            <li class="itemmenu dos">
              <a 
            data-toggle="modal" data-target="#modallogin" class="text-green" onclick="CRUDREGLAS('REGLASNEGOCIO')">Reglas de negocio</a>
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
             <i class="fa fa-file"></i><span>Informes</span> <i class="fa fa-angle-left   pull-right"></i>
          </a>
           <ul class="treeview-menu">
           
              <li class="itemmenu dos"><a ui-sref="Informes({ Informe: 'INFORMEDESPACHOS',Tituloinforme:'Informe Despacho Dia' })" ui-sref-opts="{reload: true}"><i class="fa"></i>Despachos Dia</a></li>
           
            
          </ul>
        </li>
      <?php } 
    }
    else
    {
      ?>
        <li class="active treeview itemmenu dos">
          <a data-toggle="modal" data-target="#modallogin"  onclick="CRUDUSUARIOS('LOGIN','','')">
            <i class="fa fa-user  "></i> <span>Iniciar Sesión</span> 
          </a>
        </li>
         <li class="active treeview itemmenu dos">
          <a style="color:#8a6d3b !important" data-toggle="modal" data-target="#modallogin"  onclick="CRUDUSUARIOS('REGISTRONUEVO','','')">
            <i class="fa  fa-user-plus  "></i> <span>Registrate</span> 
          </a>
        </li>
        <li class="active treeview itemmenu dos">
          <a ui-sref="Domiciliario" ui-sref-opts="{reload: true}">
            <i class="fa fa-send  "></i> <span>Reparte para nosotros</span> 
          </a>
        </li>
        
        <?php
    }
    ?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content" ui-view="">
      
    </section>
   
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; <?php date('Y');?> <a href="https://pavas.com.co">PAVAS S.A.S</a>.</strong> Todos los derechos reservados.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript::;">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript::;">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript::;">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript::;">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript::;">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript::;">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript::;">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript::;">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript::;" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>


<div id="modallogin" class="modal login fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titlelogin"></h4>
      </div>
      <div class="modal-body" id="divlogin">
           
      </div>    
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="modalregistro" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titlemodal">Modal title</h4>
      </div>
      <div class="modal-body" id="contenidomodal">
        <p>One fine body&hellip;</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" id="btnguardar" class="btn btn-primary">Guardar Cambios</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div id="modalinfo" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="titleinfo">Modal title</h4>
      </div>
      <div class="modal-body" id="contenidomodalinfo">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
         <button data-loading-text="Enviando..." type="button" id="btnenviar" class="btn btn-primary">Hacer Pedido</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


  <div class="modal left fade" id="modalleft" tabindex="-1" role="dialog" aria-labelledby="titleleft">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="titleleft"></h4>
        </div>

        <div class="modal-body" id="contenidomodalleft">
         
        </div>

      </div><!-- modal-content -->
    </div><!-- modal-dialog -->
  </div><!-- modal -->
  
  <!-- Modal -->
  <div class="modal right fade" id="modalright" tabindex="-1" role="dialog" aria-labelledby="titleright">
    <div class="modal-dialog" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="titleright"></h4>
        </div>

        <div class="modal-body" id="contenidomodalright">
          
        </div>

      </div><!-- modal-content -->
    </div><!-- modal-dialog -->
  </div><!-- modal -->

  <div class="modal pedido fade" id="modalpedido" tabindex="-1" role="dialog" aria-labelledby="titlepedido">
    <div class="modal-dialog rotateOutDownLeft animate" role="document">
      <div class="modal-content">

        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="titlepedido"></h4>
        </div>

        <div class="modal-body" id="contenidopedido">
           <input type="hidden" name="" id="inismart" value="0">
            <form action="modulos/pedidos/guardarpedido.php" id="formpedido" name="formpedido" method="POST"> 
              <!-- SmartCart element -->
              <div id="smartcart" ></div>
              <div class="panel panel-default sc-cart sc-theme-default" id="divdetallepedido">
                
              </div>

          </form>
        </div>

      </div><!-- modal-content -->
    </div><!-- modal-dialog -->
  </div><!-- modal -->

  <div class="modal custom" id="modalcustom" tabindex="-1" role="dialog" aria-labelledby="titlecustom" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
               <h4 class="modal-title" id="titlecustom"></h4>

            </div>

            <div class="modal-body" id="contenidomodalcustom">
            <p></p>
            </div>

            <div class="modal-footer">
                <div class="btnmodal" data-dismiss="modal">Cerrar &#10006;</div>

            </div>
        </div>
    </div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.min.js?<?php echo time();?>"></script>
<script src="bootstrap/js/bootstrapds.js?<?php echo time();?>"></script>
<script src="bootstrap/js/bootstrap3-typeahead.min.js?<?php echo time();?>"></script>
<script src="bootstrap/js/validator.js?<?php echo time();?>"></script>
<script src="bootstrap/bootstrapselect/dist/js/bootstrap-select.js?<?php echo time();?>"></script>
<!-- Morris.js charts -->
<script src="dist/cdn/raphael-min.js?<?php echo time();?>"></script>
<script src="plugins/morris/morris.min.js?<?php echo time();?>"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js?<?php echo time();?>"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js?<?php echo time();?>"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js?<?php echo time();?>"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js?<?php echo time();?>"></script>
<!-- datepicker -->

     <!-- Daterange picker -->
<script src="dist/cdn/moment.min.js?<?php echo time();?>"></script>
<script src="plugins/daterangepicker/daterangepicker.js?<?php echo time();?>"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js?<?php echo time();?>"></script>
<script src="plugins/datepicker/locales/bootstrap-datepicker.es.js?<?php echo time();?>"></script>

<!-- bootstrap time picker -->
<script src="plugins/timepicker/bootstrap-timepicker.js?<?php echo time();?>"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js?<?php echo time();?>"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js?<?php echo time();?>"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js?<?php echo time();?>"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.js?<?php echo time();?>"></script>

<script src="dist/dropify-master/dist/js/dropify.js?<?php echo time();?>"></script>
<script src="upload.js?<?php echo time();?>"></script>

<script  type="text/javascript" src="dist/sweetalert/sweetalert2.js?<?php echo time();?>"></script>
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill@7.1.0/dist/promise.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="dist/js/pages/dashboard.js?<?php echo time();?>"></script>-->
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js?<?php echo time();?>"></script>
<script src="llamadas.js?<?php echo time();?>" type="text/javascript"></script>
<script type="text/javascript" src="dist/DataTables-1.10.10/media/js/jquery.dataTables.js?<?php echo time();?>"></script>
<script type="text/javascript" src="dist/DataTables-1.10.10/media/js/dataTables.bootstrap.min.js?<?php echo time();?>"></script>
<script type="text/javascript" src="dist/DataTables-1.10.10/media/js/ColReorderWithResize.js?<?php echo time();?>"></script>
<script type="text/javascript" src="dist/DataTables-1.10.10/extensions/Responsive/js/dataTables.responsive.js?<?php echo time();?>"></script>
<script type="text/javascript" src="dist/DataTables-1.10.10/extensions/FixedColumns/js/dataTables.fixedColumns.min.js?<?php echo time();?>"></script>
<script type="text/javascript" src="dist/DataTables-1.10.10/extensions/FixedHeader/js/dataTables.fixedHeader.min.js?<?php echo time();?>"></script>
<!--
<script type="text/javascript" src="dist/DataTables-1.10.10/extensions/RowGroup/dataTables.rowGroup.min.js?<?php echo time();?>"></script>-->

<script src="dist/jquery.formatCurrency-1.4.0/jquery.formatCurrency-1.4.0.min.js?<?php echo time();?>"></script>
<script src="dist/jquery.formatCurrency-1.4.0/i18n/jquery.formatCurrency.all.js?<?php echo time();?>"></script>
<script src="dist/cart/js/jquery.smartCart.js?<?php echo time();?>" type="text/javascript"></script>

<script type="text/javascript" src="dist/simplepagination/pagination.js?<?php echo time();?>"></script>

<script src="dist/radios-to-slider-master/js/jquery.radios-to-slider.js?<?php echo time();?>" type="text/javascript" ></script>
<script type="text/javascript" src="dist/js/autosize.js?<?php echo time();?>"></script>
<script src="plugins/iCheck/icheck.min.js?<?php echo time();?>"></script>

<script src="dist/js/jquery.flexslider.js?<?php echo time();?>"></script>
<script src="dist/js/plusminus.js" type="text/javascript"></script>

<link href="dist/tooltipster-master/css/tooltipster.css?<?php echo time(); ?>" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="dist/tooltipster-master/css/themes/tooltipster-light.css?<?php echo time(); ?>" />
<link rel="stylesheet" type="text/css" href="dist/tooltipster-master/css/themes/tooltipster-noir.css?<?php echo time(); ?>" />
<link rel="stylesheet" type="text/css" href="dist/tooltipster-master/css/themes/tooltipster-punk.css?<?php echo time(); ?>" />
<link rel="stylesheet" type="text/css" href="dist/tooltipster-master/css/themes/tooltipster-shadow.css?<?php echo time(); ?>" />
<script src="dist/tooltipster-master/js/jquery.tooltipster.js?<?php echo time();?>"></script>


<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
<?php
  if($_GET['varContrasena'] == 1 || $_GET['varContrasena'] == 2)
  {
    //echo "<script>error('Usuario o Contraseña incorrecta. En caso de haber olvidado tu datos de inicio de sesión puedes recuperar tus datos en el link ¿Olvidaste Tu contraseña?');</script>";

    echo "<script>error2('Usuario o Contraseña incorrecta. En caso de haber olvidado tu datos de inicio de sesión puedes recuperar tus datos en el link ¿Olvidaste Tu contraseña? ');</script>";
  }
  elseif($_GET['varContrasena'] == 3)
  {
    //echo "<script>error('Su cuenta esta inactiva');</script>";
    echo "<script>error2('Su cuenta esta inactiva');</script>";
  }
  elseif($_GET['varContrasena'] == 4)
  {
    //echo "<script>error('No ha verificado captcha');</script>";
    echo "<script>error2('No ha verificado captcha');</script>";
  }
  elseif($_GET['varContrasena'] == 5)
  {
    //echo "<script>error('No ha verificado captcha');</script>";
    echo "<script>error2('Estas pendiente por confirmación de cuenta por parte de DELASIEMBRA.COM, en caso de inconvenientes comunicate con nosotros al siguiente numero: <a>+57 310 8611064</a>');</script>";
  }
  ?>
</body>
</html>
