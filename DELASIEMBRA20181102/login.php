<?php
include('data/Conexion.php'); 
  error_reporting(0); 
  //include('Classes/PHPMailer-master/PHPMailerAutoload.php');
  header('Content-Type: text/html; charset=UTF-8');
  date_default_timezone_set('America/Bogota'); 
 // echo pg_num_rows($result);
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link href="dist/img/favicon.ico?<?php echo time();?>" rel="shortcut icon">
  <title>DELASIEMBRA | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.css?<?php echo time();?>">

    <link rel="stylesheet" href="bootstrap/css/bootstrapds.css?<?php echo time();?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="dist/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css?<?php echo time();?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.css?<?php echo time();?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css?<?php echo time();?>">
  <link rel="stylesheet" href="dist/sweetalert/sweetalert2.css?<?php echo time ();?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box" style="background-color: #FFFFFF">
  <div class="login-logo">
    <a href="index.php"><img src="dist/img/LOGO.png" height="80" class="img-login"></a>
    <!--<h6>Sistema de Gestión de Tickets</h6>-->
   
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <!--<p class="login-box-msg">Sign in to start your session</p>-->
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <form action="data/validarUsuariospg.php" method="post">
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Correo electrónico" id="txtemail" name="txtemail" required>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Contraseña" id="txtcontrasena" name="txtcontrasena" required>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group">
         <div class="g-recaptcha" data-sitekey="6LeGRDoUAAAAAMHadNkp4SIxreOUP9Gv7Y7cX_e3"></div>
      </div>
      <div class="row">
        <div class="col-xs-7">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Recordarme
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-5">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Iniciar Sesión</button>
        </div>

        <!-- /.col -->
      </div>
    </form>
    <div class="social-auth-links text-center">
      <p>- ó -</p>
      <button  class="btn btn-block btn-social btn-facebook btn-flat loginfacebook"><i class="fa fa-facebook"></i> Login usando Facebook</button>
      
</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> 
Login usando 
        Google+</a>
    </div>

<!--
    <div class="social-auth-links text-center">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using
        Facebook</a>
      <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using
        Google+</a>
    </div>-->
    <!-- /.social-auth-links -->

    <a onclick="RECUPERARCONTRASENA()">Olvide Mi contraseña</a><br>  
    <a href="registro.php" class="text-center">Registrarme</a>
    <span id="status"></span>

  </div>
  <p class="login-box-msg">
 <small> PAVAS S.A.S.<br>
Copyright © Todos los derechos reservados</small>
  </p>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
<!-- jQuery 2.1.4 -->
<script src="plugins/jQuery/jQuery-2.1.4.min.js?<?php echo time();?>"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.js?<?php echo time();?>"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js?<?php echo time();?>"></script>
<script  type="text/javascript" src="dist/sweetalert/sweetalert2.js?<?php echo time();?>"></script>
<script src="llamadas.js?<?php echo time();?>" type="text/javascript"></script>
<script src="dist/js/loginfacebook.js?<?php echo time(); ?>"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
<?php
  if($_GET['varContrasena'] == 1 || $_GET['varContrasena'] == 2)
  {
    //echo "<style onload=error('Usuario o Contraseña incorrecta. En caso de haber olvidado tu datos de inicio de sesión puedes recuperar tus datos en el link ¿Olvidaste Tu contraseña?')></style>";

    echo "<script>error2('Usuario o Contraseña incorrecta. En caso de haber olvidado tu datos de inicio de sesión puedes recuperar tus datos en el link ¿Olvidaste Tu contraseña? ');</script>";
  }
  elseif($_GET['varContrasena'] == 3)
  {
    //echo "<style onload=error('Su cuenta esta inactiva')></style>";
    echo "<script>error2('Su cuenta esta inactiva');</script>";
  }
  elseif($_GET['varContrasena'] == 4)
  {
    //echo "<style onload=error('No ha verificado captcha')></style>";
    echo "<script>error2('No ha verificado captcha');</script>";
  }
  elseif($_GET['varContrasena'] == 5)
  {
    //echo "<style onload=error('No ha verificado captcha')></style>";
    echo "<script>error2('Estas pendiente por confirmación de cuenta por parte de DELASIEMBRA.COM, en caso de inconvenientes comunicate con nosotros al siguiente numero: <a>### ## ##</a>');</script>";
  }
?>
</body>
</html>


