<?php
include('../../data/Conexion.php');
session_start();
error_reporting(0);
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_SESSION["idusuario"];
$clave= $_COOKIE["clave"];
$identificacion = $_COOKIE["usIdentificacion"];
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");

$opcion = $_POST['opcion'];

if($opcion=="GUARDAREDICION")
{        
    $nom = $_POST['nom'];
    $num = $_POST['num'];
    $tip = $_POST['tip'];
    $ema = $_POST['ema'];
    $tel1 = $_POST['tel1'];
    $tel2 = $_POST['tel2'];
    $dir = $_POST['dir'];
    $obs = $_POST['obs'];
    $act = $_POST['act'];   if($act<=0){$act = 0;}     
    $idproveedor = $_POST['id'];        
    $obs = nl2br($obs);

    $sql = pg_query($dbconn,"select * from tbl_proveedor where (UPPER(prv_nombre) = UPPER('".$nom."')) AND prv_clave_int <> '".$idproveedor."' and prv_activo!=2");
    $dato = pg_fetch_array($sql);
    $conprv = $dato['prv_nombre'];

    if($ema != '')
    {
        $sql = pg_query($dbconn,"select * from tbl_proveedor where (UPPER(prv_email) = UPPER('".$ema."')) AND prv_clave_int <> '".$idproveedor."' and prv_activo!=2");
        $dato = pg_fetch_array($sql);
        $conema = $dato['prv_email'];
    }

    if($nom == '' || is_null($num))
    {
    	$res = "error";
        $msn =  "Debe ingresar el Nombre del Proveedor";
    }
    else
    if(STRTOUPPER($conprv) == STRTOUPPER($nom))
    {
    	$res = "error";
        $msn =  "El proveedor ingresado ya existe";
    }
    else
    if($num == '' || is_null($num))
    {
    	$res = "error";
        $msn = "Debe ingresar el número de documento";
    }
    else
    if($tip == '' || is_null($tip))
    {
    	$res = "error";
        $msn = "Debe ingresar el tipo de documento";
    }
    /*else
    if($ema == '' || is_null($ema))
    {
    	$res = "error";
        $msn = "Debe ingresar el Email";
    }*/
    else
    if($tel1 == '' || is_null($tel1))
    {
    	$res = "error";
        $msn = "Debe ingresar el Teléfono";
    }
    else
    if($dir == '' || is_null($dir))
    {
    	$res = "error";
        $msn = "Debe ingresar la dirección";
    }        
    else
    {
        //if($act == 'false'){$swact = 0;}elseif($act == 'true'){$swact = 1;}

        if((STRTOUPPER($conema) == STRTOUPPER($ema)) and ($ema != ''))
        {
            $res = "error";
            $msn = "El e-mail ingresado ya existe";
        }
        else
        {
            $con = pg_query($dbconn,"update tbl_proveedor set prv_nro_docum = '".$num."', prv_tipo_docum = '".$tip."', prv_nombre = '".$nom."', prv_telefono1 = '".$tel1."', prv_telefono2 = '".$tel2."', prv_email = '".$ema."', prv_direccion = '".$dir."', prv_activo = '".$act."', prv_observacion = '".$obs."', prv_usu_actualiz = '".$usuario."', prv_fec_actualiz = '".$fecha."' where prv_clave_int = '".$idproveedor."'");

            if($con >= 1)
            {
            //pg_query($dbconn,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_encabezado,obr_clave_int,loa_usu_actualiz,loa_fec_actualiz) values(null,10,49,'".$p."','".$ultimaobra."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 49=Actualización proveedor
            	$res = "ok"; 
            	$msn = "Datos grabados correctamente";
            }
            else
            {
            	$res = "error";
            	$msn = "No se han podido actualizar los datos";
            }
        }
    }
    $datos[] = array("res"=>$res, "msn"=>$msn);
   echo json_encode($datos);
}
else if($opcion=="GUARDAR")
{

    $nom = $_POST['nom'];
    $num = $_POST['num'];
    $tip = $_POST['tip'];
    $ema = $_POST['ema'];
    $tel1 = $_POST['tel1'];
    $tel2 = $_POST['tel2'];
    $dir = $_POST['dir'];
    $obs = nl2br($_POST['obs']);
    $act = $_POST['act'];

    $sql = pg_query($dbconn,"select * from tbl_proveedor where (UPPER(prv_nombre) = UPPER('".$nom."')) and prv_activo!=2");
    $dato = pg_fetch_array($sql);
    $conprv = $dato['prv_nombre'];

    if($ema != '')
    {
        $sql = pg_query($dbconn,"select * from tbl_proveedor where (UPPER(prv_email) = UPPER('".$ema."')) and prv_activo!=2");
        $dato = pg_fetch_array($sql);
        $conema = $dato['prv_email'];
    }

    if($nom == '' || $nom==NULL)
    {
        $res  ="error";
        $msn =  "Debe ingresar el Nombre del Proveedor";
    }
    else
    if(STRTOUPPER($conprv) == STRTOUPPER($nom))
    {
        $res  ="error";
            $msn =  "El proveedor ingresado ya existe";
    }
    else
    if($num == '' || $num==NULL)
    {
        $res  ="error";
         $msn =  "Debe ingresar el número de documento";
    }
    else
    if($tip == '' || $tip==NULL)
    {
        $res  ="error";
        $msn =  "Debe ingresar el tipo de documento";
    }
    /*else
    if($ema == '' || is_null($ema))
    {
        $res  ="error";
        $msn =  "Debe ingresar el Email";
    }*/
    else
    if($tel1 == '' || $tel1==NULL)
    {
        $res  ="error";
        $msn =  "Debe ingresar el Teléfono" . $tel1;
    }
    else
    if($dir == '' || $dir==NULL)
    {
        $res  ="error";
        $msn =  "Debe ingresar la dirección";
    }
   
    else
    {
        // if($act == 'false'){$swact = 0;}elseif($act == 'true'){$swact = 1;}

        if((STRTOUPPER($conema) == STRTOUPPER($ema)) and ($ema != ''))
        {
            $res  ="error";
            $msn =  "El e-mail ingresado ya existe";
        }
        else
        {
            $con = pg_query($dbconn,"insert into tbl_proveedor(prv_nro_docum,prv_tipo_docum,prv_nombre,prv_telefono1,prv_telefono2,prv_email,prv_direccion,prv_activo,prv_observacion,prv_usu_actualiz,prv_fec_actualiz) values('".$num."','".$tip."','".$nom."','".$tel1."','".$tel2."','".$ema."','".$dir."','".$act."','".$obs."','".$usuario."','".$fecha."')");

            if($con >= 1)
            {
                $con = pg_query($dbconn,"select prv_clave_int from tbl_proveedor where UPPER(prv_nombre) = UPPER('".$nom."') and prv_nro_docum = '".$num."'");
                $dato = pg_fetch_array($con);
                $claprv = $dato['prv_clave_int'];
                //pg_query($dbconn,"insert into log_actividades(loa_clave_int,ven_clave_int,tia_clave_int,loa_encabezado,obr_clave_int,loa_usu_actualiz,loa_fec_actualiz) values(null,10,48,'".$claprv."','".$ultimaobra."','".$usuario."','".$fecha."')");//Tercer campo tia_clave_int. 48=Creación proveedor
                $msn = "Proveedor Registrado Correctamente";
               	$res = "ok";
               	$cla = $claprv;
            }
            else
            {
                $res  ="error";
                $msn =  "No se han podido guardar los datos.Error BD(".pg_last_error($dbconn).")";
            }
        }
    }
    $datos[]  = array("res"=>$res,"msn"=>$msn,"cla"=>$cla);
    echo json_encode($datos);
}
else if($opcion=="FILTROS")
{
	?>
	<div class="row">
		<div class="col-md-12">
			<label for="busnombre">Nombre:</label>
			<input class="form-control input-sm"  name="busnombre" id="busnombre" maxlength="70" type="text"/>
		</div>		
		<div class="col-md-12">
			<label for="bustipo">Tipo Documento:</label>
			<select name="bustipo" id="bustipo" onchange="CRUDPROVEEDORS('PROVEEDOR','')" class="form-control input-sm selectpicker">
			<option value="">-Seleccione-</option>
			<option value="C" <?php if($tip == 'C'){ echo 'selected="selected"'; } ?>>Céd.Ciudadanía</option>
			<option value="E" <?php if($tip == 'E'){ echo 'selected="selected"'; } ?>>Céd.Extranjería</option>
			<option value="N" <?php if($tip == 'N'){ echo 'selected="selected"'; } ?>>Nit</option>
			<option value="I" <?php if($tip == 'I'){ echo 'selected="selected"'; } ?>>Tarj.Identidad</option>
			</select>
		</div>
		<div class="col-md-12">
			<label for="busnumero">Numero Documento:</label>
			<input class="form-control input-sm"  name="busnumero" id="busnumero" type="text"/>
		</div>
		<div class="col-md-12">
			<label for="bustelefono1">Telefono 1:</label>
			<input class="form-control input-sm"  name="bustelefono1" id="bustelefono1" maxlength="70" type="text" />
		</div>
		<div class="col-md-12">
			<label for="bustelefono2">Telefono 2:</label>
			<input class="form-control input-sm"  name="bustelefono2" id="bustelefono2" maxlength="70" type="text"/>
		</div>
		<div class="col-md-12">
			<label for="busemail">Correo:</label>
			<input class="form-control input-sm" name="busemail" id="busemail" maxlength="70" type="text"/>
		</div>
	    <div class="col-md-12">
	    	<label for="busdireccion">Dirección:</label>
			<input class="form-control input-sm" name="busdireccion" id="busdireccion" maxlength="70" type="text" />
		</div>
		<div class="col-md-12">
			<label for="busobservacion">Observación:</label>
			<input class="form-control input-sm" name="busobservacion" id="busobservacion" maxlength="70" type="text"/>
		</div>
		<div class="col-md-12">	
			<label for="busactivo">Estado:</label>			
			<select multiple name="busactivo" id="busactivo" onchange="CRUDPROVEEDORES('LISTAPROVEEDORES','')" class="form-control input-sm selectpicker" >
			<option value="1">Activos</option>
			<option value="0">Inactivos</option>
			<option value="">Todos</option>
			</select>
		</div>
		<div class="col-md-12">
    			<button onchange="CRUDPROVEEDORES('LISTAPROVEEDORES','')" type="button" class="btn btn-success btn-block btn-sm">Buscar <i class="fa fa-search fa-2x"></i></button>
    	</div>
	</div>
	<?php
	echo "<script>CRUDPROVEEDORES('LISTAPROVEEDORES','');</script>";
	echo "<script>INICIALIZARCONTENIDO();</script>";
}
else if($opcion=="LISTAPROVEEDORES")
{
	?>
	<script src="jsdatatable/proveedor/jslistaproveedor.js" type="text/javascript"></script>
	<table id="tbproveedor" class="table table-striped">
		<thead>
		<tr>
			<th width="20px"></th>
			<th width="20px"></th>			
			<th><strong>Nombre</strong></th>
			<th><strong>Tipo</strong></th>
			<th><strong>Documento.</strong></th>
			<th><strong>Teléfonos</strong></th>
			<th><strong>E-mail</strong></th>
			<th><strong>Dirección</strong></th>
			<th><strong>Observación</strong></th>			
			<th>Estado</th>
			
		</tr>
		</thead>
	</table>
	<?php
}
else if($opcion=="EDITAR" || $opcion=="NUEVO")
{
	$idproveedor = $_POST['id'];

	$con = pg_query($dbconn,"select * from tbl_proveedor where prv_clave_int = '".$idproveedor."' order by prv_nombre LIMIT 1"); 
	$dato = pg_fetch_array($con); 
	$claprv = $dato['prv_clave_int'];
	$nom = $dato['prv_nombre'];
	$tip = $dato['prv_tipo_docum'];
	$num = $dato['prv_nro_docum'];
	$tel1 = $dato['prv_telefono1'];
	$tel2 = $dato['prv_telefono2'];
	$ema = $dato['prv_email'];
	$dir = $dato['prv_direccion'];
	$obs = br2nl($dato['prv_observacion']);
	$act = $dato['prv_activo'];
	?>
		<div class="row">
			<div class="col-md-4 text-left">
			<strong>Nombre:</strong>
			<input class="form-control input-sm" name="nombre" id="nombre" value="<?php echo $nom; ?>" type="text"/>
           </div>
            <div class="col-md-4 text-left">
			<strong>N° Documento:</strong>
                <input class="form-control input-sm" name="numdoc" id="numdoc" value="<?php echo $num; ?>"  type="text"/>
            </div>
            <div class="col-md-4 text-left">
			<strong>Tipo Documento:</strong>

				<select name="tipo" id="tipo" class="form-control input-sm selectpicket">
				<option value="C" <?php if($tip == 'C'){ echo 'selected="selected"'; } ?>>Céd.Ciudadanía</option>
				<option value="E" <?php if($tip == 'E'){ echo 'selected="selected"'; } ?>>Céd.Extranjería</option>
				<option value="N" <?php if($tip == 'N'){ echo 'selected="selected"'; } ?>>Nit</option>
				<option value="I" <?php if($tip == 'I'){ echo 'selected="selected"'; } ?>>Tarj.Identidad</option>
				</select>
            </div>
		</div>
		<div class="row">
            <div class="col-md-4 text-left">
			<strong>Email:</strong>
			<input class="form-control input-sm" name="email" id="email" value="<?php echo $ema; ?>" maxlength="100"  type="mail" />
            </div>
            <div class="col-md-4 text-left">
			<strong>Telefonos:</strong>
                <div class="input-group">
                    <input class="form-control input-sm" name="telefono1" id="telefono1" value="<?php echo $tel1; ?>" maxlength="20"  type="text"  />
                    <span class="input-group-addon">/</span>
                    <input class="form-control input-sm" name="telefono2" id="telefono2" value="<?php echo $tel2; ?>" maxlength="20"  type="text" />
                </div>
              </div>
            <div class="col-md-4 text-left">
                <strong>Dirección:</strong>
                <input class="form-control input-sm" name="direccion" id="direccion" value="<?php echo $dir; ?>" maxlength="100" type="text"  />
            </div>
		</div>
		<div class="row">
			<div class="col-md-12 text-left">
			<strong >Observación:</strong>
			<textarea name="observacion" id="observacion" class="form-control input-sm" cols="20" rows="2" ><?php echo $obs; ?></textarea></td>
            </div>
		</div>		
		<div class="row">           
            <div class="col-md-2 text-left">
                <strong>Activo:</strong>
                <input name="activo" id="activo" <?php if($opcion=="NUEVO"){  echo 'checked="checked"';} else if($act == 1){ echo 'checked="checked"'; } ?> type="checkbox" value="1" />
            </div>
		</div>
        <?php
        if($idproveedor>0) 
        {
            ?>
        <h3>Asociar Productos</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="nav-tabs-custom" id="divlistacalidad">
                <ul class="nav nav-tabs pull-left">              
                  
                <?php 
                $l = 0;
                $concla = pg_query($dbconn,"SELECT cla_clave_int,cla_nombre from tbl_clasificacion order by cla_clave_int");
                 while($datcla = pg_fetch_array($concla))
                {
                  $idc = $datcla['cla_clave_int'];
                  $nomc  =$datcla['cla_nombre'];
                  
                  $con = pg_query($dbconn,"select COUNT(*) cant from tbl_proveedor_productos WHERE prv_clave_int = '".$idproveedor."' and cla_clave_int = '".$idc."'");
                  $dato = pg_fetch_array($con);
                  ?>
                  <li class="<?php if($l==0){ echo "active";}?>">
                  <a onclick="CRUDPROVEEDORES('LISTAPRODUCTOS','<?PHP echo $idproveedor;?>','<?php echo $idc;?>')"  data-toggle="tab" aria-expanded="false">
                  <span id="cal_<?php echo $idc;?>"><?php echo $nomc." <span class='badge'>".$dato['cant']."</span>"; ?></span>
                  </a></li>
                  <?php
                  $l++;
                }
                
                ?>            
                </ul>
              </div>
            </div>
            <div class="col-md-2">
                <select name="selestado" id="selestado" class="form-control selectpicker"  onchange="CRUDPROVEEDORES('LISTAPRODUCTOS','<?PHP echo $idproveedor;?>','')">
                    <option value="">Todos</option>
                     <option value="Asignados">Asignados</option>
                     <option value="SinAsignar">Sin asignar</option>
                    
                </select>
            </div>
            <div class="col-md-4">
                  <select id="selproductoproveedor" onchange="CRUDPROVEEDORES('LISTAPRODUCTOS','<?PHP echo $idproveedor;?>','')" class="form-control selectpicker" placeholder="seleccione un producto" data-header="Buscar por codigo, nombre" multiple data-actions-box="true" data-live-search="true" data-selected-text-format="count > 2" title="Seleccionar Productos">
                  <?php
                  $concate = pg_query($dbconn,"select cat_clave_int,cat_nombre from tbl_categorias where cat_activo = 1");
                  $numcate = pg_num_rows($concate);
                  for($nc=0;$nc<$numcate;$nc++)
                  {
                    $datc = pg_fetch_array($concate);
                    ?>

                     <optgroup label="<?php echo $datc['cat_nombre'];?>">  
                      <?php
                    $conpro = pg_query($dbconn,"select pro_clave_int,pro_nombre from tbl_productos where cat_clave_int = '".$datc['cat_clave_int']."' and pro_activo!=2 order by LOWER (pro_nombre) ASC");
                    $numpro = pg_num_rows($conpro);
                      for($np=0;$np<$numpro;$np++)
                      {
                        $datp = pg_fetch_array($conpro);
                        $idp = $datp['pro_clave_int'];
                        $nomp = $datp['pro_nombre'];
                        $con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idp."' and pri_activo = 1 LIMIT 1");
                        $dat = pg_fetch_row($con);
                        $cimg = $dat[0];
                        $img = $dat[1];
                        if($img=="" || $img==NULL)
                        {
                          $img= "dist/img/nofoto.png";

                        }
                        else
                        {
                          $img = $urlweb.$img;
                        }
                        $imagen = '<img  src="'.$img.'" alt="message user image">';
                        ?>
                        <option  value="<?php echo $idp;?>" data-content="<?php echo $nomp;?><img class='pull-left' src='<?php echo $img;?>'  width='20' height='20'/>"><?php echo $nomp;?></option>
                        <?php
                      }
                    ?>
                     
                    </optgroup>
                    <?php
                  }
                  ?>

                </select>
                
            </div>

        </div>
        <div class="row">
            <div class="col-md-12" id="divproductosproveedor">               
            </div>
        </div>
      


	<?php
            echo "<script>CRUDPROVEEDORES('LISTAPRODUCTOS','".$idproveedor."','1');</script>";
            echo "<script>INICIALIZARCONTENIDO();</script>";
   
        }	
}
else if($opcion=="TODOSPRODUCTOS")
{
    $idcalidad = $_POST['idcalidad'];
    $idproveedor = $_POST['idproveedor'];
    ?>
    <ul class="nav nav-tabs pull-left">              
          
        <?php 
        $l = 0;
        $concla = pg_query($dbconn,"SELECT cla_clave_int,cla_nombre from tbl_clasificacion order by cla_clave_int");
         while($datcla = pg_fetch_array($concla))
        {
          $idc = $datcla['cla_clave_int'];
          $nomc  =$datcla['cla_nombre'];
          
          $con = pg_query($dbconn,"select COUNT(*) cant from tbl_proveedor_productos WHERE prv_clave_int = '".$idproveedor."' and cla_clave_int = '".$idc."'");
          $dato = pg_fetch_array($con);
          ?>
          <li class="<?php if($idcalidad==$idc){ echo "active";}?>">
          <a onclick="CRUDPROVEEDORES('LISTAPRODUCTOS','<?PHP echo $idproveedor;?>','<?php echo $idc;?>')"  data-toggle="tab" aria-expanded="false">
          <span id="cal_<?php echo $idc;?>"><?php echo $nomc." <span class='badge'>".$dato['cant']."</span>"; ?></span>
          </a></li>
          <?php
          $l++;
        }
        
        ?>            
        </ul>
    <?php
}
else if($opcion=="LISTAPRODUCTOS")
{
    $idcalidad  = $_POST['idcalidad'];
    $idproveedor = $_POST['idproveedor'];
    ?>
     <script id="jsproductosasignados" data-calidad = "<?php echo $idcalidad;?>" data-proveedor="<?php echo $idproveedor;?>" src="jsdatatable/proveedor/jsasignados.js"></script>
                <table class="table table-bordered" id="tbProductosProveedor" data-calidad = "<?php echo $idcalidad;?>" style="font-size: 11px">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Producto</th>
                            <th>Descripción</th>
                            
                            <th>Valor Compra</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tfoot>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                           
                            <th></th>
                        </tr>
                    </tfoot>
                </table>
    <?php

}
else if($opcion=="ASIGNARPRODUCTO")
{
    $idproveedor = $_POST['idproveedor'];
    $idproducto = $_POST['idproducto'];
    $idcalidad = $_POST['idcalidad'];
    $valor = $_POST['valor'];
    $ck = $_POST['ck'];
    if($ck==0)
    {
        $del = pg_query($dbconn,"DELETE FROM tbl_proveedor_productos WHERE prv_clave_int = '".$idproveedor."' and pro_clave_int = '".$idproducto."' and cla_clave_int = '".$idcalidad."'");
        if($del>0)
        {
            $res = "ok1";
            $msn = "Producto desasignado correctamente";
        }
        else
        {
            $res = "error";
            $msn = "Surgió un error al desasignar producto. Error BD(".pg_last_error($dbconn).")";
        }
    }
    else
    {
        $veri = pg_query($dbconn, "SELECT * FROM tbl_proveedor_productos WHERE prv_clave_int = '".$idproveedor."' and pro_clave_int = '".$idproducto."' and cla_clave_int = '".$idcalidad."'");
        $num = pg_num_rows($veri);
        if($num>0)
        {
            $upd = pg_query($dbconn,"UPDATE tbl_proveedor_productos SET ppr_compra = '".$valor."',ppr_usu_actualiz = '".$usuario."',ppr_fec_actualiz = '".$fecha."' where prv_clave_int = '".$idproveedor."' and pro_clave_int = '".$idproducto."' and cla_clave_int = '".$idcalidad."'");
            if($upd>0)
            {
                $res = "ok";
                $msn = "Cambios guardados correctamente";
            }
            else
            {
                $res = "error";
                 $msn = "Surgió un error al guardar cambios. Error BD(".pg_last_error($dbconn).")";
            }
        }
        else
        {
            $ins = pg_query($dbconn,"INSERT INTO tbl_proveedor_productos(prv_clave_int,pro_clave_int,ppr_compra,ppr_usu_actualiz,ppr_fec_actualiz,cla_clave_int) VALUES('".$idproveedor."','".$idproducto."','".$valor."','".$usuario."','".$fecha."','".$idcalidad."')");
            if($ins>0)
            {
                $res = "ok";
                $msn = "Producto asignado correctamente";
            }
            else
            {
                $res = "error";
                 $msn = "Surgió un error al guardar asignación. Error BD(".pg_last_error($dbconn).")";
            }
        }
    }
    $datos[] = array("res"=>$res,"msn"=>$msn);
    echo json_encode($datos);
}
 else if($opcion=="ELIMINARPROVEEDOR")

{
	$id = $_POST['id'];
	$del = pg_query($dbconn, "UPDATE tbl_proveedor SET prv_activo = 2,prv_usu_actualiz = '".$usuario."',prv_fec_actualiz = '".$fecha."' where prv_clave_int = '".$id."'");
	if($del>0)
	{
		$msn = "Proveedor eliminado";
		$res = "ok";
	}
	else
	{
		$res = "error";
		$msn = "Error BD. No se elimino el proveedor";
	}
	$datos[] = array("res"=>$res,"msn"=>$msn);
	echo json_encode($datos);
}