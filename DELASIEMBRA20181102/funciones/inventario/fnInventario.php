<?php
	session_start();
	include('../../data/Conexion.php');
	error_reporting(0);
	// variable login que almacena el login o nombre de usuario de la persona logueada
	$login= isset($_SESSION['persona']);
	// cookie que almacena el numero de identificacion de la persona logueada
	$usuario= $_COOKIE['usuario'];
	$idUsuario= $_SESSION["idusuario"];
	$clave= $_COOKIE["clave"];
	$identificacion = $_COOKIE["usIdentificacion"];
	date_default_timezone_set('America/Bogota');
	$fecha=date("Y/m/d H:i:s");

     
    $opcion = $_POST['opcion'];
    if($opcion=="LISTAINVENTARIO")
    {
    	?>
    	<script src="jsdatatable/inventario/jslistainventario.js" type="text/javascript"></script>
    	<div class="table-responsive">
    	<table class="table table-bordered table-striped" id="tbinventario">
    		<thead>
    			<tr>
    				<th class="dt-head-center">Cod</th>
    				<th class="dt-head-center">Producto</th>
    				<th class="dt-head-center">Categoria</th>
                    <th class="dt-head-center">Calidad</th>    				
    				<th class="dt-head-center">Entradas</th>
    				<th class="dt-head-center">Salidas</th>
    				<th class="dt-head-center">Disponible</th>
    			</tr>
    		</thead>
    		<tfoot>
    			<tr>
    				<th></th>
                    <th></th>
    				<th></th>
    				<th></th>    				
    				<th></th>
    				<th></th>
    				<th></th>
    			</tr>
    		</tfoot>
    	</table>
    </div>
    	<?php
    }
    else if($opcion=="LISTAENTRADAS")
    {
    	?>
    	<script src="jsdatatable/inventario/jslistaentradas.js" type="text/javascript"></script>
    	<div class="row" id="divnuevaentrada">
    	<div class="col-md-2">
    		<label for="txtfechaentrada">Fecha:</label>    		
    		<input id="txtfechaentrada" name="txtfechaentrada" class="form-control input-sm  datepicker" data-date-format="yyyy-mm-dd" autocomplete="off" value="<?php echo date('Y-m-d');?>">
    	</div>
    	<div class="col-md-2">
    		<label for="seltipoentrada">Tipo Entrada:</label>
    		<select name="seltipoentrada" id="seltipoentrada" class="form-control selectpicker" >
    			<option value="">--Seleccione--</option>
    			<?php 
    			$contie = pg_query($dbconn, "select tie_clave_int,tie_nombre from tbl_tipo_entrada");
    			while($datie = pg_fetch_array($contie))
    			{
    				$tie = $datie['tie_clave_int'];
    				$nomtie = $datie['tie_nombre'];
    				?>
    				<option value="<?php echo $tie;?>"><?php echo $nomtie; ?></option>
    				<?php
    			}
    			?>
    		</select>
    	</div>
        
        
        
    	<div class="col-md-6">
    		<label for="selproductoentrada">Producto</label>
    		<select name="selproductoentrada" id="selproductoentrada" class="form-control selectpicker" onchange="CRUDINVENTARIO('VALIDARUNIDAD','')">
    			<option value=""></option>
    			<?php
                $conpro = pg_query($dbconn,"select p.pro_clave_int,pro_nombre,pro_codigo,pro_uni_compra,pro_pes_compra,pro_mu_compra,pro_mp_compra from tbl_productos p join tbl_proveedor_productos pp on pp.pro_clave_int = p.pro_clave_int  where pro_activo!=2 and (pro_uni_compra = 1 or pro_pes_compra = 1) group by p.pro_clave_int order by pro_nombre");
                $numpro = pg_num_rows($conpro);

                  for($np=0;$np<$numpro;$np++)
                  {
                    $datp = pg_fetch_array($conpro);
                   
                    $idp = $datp['pro_clave_int'];
                    $nomp = $datp['pro_nombre']; $nompt= $nomp;
                    $codp = $datp['pro_codigo']; 
                    $uni = $datp['pro_uni_compra'];
                    $pes = $datp['pro_pes_compra'];
                    $muni = $datp['pro_mu_compra'];
                    $mpes = $datp['pro_mp_compra'];
                    if($codp!="")
                    {
                    	$nompt.="<span class='badge bg-purple pull-right'>".$codp."</span>";
                    }
                    $con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idp."' and pri_activo = 1 LIMIT 1");
                    $dat = pg_fetch_row($con);
                    $cimg = $dat[0];
                    $img = $dat[1];
                    if($img=="" || $img==NULL)
                    {
                      $img= "dist/img/nofoto.png";

                    }
                    else
                    {
                      $img = $urlweb.$img;
                    }
                    $imagen = '<img class="pull-right"  src="'.$img.'" alt="message user image">';
                    ?>
                    <option data-unidad='<?php echo $uni;?>' data-peso='<?php echo $pes;?>' data-min-unidad = '<?php echo $muni;?>' data-min-peso = '<?php echo $mpes;?>'  value="<?php echo $idp;?>" data-content="<?php echo $nompt;?><img class='pull-right' src='<?php echo $img;?>' height='20'  width='20'/>"><?php echo $nomp."-".$codp;?></option>
                    <?php
                  }
                ?>
    		</select>
    	</div>
        <div class="col-md-2">
            <label for="selcalidad">Calidad:</label>
            <select name="selcalidad" id="selcalidad" class="form-control selectpicker" onchange ="CRUDINVENTARIO('CARGARPROVEEDOR','')" >
                <option value="">--Seleccione--</option>
                <?php 
                /*$concla = pg_query($dbconn, "select cla_clave_int,cla_nombre from tbl_clasificacion");
                while($datcla = pg_fetch_array($concla))
                {
                    $cla = $datcla['cla_clave_int'];
                    $nomcla = $datcla['cla_nombre'];
                    ?>
                    <option value="<?php echo $cla;?>"><?php echo $nomcla; ?></option>
                    <?php
                }*/
                ?>
            </select>
        </div>
        <div class="col-md-4">
            <label for="selproveedor">Proveedor:</label>
            <select name="selproveedor" id="selproveedor" class="form-control selectpicker" >
                <option value="">--Seleccione--</option>
                <?php 
               /* $conprov = pg_query($dbconn, "select prv_clave_int,prv_nombre,prv_nro_docum,prv_email from tbl_proveedor where prv_activo = 1");
                while($datprov = pg_fetch_array($conprov))
                {
                    $prov = $datprov['prv_clave_int'];
                    $nomprov = $datprov['prv_nombre'];
                    $emaprov = $datprov['prv_email'];
                    $docprov = $datprov['prv_nro_docum'];
                    ?>
                    <option data-subtext="<?php echo $emaprov;?>" value="<?php echo $prov;?>"><?php echo $nomprov."-".$docprov; ?></option>
                    <?php
                }*/
                ?>
            </select>
        </div>
    
    	<div class="col-md-2" id="divcantentrada" style="display: none;">
    		<label for="txtcantentrada">Cantidad:</label>
    		<input id="txtcantentrada" name="txtcantentrada" class="form-control input-sm" autocomplete="off" type="number" min="" onkeypress="return validar_texto(event)">
    	</div>
        <div class="col-md-2" id="divpesoentrada" style="display:none">
            <label for="txtpesentrada">Peso total:</label>
            <input id="txtpesoentrada" name="txtpesoentrada" class="form-control input-sm" autocomplete="off"  type="number" min="" onkeypress="return validar_texto(event)" onkeyup="CALCULARCANTIDAD()" onchange="CALCULARCANTIDAD()">
        </div>
        <div class="col-md-1" id="divmedidaentrada" style="display:none"    >
            <label for="selmedida">Medida:</label><br>
            <select name="selmedida" id="selmedida" class="selectpicker" data-width="80px" onchange="CALCULARCANTIDAD()">
                <option value="0">UND</option>
                <option value="1">GR</option>
                <option value="2">KG</option>              
            </select>
        </div>
        <div class="col-md-2" id="divpesounidad" style="display:none">
            <label for="txtpesunidad">Peso X Und:</label>
            <div class="has-feedback">
            <input id="txtpesunidad" name="txtpesunidad" class="form-control input-sm" autocomplete="off"  type="text" min="" onkeypress="return validar_texto(event)" onkeyup="CALCULARCANTIDAD()" onchange="CALCULARCANTIDAD()">
            <span class=" form-control-feedback">GR</span>
            </div>
            
        </div>
        <div class="col-md-1" id="divtotalunidad" style="display:none">
            <label for="">Total  Und:</label>
            <span class="form-control  input-sm" id="spanunidades"></span>
        </div>
    	<div class="col-md-2">
    		<label for="txtvalentrada">Vr.Unidad:</label>
    		<input id="txtvalentrada" name="txtvalentrada" class="form-control input-sm currency2" autocomplete="off" type="text" onkeypress="return validar_texto(event)">
    	</div>
    	<div class="col-md-1">
    		<br>
    		<a id="btnguardarentrada" onclick="CRUDINVENTARIO('GUARDARENTRADA','')" class="btn btn-primary" data-toggle='tooltip' title="Guardar Entrada"><i class="fa fa-save"></i></a>
    	</div>
    </div>
    	<div class="table-responsive">
    	<table class="table table-bordered table-striped" id="tbentradas">
    		<thead>
    			<tr>
    				<th></th>
    				<th>Cod</th>
    				<th>Producto</th>
    				<th>Categoria</th>    				
    				<th>Cantidad</th>
    			</tr>
    		</thead>
    		<tfoot>
    			<tr>
    				<th></th>
    				<th></th>
    				<th></th>    				
    				<th></th>
    				<th></th>    				
    			</tr>
    		</tfoot>
    	</table>
    </div>  
    
    	<?php
    	echo "<script>INICIALIZARCONTENIDO();</script>";
    }
    else if($opcion=="LISTASALIDAS")
    {
    	?>
    	<script src="jsdatatable/inventario/jslistasalidas.js" type="text/javascript"></script>
    	 <div class="row" id="divnuevasalida">
    	<div class="col-md-2">
    		<label for="txtfechasalida">Fecha:</label>    		
    		<input id="txtfechasalida" name="txtfechasalida" class="form-control input-sm  datepicker" data-date-format="yyyy-mm-dd" autocomplete="off">
    	</div>
    	<div class="col-md-2">
    		<label for="seltiposalida">Tipo Salida</label>
    		<select onchange="CRUDINVENTARIO('VALIDARMOTIVO','')" name="seltiposalida" id="seltiposalida" class="form-control selectpicker" title="Seleccione" >

    			<option value="otro">Otro</option>
    			
    		</select>
    	</div>
         <div class="col-md-2">
            <label for="selcalidad">Calidad:</label>
            <select name="selcalidadsalida" id="selcalidadsalida" class="form-control selectpicker" >
                <option value="">--Seleccione--</option>
                <?php 
                $concla = pg_query($dbconn, "select cla_clave_int,cla_nombre from tbl_clasificacion");
                while($datcla = pg_fetch_array($concla))
                {
                    $cla = $datcla['cla_clave_int'];
                    $nomcla = $datcla['cla_nombre'];
                    ?>
                    <option value="<?php echo $cla;?>"><?php echo $nomcla; ?></option>
                    <?php
                }
                ?>
            </select>
        </div>
    	<div class="col-md-6">
    		<label for="selproductosalida">Producto</label>
    		<select name="selproductosalida" id="selproductosalida" class="form-control selectpicker" onchange="CRUDINVENTARIO('VALIDARUNIDAD2','')">
    			<option value=""></option>

    			<?php
                $conpro = pg_query($dbconn,"select p.pro_clave_int,p.pro_nombre,p.pro_uni_venta,p.pro_pes_venta,p.pro_mu_venta,p.pro_mp_venta from tbl_productos p join tbl_inventario i on i.pro_clave_int = p.pro_clave_int where pro_activo!=2 group by p.pro_clave_int order by pro_nombre");
                $numpro = pg_num_rows($conpro);

                  for($np=0;$np<$numpro;$np++)
                  {
                    $datp = pg_fetch_array($conpro);
                    $idp = $datp['pro_clave_int'];
                    $nomp = $datp['pro_nombre'];
                    $uni = $datp['pro_uni_venta'];
                    $pes = $datp['pro_pes_venta'];
                    $muni = $datp['pro_mu_venta'];
                    $mpes = $datp['pro_mp_venta'];

                    $con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idp."' and pri_activo = 1 LIMIT 1");
                    $dat = pg_fetch_row($con);
                    $cimg = $dat[0];
                    $img = $dat[1];
                    if($img=="" || $img==NULL)
                    {
                      $img= "dist/img/nofoto.png";

                    }
                    else
                    {
                      $img = $urlweb.$img;
                    }
                    $imagen = '<img  src="'.$img.'" alt="message user image">';
                    ?>
                    <option data-unidad = '<?php echo $uni;?>' data-peso = '<?php echo $pes;?>' data-min-unidad ='<?php echo $muni; ?>' data-min-peso ='<?php echo $mpes;?>'  value="<?php echo $idp;?>" data-content="<?php echo $nomp;?><img src='<?php echo $img;?>' height='20'  width='20'/>"><?php echo $nomp;?></option>
                    <?php
                  }
                ?>
    		</select>
    	</div>
    	<div class="col-md-2" id="divcantsalida" style="display: none">
    		<label for="txtcantsalida">Cantidad:</label>
    		<input id="txtcantsalida" name="txtcantsalida" class="form-control input-sm" autocomplete="off" type="text" onkeypress="return validar_texto(event)">
    	</div>
        <div class="col-md-2" id="divpesosalida" style="display: none">
            <label for="txtpesosalida">Peso:</label>
            <input id="txtpesosalida" name="txtpesosalida" class="form-control input-sm" autocomplete="off" type="text" onkeypress="return validar_texto(event)">
        </div>
    	<div class="col-md-1"><br>
    		<a id="btnguardarsalida" onclick="CRUDINVENTARIO('GUARDARSALIDA','')" class="btn btn-primary" data-toggle="tooltip" title="Guardar Salida"><i class="fa fa-save"></i></a>
    	</div>
    </div>
    	<div class="table-responsive">
    	<table class="table table-bordered table-striped" id="tbsalidas">
    		<thead>
    			<tr>
    				<th></th>
    				<th>Cod</th>
    				<th>Producto</th>
    				<th>Categoria</th>    				
    				<th>Cantidad</th>  
    			</tr>
    		</thead>
    		<tfoot>
    			<tr>
    				<th></th>
    				<th></th>
    				<th></th>    				
    				<th></th>
    				<th></th>    				
    			</tr>
    		</tfoot>
    	</table>
    </div>
   
    	<?php
    	echo "<script>CRUDINVENTARIO('CARGARMOTIVOS','');</script>";
    	echo "<script>INICIALIZARCONTENIDO();</script>";

    }
    else if($opcion=="GUARDARENTRADA")
    {
    	$idproducto = $_POST['idproducto'];
    	$fecent = $_POST['fecent'];
    	$tipent = $_POST['tipent'];
    	$canent = $_POST['cantent'];
        $pesent = $_POST['pesoent'];
    	$valent = $_POST['valent'];
        $provent = $_POST['provent'];
        $calent = $_POST['calent'];
        $pes = $_POST['pes'];
        $uni = $_POST['uni'];
        $pesund = $_POST['pesund']; if($pesund<=0 || $pesund==NULL){ $pesund = 0; }

        $med = $_POST['med'];

        if($pes==1 and $uni<=0)
        {
            if($med==1)
            {
                $canent = $pesent/$pesund ;
            }
            else if($med==2)
            {
                $canent = ($pesent*1000)/$pesund;
            }
        }

        if($med==2)
        {
            $pesoentrada = $pesent * 1000;//convertir de kilogramos a gramos
        }
        else
        {
            $pesoentrada = $pesent;
        }

    	//verificar inventario
    	$veriinv = pg_query($dbconn, "SELECT inv_clave_int FROM tbl_inventario WHERE pro_clave_int = '".$idproducto."' and cla_clave_int = '".$calent."'");
    	$numvinv = pg_num_rows($veriinv);
    	if($numvinv>0)
    	{
    		$datinv = pg_fetch_array($veriinv);
    		$idinventario =$datinv['inv_clave_int'];
    	}
    	else
    	{
    		$insinv = pg_query($dbconn, "INSERT INTO tbl_inventario (pro_clave_int,inv_usu_actualiz,inv_fec_actualiz,cla_clave_int) VALUES('".$idproducto."','".$usuario."','".$fecha."','".$calent."')");
    		if($insinv>0)
    		{
    			$uid = pg_fetch_row(pg_query($dbconn,"SELECT setval('inv_id_seq',nextval('inv_id_seq')-1) as id;"));
				$idinventario = $uid[0];
    		}
    		else
    		{
    			$idinventario = 0;
    		}
    	}

        //verificar inventario proveedor
        $veriinvprov = pg_query($dbconn, "SELECT pve_clave_int FROM tbl_proveedor_entradas WHERE pro_clave_int = '".$idproducto."' and prv_clave_int = '".$provent."' and cla_clave_int = '".$calent."'");
        $numvinvprov = pg_num_rows($veriinvprov);
        if($numvinvprov>0)
        {
            $datinvprov = pg_fetch_array($veriinvprov);
            $idinventarioprov =$datinvprov['pve_clave_int'];
        }
        else
        {
            $insinvprov = pg_query($dbconn, "INSERT INTO tbl_proveedor_entradas (pro_clave_int,prv_clave_int,pve_usu_actualiz,pve_fec_actualiz,cla_clave_int) VALUES('".$idproducto."','".$provent."','".$usuario."','".$fecha."','".$calent."')");
            if($insinvprov>0)
            {
                $uid = pg_fetch_row(pg_query($dbconn,"SELECT setval('pve_id_seq',nextval('pve_id_seq')-1) as id;"));
                $idinventarioprov = $uid[0];
            }
            else
            {
                $idinventarioprov = 0;
            }
        }
    	if($idinventario<=0)
    	{
    		$res = "error";
    		$msn = "No hay un inventario del producto al que desea registrarle una entrada. Error BD(".pg_last_error($dbconn).")";
    	}
        else if($idinventarioprov<=0)
        {
            $res = "error";
            $msn = "No hay un inventario del proveedor y producto al que desea registrarle una entrada. Error BD(".pg_last_error($dbconn).")";
        }
    	else
    	{
    		$insent = pg_query($dbconn, "INSERT INTO tbl_entradas(pro_clave_int,ent_fecha,tie_clave_int,ent_cantidad,ent_valor,ent_usu_actualiz,ent_fec_actualiz,prv_clave_int,ent_peso,ent_real,ent_medida,cla_clave_int,ent_pes_unidad) VALUES('".$idproducto."','".$fecent."','".$tipent."','".$canent."','".$valent."','".$usuario."','".$fecha."','".$provent."','".$pesoentrada."','".$pesent."','".$med."','".$calent."','".$pesund."')");
    		if($insent>0)
    		{
    			$updinv = pg_query($dbconn,"update tbl_inventario set inv_entradas = inv_entradas + ".$canent.", inv_usu_actualiz = '".$usuario."',inv_fec_actualiz = '".$fecha."',inv_entradas_peso = inv_entradas_peso + ".$pesoentrada." WHERE inv_clave_int = '".$idinventario."'");

                $updinvprov = pg_query($dbconn,"update tbl_proveedor_entradas set pve_cantidad = pve_cantidad + ".$canent.", pve_usu_actualiz = '".$usuario."',pve_fec_actualiz = '".$fecha."',pve_peso = pve_peso + ".$pesoentrada." WHERE pve_clave_int = '".$idinventarioprov."'");

    			if($updinv>0)
    			{
    				$res = "ok";

                    //actualizacion de costos en mercado en la calidad seleccionada
                    $conme = pg_query($dbconn, "SELECT mer_clave_int,mer_porcentaje, mer_clasificacion FROM tbl_mercado WHERE mer_activo = 1");
                    $numme = pg_num_rows($conme);
                    if($numme>0)
                    {
                        for($nm=0;$nm<$numme;$nm++)
                        {
                            $datme = pg_fetch_array($conme);
                            $idm = $datme['mer_clave_int'];
                            $porc = $datme['mer_porcentaje'];
                            $clas = explode(",", $datme['mer_clasificacion']);
                            if(in_array($calent, $clas))
                            {
                                $val = $valent;
                                $ven = $val + ($val*($porc/100));
                                $ven = ceil($ven/50)*50;
                                if($ven<=0 || $ven==NULL){ $ven = 0; }
                                $veripre = pg_query($dbconn, "SELECT * FROM tbl_precios_mercado WHERE pro_clave_int = '".$idproducto."' and mer_clave_int ='".$idm."' and cla_clave_int = '".$calent."'");
                                $numvpre = pg_num_rows($veripre);
                                if($numvpre>0)
                                {
                                    //$sqlpre = pg_query($dbconn,"UPDATE tbl_precios_mercado SET prm_costo = '".$val."',prm_venta = '".$ven."',prm_usu_actualiz = '".$usuario."',prm_fec_actualiz = '".$fecha."' WHERE pro_clave_int = '".$idproducto."' and mer_clave_int ='".$idm."' and cla_clave_int = '".$calent."'"); 

                                    $sqlpre = pg_query($dbconn,"UPDATE tbl_precios_mercado SET prm_peso_unidad = '".$pesund."',prm_usu_actualiz = '".$usuario."',prm_fec_actualiz = '".$fecha."' WHERE pro_clave_int = '".$idproducto."' and mer_clave_int ='".$idm."' and cla_clave_int = '".$calent."'");                                   
                                }
                                else
                                {
                                    //$sqlpre = pg_query($dbconn, "INSERT INTO tbl_precios_mercado(pro_clave_int,mer_clave_int,cla_clave_int,prm_costo,prm_usu_actualiz,prm_fec_actualiz,prm_venta) VALUES('".$idproducto."','".$idm."','".$calent."','".$val."','".$usuario."','".$fecha."','".$ven."')"); 

                                    $sqlpre = pg_query($dbconn, "INSERT INTO tbl_precios_mercado(pro_clave_int,mer_clave_int,cla_clave_int,prm_peso_unidad,prm_usu_actualiz,prm_fec_actualiz) VALUES('".$idproducto."','".$idm."','".$calent."','".$pesund."','".$usuario."','".$fecha."')");                                    
                                }
                            }
                        }   
                    }

    				$msn = "Entrada guardada correctamente";
    			}
    			else
    			{
    				$res = "error";
    				$msn = "Se guardo la entrada pero no se actualizo el inventario. Error BD(".pg_last_error($dbconn).")";
    			}
    		}
    		else
    		{
				$res = "error";
				$msn = "Se guardo la entrada pero no se actualizo el inventario. Error BD(".pg_last_error($dbconn).")";
    		}
    	}
    	$sql = "INSERT INTO tbl_entradas(pro_clave_int,ent_fecha,tie_clave_int,ent_cantidad,ent_valor,ent_usu_actualiz,ent_fec_actualiz,ent_peso) VALUES('".$idproducto."','".$fecent."','".$tipent."','".$cantent."','".$valent."','".$usuario."','".$fecha."','".$pesent."')";
    	$datos[] = array('res' => $res , 'msn' => $msn, "sql"=>$sql );
    	echo json_encode($datos);
    }
    else if($opcion=="NUEVOMOTIVOSALIDA")
    {
    	?>
    	<div class="row">
    		<div class="col-md-12">
    			<label>Motivo Salida:</label>
    			<input type="text" id="txtmotivo" name="txtmotivo" value="" class="form-control input-sm">
    		</div>
    	</div>
    	<div class="row">
    		<div class="col-md-12">
    			<a class="btn btn-success btn-block btn-sm" onclick="CRUDINVENTARIO('GUARDARMOTIVOSALIDA','')">Guardar</a>
    		</div>
    	</div>
    	<?php
    }
    else if($opcion=="GUARDARMOTIVOSALIDA")
    {
    	$mot = $_POST['mot'];
    	$veri = pg_query($dbconn,"select * from tbl_motivo_salida where mos_nombre = '".$mot."'");
    	$numv = pg_num_rows($veri);
    	$idm = 0;
    	if($numv>0)
    	{
    		$res = "error";
    		$msn = "El motivo de salida ingresado ya existe";
    	}
    	else
    	{
    		$ins = pg_query($dbconn, "INSERT INTO tbl_motivo_salida(mos_nombre,mos_usu_actualiz,mos_fec_actualiz) values('".$mot."','".$usuario."','".$fecha."')");
    		if($ins>0)
    		{
    			$uid = pg_fetch_row(pg_query($dbconn,"SELECT setval('mos_id_seq',nextval('mos_id_seq')-1) as id;"));
    			$idm = $uid[0];
    			$res = "ok";
    			$msn = "Motivo registrado correctamente";
    		}
    		else
    		{
    			$res = "error";
    			$msn = "Surgió un error al guardar el motivo de salida. Error BD(".pg_last_error($dbconn).")";
    		}
    	}
    	$datos[] = array("res"=>$res,"msn" =>$msn,"idm"=>$idm);
    	echo json_encode($datos);
    }
    else if($opcion=="CARGARMOTIVOS")
    {
    	$sql = pg_query($dbconn, "select mos_clave_int, mos_nombre from tbl_motivo_salida order by mos_nombre asc");
    	$num = pg_num_rows($sql);
    	if($num>0)
    	{
    		for($k=0;$k<$num;$k++)
    		{
    			$dat = pg_fetch_array($sql);
    			$idm = $dat['mos_clave_int'];
    			$mot = $dat['mos_nombre'];
    			$datos[] = array("res"=>"si","id"=>$idm,"literal"=>$mot);
    		}
    	}
    	else
    	{
    		$datos[] = array("res"=>"no");
    	}
    	echo json_encode($datos);
    }
    else if($opcion=="GUARDARSALIDA")
    {
    	$idproducto = $_POST['idproducto'];
    	$fecsal = $_POST['fecsal'];
    	$tipsal = $_POST['tipsal'];
    	$cansal = $_POST['cansal'];
        $calsal = $_POST['calsal'];
        $pessal = $_POST['pesosal']; if($pessal<=0 || $pessal==NULL){ $pessal = 0;}
        $cansal = $_POST['calsal']; if($cansal<=0 || $cansal==NULL){ $cansal = 0;}
 
    	//verificar inventario
    	$veriinv = pg_query($dbconn, "SELECT inv_clave_int,inv_entradas,inv_salidas,inv_entradas_peso,inv_salidas_peso FROM tbl_inventario WHERE pro_clave_int = '".$idproducto."' and cla_clave_int = '".$calsal."'");
    	$numvinv = pg_num_rows($veriinv);
    	if($numvinv>0)
    	{
    		$datinv = pg_fetch_array($veriinv);
    		$idinventario =$datinv['inv_clave_int'];
    		$entrada = $datinv['inv_entradas'];
    		$salidas = $datinv['inv_salidas'];
            $entradapeso = $datinv['inv_entradas_peso'];
            $salidaspeso = $datinv['inv_salidas_peso'];
    	}
    	else
    	{
    		$insinv = pg_query($dbconn, "INSERT INTO tbl_inventario (pro_clave_int,inv_usu_actualiz,inv_fec_actualiz,cla_clave_int) VALUES('".$idproducto."','".$usuario."','".$fecha."','".$calsal."')");
    		if($insinv>0)
    		{
    			$uid = pg_fetch_row(pg_query($dbconn,"SELECT setval('inv_id_seq',nextval('inv_id_seq')-1) as id;"));
				$idinventario = $uid[0];
    		}
    		else
    		{
    			$idinventario = 0;
    		}
    	}
        //cantidades
    	$salidasactual = $cansal + $salidas;
    	$disponible = $entrada - $salidasactual;
    	$disponibleactual = $entrada - $salidas;
        //pesp
        $salidasactualpeso = $pessal + $salidaspeso;
        $disponiblepeso = $entradapeso - $salidasactualpeso;
        $disponibleactualpeso = $entradapeso - $salidaspeso; 
    	


    	if($idinventario<=0)
    	{
    		$res = "error";
    		$msn = "No hay un inventario del producto al que desea registrarle una salida. Error BD(".pg_last_error($dbconn).")";
    	}
    	else if($disponible<0  and $disponiblepeso<0)
    	{
    		$res = "error";
    		$msn = "Excedio la cantidad disponible para darle salidas. Cantidad disponible en unidades: ".$disponibleactual."- Cantidad Disponible en Peso: ".$disponibleactualpeso;
    	}
    	else
    	{

    		$insent = pg_query($dbconn, "INSERT INTO tbl_salidas(pro_clave_int,sal_fecha,mos_clave_int,sal_cantidad,sal_usu_actualiz,sal_fec_actualiz,sal_peso,cla_clave_int) VALUES('".$idproducto."','".$fecsal."','".$tipsal."','".$cansal."','".$usuario."','".$fecha."','".$pessal."','".$calsal."')");
    		if($insent>0)
    		{
    			$updinv = pg_query($dbconn,"update tbl_inventario set inv_salidas = inv_salidas + ".$cansal.", inv_usu_actualiz = '".$inv_usu_actualiz."',inv_fec_actualiz = '".$fecha."',inv_salidas_peso = inv_salidas_peso + ".$pessal." WHERE inv_clave_int = '".$idinventario."'");
    			if($updinv>0)
    			{
    				$res = "ok";
    				$msn = "Salida guardada correctamente";
    			}
    			else
    			{
    				$res = "error";
    				$msn = "Se guardo la salida pero no se actualizo el inventario. Error BD(".pg_last_error($dbconn).")";
    			}
    		}
    		else
    		{
				$res = "error";
				$msn = "Se guardo la salida pero no se actualizo el inventario. Error BD(".pg_last_error($dbconn).")";
    		}
    	}
    	$sql = "INSERT INTO tbl_salidas(pro_clave_int,sal_fecha,mos_clave_int,sal_cantidad,sal_usu_actualiz,sal_fec_actualiz,sal_peso,cla_clave_int) VALUES('".$idproducto."','".$fecsal."','".$tipsal."','".$cansal."','".$usuario."','".$fecha."','".$pessal."','".$calsal."')";
    	$datos[] = array('res' => $res , 'msn' => $msn, "sql"=>$sql );
    	echo json_encode($datos);
    }
    else if($opcion=="CARGARPROVEEDOR")
    {
        $idproducto = $_POST['idproducto'];
        $idcalidad = $_POST['idcalidad'];
        $conpro = pg_query($dbconn, "SELECT p.prv_clave_int,p.prv_nombre,pp.ppr_compra FROM tbl_proveedor p JOIN tbl_proveedor_productos pp ON pp.prv_clave_int = p.prv_clave_int where pp.pro_clave_int = '".$idproducto."' and cla_clave_int = '".$idcalidad."' order by pp.ppr_compra ASC");
        $numpro = pg_num_rows($conpro);
        if($numpro>0)
        {
           while($datpro =  pg_fetch_array($conpro))
           {
              $datos[] = array("id"=>$datpro['prv_clave_int'],"literal"=>$datpro['prv_nombre'],"valor"=>"$".number_format($datpro['ppr_compra'],0,',',','),"res"=>"si");
           }
        }
        else
        {
            $datos[] = array("res"=>"no");
        }
        echo json_encode($datos);
    }
    else if($opcion=="CARGARCALIDAD")
    {
        $idproducto = $_POST['idproducto'];
        $concla = pg_query($dbconn, "SELECT c.cla_clave_int,c.cla_nombre FROM tbl_clasificacion c JOIN tbl_proveedor_productos pp on pp.cla_clave_int = c.cla_clave_int where  pp.pro_clave_int = '".$idproducto."' group by c.cla_clave_int");
        $numc = pg_num_rows($concla);
        if($numc>0)
        {
           while($datcla =  pg_fetch_array($concla))
           {
              $datos[] = array("id"=>$datcla['cla_clave_int'],"literal"=>$datcla['cla_nombre'],"res"=>"si");
           }
        }
        else
        {
            $datos[] = array("res"=>"no");
        }
        echo json_encode($datos);

    }