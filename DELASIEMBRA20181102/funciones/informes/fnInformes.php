<?php
include('../../data/Conexion.php');
session_start();
error_reporting(0);
$IP = $_SERVER['REMOTE_ADDR'];
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_SESSION["idusuario"];
$identificacion = $_COOKIE["usIdentificacion"];
date_default_timezone_set('America/Bogota');
setlocale(LC_ALL,"es_ES.utf8","es_ES","esp");
$fecha=date("Y/m/d H:i:s");
$horaa = date("H:i");
$conusu= pg_query($dbconn,"SELECT mer_clave_int,dir_clave_int,usu_ult_telefono,prf_clave_int,usu_telefono from tbl_usuario where usu_clave_int = '".$idUsuario."'");
$datusu = pg_fetch_array($conusu);
$idmercado = $datusu['mer_clave_int']; if($idmercado<=0 || $idmercado==null){ $idmercado = 3;}
$ultimadireccion = $datusu['dir_clave_int'];
$ultimotelefono = $datusu['usu_ult_telefono'];
$usutelefono = $datusu['usu_telefono'];
if($ultimotelefono==""){
	$ultimotelefono = $usutelefono;
}
$perfil = $datusu['prf_clave_int'];

$opcion = $_POST['opcion'];
if($opcion=="INFORMEDESPACHOS")
{
	$con = pg_query($dbconn, "SELECT min(ped_fec_programada) fmin,max(ped_fec_programada) fmax FROM tbl_pedidos WHERE ped_estado in( 1,2) ");
	$dat = pg_fetch_array($con);
	$fmin = $dat['fmin'];
	$fmax = $dat['fmax'];
?>
<div class="row">
	<div class="col-md-2">
		<label>Desde</label>
		<input type="date" id="busdesde" name="busdesde" class="form-control" onchange="CRUDINFORMES('LISTAINFORMEDESPACHOS')" value="<?php echo $fmin;?>" min="<?php echo $fmin;?>" max="<?php echo $fmax;?>">
	</div>
	<div class="col-md-2">
		<label>Hasta</label>
		<input type="date" id="bushasta" name="bushasta" class="form-control" onchange="CRUDINFORMES('LISTAINFORMEDESPACHOS')" value="<?php echo $fmax;?>" max="<?php echo $fmax;?>">
	</div>
</div>
<div class="row">
	<div class="col-md-12" id="divlistadespacho"></div>
</div>
<?php

echo "<script>CRUDINFORMES('LISTAINFORMEDESPACHOS');</script>";
}
else if($opcion=="LISTAINFORMEDESPACHOS")
{
	?>
	<script src="jsdatatable/informes/jsinformedespacho.js"></script>
	<table class="table table-bordered table-striped compact responsive" id="tbDespachos" style="width: 100%">
		<thead>
			<tr>
				<th>Fecha</th>
								
				<th>Producto</th>
				<th>Calidad</th>
				<th>Tamaño</th>
				<th>Maduracion</th>
				<th>Cant.Total</th>
				<th>Peso.Estimado</th>
			</tr>
		</thead>
		<tfoot>
			<tr>
				<th></th>
							
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
				<th></th>
			</tr>
		</tfoot>
	</table>
	<?php
}