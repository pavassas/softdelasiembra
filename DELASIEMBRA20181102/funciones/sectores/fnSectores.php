<?php
include ("../../data/Conexion.php");
session_start();
error_reporting(0);
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_SESSION["idusuario"];
$clave= $_COOKIE["clave"];
$identificacion = $_COOKIE["usIdentificacion"];
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");

$opcion = $_POST['opcion'];
$ventana = $_POST['ventana'];
if($opcion=="NUEVO" and  $ventana=="BARRIO")
{
?>
  <form name="form1" id="form1" class="form-horizontal">
    <div class="form-group">
         <div class="col-xs-12"><strong>Departamento:<span class="symbol required"></span></strong>
         <select name="seldepartamento" id="seldepartamento" class="form-control input-sm selectpicker" onChange="cargarsector('selmunicipio','seldepartamento','CARGARMUNICIPIOS')" style="width:100%">
         <option value="">--seleccione--</option>
         <?php
        $con = pg_query($dbconn,"select d.dep_clave_int ids ,d.dep_nombre as dep from tbl_departamento d where d.est_clave_int=1 order by LOWER(d.dep_nombre) asc");
        while($dat = pg_fetch_array($con))
        {
            ?>
              <option value="<?php echo $dat['ids'];?>"><?php echo $dat['dep'];?></option>
              <?php
        }
      ?>
         </select>
         </div>
    </div>

    <div class="form-group">
         <div class="col-xs-12"><strong>Municipio:<span class="symbol required"></span></strong>
         <select name="selmunicipio" id="selmunicipio" class="form-control input-sm selectpicker" onChange="cargarsector('selsector','selmunicipio','CARGARSECTORES')" style="width:100%">
         <option value="">--seleccione--</option>        
         </select>
         </div>
    </div>
    
    <div class="form-group">
         <div class="col-xs-12"><strong>Sector:<span class="symbol required"></span></strong>
         <select name="selsector" id="selsector" class="form-control input-sm selectpicker" onChange="" style="width:100%">
         <option value="">--seleccione--</option>
         <?php
        $con = pg_query($dbconn,"select d.sec_clave_int ids ,d.sec_nombre as sec from tbl_sector d where d.est_clave_int=1 order by sec");
        while($dat = pg_fetch_array($con))
        {
            ?>
              <option value="<?php echo $dat['ids'];?>"><?php echo $dat['sec'];?></option>
              <?php
        }
      ?>
         </select>
         </div>
    </div>
    <div class="form-group">
         <div class="col-md-6"><strong>Nombre:<span class="symbol required"></span></strong>
         <input  name="txtbarrio" id="txtbarrio" class="form-control input-sm" onChange="" type="text" autocomplete="off" placeholder="Ingrese nombre del barrio">
        
         </div>
          <div class="col-md-6"><strong>Estado:</strong><br>
        
                 <label for="opcion1"> <input type="radio" name="radestado" id="opcion1"  checked value="1">
                 Activo</label>
                
                 <label> <input type="radio" name="radestado" id="opcion2" value="0">
                Inactivo
                </label>
         </div>
    </div>
  </form>
  <?php  
   echo "<script>INICIALIZARCONTENIDO();</script>";
}
else if($opcion=="NUEVO" and  $ventana=="SECTOR")
{
  ?>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">

    <div class="form-group">
         <div class="col-xs-12"><strong>Departamento:<span class="symbol required"></span></strong>
         <select name="seldepartamento" id="seldepartamento" class="form-control input-sm selectpicker" onChange="cargarsector('selmunicipio','seldepartamento','CARGARMUNICIPIOS')" style="width:100%">
         <option value="">--seleccione--</option>
         <?php
        $con = pg_query($dbconn,"select d.dep_clave_int ids ,d.dep_nombre as dep from tbl_departamento d where d.est_clave_int=1 order by LOWER(d.dep_nombre) asc");
        while($dat = pg_fetch_array($con))
        {
            ?>
              <option value="<?php echo $dat['ids'];?>"><?php echo $dat['dep'];?></option>
              <?php
        }
      ?>
         </select>
         </div>
    </div>

    <div class="form-group">
         <div class="col-xs-12"><strong>Municipio:<span class="symbol required"></span></strong>
         <select name="selmunicipio" id="selmunicipio" class="form-control input-sm selectpicker" onChange="" style="width:100%">
         <option value="">--seleccione--</option>        
         </select>
         </div>
    </div>
    <div class="form-group">
       <div class="col-md-6"><strong>Nombre:<span class="symbol required"></span></strong>
       <input  name="txtsector" id="txtsector" class="form-control input-sm" type="text" autocomplete="off" placeholder="Ingrese nombre del sector">
      
       </div>
       <div class="col-md-6"><strong>Tiempo Entrega:<span class="symbol required"></span></strong>
        <div class="input-group">
          <input  name="txthoras" id="txthoras" class="form-control input-sm" type="number" min="1" max="24">
          <span class="input-group-addon">hor</span>
          <input  name="txtminutos" id="txtminutos" class="form-control input-sm" type="number" min="0" max="59">
          <span class="input-group-addon">min</span>
        </div>
       </div>
       <div class="col-md-6"><strong>Domicilio:<span class="symbol required"></span></strong>
       <input  name="txtdomicilio" id="txtdomicilio" class="form-control input-sm currency2" type="text" autocomplete="off" placeholder="Ingrese costo del domicilio">      
       </div>
        <div class="col-md-6"><strong>Monto máximo para domicilio gratis:<span class="symbol required"></span></strong>
       <input  name="txtmonto" id="txtmonto" class="form-control input-sm currency2" type="text" autocomplete="off" placeholder="Ingrese monto">      
       </div>
        <div class="col-md-6"><strong>Estado:</strong><br>      
           <label for="opcion1"> <input type="radio" name="radestado" id="opcion1"  checked value="1">
           Activo</label>          
           <label> <input type="radio" name="radestado" id="opcion2"  value="0">
          Inactivo
          </label>
       </div>
    </div>
  </form>
  <?php 
     echo "<script>INICIALIZARCONTENIDO();</script>"; 
}
else if($opcion=="NUEVO" and  $ventana=="DEPARTAMENTO")
{
  ?>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">
    <div class="form-group">
       <div class="col-md-6"><strong>Nombre:<span class="symbol required"></span></strong>
       <input  name="txtdepartamento" id="txtdepartamento" class="form-control input-sm" type="text" autocomplete="off" placeholder="Ingrese nombre del departamento">
      
       </div>      
        <div class="col-md-6"><strong>Estado:</strong><br>      
           <label for="opcion1"> <input type="radio" name="radestado" id="opcion1"  checked value="1">
           Activo</label>          
           <label> <input type="radio" name="radestado" id="opcion2" value="0">
          Inactivo
          </label>
       </div>
    </div>
  </form>
  <?php 
     echo "<script>INICIALIZARCONTENIDO();</script>"; 
} 
else
if($opcion=="NUEVO" and  $ventana=="MUNICIPIO")
{
?>
  <form name="form1" id="form1" class="form-horizontal">
    
    <div class="form-group">
         <div class="col-xs-12"><strong>Departamento:<span class="symbol required"></span></strong>
         <select name="seldepartamento" id="seldepartamento" class="form-control input-sm selectpicker" onChange="" style="width:100%">
         <option value="">--seleccione--</option>
         <?php
        $con = pg_query($dbconn,"select d.dep_clave_int ids ,d.dep_nombre as dep from tbl_departamento d where d.est_clave_int=1 order by LOWER(d.dep_nombre) asc");
        while($dat = pg_fetch_array($con))
        {
            ?>
              <option value="<?php echo $dat['ids'];?>"><?php echo $dat['dep'];?></option>
              <?php
        }
      ?>
         </select>
         </div>
    </div>
    <div class="form-group">
         <div class="col-md-6"><strong>Nombre:<span class="symbol required"></span></strong>
         <input  name="txtmunicipio" id="txtmunicipio" class="form-control input-sm" onChange="" type="text" autocomplete="off" placeholder="Ingrese nombre del municipio">
        
         </div>
          <div class="col-md-6"><strong>Estado:</strong><br>
        
                 <label for="opcion1"> <input type="radio" name="radestado" id="opcion1"  checked value="1">
                 Activo</label>
                
                 <label> <input type="radio" name="radestado" id="opcion2" value="0">
                Inactivo
                </label>
         </div>
    </div>
  </form>
  <?php  
   echo "<script>INICIALIZARCONTENIDO();</script>";
}
else if($opcion=="EDITAR" and  $ventana=="BARRIO")
{
	  $id = $_POST['id'];
	  $coninfo = pg_query($dbconn,"select c.bar_nombre as nom,s.sec_clave_int as sec,c.est_clave_int as est,d.dep_clave_int,m.mun_clave_int from tbl_barrio c join tbl_sector s on s.sec_clave_int = c.sec_clave_int join tbl_municipio m on m.mun_clave_int = s.mun_clave_int left outer join tbl_departamento d on d.dep_clave_int = m.dep_clave_int   where c.bar_clave_int = '".$id."' limit 1");
	  $datinfo = pg_fetch_array($coninfo);
	  $nom = $datinfo['nom'];	  
	  $sec = $datinfo['sec'];
	  $est = $datinfo['est'];
    $dep = $datinfo['dep_clave_int'];
    $mun = $datinfo['mun_clave_int'];
  ?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">
  <input type="hidden" id="idedicion" value="<?php echo $id;?>"> 
   <div class="form-group">
         <div class="col-xs-12"><strong>Departamento:<span class="symbol required"></span></strong>
         <select name="seldepartamento" id="seldepartamento" class="form-control input-sm selectpicker" onChange="cargarsector('selmunicipio','seldepartamento','CARGARMUNICIPIOS')" style="width:100%">
         <option value="">--seleccione--</option>
         <?php
        $con = pg_query($dbconn,"select d.dep_clave_int ids ,d.dep_nombre as dep from tbl_departamento d where d.est_clave_int=1 order by LOWER(d.dep_nombre) asc");
        while($dat = pg_fetch_array($con))
        {
            ?>
              <option  <?php if($dep==$dat['ids']){ echo "selected"; }?> value="<?php echo $dat['ids'];?>"><?php echo $dat['dep'];?></option>
              <?php
        }
      ?>
         </select>
         </div>
    </div>

    <div class="form-group">
         <div class="col-xs-12"><strong>Municipio:<span class="symbol required"></span></strong>
         <select name="selmunicipio" id="selmunicipio" class="form-control input-sm selectpicker" onChange="cargarsector('selsector','selmunicipio','CARGARSECTORES')" style="width:100%">
         <option value="">--seleccione--</option>
        <?php
        $con = pg_query($dbconn,"select m.mun_clave_int ids ,m.mun_nombre as mun from tbl_municipio m where m.est_clave_int=1 and (m.dep_clave_int = '".$dep."' or m.mun_clave_int = '".$mun."') order by LOWER(m.mun_nombre) asc");
        while($dat = pg_fetch_array($con))
        {
        ?>
        <option <?php if($mun==$dat['ids']){ echo "selected"; }?>  value="<?php echo $dat['ids'];?>"><?php echo $dat['mun'];?></option>
        <?php
        }
        ?>        
         </select>
         </div>
    </div>
    
    <div class="form-group">
         <div class="col-xs-12"><strong>Sector:<span class="symbol required"></span></strong>
         <select name="selsector" id="selsector" class="form-control selectpicker">
         <option value="">--seleccione--</option>
         <?php
          $con = pg_query($dbconn,"select d.sec_clave_int ids ,d.sec_nombre as sec from tbl_sector d where d.est_clave_int=1 or d.sec_clave_int = '".$sec."' order by sec");
          while($dat = pg_fetch_array($con))
          {
          ?>
          <option <?php if($sec==$dat['ids']){echo 'selected';}?> value="<?php echo $dat['ids'];?>"><?php echo $dat['sec'];?></option>
          <?php
          }
		 ?>
         </select>
         </div>
    </div>
    <div class="form-group">
         <div class="col-md-6"><strong>Barrio:<span class="symbol required"></span></strong>
         <input  name="txtbarrio" id="txtbarrio" class="form-control input-sm" value="<?php echo $nom;?>" type="text" autocomplete="off" placeholder="Ingrese nombre del barrio">        
         </div>
          <div class="col-md-6"><strong>Estado:</strong><br>        
             <label for="opcion1"> <input type="radio" name="radestado" id="opcion1" value="1" <?php if($est==1 || $est==""){echo 'checked';}?>>
             Activo</label>
            
             <label for="opcion2"> <input type="radio" name="radestado" id="opcion2"  value="0" <?php if($est==0){echo 'checked';}?>>
            Inactivo
            </label>
         </div>
    </div>
  </form>
  <?php  
  echo "<script>INICIALIZARCONTENIDO();</script>";
}
else if($opcion=="EDITAR" and  $ventana=="SECTOR")
{
	  $id = $_POST['id'];
	   $coninfo = pg_query($dbconn,"select s.sec_clave_int as ids,s.sec_nombre as nom,s.est_clave_int as est,sec_horas,sec_minutos,sec_domicilio,d.dep_clave_int,m.mun_clave_int,s.sec_monto from tbl_sector s left outer join tbl_municipio m on m.mun_clave_int = s.mun_clave_int left outer join tbl_departamento d on d.dep_clave_int = m.dep_clave_int where s.sec_clave_int = '".$id."' limit 1");
	  $datinfo = pg_fetch_array($coninfo);
	 
	  $sec = $datinfo['dep'];
	  $nomd  = $datinfo['nom'];
	  $est = $datinfo['est'];
    $hor = $datinfo['sec_horas'];
    $min = $datinfo['sec_minutos'];
    $domi = $datinfo['sec_domicilio'];
    $dep = $datinfo['dep_clave_int'];
    $mun = $datinfo['mun_clave_int'];
    $mon = $datinfo['sec_monto'];
  ?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">
  <input type="hidden" id="idedicion" value="<?php echo $id;?>">
    
    <div class="form-group">
         <div class="col-xs-12"><strong>Departamento:<span class="symbol required"></span></strong>
         <select name="seldepartamento" id="seldepartamento" class="form-control input-sm selectpicker" onChange="cargarsector('selmunicipio','seldepartamento','CARGARMUNICIPIOS')" style="width:100%">
         <option value="">--seleccione--</option>
         <?php
        $con = pg_query($dbconn,"select d.dep_clave_int ids ,d.dep_nombre as dep from tbl_departamento d where d.est_clave_int=1 order by LOWER(d.dep_nombre) asc");
        while($dat = pg_fetch_array($con))
        {
            ?>
              <option <?php if($dep==$dat['ids']){ echo "selected";} ?> value="<?php echo $dat['ids'];?>"><?php echo $dat['dep'];?></option>
              <?php
        }
      ?>
         </select>
         </div>
    </div>

    <div class="form-group">
         <div class="col-xs-12"><strong>Municipio:<span class="symbol required"></span></strong>
         <select name="selmunicipio" id="selmunicipio" class="form-control input-sm selectpicker" onChange="" style="width:100%">
         <option value="">--seleccione--</option>
        <?php
        $con = pg_query($dbconn,"select m.mun_clave_int ids ,m.mun_nombre as mun from tbl_municipio m where m.est_clave_int=1 and (m.dep_clave_int = '".$dep."' or m.mun_clave_int = '".$mun."') order by LOWER(m.mun_nombre) asc");
        while($dat = pg_fetch_array($con))
        {
        ?>
        <option <?php if($mun==$dat['ids']){ echo "selected"; }?>  value="<?php echo $dat['ids'];?>"><?php echo $dat['mun'];?></option>
        <?php
        }
        ?>        
         </select>
         </div>
    </div>
    <div class="form-group">
         <div class="col-md-6"><strong>Sector:<span class="symbol required"></span></strong>
         <input  name="txtsector" id="txtsector" class="form-control input-sm" type="text" autocomplete="off" placeholder="Ingrese nombre del sector" value="<?php echo $nomd;?>">
        
         </div>
         <div class="col-md-6"><strong>Tiempo Entrega:<span class="symbol required"></span></strong>
          <div class="input-group">

         <input  name="txthoras" id="txthoras" class="form-control input-sm" type="number" min="1" max="24" value="<?php echo $hor;?>">
         <span class="input-group-addon">hor</span>
          <input  name="txtminutos" id="txtminutos" class="form-control input-sm" type="number" min="0" max="59" value="<?php echo $min;?>">
          <span class="input-group-addon">min</span>

          </div>
         </div>
         <div class="col-md-6"><strong>Domicilio:<span class="symbol required"></span></strong>
         <input  name="txtdomicilio" id="txtdomicilio" class="form-control input-sm currency2" type="text" autocomplete="off" placeholder="Ingrese costo del domicilio" value="<?php echo $domi;?>" onkeypress="return validar_texto(event)">
        
         </div>
           <div class="col-md-6"><strong>Monto máximo para domicilio gratis:<span class="symbol required"></span></strong>
       <input  name="txtmonto" id="txtmonto" class="form-control input-sm currency2" type="text" autocomplete="off" placeholder="Ingrese monto" value="<?php echo $mon;?>">      
       </div>
          <div class="col-md-6"><strong>Estado:</strong><br>
        
                 <label for="opcion1"> <input type="radio" name="radestado" id="opcion1"  checked value="1" <?php if($est==1 || $est==""){echo 'checked';}?>>
                 Activo</label>
                
                 <label for="opcion2"> <input type="radio" name="radestado" id="opcion2"  value="0" <?php if($est==0){echo 'checked';}?>>
                Inactivo
                </label>
         </div>
    </div>
  </form>
  <?php  
    echo "<script>INICIALIZARCONTENIDO();</script>";
}
else if($opcion=="EDITAR" and  $ventana=="DEPARTAMENTO")
{
    $id = $_POST['id'];
     $coninfo = pg_query($dbconn,"select d.dep_clave_int as ids,d.dep_nombre as nom,d.est_clave_int as est from tbl_departamento d  where d.dep_clave_int = '".$id."' limit 1");
    $datinfo = pg_fetch_array($coninfo);
   
    $dep = $datinfo['dep'];
    $nomd  = $datinfo['nom'];
    $est = $datinfo['est'];
  
  ?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">
  <input type="hidden" id="idedicion" value="<?php echo $id;?>">
    
  
    <div class="form-group">
         <div class="col-md-6"><strong>Departamento:<span class="symbol required"></span></strong>
         <input  name="txtdepartamento" id="txtdepartamento" class="form-control input-sm" type="text" autocomplete="off" placeholder="Ingrese nombre del departamento" value="<?php echo $nomd;?>">
        
         </div>
        
          <div class="col-md-6"><strong>Estado:</strong><br>
        
                 <label for="opcion1"> <input type="radio" name="radestado" id="opcion1" checked value="1" <?php if($est==1 || $est==""){echo 'checked';}?>>
                 Activo</label>
                
                 <label for="opcion2"> <input type="radio" name="radestado" id="opcion2" value="0" <?php if($est==0){echo 'checked';}?>>
                Inactivo
                </label>
         </div>
    </div>
  </form>
  <?php  
    echo "<script>INICIALIZARCONTENIDO();</script>";
}
else if($opcion=="EDITAR" and  $ventana=="MUNICIPIO")
{
    $id = $_POST['id'];
    $coninfo = pg_query($dbconn,"select c.mun_nombre as nom,d.dep_clave_int as dep,c.est_clave_int as est from tbl_municipio c join tbl_departamento d on d.dep_clave_int = c.dep_clave_int  where c.mun_clave_int = '".$id."' limit 1");
    $datinfo = pg_fetch_array($coninfo);
    $nom = $datinfo['nom'];   
    $dep = $datinfo['dep'];
    $est = $datinfo['est'];
  ?>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">
  <input type="hidden" id="idedicion" value="<?php echo $id;?>"> 
    
    <div class="form-group">
         <div class="col-xs-12"><strong>Departamento:<span class="symbol required"></span></strong>
         <select name="seldepartamento" id="seldepartamento" class="form-control selectpicker">
         <option value="">--seleccione--</option>
         <?php
          $con = pg_query($dbconn,"select d.dep_clave_int ids ,d.dep_nombre as dep from tbl_departamento d where d.est_clave_int=1 or d.dep_clave_int = '".$dep."' order by d.dep_nombre ASC");
          while($dat = pg_fetch_array($con))
          {
          ?>
          <option <?php if($dep==$dat['ids']){echo 'selected';}?> value="<?php echo $dat['ids'];?>"><?php echo $dat['dep'];?></option>
          <?php
          }
     ?>
         </select>
         </div>
    </div>
    <div class="form-group">
         <div class="col-md-6"><strong>Munucipio:<span class="symbol required"></span></strong>
         <input  name="txtmunicipio" id="txtmunicipio" class="form-control input-sm" value="<?php echo $nom;?>" type="text" autocomplete="off" placeholder="Ingrese nombre del municipio">        
         </div>
          <div class="col-md-6"><strong>Estado:</strong><br>        
             <label for="opcion1"> <input type="radio" name="radestado" id="opcion1"   value="1" <?php if($est==1 || $est==""){echo 'checked';}?>>
             Activo</label>
            
             <label for="opcion2"> <input type="radio" name="radestado" id="opcion2"  value="0" <?php if($est==0){echo 'checked';}?>>
            Inactivo
            </label>
         </div>
    </div>
  </form>
  <?php  
  echo "<script>INICIALIZARCONTENIDO();</script>";
}
else if($opcion=="GUARDAR" and $ventana=="SECTOR")
{    
 $sector  = $_POST['sector'];
 $estado  = $_POST['estado'];
 $municipio = $_POST['mun'];
 $hor = $_POST['hor'];
 $min = $_POST['min'];
 $dom = $_POST['dom'];
 $mon = $_POST['mon'];
 $veri = pg_query($dbconn,"select * from tbl_sector where UPPER(sec_nombre) = UPPER('".$sector."') and mun_clave_int = '".$municipio."' and   AND est_clave_int!=2");
 $numv = pg_num_rows($veri);
 if($numv>0)
 {
    $res = "error";
    $msn = "El sector ingresado ya existe en el municipio seleccionado. Verificar";
 }
  else 
  {
    $ins = pg_query($dbconn,"insert into tbl_sector(sec_nombre,est_clave_int,sec_usu_actualiz,sec_fec_actualiz,sec_horas,sec_minutos,sec_domicilio,mun_clave_int,sec_monto) values('".$sector."','".$estado."','".$usuario."','".$fecha."','".$hor."','".$min."','".$dom."','".$municipio."','".$mon."')");
  	if($ins>0)
  	{
      $res = "ok";
      $msn = "Sector guardado correctamente";
  	   
  	}
  	else
    {
  	  $res = "error";
      $msn = "Error BD. No guardo el sector";
  	}	
  }	 
  $datos[] = array("res"=>$res,"msn"=>$msn);
  echo json_encode($datos);

}
else if($opcion=="GUARDAR" and $ventana=="DEPARTAMENTO")
{    
 $departamento  = $_POST['departamento'];
 $estado  = $_POST['estado'];

 $veri = pg_query($dbconn,"select * from tbl_departamento where UPPER(dep_nombre) = UPPER('".$departamento."')  AND dep_activo!=2");
 $numv = pg_num_rows($veri);
 if($numv>0)
 {
    $res = "error";
    $msn = "El departamento ingresado ya existe. Verificar";
 }
  else 
  {
    $ins = pg_query($dbconn,"insert into tbl_departamento(dep_nombre,est_clave_int,dep_usu_actualiz,dep_fec_actualiz) values('".$departamento."','".$estado."','".$usuario."','".$fecha."')");
    if($ins>0)
    {
      $res = "ok";
      $msn = "Departamento guardado correctamente";
       
    }
    else
    {
      $res = "error";
      $msn = "Error BD. No guardo el departamento";
    } 
  }  
  $datos[] = array("res"=>$res,"msn"=>$msn);
  echo json_encode($datos);

}
else if($opcion=="GUARDAR" and $ventana=="MUNICIPIO")
{ 
   $departamento  = $_POST['departamento'];
   $municipio = $_POST['municipio'];
   $estado  = $_POST['estado'];
  
   $veri = pg_query($dbconn,"select * from tbl_municipio c where UPPER(mun_nombre) = UPPER('".$municipio."')  and c.dep_clave_int  = '".$departamento."' AND est_clave_int!=2");
   $numv = pg_num_rows($veri);
   if($numv>0)
   {
     $res = "error";
     $msn = "El municipio ingresado ya existe en el departamento seleccionado";
   }
   else 
   {
      $ins = pg_query($dbconn,"insert into tbl_municipio(dep_clave_int,mun_nombre,est_clave_int,mun_usu_actualiz,mun_fec_actualiz) values('".$departamento."','".$municipio."','".$estado."','".$usuario."','".$fecha."')");
        if($ins>0)
        {
          $res = "ok";
          $msn = "Municipio guardado correctamente";
      
        }
        else
        {
          $res = "error";
          $msn = "No guardo el municipio. Error BD (".pg_last_error($dbconn).")";
        } 
  }
  $datos[] = array("res"=>$res,"msn"=>$msn);
  echo json_encode($datos);  
}
else if($opcion=="GUARDAR" and $ventana=="BARRIO")
{ 
	 $sector  = $_POST['sector'];
	 $barrio = $_POST['barrio'];
	 $estado  = $_POST['estado'];
	
	 $veri = pg_query($dbconn,"select * from tbl_barrio c where UPPER(bar_nombre) = UPPER('".$barrio."')  and c.sec_clave_int  = '".$sector."' AND est_clave_int!=2");
	 $numv = pg_num_rows($veri);
	 if($numv>0)
	 {
     $res = "error";
     $msn = "El barrio ingresado ya existe en el sector seleccionado";
	 }
	 else 
	 {
      $ins = pg_query($dbconn,"insert into tbl_barrio(sec_clave_int,bar_nombre,est_clave_int,bar_usu_actualiz,bar_fec_actualiz) values('".$sector."','".$barrio."','".$estado."','".$usuario."','".$fecha."')");
        if($ins>0)
        {
          $res = "ok";
          $msn = "Barrio guardado correctamente";
      
        }
        else
        {
          $res = "error";
          $msn = "Error BD. No guardo el barrio";
        }	
  }
  $datos[] = array("res"=>$res,"msn"=>$msn);
  echo json_encode($datos);	 
}

else if($opcion=="GUARDAREDICION" and $ventana=="SECTOR")
{
  $ide =$_POST['id'];
  $sector  = $_POST['sector'];
  $estado  = $_POST['estado'];
  $hor = $_POST['hor'];
  $min = $_POST['min'];
  $dom = $_POST['dom'];
  $mun = $_POST['mun'];
  $mon = $_POST['mon'];
  $veri = pg_query($dbconn,"select * from tbl_sector where UPPER(sec_nombre) = UPPER('".$sector."') and mun_clave_int = '".$municipio."' and sec_clave_int!='".$ide."' and est_clave_int!=2");
  $numv = pg_num_rows($veri);
  if($numv>0)
  { 
    $res = "error";
    $msn = "Ya hay un sector con el nombre indicado en el municipio seleccionado. Verificar";
  }
  else 
  {
    $upd = pg_query($dbconn,"update tbl_sector set sec_nombre='".$sector."',est_clave_int='".$estado."',sec_usu_actualiz='".$usuario."',sec_fec_actualiz='".$fecha."',sec_horas = '".$hor."',sec_minutos = '".$min."',sec_domicilio = '".$dom."',mun_clave_int = '".$mun."', sec_monto = '".$mon."' where sec_clave_int = '".$ide."'");
    if($upd>0)
    {
      $res = "ok";
      $msn = "Sector modificado correctamente";
    //echo 1;
    }
    else
    {
      $res = "error";
      $msn = "Error BD. Surgió un error al modificar el sector"; 
    }	
  }	
  $datos[] = array("res"=>$res,"msn"=>$msn);
  echo json_encode($datos);
}
else if($opcion=="GUARDAREDICION" and $ventana=="BARRIO")
{
  $ide = $_POST['id'];
  $sector  = $_POST['sector'];
  $barrio = $_POST['barrio'];
  $estado  = $_POST['estado'];
	
  $veri = pg_query($dbconn,"select * from tbl_barrio c   where UPPER(bar_nombre) = UPPER('".$barrio."')  and c.sec_clave_int  = '".$sector."' and c.bar_clave_int !='".$ide."' AND c.est_clave_int!=2");
  $numv = pg_num_rows($veri);
  if($numv>0)
  {
    $res = "error";
    $msn = "Ya hay un barrio con el nombre indicado en el sector seleccionado. Verificar";
  }
  else 
  {
    $upd = pg_query($dbconn,"update tbl_barrio set sec_clave_int='".$sector."',bar_nombre='".$barrio."',est_clave_int='".$estado."',bar_usu_actualiz='".$usuario."',bar_fec_actualiz='".$fecha."' where bar_clave_int = '".$ide."'");
    if($upd>0)
    {
      $res = "ok";
      $msn = "Barrio modificado correctamente";
    }
    else
    {
      $res = "error";
      $msn = "Error BD. Surgió un error al modificar el barrio";
    }	
	}	
  $datos[] = array("res"=>$res,"msn"=>$msn);
  echo json_encode($datos);
}

else if($opcion=="GUARDAREDICION" and $ventana=="MUNICIPIO")
{
  $ide = $_POST['id'];
  $departamento  = $_POST['departamento'];
  $municipio = $_POST['municipio'];
  $estado  = $_POST['estado'];
  
  $veri = pg_query($dbconn,"select * from tbl_municipio c   where UPPER(mun_nombre) = UPPER('".$municipio."')  and c.dep_clave_int  = '".$departamento."' and c.mun_clave_int !='".$ide."' AND c.est_clave_int!=2");
  $numv = pg_num_rows($veri);
  if($numv>0)
  {
    $res = "error";
    $msn = "Ya hay un municipio con el nombre indicado en el departamento seleccionado. Verificar";
  }
  else 
  {
    $upd = pg_query($dbconn,"update tbl_municipio set dep_clave_int='".$departamento."',mun_nombre='".$municipio."',est_clave_int='".$estado."',mun_usu_actualiz='".$usuario."',mun_fec_actualiz='".$fecha."' where mun_clave_int = '".$ide."'");
    if($upd>0)
    {
      $res = "ok";
      $msn = "Municipio modificado correctamente";
    }
    else
    {
      $res = "error";
      $msn = "Error BD. Surgió un error al modificar el municipio";
    } 
  } 
  $datos[] = array("res"=>$res,"msn"=>$msn);
  echo json_encode($datos);
}
else if($opcion=="GUARDAREDICION" and $ventana=="DEPARTAMENTO")
{
  $ide = $_POST['id'];
  $departamento  = $_POST['departamento'];
  $estado  = $_POST['estado'];
  
  $veri = pg_query($dbconn,"select * from tbl_departamento c   where UPPER(dep_nombre) = UPPER('".$departamento."') and  c.dep_clave_int !='".$ide."' AND c.est_clave_int!=2");
  $numv = pg_num_rows($veri);
  if($numv>0)
  {
    $res = "error";
    $msn = "Ya hay un departamento con el nombre indicado . Verificar";
  }
  else 
  {
    $upd = pg_query($dbconn,"update tbl_departamento set dep_nombre='".$departamento."',est_clave_int='".$estado."',dep_usu_actualiz='".$usuario."',dep_fec_actualiz='".$fecha."' where dep_clave_int = '".$ide."'");
    if($upd>0)
    {
      $res = "ok";
      $msn = "Departamento modificado correctamente";
    }
    else
    {
      $res = "error";
      $msn = "Error BD. Surgió un error al modificar el departamento";
    } 
  } 
  $datos[] = array("res"=>$res,"msn"=>$msn);
  echo json_encode($datos);
}
else if($opcion=="LISTABARRIOS")
{
	  ?> 
	  <script src="jsdatatable/sectores/jssectores.js"></script>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <div class="table-responsive">
    <table id="tbsectores" class="table table-bordered table-hover">
      <thead>
      <tr>
      <th width="20"></th>
      <th width="20"></th>
      <th>Departamento</th>
      <th>Municipio</th>                 
      <th>Sector</th>
      <th>Barrio</th>
      <th>Estado</th>
      </tr>
      </thead>               
      <tbody>
       <?php
        $con = pg_query($dbconn,"select c.bar_clave_int as idb,c.bar_nombre bar,s.sec_nombre sec,c.est_clave_int as est,e.est_nombre as estn,d.dep_nombre,m.mun_nombre from tbl_barrio c join tbl_sector s on s.sec_clave_int  = c.sec_clave_int join tbl_estados e on e.est_clave_int  = c.est_clave_int left outer join tbl_municipio m on m.mun_clave_int = s.mun_clave_int left outer join tbl_departamento d on d.dep_clave_int = m.dep_clave_int where e.est_clave_int not in(2) order by sec,bar");
			   
        while($dat = pg_fetch_array($con))
        {
				   $idb = $dat['idb'];
				   $bar = $dat['bar'];
				   $sec = $dat['sec'];				   
				   $est = $dat['est'];
				   $estnom = $dat['estn'];
           $dep = $dat['dep_nombre'];
           $mun = $dat['mun_nombre'];
				   
          if($est=="0"){$est='<span class="label label-warning pull-right">'.$estnom.'</span>';}
          else if($est=="2"){$est='<span class="label label-danger pull-right">'.$estnom.'</span>';}
          else {$est='<span class="label label-success pull-right">'.$estnom.'</span>';}
				   ?>
          <tr id="row_bar<?php echo $idc;?>">
          <td><a class="btn btn-block btn-warning btn-xs" onClick="CRUDSECTOR('BARRIO','EDITAR',<?php echo $idb;?>)" data-toggle="modal" data-target="#modalregistro" style="width:34px; height:22px"><i class="glyphicon glyphicon-pencil"></i></a></td>
          <td><a class="btn btn-block btn-danger btn-xs" onClick="CRUDSECTOR('BARRIO','ELIMINAR',<?php echo $idb;?>)" style="width:34px; height:22px"><i class="glyphicon glyphicon-trash" ></i></a></td>
          <td><?php echo $dep;?></td>
          <td><?php echo $mun;?></td> 
          <td><?php echo $sec;?></td>
          <td><?php echo $bar;?></td>               
          <td><?php echo $est;?></td>
          </tr>
          <?php
			   }
			   ?>
      </tbody>
      <tfoot>
        <tr>
        <th></th>
        <th></th>
        <th>Departamento</th>
        <th>Municipio</th>
        <th>Barrio</th>
        <th>Sector</th>                 
        <th>Estado</th>
        </tr>
      </tfoot>
    </table>
  </div>
<?php
}
else if($opcion=="LISTAMUNICIPIOS")
{
    ?> 
    <script src="jsdatatable/sectores/jssectores.js"></script>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <div class="table-responsive">
    <table id="tbsectores" class="table table-bordered table-hover">
      <thead>
      <tr>
      <th width="20"></th>
      <th width="20"></th>                 
      <th>Departamento</th>
      <th>Municipio</th>
      <th>Estado</th>
      </tr>
      </thead>               
      <tbody>
       <?php
        $con = pg_query($dbconn,"select c.mun_clave_int as idb,c.mun_nombre mun,d.dep_nombre dep,c.est_clave_int as est,e.est_nombre as estn from tbl_municipio c join tbl_departamento d on d.dep_clave_int  = c.dep_clave_int join tbl_estados e on e.est_clave_int  = c.est_clave_int where e.est_clave_int not in(2) order by dep,mun");
         
        while($dat = pg_fetch_array($con))
        {
           $idb = $dat['idb'];
           $mun = $dat['mun'];
           $dep = $dat['dep'];           
           $est = $dat['est'];
           $estnom = $dat['estn'];
           
          if($est=="0"){$est='<span class="label label-warning pull-right">'.$estnom.'</span>';}
          else if($est=="2"){$est='<span class="label label-danger pull-right">'.$estnom.'</span>';}
          else {$est='<span class="label label-success pull-right">'.$estnom.'</span>';}
           ?>
          <tr id="row_mun<?php echo $idc;?>">
          <td><a class="btn btn-block btn-warning btn-xs" onClick="CRUDSECTOR('MUNICIPIO','EDITAR',<?php echo $idb;?>)" data-toggle="modal" data-target="#modalregistro" style="width:34px; height:22px"><i class="glyphicon glyphicon-pencil"></i></a></td>
          <td><a class="btn btn-block btn-danger btn-xs" onClick="CRUDSECTOR('MUNICIPIO','ELIMINAR',<?php echo $idb;?>)" style="width:34px; height:22px"><i class="glyphicon glyphicon-trash" ></i></a></td>
          <td><?php echo $dep;?></td>
          <td><?php echo $mun;?></td>               
          <td><?php echo $est;?></td>
          </tr>
          <?php
         }
         ?>
      </tbody>
      <tfoot>
        <tr>
        <th></th>
        <th></th>
        <th>Barrio</th>
        <th>Sector</th>                 
        <th>Estado</th>
        </tr>
      </tfoot>
    </table>
  </div>
<?php
}
else if($opcion=="LISTASECTORES")
{
	  ?>  
    <script src="jsdatatable/sectores/jssectores.js"></script>
	  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <div class="table-responsive">
  <table id="tbsectores" class="table table-bordered table-hover">
    <thead>
      <tr>
      <th width="20"></th>
      <th width="20"></th>
      <th>Departamento</th>
      <th>Municipio</th>
      <th>Sector</th>
      <th>T.Entrega</th>
      <th>Domicilio</th>
      <th>Monto</th>
      <th>Estado</th>
      </tr>
    </thead>
    <tbody>
    <?php
    $con = pg_query($dbconn,"select d.dep_nombre dep,m.mun_nombre mun, s.sec_clave_int as ids,s.sec_nombre sec,s.est_clave_int as est,e.est_nombre as estn,s.sec_horas,s.sec_minutos,s.sec_domicilio,s.sec_monto from tbl_sector s join tbl_estados e on e.est_clave_int  = s.est_clave_int left outer join tbl_municipio m on m.mun_clave_int = s.mun_clave_int left outer join tbl_departamento d on d.dep_clave_int = m.dep_clave_int where e.est_clave_int not in(2) order by sec");

    while($dat = pg_fetch_array($con))
    {
      $ids = $dat['ids'];				 
      $sec = $dat['sec'];				
      $est = $dat['est'];
      $dep  =$dat['dep'];
      $mun = $dat['mun'];
      $mon = $dat['sec_monto'];

      $estnom = $dat['estn'];
      $tiempo = $dat['sec_horas']." h ".$dat['sec_minutos']." min";
      $domi = $dat['sec_domicilio'];

      if($est=="0"){$est='<span class="label label-warning pull-right">'.$estnom.'</span>';}
      else if($est=="2"){$est='<span class="label label-danger pull-right">'.$estnom.'</span>';}
      else {$est='<span class="label label-success pull-right">'.$estnom.'</span>';}
				   ?>
        <tr id="row_sec<?php echo $idc;?>">
        <td><a class="btn btn-block btn-warning btn-xs" onClick="CRUDSECTOR('SECTOR','EDITAR',<?php echo $ids;?>)" data-toggle="modal" data-target="#modalregistro" style="width:34px; height:22px"><i class="glyphicon glyphicon-pencil"></i></a></td>
        <td><a class="btn btn-block btn-danger btn-xs" onClick="CRUDSECTOR('SECTOR','ELIMINAR',<?php echo $ids;?>)" data-toggle="tooltip" title="Eliminar Sector" style="width:34px; height:22px"><i class="glyphicon glyphicon-trash"></i></a></td>
        
        <td><?php echo $dep;?></td>
        <td><?php echo $mun;?></td>
        <td><?php echo $sec;?></td>
        <td><?php echo $tiempo;?></td>
        <td class="currency dt-right"><?php echo $domi;?></td>
          <td class="currency dt-right"><?php echo $mon;?></td>
        <td><?php echo $est;?></td>
        </tr>
                <?php
			   }
			   ?>
      </tbody>
      <tfoot>
      <tr>
      <th></th>
      <th></th>  
      <th>Departamento</th>
      <th>Municipio</th>              
      <th>Sector</th>
      <th>T.Entrega</th>
      <th>Domicilio</th>
      <th>Monto</th>
      <th>Estado</th>
      </tr>
      </tfoot>
    </table>
  </div>
    <?php
    echo "<script>INICIALIZARCONTENIDO();</script>";
}
else if($opcion=="LISTADEPARTAMENTOS")
{
    ?>  
    <script src="jsdatatable/sectores/jssectores.js"></script>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
        <div class="table-responsive">
  <table id="tbsectores" class="table table-bordered table-hover">
    <thead>
      <tr>
      <th width="20"></th>
      <th width="20"></th>
      <th>Departamento </th>
      
      <th>Estado</th>
      </tr>
    </thead>
    <tbody>
    <?php
    $con = pg_query($dbconn,"select d.dep_clave_int as ids,d.dep_nombre dep,d.est_clave_int as est,e.est_nombre as estn from tbl_departamento d join tbl_estados e on e.est_clave_int  = d.est_clave_int where e.est_clave_int not in(2) order by LOWER(d.dep_nombre) ASC ");

    while($dat = pg_fetch_array($con))
    {
      $ids = $dat['ids'];        
      $dep = $dat['dep'];       
      $est = $dat['est'];
      $estnom = $dat['estn'];
      
      if($est=="0"){$est='<span class="label label-warning pull-right">'.$estnom.'</span>';}
      else if($est=="2"){$est='<span class="label label-danger pull-right">'.$estnom.'</span>';}
      else {$est='<span class="label label-success pull-right">'.$estnom.'</span>';}
           ?>
        <tr id="row_dep<?php echo $idc;?>">
        <td><a class="btn btn-block btn-warning btn-xs" onClick="CRUDSECTOR('DEPARTAMENTO','EDITAR',<?php echo $ids;?>)" data-toggle="modal" data-target="#modalregistro" style="width:34px; height:22px"><i class="glyphicon glyphicon-pencil"></i></a></td>
        <td><a class="btn btn-block btn-danger btn-xs" onClick="CRUDSECTOR('DEPARTAMENTO','ELIMINAR',<?php echo $ids;?>)" data-toggle="tooltip" title="Eliminar Sector" style="width:34px; height:22px"><i class="glyphicon glyphicon-trash"></i></a></td>
        <td><?php echo $dep;?></td>
        <td><?php echo $est;?></td>
        </tr>
                <?php
         }
         ?>
      </tbody>
      <tfoot>
      <tr>
      <th></th>
      <th></th>                
      <th>Departamento</th>      
      <th>Estado</th>
      </tr>
      </tfoot>
    </table>
  </div>
    <?php
    echo "<script>INICIALIZARCONTENIDO();</script>";
}
else if($opcion=="ELIMINAR" and $ventana=="BARRIO")
{
  $id = $_POST['id'];
  $update = pg_query($dbconn,"update tbl_barrio set est_clave_int = 2 where bar_clave_int = '".$id."'");
  if($update>0)
  {
    $res = "ok";
    $msn = "Barrio eliminado correctamente";
  } 
  else
  {
    $res = "error";
    $msn  ="Surgió un error al eliminar el barrio";
  }
  $datos[] = array("res"=>$res,"msn"=>$msn);
  echo json_encode($datos);
}
else if($opcion=="ELIMINAR" and $ventana=="SECTOR")
{
  $id = $_POST['id'];
  $update = pg_query($dbconn,"update tbl_sector set est_clave_int = 2,sec_usu_actualiz= '".$usuario."',sec_fec_actualiz = '".$fecha."' where sec_clave_int = '".$id."'");
  if($update>0)
  {
    $res = "ok";
    $msn = "Sector eliminado correctamente";
  } 
  else
  {
    $res = "error";
    $msn  ="Surgió un error al eliminar el sector";
  }
  $datos[] = array("res"=>$res,"msn"=>$msn);
  echo json_encode($datos);
}
else if($opcion=="ELIMINAR" and $ventana=="DEPARTAMENTO")
{
  $id = $_POST['id'];
  $update = pg_query($dbconn,"update tbl_departamento set est_clave_int = 2,dep_usu_actualiz= '".$usuario."',dep_fec_actualiz = '".$fecha."' where dep_clave_int = '".$id."'");
  if($update>0)
  {
    $res = "ok";
    $msn = "Departamento eliminado correctamente";
  } 
  else
  {
    $res = "error";
    $msn  ="Surgió un error al eliminar el departamento";
  }
  $datos[] = array("res"=>$res,"msn"=>$msn);
  echo json_encode($datos);
}
else if($opcion=="ELIMINAR" and $ventana=="MUNICIPIO")
{
  $id = $_POST['id'];
  $update = pg_query($dbconn,"update tbl_municipio set est_clave_int = 2 where mun_clave_int = '".$id."'");
  if($update>0)
  {
    $res = "ok";
    $msn = "Municipio eliminado correctamente";
  } 
  else
  {
    $res = "error";
    $msn  ="Surgió un error al eliminar el municipio";
  }
  $datos[] = array("res"=>$res,"msn"=>$msn);
  echo json_encode($datos);
}
else if($opcion=="CARGARMUNICIPIOS" || $opcion =="CARGARSECTORES" || $opcion=="CARGARBARRIOS" )
{
   $sel2 = $_POST['sel2'];

   if($opcion=="CARGARMUNICIPIOS"){
      $sql = "SELECT m.mun_clave_int,m.mun_nombre FROM tbl_municipio m WHERE  m.est_clave_int = 1 and m.dep_clave_int = '".$sel2."' ORDER BY LOWER(m.mun_nombre) ASC";
   }
   else if($opcion=="CARGARSECTORES")
   {
      $sql = "SELECT s.sec_clave_int,s.sec_nombre FROM tbl_sector s WHERE  s.est_clave_int = 1 and s.mun_clave_int = '".$sel2."' ORDER BY LOWER(s.sec_nombre) ASC";
   }
   else if($opcion=="CARGARBARRIOS")
   {
      $sql = "SELECT b.bar_clave_int,b.bar_nombre FROM tbl_barrio b WHERE  b.est_clave_int = 1 and b.sec_clave_int = '".$sel2."' ORDER BY LOWER(b.bar_nombre) ASC";
   }

   $con = pg_query($dbconn, $sql);
   $num = pg_num_rows($con);
   if($num>0)
   {
      for($k=0;$k<$num;$k++)
      {
        $dat = pg_fetch_row($con);
        $id = $dat[0];
        $literal = $dat[1];
        $datos[] = array("res" => "si", "id" => $id, "literal"=>$literal);

      }
   }
   else
   {
      $datos[] = array("res" => "no", "id" => "", "literal"=>"");
   }
   echo json_encode($datos);

}
 

?>