  <div class="col-md-3 col-sm-6 col-lg-3 data-row hide "  >
					            <div class="sc-product-item thumbnail <?php if($numpedido>0){ echo ' sc-added-item';} ?>" data-idp="<?php echo $idp;?>">
									<div  class="circle colored effect3  bottom_to_top"><a href="#">
										<div class="img">
											<ul class="caption-style-4">
											<li>
											<img data-name="product_image" src="<?php echo $img;?>" alt="img" width="100%" height="150">
											<div class="caption">
											<div class="blur"></div>
											<div class="caption-text">
											<h1 data-toggle="modal" data-target="#modalinfo" onclick="CRUDPEDIDOS('INFOPRODUCTO','<?PHP echo $idp;?>')"><i class="fa fa-plus fa-2x"></i></h1>
											<p><?php echo $descp;?></p>
											</div>
											</div>
											</li>
											</ul>

											
										</div>
										<div class="info">
										<h4 data-name="product_name"><?php echo $nomp;?></h4>
										<p style="display: none" data-name="product_desc"><?php echo $descp;?></p>
										</div></a>
									</div>
					                <div class="caption">
					                            <!--<hr class="line">-->
					                            
					              	
					                <div class="form-group" style="margin-bottom: 5px; margin-top: 0px">
					                	<h2 id="precio_<?php echo $idp;?>" style="font-size: 24px;padding: 2px; margin-top:0px " class="price badge bg-default currency2 "><?php echo $venta;?></h2>
					                </div>
					                <div class="row <?php if(count($arrayclasificacion)<=1){ echo ' invisible';}?>" style="margin-bottom:  16px">
					                	<div class="col-md-6" style="display: none;">
					                    <label>Clasificacion: </label>
					                	</div>
					                	<div class="col-md-12">
					                    <select name="product_clasificacion" id="product_clasificacion_<?php echo $idp;?>" class="form-control input-sm selectpicker" onchange="CRUDPEDIDOS('CAMBIARPRECIO','<?php echo $idp; ?>',this.value)">
                                    	<?php
                                    	
                                    		for($c=0;$c<count($arrayclasificacion);$c++)
                                    		{ 
                                    			$concla = pg_query($dbconn,"SELECT * FROM tbl_clasificacion where cla_clave_int = '".$arrayclasificacion[$c]."'");
                                    			$datc = pg_fetch_array($concla)
                                    			?>
	                                        	<option value="<?php echo $arrayclasificacion[$c];?>"><?php echo $datc['cla_nombre'];?></option>	
	                                        <?php 
	                                    	}
	                                    ?>
	                                    </select>
	                                	</div>
					                </div>
					                <div title="<?php echo $aplicatamano;?>" class="row<?php if($aplicatamano==1){ }else{ echo ' invisible'; } ?>">
					                	<div class="col-md-12">
											<div class="toggle_radio">
											<input data-name="product_tam" name="product_tamano_<?php echo $idp;?>" type="radio" class="toggle_option grande_toggle" id="tamano1_<?php echo $idp;?>" name="toggle_option" value="Grande">
											<input data-name="product_tam" name="product_tamano_<?php echo $idp;?>" type="radio"  class="toggle_option mediano_toggle" id="tamano2_<?php echo $idp;?>" name="toggle_option" value="Mediano">
											<input data-name="product_tam" name="product_tamano_<?php echo $idp;?>" type="radio" class="toggle_option pequeno_toggle" id="tamano3_<?php echo $idp;?>" name="toggle_option" value="Pequeño">
											<label for="tamano1_<?php echo $idp;?>"><p>Grande</p></label>
											<label for="tamano2_<?php echo $idp;?>"><p>Mediano</p></label>
											<label for="tamano3_<?php echo $idp;?>"><p>Pequeño</p></label>
												<div class="toggle_option_slider">
												</div>
											</div>					                   
					                    </div>                            
					               </div>
					                <div class="row <?php if($mad==0){ echo "invisible";}?>">
					                	<div class="col-md-12">
											<div class="toggle_radio">
											<input data-name="product_est" name="product_estado_<?php echo $idp;?>" type="radio" class="toggle_option first_toggle" id="estado1_<?php echo $idp;?>" name="toggle_option" value="Verde">
											<input data-name="product_est" name="product_estado_<?php echo $idp;?>" type="radio" checked class="toggle_option second_toggle" id="estado2_<?php echo $idp;?>" name="toggle_option" value="Pinton">
											<input data-name="product_est" name="product_estado_<?php echo $idp;?>" type="radio" class="toggle_option third_toggle" id="estado3_<?php echo $idp;?>" name="toggle_option" value="Maduro">
											<label for="estado1_<?php echo $idp;?>"><p>Verde</p></label>
											<label for="estado2_<?php echo $idp;?>"><p>Pintón</p></label>
											<label for="estado3_<?php echo $idp;?>"><p>Maduro</p></label>
												<div class="toggle_option_slider">
												</div>
											</div>					                   
					                    </div>                            
					               </div>
					               <div class="row <?php if(($puv==1 and $ppv==0) || ($ppv==1 and $puv==0)){ echo " invisible"; } ?>">
					               		<div class="col-md-12">
					               			<div class="nav-tabs-custom sel-unidad" >
											<ul class="nav nav-tabs pull-left">    <li class="active unidad" ><a  data-toggle="tab" aria-expanded="true" onclick="ckradio('product_unidad_<?php echo $idp;?>','0')" ><label>Unidad</label>
											<input style="display: none;" data-name="product_uni" name="product_unidad_<?php echo $idp;?>" type="radio" id="unidad1_<?php echo $idp;?>" value="0" <?php if($dataunidad==0){ echo "checked";}?> data-uni-min = '<?php echo $pmuv;?>' data-text-min="UND" data-idp="<?php echo $idp;?>">
											</a>
												</li>
												<li class="peso" onclick="ckradio('product_unidad_<?php echo $idp;?>','1')">
												<a data-toggle="tab" aria-expanded="false"><label>Peso</label>
													<input style="display: none;" data-name="product_uni" name="product_unidad_<?php echo $idp;?>" type="radio" id="unidad2_<?php echo $idp;?>" value="1" <?php if($dataunidad==1){ echo "checked";}?> data-uni-min = '<?php echo $pmpv;?>' data-text-min="GR" data-idp="<?php echo $idp;?>">

												</a></li>

											</ul>
										</div>
					               </div>
					           </div>
					               <div class="row">
										
						               	<div class="col-md-8">
										<div class="input-group">
										<span class="input-group-btn">
										<button type="button" class="btn btn-default btn-number btn-sm" disabled="disabled" data-type="minus" data-field="product_quantity_<?php echo $idp;?>">
										<span class="glyphicon glyphicon-minus"></span>
										</button>
										</span>
										<div class="has-feedback">
										<input type="text" id="product_quantity_<?php echo $idp;?>" name="product_quantity" class="form-control input-number input-sm sc-cart-item-qty" step="<?php echo $unidadmin?>" value="<?php echo $unidadmin;?>" min="<?php echo $unidadmin;?>" max="9999999" data-unidad="<?php echo $datunidad; ?>">
										<span class="form-control-feedback" id="unidadproducto<?php echo $idp;?>"><?php if($dataunidad==1){ echo "GR"; }else{ echo "UND";}?></span>
										</div>
										
										<span class="input-group-btn">
										<button type="button" class="btn btn-default btn-number btn-sm" data-type="plus" data-field="product_quantity_<?php echo $idp;?>">
										<span class="glyphicon glyphicon-plus"></span>									
										</button>

										</span>
										</div>

						                    <!--<input class="form-control sc-cart-item-qty" name="product_quantity" id="product_quantity_<?php echo $idp;?>" min="1" value="1" type="number">-->
						                </div>
						                <div class="col-md-4">
						                	<?php if($idUsuario>0)
						                	{
						                		?>
						                    <button class="sc-add-to-cart btn btn-success btn-sm pull-right">Añadir</button>
						                    <?php
						                	}
						                	else
						                	{
						                			?>
						                    <button type="button" class="sc-logout btn btn-success btn-sm pull-right">Añadir</button>
						                    <?php
						                	}
						                	?>
						                </div>
					                </div>
	                                
	                                
	                                <input name="product_price" id="product_price_<?php echo $idp;?>" value="<?php echo $venta;?>" type="hidden" />
	                                <input class="sc-product-id" name="product_id" value="<?php echo $idp;?>" type="hidden" />
	                                
					                                <!-- FIN AGREGAR PRODUCTOS-->

	                            
	                            <div class="clearfix"></div>
	                        </div>
	                    </div>
	                </div>