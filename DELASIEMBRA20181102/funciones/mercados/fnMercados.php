<?php
	include ("../../data/Conexion.php");
  session_start();
	error_reporting(0);
	// variable login que almacena el login o nombre de usuario de la persona logueada
  $login= isset($_SESSION['persona']);
  // cookie que almacena el numero de identificacion de la persona logueada
  $usuario= $_COOKIE['usuario'];
  $idUsuario= $_SESSION["idusuario"];
  $clave= $_COOKIE["clave"];
  $identificacion = $_COOKIE["usIdentificacion"];
  date_default_timezone_set('America/Bogota');
  $fecha=date("Y/m/d H:i:s");

  $opcion = $_POST['opcion'];
	
  if($opcion=="NUEVO")
  {
  ?>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
    <form name="form1" id="form1" class="form-horizontal">
      <div class="form-group">
           
           <div class="col-md-4"><strong>Nombre:</strong>
           
           <input  name="txtnombre" id="txtnombre" class="form-control input-sm" type="text" autocomplete="off" placeholder="Ingrese nombre">
           
          
           </div>
           <div class="col-md-4"><strong>Porcentaje Ganancia:</strong>
           
           <input  name="txtporcentaje" id="txtporcentaje" onkeypress="return validar_texto(event)" class="form-control input-sm" type="number">
           
          
           </div>
           <div class="col-md-4"><strong>Clasificaciones que aplican:</strong>           
           <select name="selclasificacion" id="selclasificacion" class="form-control selectpicker" multiple>
             <option value="">-Seleccione-</option>
             <?php $sql = pg_query($dbconn, "select cla_clave_int,cla_nombre from tbl_clasificacion");
             while($dat=  pg_fetch_array($sql))
             {
                ?>
                <option value="<?php echo $dat['cla_clave_int'];?>"><?php echo $dat['cla_nombre'];?></option>            
                <?php
             }
             ?>
             
           </select>       
           </div> 
           <div class="col-md-3 hide"><strong>Aplica Tamaño:</strong>           
           <input  name="cktamano" id="cktamano"  type="checkbox" value="1"/>          
           </div>
      </div>
   
    </form>
  <?php  
  echo "<script>INICIALIZARCONTENIDO();</script>";
  
} 
else if($opcion=="EDITAR" )
{
	  $id = $_POST['id'];
	  $coninfo = pg_query($dbconn,"select m.mer_clave_int id,m.mer_clasificacion as cla,m.mer_nombre as nom,m.mer_activo as est,mer_tamano tam,mer_porcentaje por from tbl_mercado m where m.mer_clave_int = '".$id."' limit 1");
	  $datinfo = pg_fetch_array($coninfo);	
	  $id = $datinfo['id'];
	  $cla = $datinfo['cla'];
	  $nom = $datinfo['nom'];
    $tam = $datinfo['tam'];
    $por = $datinfo['por'];

	//  $est = $datinfo['est'];
    $cla = explode(",", $cla);
  ?>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <form name="form1" id="form1" class="form-horizontal">
  <input type="hidden" id="idedicion" value="<?php echo $id;?>">
    <div class="form-group">
           
           <div class="col-md-4"><strong>Nombre:</strong>
           
           <input  name="txtnombre" id="txtnombre" class="form-control input-sm" type="text" autocomplete="off" placeholder="Ingrese nombre" value="<?php echo $nom;?>">
           
          
           </div>
            <div class="col-md-4"><strong>Porcentaje Ganancia:</strong>
           
           <input  name="txtporcentaje" id="txtporcentaje" onkeypress="return validar_texto(event)" class="form-control input-sm" type="number" value="<?PHP echo $por;?>">
           
          
           </div>
           <div class="col-md-4"><strong>Clasificaciones que aplican:</strong>           
           <select name="selclasificacion" id="selclasificacion" class="form-control selectpicker" multiple>
             <option value="">-Seleccione-</option>
             <?php $sql = pg_query($dbconn, "select cla_clave_int,cla_nombre from tbl_clasificacion");
             while($dat=  pg_fetch_array($sql))
             {
                ?>
                <option <?php if(in_array($dat['cla_clave_int'],  $cla)){ echo "selected"; } ?> value="<?php echo $dat['cla_clave_int'];?>"><?php echo $dat['cla_nombre'];?></option>            
                <?php
             }
             ?>             
           </select>       
           </div> 
           <div class="col-md-3 hide"><strong>Aplica Tamaño:</strong>           
           <input <?php if($tam==1){ echo "checked"; }?>  name="cktamano" id="cktamano" type="checkbox" value="1">          
           </div>
      </div>  
  </form>
  <?php  
  echo "<script>INICIALIZARCONTENIDO();</script>";
}
else if($opcion=="GUARDAR")
{
  $cla = implode(",",$_POST['cla']);
  $nombre  = $_POST['nombre'];
  $tamano = $_POST['tamano'];
  $porcentaje = $_POST['porcentaje']; if($por<=0 || $por==null){ $por = 0;}
  $veri = pg_query($dbconn,"select * from tbl_mercado where UPPER(mer_nombre) = UPPER('".$nombre."') and mer_activo!=2");
  $numv = pg_num_rows($veri);
  if($numv>0)
  {
     $res= "error";
     $msn = "El mercado ingresado ya existe";
  }
  else 
  {
    $ins = pg_query($dbconn,"insert into tbl_mercado(mer_nombre,mer_clasificacion,mer_usu_actualiz,mer_fec_actualiz,mer_tamano,mer_porcentaje) values('".$nombre."','".$cla."','".$usuario."','".$fecha."','".$tamano."','".$por."')");
    if($ins>0)
    {
      $res = "ok";
      $msn = "Mercado guardado correctamente";
    }
    else
    {
      $res = "error";
      $msn = "Surgió un error al guardar mercado. Error BD(".pg_last_error($dbconn).")";
    }	
	} 	 
  $datos[] = array('res' => $res ,'msn' => $msn );
  echo json_encode($datos);
} 
else if($opcion=="GUARDAREDICION" )
{
  $ide =$_POST['id'];
  $cla = implode(",",$_POST['cla']);
  $nombre  = $_POST['nombre'];
  $tamano = $_POST['tamano'];
  $por = $_POST['porcentaje']; if($por<=0 || $por==null){ $por = 0;}
  $veri = pg_query($dbconn,"SELECT * FROM tbl_mercado WHERE UPPER(uni_nombre  = '".$nombre."')) and mer_clave_int!='".$ide."' and (mer_activo != 2)");
  $numv = pg_num_rows($veri);
  if($numv>0)
  {
     $res= "error";
     $msn = "El mercado ingresado ya existe";
  }
  else 
  {
    $upd = pg_query($dbconn,"update tbl_mercado set mer_clasificacion='".$cla."',mer_nombre='".$nombre."',mer_usu_actualiz='".$usuario."',mer_fec_actualiz='".$fecha."',mer_tamano = '".$tamano."',mer_porcentaje = '".$por."' where mer_clave_int = '".$ide."'");
    if($upd>0)
    {
      
      $res = "ok";
      $msn = "Mercado actualizado correctamente"; 
    }
    else
    {
      $res = "error";
      $msn = "Surgió un error al modificar mercado. Error BD(".pg_last_error($dbconn).")";
    }	
	}	 
  $datos[] = array('res' => $res ,'msn' => $msn );
  echo json_encode($datos);
}
else if($opcion=="LISTAMERCADOS")
{
	?> 
  <script src="jsdatatable/mercados/jsmercados.js"></script>
  <meta http-equiv="Content-Type" content="text/html;charset=utf-8">
  <div>
    <table id="tbmercados" class="table table-striped" >
      <thead>
      <tr>
        <th class="dt-head-center" style="width:20px"></th>
        <th class="dt-head-center" style="width:20px"></th>
        <th class="dt-head-center" style="width:40px">NOMBRE</th>
        <th class="dt-head-center">CLASIFICACION</th> 
        <th class="dt-head-center">PORCENTAJE</th>             
      </tr>
      </thead>
        <tfoot>
      <tr>
      <th></th>
      <th></th>
      <th class="dt-head-center"></th>
      <th class="dt-head-center"></th>
      <th></th>
      </tr>
      </tfoot>           
      </tbody>
    </table>
  </div>
<?php
}
else if($opcion=="ELIMINAR")
{
  $id = $_POST['id'];
	$update = pg_query($dbconn,"update tbl_mercado set mer_activo = 2,mer_usu_actualiz ='".$usuario."',mer_fec_actualiz='".$fecha."' where mer_clave_int = '".$id."'");
	if($update>0){ $res = "ok"; $mns = "Mercado eliminado";  }else { $res = "error"; $msn = "Error BD. No se elimino el mercado"; }
  $datos[] = array("res"=>$res,"msn"=>$msn);
  echo json_encode($datos);
} 
?>