<?PHP 
session_start();
error_reporting(0);
$IP = $_SERVER['REMOTE_ADDR'];
include ("../../data/Conexion.php");
use  PHPMailer\PHPMailer\PHPMailer;
use  PHPMailer\PHPMailer\Exception;
require ('../../PHPMailer-master/src/PHPMailer.php');
require ('../../PHPMailer-master/src/Exception.php');
require ('../../PHPMailer-master/src/SMTP.php');
require_once('../../clases/pdf/html2pdf.class.php');
setlocale(LC_ALL,"es_ES.utf8","es_ES","esp");

function rgb2hex($rgb) {
   $rgb = array(str_replace(")","",str_replace("rgba(","",$rgb)));
   $hex = "#";
   $hex .= str_pad(dechex($rgb[0]), 2, "0", STR_PAD_LEFT);
   $hex .= str_pad(dechex($rgb[1]), 2, "0", STR_PAD_LEFT);
   $hex .= str_pad(dechex($rgb[2]), 2, "0", STR_PAD_LEFT);

   return $hex; // returns the hex value including the number sign (#)
}

function hex2rgb($hex) {
   $hex = str_replace("#", "", $hex);

   if(strlen($hex) == 3) {
      $r = hexdec(substr($hex,0,1).substr($hex,0,1));
      $g = hexdec(substr($hex,1,1).substr($hex,1,1));
      $b = hexdec(substr($hex,2,1).substr($hex,2,1));
   } else {
      $r = hexdec(substr($hex,0,2));
      $g = hexdec(substr($hex,2,2));
      $b = hexdec(substr($hex,4,2));
   }
   // $rgb = array($r, $g, $b);
   $rgb = 'rgba('.$r.','.$g.','.$b.', 0.5)';
   //return implode(",", $rgb); // returns the rgb values P12 by commas
   return $rgb; // returns an array with the rgb values
}
function decrypt($string, $key)
	{
		$result = "";
		$string = base64_decode($string);
		for($i=0; $i<strlen($string); $i++) 
		{
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)-ord($keychar));
			$result.=$char;
		}
		return $result;
	}
	function encrypt($string, $key) 
	{
		$result = "";	
		for($i=0; $i<strlen($string); $i++) 
		{	
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key))-1, 1);
			$char = chr(ord($char)+ord($keychar));
			$result.=$char;
		}
		return base64_encode($result);
	}
	function CalculaEdad( $fec ) {
    	list($Y,$m,$d) = explode("-",$fec);
    	return( date("md") < $m.$d ? date("Y")-$Y-1 : date("Y")-$Y );
	}
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
	//$idUsuario= $_SESSION["idusuario"];
$idUsuario = $_COOKIE["usIdentificacion"];
$conusu= pg_query($dbconn,"SELECT mer_clave_int,dir_clave_int,prf_clave_int,usu_imagen from tbl_usuario where usu_clave_int = '".$idUsuario."'");
$datusu = pg_fetch_array($conusu);
$idmercado = $datusu['mer_clave_int'];
$ultimadireccion = $datusu['dir_clave_int'];
$perfil = $datusu['prf_clave_int'];
$imgperfil = $datusu['usu_imagen'];
if($imgperfil=="" || $imgperfil==NULL)
{
  $imgperfil = "../../dist/img/default-user.png";
}
else 
{
	$imgperfil = "https://www.pavas.com.co/delasiembra/".$imgperfil;
}
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
$opcion = $_POST['opcion'];
 if($opcion=="VERPQR")
 {


   $conpreguntas = pg_query($dbconn,"SELECT pqr_clave_int,usu_clave_int, pqr_fec_actualiz FROM tbl_pqr WHERE usu_clave_int = '".$idUsuario."' and pqr_tipo = 0 order by pqr_fec_actualiz DESC")
 	//PREGUNTA RESPUESTA ORDENAMOS POR FECHA DE REGISTRO USUARIO NOTAS USUARIO A LA DERECHA
 	?>
  <div class="box box-warning direct-chat direct-chat-warning">
     <div class="box-header">
      <form id="forpregunta" >
        <div class="input-group">
          <textarea name="txtpqr" id="txtpqr" required placeholder="Ingrese la inquietud que presenta" class="form-control"></textarea> 
              <span class="input-group-btn" style="vertical-align: top">
                <button data-loading-text="Enviando..." type="submit" onclick="CRUDPQR('GUARDARPREGUNTA','')" id="btnenviarpregunta" class="btn btn-warning  btn-flat">Enviar</button>
              </span>
        </div>
      </form>
    </div>  
    <div class="box-body">
 	    <div class="direct-chat-messages" id="divcontenidopqr">



      </div>
    </div>
                 
    <!-- /.box-footer-->
</div>
 	<?php
  echo "<script>CRUDPQR('MOSTRARPQR');</script>";
 }
 else if($opcion=="MOSTRARPQR")
 {
      $conpreguntas = pg_query($dbconn,"SELECT pqr_clave_int,u.usu_clave_int,pqr_tipo,pqr_descripcion,pqr_usu_actualiz,pqr_fec_actualiz,pqr_estado,u.usu_nombre,u.usu_apellido,usu_imagen FROM tbl_pqr p join tbl_usuario u ON u.usu_clave_int = p.usu_clave_int WHERE u.usu_clave_int = '".$idUsuario."' ORDER by pqr_fec_actualiz DESC");

      $numpre = pg_num_rows($conpreguntas);
      if($numpre>0)
      {
          for($np=0;$np<$numpre;$np++)
          {
            $datp = pg_fetch_array($conpreguntas);
            $nomu = $datp['usu_nombre']." ".$datp['usu_apellido'];
            $imgu = $datp['usu_imagen'];
            $descp = $datp['pqr_descripcion'];
            $fecp = $datp['pqr_fec_actualiz'];
            if($imgu=="" || $imgu==NULL)
            {
              $imgu = "dist/img/default-user.png";
            }
            $an = strftime("%Y",strtotime($fecp));
            $dia = strftime("%a",strtotime($fecp));
            $dia1 = strftime("%d",strtotime($fecp));
            $mes = strftime("%B",strtotime($fecp));
            $hi = strftime("%H:%M",strtotime($fecp));
            $fecp = $dia.", ".$dia1." de ".$mes." de ".$an;
            $fecp = $fecp . ", ".$hi;
            ?>
            <div class="direct-chat-msg">
              <div class="direct-chat-info clearfix">
                <span class="direct-chat-name pull-left"><?php echo $nomu;?></span>
                <span class="direct-chat-timestamp pull-right"><?PHP echo $fecp;?></span>
              </div>
              <!-- /.direct-chat-info -->
              <img class="direct-chat-img" src="<?php echo $imgu; ?>" alt="imagen usuario"><!-- /.direct-chat-img -->
              <div class="direct-chat-text">
                <?php echo $descp;?>
                <span class="pull-right"><i class="fa fa-question-circle fa-2x"></i></span>
              </div>
          <!-- /.direct-chat-text -->
            </div>
            <div class="direct-chat-msg respuesta">
               <div class="direct-chat-text">
               <textarea name="" class="form-control"></textarea>
              </div>
            </div>
            <?php
          }
      }
 }
 else if($opcion=="GUARDARPREGUNTA")
 {
     $descripcion = nl2br($_POST['txtpqr']);

     $inspre = pg_query($dbconn,"INSERT INTO tbl_pqr(usu_clave_int,pqr_tipo,pqr_descripcion,pqr_usu_actualiz,pqr_fec_actualiz,pqr_estado) VALUES('".$idUsuario."','0','".$descripcion."','".$usuario."','".$fecha."','0')");
     if($inspre>0)
     {
        $res = "ok";
        $msn = "Tu inquietud fue enviada a delasiembra.com, en breve estaremos dando respuesta a tu inquietud";
     }
     else
     {
        $res = "error";
        $msn = "Surgió un error al enviar inquietud. Error BD(".pg_last_error($dbconn).")";
     }

    $datos = array();
    $datos["res"] = $res;
    $datos["msn"] = $msn;

     //echo json_encode($datos);
     echo json_encode($datos, JSON_FORCE_OBJECT);
 }
?>