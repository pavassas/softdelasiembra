
<?php
include ("../../data/Conexion.php");
session_start();
error_reporting(0);
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_SESSION["idusuario"];
$clave= $_COOKIE["clave"];
$identificacion = $_COOKIE["usIdentificacion"];
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//Vvariable GET
//$Socios = $_GET['Socios'];

// DB table to use
$table = 'tbl_ofertas';
// Table's primary key
$primaryKey = 'o.ofe_clave_int';
$nom = $_POST['nom'];
$cod = $_POST['cod'];
$cat = $_POST['cat']; $cat = implode(', ', (array)$cat); if($cat==""){$cat1="'0'";}else {$cat1=$cat;}

$mar = $_POST['mar'];

$pro = $_POST['pro']; $pro = implode(', ', (array)$pro); if($pro==""){$pro1="'0'";}else {$pro1=$pro;}

$est = $_POST['est']; $est = implode(', ', (array)$est); if($est==""){$est1="'0'";}else {$est1=$est;}

$ofe = $_POST['ofe']; $ofe = implode(', ', (array)$ofe); if($ofe==""){$ofe1="'0'";}else {$ofe1=$ofe;}

// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object


// parameter names
$columns = array(
		array(
			'db' => 'o.ofe_clave_int',
			'dt' => 'DT_RowId', 'field' => 'ofe_clave_int',
			'formatter' => function( $d, $row ) {
				// Technically a DOM id cannot start with an integer, so we prefix
				// a string. This can also be useful if you have multiple tables
				// to ensure that the id is unique with a different prefix
				return 'rowo_'.$d;
			}
		),		
		array( 'db' => 'o.ofe_clave_int', 'dt' => 'Eliminar', 'field' => 'ofe_clave_int','formatter'=>function($d,$row){
			return "<a class='btn btn-circle btn-block btn-danger btn-xs' onclick=CRUDOFERTAS('ELIMINAROFERTA','".$d."') title='Eliminar Oferta' style='heigth:22px; width:22px'><i class='fa fa-trash'></i></a>";	
		}),
		array( 'db' => 'o.ofe_clave_int', 'dt' => 'Editar', 'field' => 'ofe_clave_int','formatter'=>function($d,$row){
			return "<a class='btn btn-circle btn-block btn-warning btn-xs' onClick=CRUDOFERTAS('EDITAROFERTA','".$d."') title='Editar Oferta' style='heigth:22px; width:22px' data-toggle='modal' data-target='#modalregistro'><i class='fa fa-pencil'></i></a>";		
		}),
		array( 'db' => 'o.ofe_titulo', 'dt' => 'Nombre', 'field' => 'ofe_titulo'),
        array( 'db' => 'o.ofe_inicio', 'dt' => 'Inicio', 'field' => 'ofe_inicio' ),
        array( 'db' => 'o.ofe_fin', 'dt' => 'Fin', 'field' => 'ofe_fin'),
        array( 'db' => 'o.ofe_hor_inicio', 'dt' => 'Hi', 'field' => 'ofe_hor_inicio'),       
        array( 'db' => 'o.ofe_hor_fin', 'dt' => 'Hf', 'field' => 'ofe_hor_fin'),
        array( 'db' => 'o.ofe_estado', 'dt' => 'Estado', 'field' => 'ofe_estado','formatter'=>function($d,$row){
        	if($d==0){ return "Inactiva";}
        		else if($d==1){ return "Activa";}
        			else if($d==2){ return "Vencida";}

        }),
        array( 'db' => 'o.ofe_tipo_descuento', 'dt' => 'Tipo', 'field' => 'ofe_tipo_descuento','formatter'=>function($d,$row){
        	if($d==1){
        		return "Valor";
        	}
        	else
        	{
        		return "Porcentaje";
        	}
        }),   
        array( 'db' => 'o.ofe_descuento', 'dt' => 'Descuento', 'field' => 'ofe_descuento')
);

$sql_details = array(
	'user' => 'usrpavas',
	'pass' => '9A12)WHFy$2p4v4s',
	'db'   => 'dbdelasiembra',
	'port'   => '5432',
	'host' => '127.0.0.1'
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );
$whereAll = "";// customerid =".$customerid." AND date( orderdate ) >= '".$startdate."' AND date( orderdate ) <= '".$enddate."'";
$groupBy = 'o.ofe_clave_int';//'p.pro_clave_int';
$with = '';
$joinQuery = "FROM tbl_productos AS p JOIN tbl_categorias c ON c.cat_clave_int = p.cat_clave_int join tbl_ofertas_producto AS op on op.pro_clave_int = p.pro_clave_int join tbl_ofertas AS o on o.ofe_clave_int = op.ofe_clave_int ";
$extraWhere =" ( p.pro_nombre ILIKE REPLACE('".$nom."%',' ','%') OR '".$nom."' IS NULL OR '".$nom."' = '' ) and ( p.pro_codigo ILIKE REPLACE('".$cod."%',' ','%') OR '".$cod."' IS NULL OR '".$cod."' = '' ) and ( p.pro_marca ILIKE REPLACE('".$mar."%',' ','%') OR '".$mar."' IS NULL OR '".$mar."' = '' ) and ( p.cat_clave_int IN(".$cat1.") OR '".$cat."' IS NULL OR '".$cat."' = '') and ( p.pro_clave_int IN(".$pro1.") OR '".$pro."' IS NULL OR '".$pro."' = '') and ( o.ofe_clave_int IN(".$ofe1.") OR '".$ofe."' IS NULL OR '".$ofe."' = '') and ( o.ofe_estado IN(".$est1.") OR '".$est."' IS NULL OR '".$est."' = '') and pro_activo!=2 and ofe_estado!=3";


echo json_encode(
	SSP::simple($_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy,$with )

);