<?php

	setlocale(LC_ALL,"es_ES.utf8","es_ES","esp");
	error_reporting(E_ALL);

	$a=49;
	echo ceil($a/50)*50;

	include('../../data/Conexion.php');
	$idpedido = 76;
			
	$coninfo = pg_query($dbconn,"select u.usu_nombre,u.usu_apellido,u.usu_usuario,p.ped_fecha,u.usu_email,p.dir_clave_int,p.ped_domicilio,p.ped_telefono,ped_codigo,ped_tiempo_entrega,ped_nota,ped_fec_programada,ped_hor_programada from tbl_pedidos p JOIN tbl_usuario u ON u.usu_clave_int = p.usu_clave_int WHERE p.ped_clave_int = '".$idpedido."'");
	$datinfo = pg_fetch_array($coninfo);
	$cliente = $datinfo['usu_nombre']." ".$datinfo['usu_apellido'];
	$pedfecha = $datinfo['ped_fecha'];
	$pedentrega = $datinfo['ped_fec_programada'];
	$pedhora = $datinfo['ped_hor_programada'];
	$email = $datinfo['usu_email'];
	$dir = $datinfo['dir_clave_int'];
	$domicilio = $datinfo['ped_domicilio'];
	$telefono = $datinfo['ped_telefono'];
	$codigoped = $datinfo['ped_codigo'];
	$tiempo = $datinfo['ped_tiempo_entrega']." min";
	$an = strftime("%Y",strtotime($pedfecha));
	$dia = strftime("%a",strtotime($pedfecha));
	$dia1 = strftime("%d",strtotime($pedfecha));
	$mes = strftime("%B",strtotime($pedfecha));
	$hi = strftime("%H:%M",strtotime($pedfecha));
	$pedfecha = $dia.", ".$dia1." de ".$mes." de ".$an;
	$pedfecha = $pedfecha . ", ".$hi;
	$nota = $datinfo['ped_nota'];

	$an = strftime("%Y",strtotime($pedentrega));
	$dia = strftime("%a",strtotime($pedentrega));
	$dia1 = strftime("%d",strtotime($pedentrega));
	$mes = strftime("%B",strtotime($pedentrega));
	$hi = strftime("%H:%M",strtotime($pedentrega));
	$pedentrega = $dia.", ".$dia1." de ".$mes." de ".$an;



	$condir = pg_query($dbconn,"select s.sec_nombre,b.bar_nombre,s.sec_domicilio,d.dir_nomenclatura,d.dir_letra,d.dir_numero1,d.dir_numero2,d.dir_detalle from tbl_direcciones d join tbl_barrio b on b.bar_clave_int = d.bar_clave_int join tbl_sector s on s.sec_clave_int = b.sec_clave_int where d.dir_clave_int = '".$dir."'");
	$datdir = pg_fetch_array($condir);
	$sector = $datdir['sec_nombre'];
	$barrio = $datdir['bar_nombre'];
	$nome = $datdir['dir_nomenclatura'];
	$letra = $datdir['dir_letra'];
	$nume1 = $datdir['dir_numero1'];
	$nume2 = $datdir['dir_numero2'];
	$dirdetalle = $datdir['dir_detalle'];
	$direccion = $nome." ".$letra."#".$nume1."-".$nume2;
   	 

	$condet = pg_query($dbconn, "SELECT d.pde_clave_int,p.pro_clave_int,p.pro_nombre,p.pro_descripcionbreve,p.pro_codigo,c.cla_nombre,pde_estado,pde_tamano,cla_nombre,pde_cantidad,pde_valor,pde_unidad,pro_uni_venta,pro_pes_venta,pro_mu_venta,pro_mp_venta,pde_minimo,pde_unidad ,pde_codigo from tbl_pedidos_detalle d JOIN tbl_productos p on p.pro_clave_int = d.pro_clave_int join tbl_clasificacion c on c.cla_clave_int = d.cla_clave_int where d.ped_clave_int = '".$idpedido."' and pde_confirmado = 2 order by p.pro_nombre,c.cla_nombre");
	$numdet = pg_num_rows($condet);

   	  ?>
   	 <!DOCTYPE html>
		<html lang="en">
		<head>
		<meta charset="UTF-8">
		<title>Document</title>
		<style type="text/css" media="screen">

		.texttituloleft
		{
		text-align: left;font-weight: bold;
		}
		.textleft
		{
		text-align: left;
		}

		.textcenter
		{
		text-align: center;
		}
		.textright
		{
		text-align: right;
		}
		.titulocenter
		{
		text-align: center;font-weight: bold;
		}
		.tdcenter
		{
		text-align: center;
		}
		.titulorigth
		{
		text-align: right;font-weight: bold;
		}
		</style>

		</head>
		<body>
   	  <div id="divresumenpedido" style="width: 820px;background:#FFF url('https://wwww.delasiembra.com/dist/img/plantillas/hojaCorreo.jpg') no-repeat top center;font-family: calibri;font-size: 12px;padding: 10px">
     


       <table cellpadding="2" cellspacing="2" style="width: 80%; margin: 0 auto">
       	<tr><th colspan="5" style="padding: 10px;color:#000;font-size: 23px;">Cliente</th>
        <tr>
        <tr>
        	<th colspan="5" class="textleft" style="padding: 20px; text-align: left;">
        		<p><strong>Nombre:</strong><?php  echo $cliente;?></p>
        		<p><strong>Correo electrónico:</strong><?php echo $email;?></p>
        		<p><strong>Telefono:</strong><?php echo $telefono;?></p>
        	
        </th>
    	</tr>
       	<tr><th colspan="5" style="padding: 10px;background-color: #8BC34A; color:#FFF;font-size: 20px;">Detalles del pedido</th>
        <tr>
        	<th colspan="5" class="textleft" style="padding: 20px; text-align: left;">
        		<p>Pedido <span style="color:#8BC34A; font-weight: bold;"><?php echo $codigoped; ?></span> realizado el <span style="color:#8BC34A; font-weight: bold;"><?php echo $pedfecha;?></span> </p>
        		<p><strong>Fecha Entrega:</strong> <?php echo $pedentrega;?></p>
        		<p><strong>Hora Entrega:</strong> <?php echo $pedhora;?></p>
        		<p><strong>Dirección:</strong> <?php echo $direccion."(".$sector."-".$barrio.")";?></p>
        		<p><strong><?php echo $dirdetalle;?></p>

        	
        </th>
    	</tr>
    	<tr>
    	<th style="background-color: #cfe2ba;">Codigo</th>
    	<th style="background-color: #cfe2ba;">Producto</th>
    	<th style="background-color: #cfe2ba;">Precio Unidad</th>
    	<th style="background-color: #cfe2ba;">Cantidad</th>
    	<th style="background-color: #cfe2ba;">Precio Total</th>
    	</tr>
    	<?php
			$total = 0;
			while($datdet= pg_fetch_array($condet))
			{
				$iddetalle = $datdet['pde_clave_int'];
				$idp = $datdet['pro_clave_int'];
				$codlinea = $datdet['pde_codigo'];
				$nompro = $datdet['pro_nombre'];
				$codpro = $datdet['pro_codigo'];
				$nomcla = $datdet['cla_nombre'];
				$maduracion = $datdet['pde_estado'];
				$descp = $datdet['pro_descripcionbreve'];
				$precio = $datdet['pde_valor'];
				$cantidad = $datdet['pde_cantidad'];
				$subtotal = $precio * $cantidad;
				$tamano = $datdet['pde_tamano'];

				$observacion = "<strong>Clasificación:</strong>".$nomcla." <br>";
				if($tamano!=""){$observacion.="<strong>Tamano:</strong>".$tamano."<br> ";}
				if($maduracion!=""){$observacion.="<strong>Maduración:</strong>".$maduracion." ";}
				if($tamano!=""){ $tamano = '-'.$tamano;}
				
				$puv = $datdet['pro_uni_venta'];
				$ppv = $datdet['pro_pes_venta'];
				$pmuv = $datdet['pro_mu_venta'];
				$pmpv = $datdet['pro_mp_venta'];
				$min = $datdet['pde_minimo'];
				$uni = $datdet['pde_unidad'];

				if($uni==0)
				{
					$unimin = $pmuv ;
					$cantidad = $cantidad;
					$vis = $cantidad." UND";
				}
				else
				{
					$unimin = $pmpv;
					$cantidad = $cantidad * $min;//esto debido a que el precio del producto cuando es en peso es precio por la unidad minima ejemplo:250gr = 1 unidad
					if($cantidad>=1000)
					{
						$vis = $cantidad/1000 ." KG";
					}
					else 
					{
						$vis = $cantidad." GR";
					}
				}
				
				
				$con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idp."' and pri_activo = 1 LIMIT 1");
				$dat = pg_fetch_row($con);
				$cimg = $dat[0];
				$img = $dat[1];
				?>

		    	<tr>
		    	<td><?php echo $codpro;?></td>
		    	<td><?php echo $nompro."(".$maduracion.$tamano.")";?></td>
		    	<td class="textright">$<?php echo number_format($precio,0,',',',');?></td>
		    	<td class="textcenter"><?php echo $vis;?></td>
		    	<td class="textright">$<?php echo number_format($subtotal,0,',',',');?></td>
		    	</tr>
    		<?php
				$total = $total + $subtotal;
			}
			$totalpedido = $total + $domicilio;
			?>

    	<tr>
    	<td></td>
    	<td colspan="3" style="background-color: #d1cece;text-align: right;">Productos</td>    	
    	<td style="background-color: #d1cece;text-align: right;">$<?php echo number_format($total,0,',',',');?></td>
    	</tr>
    	<tr>
    	<td></td>
    	<td colspan="3" style="background-color: #d1cece;text-align: right;">Domicilio</td>    	
    	<td style="background-color: #d1cece;text-align: right;">$<?php echo number_format($domicilio,0,',',',');?></td>
    	</tr>
    	<tr>
    	<td></td>
    	<td colspan="3" style="background-color: #a3c977; color:#FFF;font-size: 16px; font-weight: bold; text-align: right;">TOTAL</td>    	
    	<td style="background-color: #a3c977; color:#FFF;font-size: 16px; font-weight: bold; text-align: right;">$<?php echo number_format($totalpedido,0,',',',');?></td>
    	</tr>
    	<tr>									
				<td colspan="5">
					<div style="text-align:left">
						<div style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:rgba(0,0,0,0.54);font-size:12px;line-height:20px;padding-top:10px">
							<div>Este mensaje es generado automáticamente por <strong>delasiembra.com</strong> , por favor no responda a este correo, cualquier duda adicional puede resolverla comunicandose al +57 310 8611064
							</div>
							<div style="direction:ltr; font-weight: bold;color: #8BC34A">© <?php echo date('Y')?> delasiembra.com  
							</div>
						</div>
						<div style="display:none!important;max-height:0px;max-width:0px">et:100</div>
					</div>
				</td>
			</tr>
       </table>

      

   	  	
   	  </div>

   	  </body>
</html>


