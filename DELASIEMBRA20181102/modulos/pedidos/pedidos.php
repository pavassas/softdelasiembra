<?php 
  include ("../../data/Conexion.php");
  session_start();
  error_reporting(0);
  // variable login que almacena el login o nombre de usuario de la persona logueada
  $login= isset($_SESSION['persona']);
  // cookie que almacena el numero de identificacion de la persona logueada
  $usuario= $_COOKIE['usuario'];
  $idUsuario= $_SESSION["idusuario"];
  $clave= $_COOKIE["clave"];
  $identificacion = $_COOKIE["usIdentificacion"];
  date_default_timezone_set('America/Bogota');
  $fecha=date("Y/m/d H:i:s");
 
  //echo "<script>INICIALIZAR('Pedidos');</script>";
 
//echo "<script>CRUDPRODUCTOS('LISTAPRODUCTOS','');</script>"; 
?>
<script>
  INICIALIZAR('Pedidos');
</script>
<input id="idcategoria" value="" type="hidden">
<section class="content-header">
      <h4>
        <a ui-sref="Pedidos({ Idestado: ''})" ui-sref-opts="{reload: true}">Inicio</a>
        <small>/ <span id="gadcategoria"></span></small>
      </h4>
      <ol class="breadcrumb">
      	<li><a href="#"><i class="fa fa-dashboard"></i> Pedidos</a></li>              
      </ol>
</section>
<section class="content">
  
  <div class="row">
    <div class="col-md-12" id="tabladatos"></div>
   <!-- <aside class="col-md-4">
     
      
    </aside>-->
    <div class="col-md-12">
      <em><strong><span class="text-red">IMPORTANTE:</span></strong>El muestrario de productos aquí descritos está sujeto a disponibilidad y stock</em>
    </div>
  </div>
 </section>