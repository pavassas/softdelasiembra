<?php
include ("../../data/Conexion.php");
session_start();
error_reporting(0);
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_SESSION["idusuario"];
$clave= $_COOKIE["clave"];
$identificacion = $_COOKIE["usIdentificacion"];
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//Vvariable GET
//$Socios = $_GET['Socios'];

// DB table to use
$table = 'tbl_pedidos';
// Table's primary key
$primaryKey = 'p.ped_clave_int';

$estado = $_POST['estado'];
$cliente = $_POST['cliente']; $cliente = implode(', ', (array)$cliente); 
if($cliente==""){$cliente1="'0'";}else {$cliente1=$cliente;}
// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object


// parameter names
$columns = array(
	array(
		'db' => 'p.ped_clave_int',
		'dt' => 'DT_RowId', 'field' => 'ped_clave_int',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'rowp_'.$d;
		}
	),	
        
        array( 'db' => 'p.ped_clave_int', 'dt' => 'Verificar', 'field' => 'ped_clave_int','formatter'=>function($d,$row){

                if($row[7] and $row[13]==0)
                {
                    return "<a class='btn btn-circle btn-block btn-danger btn-xs' onclick=CRUDPEDIDOS('VERIFICARPEDIDO','".$d."') title='Verificar Pedido' style='heigth:22px; width:22px'><i class='fa fa-check'></i></a>";
                }
                else
                {
                    return "<a class='btn btn-circle btn-block btn-info btn-xs' onclick=CRUDPEDIDOS('VERDETALLE','".$d."') title='Ver detalle' style='heigth:22px; width:22px'><i class='fa fa-eye'></i></a>";
                 }
        
        }),
        array( 'db' => 'p.ped_clave_int', 'dt' => 'Asociar', 'field' => 'ped_clave_int','formatter'=>function($d,$row){
        		if($row[7]==1 and $row[13]==1) {//verificar si hay productos sin confirmar y bpquear si si

                return "<button type='button' class='btn btn-circle btn-block btn-warning btn-xs' onClick=CRUDPEDIDOS('ASIGNARPEDIDO','".$d."') title='Asignar Pedido' style='heigth:22px; width:22px'><i class='fa fa-motorcycle'></i></button>";
                }
                else if($row[7]==1 and $row[13]==0)
                {
                    return "<button type='button' class='btn btn-circle btn-block btn-default btn-xs'  title='El pedido esta pendiente de verificacion' style='heigth:22px; width:22px'><i class='fa fa-motorcycle'></i></button>";
                }
                else if($row[7]==3)
                {
                	return "<button type='button' class='btn btn-circle btn-block btn-success btn-xs' title='Pedido entregado' style='heigth:22px; width:22px'><i class='ion ion-android-checkmark-circle'></i></button>";	
                }        
                else if($row[7]==2)
                {
                    return "<button type='button' class='btn btn-circle btn-block btn-success btn-xs' title='Terminar entrega' style='heigth:22px; width:30px' onClick=CRUDPEDIDOS('CERRARPEDIDO2','".$d."')><i class='fa fa-handshake-o'></i></button>";  
                }

               // return "<ul class='progressbar2'><li class='active'></li><li></li><li></li><li></li></ul>";
        }),	
		
		array( 'db' => 'p.ped_codigo', 'dt' => 'Codigo', 'field' => 'ped_codigo'),
        array( 'db' => 'p.ped_fecha', 'dt' => 'Fecha', 'field' => 'ped_fecha'),
		array( 'db' => 'p.ped_total', 'dt' => 'Total', 'field' => 'ped_total' ),
        array( 'db' => 'p.ped_domicilio', 'dt' => 'Domicilio', 'field' => 'ped_domicilio' ),
        array( 'db' => 'p.ped_estado', 'dt' => 'Estado', 'field' => 'ped_estado'),
        array( 'db' => "u.usu_nombre", 'dt' => 'Cliente',  'field' => 'usu_nombre','formatter'=>function($d, $row){
        	return $d." ".$row[9];
        }), 
        array('db' => "u.usu_apellido", 'dt' => 'apellido',  'field' => 'usu_apellido'),     
        array( 'db' => 'p.ped_clave_int', 'dt' => 'Clave', 'field' => 'ped_clave_int'),
        array( 'db' => 'p.ped_domiciliario', 'dt' => 'Domiciliario', 'field' => 'ped_domiciliario'),
        array( 'db' => 'p.dir_clave_int', 'dt' => 'Direccion', 'field' => 'dir_clave_int','formatter'=>function($d,$row){
            global $dbconn;
            $condir = pg_query($dbconn,"select s.sec_nombre,b.bar_nombre,s.sec_domicilio,d.dir_nomenclatura,d.dir_letra,d.dir_numero1,d.dir_numero2,d.dir_detalle from tbl_direcciones d join tbl_barrio b on b.bar_clave_int = d.bar_clave_int join tbl_sector s on s.sec_clave_int = b.sec_clave_int where d.dir_clave_int = '".$d."'");
            $datdir = pg_fetch_array($condir);
            $sector = $datdir['sec_nombre'];
            $barrio = $datdir['bar_nombre'];
            $nome = $datdir['dir_nomenclatura'];
            $letra = $datdir['dir_letra'];
            $nume1 = $datdir['dir_numero1'];
            $nume2 = $datdir['dir_numero2'];
            $direccion = $nome." ".$letra."#".$nume1."-".$nume2;
            return $direccion;
          }),
        array( 'db' => 'p.ped_verificado', 'dt' => 'Verificacion', 'field' => 'ped_verificado'),
);

$sql_details = array(
	'user' => 'usrpavas',
	'pass' => '9A12)WHFy$2p4v4s',
	'db'   => 'dbdelasiembra',
	'port'   => '5432',
	'host' => '127.0.0.1'
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );
$whereAll = "";
$groupBy = '';
$with = '';
$joinQuery = "FROM tbl_pedidos AS p JOIN tbl_usuario u ON u.usu_clave_int = p.usu_clave_int";
$extraWhere =" p.ped_estado = '".$estado."' and ( u.usu_clave_int IN(".$cliente1.") OR '".$cliente."' IS NULL OR '".$cliente."' = '')";

echo json_encode(
	SSP::simple($_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy,$with )

);