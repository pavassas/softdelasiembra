function ajaxFunction()
  {
  var xmlHttp;
  try
    {
    // Firefox, Opera 8.0+, Safari
    xmlHttp=new XMLHttpRequest();
    return xmlHttp;
    }
  catch (e)
    {
    // Internet Explorer
    try
      {
      xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
      return xmlHttp;
      }
    catch (e)
      {
      try
        {
        xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
        return xmlHttp;
        }
      catch (e)
        {
        alert("Your browser does not support AJAX!");
        return false;
        }
      }
    }
  }

function EDITAR(v,m)
{	
	if(m == 'EMPRESA')
	{
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('editarempresa').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#editarempresa").html("<img alt='cargando' src='img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?editarepr=si&epredi="+v,true);
		ajax.send(null);
	}
}
function GUARDAR(v,id)
{	
	if(v == 'EMPRESA')
	{
		var epr = form1.empresa1.value;
		var le = epr.length;
		var doc = form1.documento1.value;
		var tel = form1.telefono1.value;
		var dir = form1.direccion1.value;
		var ema = form1.email1.value;
		var fax = form1.fax1.value;
		var ciu = form1.ciudad1.value;
		
		var ajax;
		ajax=new ajaxFunction();
		ajax.onreadystatechange=function()
		{
			if(ajax.readyState==4)
		    {
	   		     document.getElementById('datos').innerHTML=ajax.responseText;
		    }
		}
		jQuery("#datos").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		ajax.open("GET","?guardarepr=si&epr="+epr+"&doc="+doc+"&tel="+tel+"&dir="+dir+"&ema="+ema+"&fax="+fax+"&ciu="+ciu+"&le="+le+"&e="+id,true);
		ajax.send(null);
		if(epr != '' && doc != '' && le >= 3)
		{
			setTimeout("CRUDEMPRESA('LISTAEMPRESAS','');",1000);//setInterval("window.location.href='empresas.php';",3000);
		}
	}
}

function CRUDEMPRESA(o,id)
{
	if(o=="LISTAEMPRESAS")
	{
	   $("#empresas").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='50' />"); //loading gif will be overwrited when ajax have success
		$.post('fnEmpresas.php',{opcion:o},
		function(data)
		{
		   $('#empresas').html(data);
		});
	}
	else if(o=="ACTIVAR")
    {
		 $.post('fnEmpresas.php',{opcion:o,id:id},
		 function(data)
		 {
			 data = $.trim(data)
			 if(data==1)
			 {
		     }
			 else
			 {
				$('#msn').html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4><i class="icon fa fa-ban"></i>Alerta!</h4>Ocurrio un Error al activar Empresa N'+id+'. Verificar</div>');
			 }
		 });
    }
	else if(o=="INACTIVAR")
    {
		  $.post('fnEmpresas.php',{opcion:o,id:id},
		 function(data)
		 {
			 data = $.trim(data)
			 if(data==1)
			 {
		     }
			 else
			 {
			    $('#msn').html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4><i class="icon fa fa-ban"></i>Alerta!</h4>Ocurrio un Error al inactivar Empresa N'+id+'. Verificar</div>');
			 }
		 });
    }
	else if(o=="ELIMINAR")
    {
		  $.post('fnEmpresas.php',{opcion:o,id:id},
		 function(data)
		 {
			 data = $.trim(data)
			 if(data==1)
			 { 
			 var table = $('#tbEmpresas').DataTable();
		table.row('#row_'+id).remove().draw(false);	
				setTimeout(CRUDEMPRESA('TODOS',''),1);
		     }
			 else
			 {
			    $('#msn').html('<div class="alert alert-danger"> <button type="button" class="close" data-dismiss="alert">&times;</button><h4><i class="icon fa fa-ban"></i>Alerta!</h4>Ocurrio un Error al eliminar Empresa N'+id+'. Verificar</div>');
			 }
		 });
		
    }else
	if(o=="TODOS")
	{
	   $("#todose").html("<img alt='cargando' src='../../img/cargando.gif' height='20' width='30' />"); //loading gif will be overwrited when ajax have success
		$.post('fnEmpresas.php',{opcion:o},
		function(data)
		{
		   $('#todose').html(data);
		});
	}
   else if(o == 'nuevaempresa')
	{
		var epr = $('#empresa').val();
		var le = epr.length;
		var doc = $('#documento').val();
		var tel = $('#telefono').val();
		var dir = $('#direccion').val();
		var ema = $('#email').val();
		var fax = $('#fax').val();
		var ciu = $('#ciudad').val();	
	
		$("#datos1").html("<img alt='cargando' src='../../img/ajax-loader.gif' height='20' width='20' />"); //loading gif will be overwrited when ajax have success
		$.post("fnEmpresas.php",{opcion:o,epr:epr,doc:doc,tel:tel,dir:dir,ema:ema,fax:fax,ciu:ciu,le:le},
		function(data)
		{
		    if(data[0].res==1)
			{
				$('#empresa').val('');				
				$('#documento').val('');
				$('#telefono').val('');
				$('#direccion').val('');
				$('#email').val('');
				$('#fax').val('');
				$('#ciudad').val('');	
				$("#datos1").html(data[0].msn);
				setTimeout("CRUDEMPRESA('LISTAEMPRESAS','');",1000);
				setTimeout("CRUDEMPRESA('TODOS','');",1000);
			}
			else
			{
			    $("#datos1").html(data[0].msn);
			}
		},"json");		
	}
}
function seltodas()
{
	var to = $('#todos').is(':checked'); 
	if(to==true)
	{
	
	$("#tbEmpresas tbody tr").each(function (index) 
			{
	        $(this).addClass('selected');
			});
	}
	else
	{			
	$("#tbEmpresas tbody tr").each(function (index) 
			{
	        $(this).removeClass('selected');
			});
	}	
}