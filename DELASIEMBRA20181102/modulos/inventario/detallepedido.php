<?php

	setlocale(LC_ALL,"es_ES.utf8","es_ES","esp");
			
	$coninfo = pg_query($dbconn,"select u.usu_nombre,u.usu_apellido,u.usu_usuario,p.ped_fecha,u.usu_email,p.dir_clave_int,p.ped_domicilio,p.ped_telefono,ped_codigo,ped_tiempo_entrega,ped_nota,ped_fec_programada,ped_hor_programada from tbl_pedidos p JOIN tbl_usuario u ON u.usu_clave_int = p.usu_clave_int WHERE p.ped_clave_int = '".$idpedido."'");
	$datinfo = pg_fetch_array($coninfo);
	$cliente = $datinfo['usu_nombre']." ".$datinfo['usu_apellido'];
	$pedfecha = $datinfo['ped_fecha'];
	$email = $datinfo['usu_email'];
	$dir = $datinfo['dir_clave_int'];
	$domicilio = $datinfo['ped_domicilio'];
	$telefono = $datinfo['ped_telefono'];
	$codigoped = $datinfo['ped_codigo'];
	$tiempo = $datinfo['ped_tiempo_entrega']." min";
	$an = strftime("%Y",strtotime($pedfecha));
	$dia = strftime("%a",strtotime($pedfecha));
	$dia1 = strftime("%d",strtotime($pedfecha));
	$mes = strftime("%B",strtotime($pedfecha));
	$hi = strftime("%H:%M",strtotime($pedfecha));
	$pedfecha = $dia.", ".$dia1." de ".$mes." de ".$an;
	$pedfecha = $pedfecha . ", ".$hi;
	$nota = $datinfo['ped_nota'];

	$fecent = $datinfo['ped_fec_programada'];
	$horent = $datinfo['ped_hor_programada'];
	$an = strftime("%Y",strtotime($fecent));
	$dia = strftime("%a",strtotime($fecent));
	$dia1 = strftime("%d",strtotime($fecent));
	$mes = strftime("%B",strtotime($fecent));
	
	$fecent = $dia.", ".$dia1." de ".$mes." de ".$an;
	$fecent = $fecent . ", ".date("g:i a",strtotime($horent));




	$condir = pg_query($dbconn,"select s.sec_nombre,b.bar_nombre,s.sec_domicilio,d.dir_nomenclatura,d.dir_letra,d.dir_numero1,d.dir_numero2,d.dir_detalle from tbl_direcciones d join tbl_barrio b on b.bar_clave_int = d.bar_clave_int join tbl_sector s on s.sec_clave_int = b.sec_clave_int where d.dir_clave_int = '".$dir."'");
	$datdir = pg_fetch_array($condir);
	$sector = $datdir['sec_nombre'];
	$barrio = $datdir['bar_nombre'];
	$nome = $datdir['dir_nomenclatura'];
	$letra = $datdir['dir_letra'];
	$nume1 = $datdir['dir_numero1'];
	$nume2 = $datdir['dir_numero2'];
	$direccion = $nome." ".$letra."#".$nume1."-".$nume2;
	$dirdetalle = $datdir['dir_detalle'];
   	 

	$condet = pg_query($dbconn, "SELECT d.pde_clave_int,p.pro_clave_int,p.pro_nombre,p.pro_descripcionbreve,p.pro_codigo,c.cla_nombre,pde_estado,pde_tamano,cla_nombre,pde_cantidad,pde_valor,pde_unidad,pro_uni_venta,pro_pes_venta,pro_mu_venta,pro_mp_venta,pde_minimo,pde_unidad ,pde_codigo from tbl_pedidos_detalle d JOIN tbl_productos p on p.pro_clave_int = d.pro_clave_int join tbl_clasificacion c on c.cla_clave_int = d.cla_clave_int where d.ped_clave_int = '".$idpedido."' order by p.pro_nombre,c.cla_nombre");
	$numdet = pg_num_rows($condet);

   	  ?>
   	  <style type="text/css" media="screen">
.table1
{
	width: 850px;height: 822px;background:#8ba987 url('https://www.delasiembra.com/dist/img/plantillas/hojaCorreo.jpg') no-repeat center center;font-family: calibri;font-size: 12px;padding: 10px
}	
.texttituloleft
{
	text-align: left;font-weight: bold;
}
.textleft
{
	text-align: left;
}
.textright
{
	text-align: right;
}
.titulocenter
{
	text-align: center;font-weight: bold;
}
.tdcenter
{
	text-align: center;
}
.titulorigth
{
	text-align: right;font-weight: bold;
}
</style>
   	  <div id="divresumenpedido" style="width: 820px;background:#FFF url('https://www.pavas.com.co/delasiembra/dist/img/plantillas/pestana1.jpg') no-repeat top center;font-family: calibri;font-size: 12px;padding: 10px">
   	  	<p style="padding-top: 40px; font-weight: bold;">Se ha generado un nuevo pedido, con la siguiente información:</p>
   	  	<table width="100%" style="margin-top: 50px;font-size: 11px;" cellpadding="2" cellspacing="2">   	  		
   	  		<tr><th colspan="6" style="font-size: 14px;font-weight: bold; text-align: left;">Numero de orden: <?php echo $codigoped; ?></th></tr>
   	  		<tr>
   	  			<th style="text-align: left;font-weight: bold;">Cliente:</th><th style="text-align: left"><?php  echo $cliente;?></th>
   	  			<th style="text-align: left;font-weight: bold;">Fecha de pedido:</th><th style="text-align: left"><?php echo $pedfecha;?></th>
   	  			<th style="text-align: left;font-weight: bold;"><strong>Fecha de entrega:</strong></th><th style="text-align: left"><?php echo $fecent;?></th>
   	  			<!--<th style="text-align: left;font-weight: bold;">Tiempo Entrega:</th><th style="text-align: left"><?php echo $tiempo;?></th>-->
   	  		</tr>
   	  		<tr>
   	  			<th style="text-align: left;font-weight: bold;">Email:</th><th style="text-align: left"><?php echo $email;?></th>
   	  			<th style="text-align: left;font-weight: bold;">Telefono:</th><th style="text-align: left"><?php echo $telefono;?></th>
   	  		</tr>
   	  		<tr>
   	  			<th style="text-align: left;font-weight: bold;">Sector:</th><th style="text-align: left"><?php echo $sector;?></th>
   	  			<th style="text-align: left;font-weight: bold;">Barrio:</th><th style="text-align: left"><?php echo $barrio;?></th>
   	  			<th style="text-align: left;font-weight: bold;">Dirección:</th><th style="text-align: left"><?php echo $direccion;?></th>
   	  			<th style="text-align: left;font-weight: bold;" colspan="2"><?php echo $dirdetalle;?></th>
   	  		</tr>
   	  		<?php if($nota!="")
   	  		{
   	  			?>
   	  			<tr><th colspan="6" style="text-align: left"><strong>Solicitud especial:</strong><br><?php echo $nota;?></th>
   	  			<?php
   	  		}
   	  		?>
   	  		<tr><th colspan="6"><hr></th></tr>
   	  		<tr><th colspan="6">
   	  			<?php 
    	if($numdet<=0)
    	{ 
    	
    	}
    	else
    	{ 
    		?>
   	  			<table style="font-size: 10px; width: 100%;border: 1px solid #f4f4f4" cellspacing="2" cellpadding="2" border="1">  	  				
   	  			
   	  				<tr>
   	  					<th style="text-align: center; font-weight:bold;background-color: #dff0d8">Linea</th>
	   	  				<th style="text-align: center; font-weight:bold;background-color: #dff0d8">Producto</th>
	   	  				<th style="text-align: center; font-weight:bold;background-color: #dff0d8">Descripción</th>	
	   	  				<th style="text-align: center; font-weight:bold;background-color: #dff0d8">Precio</th>   	  				
	   	  				<th style="text-align: center; font-weight:bold;background-color: #dff0d8">Cantidad</th>
	   	  				<th style="text-align: center; font-weight:bold;background-color: #dff0d8">Unidad Medida</th>
	   	  				<th style="text-align: center; font-weight:bold;background-color: #dff0d8">Observación</th>
	   	  				<th style="text-align: center; font-weight:bold;background-color: #dff0d8">Costo</th>	   	  				
	   	  			</tr>
	   	  			<?php
	   	  			$total = 0;
		    		while($datdet= pg_fetch_array($condet))
		    		{
		    			$iddetalle = $datdet['pde_clave_int'];
		    			$idp = $datdet['pro_clave_int'];
		    			$codlinea = $datdet['pde_codigo'];
		    			$nompro = $datdet['pro_nombre'];
		    			$codpro = $datdet['pro_codigo'];
		    			$nomcla = $datdet['cla_nombre'];
		    			$maduracion = $datdet['pde_estado'];
		    			$descp = $datdet['pro_descripcionbreve'];
		    			$precio = $datdet['pde_valor'];
		    			$cantidad = $datdet['pde_cantidad'];
		    			$subtotal = $precio * $cantidad;
		    			$tamano = $datdet['pde_tamano'];
		    			$observacion = "<strong>Clasificación:</strong>".$nomcla." <br>";
		    			if($tamano!=""){$observacion.="<strong>Tamano:</strong>".$tamano."<br> ";}
		    			if($maduracion!=""){$observacion.="<strong>Maduración:</strong>".$maduracion." ";}
		    			if($tamano!=""){ $tamano = '-'.$tamano;}
		    			
		    			$puv = $datdet['pro_uni_venta'];
		    			$ppv = $datdet['pro_pes_venta'];
		    			$pmuv = $datdet['pro_mu_venta'];
		    			$pmpv = $datdet['pro_mp_venta'];
		    			$min = $datdet['pde_minimo'];
		    			$uni = $datdet['pde_unidad'];
		    			if($uni==0)
		    			{
		    				$unimin = $pmuv ;
		    				$cantidad = $cantidad;
		    			}
		    			else
		    			{
		    				$unimin = $pmpv;
		    				$cantidad = $cantidad * $min;//esto debido a que el precio del producto cuando es en peso es precio por la unidad minima ejemplo:250gr = 1 unidad
		    			}
		    			//DESCONTAR DE INVENTARIO

		    			//1 INSERTAR LA SALIDA

						$con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idp."' and pri_activo = 1 LIMIT 1");
						$dat = pg_fetch_row($con);
						$cimg = $dat[0];
						$img = $dat[1];
						?>
						<tr>
							<td style="text-align: left"><?php echo $codlinea;?></td>
							<td style="text-align: left"><?php echo $codpro;?></td>
							<td style="text-align: left"><?php echo $nompro;?></td>
							<td style="text-align: right">	<?php echo number_format($precio,0,',',',');?></td>
							<td style="text-align: center"><?php echo $cantidad;?></td>
							<td ><?php if($uni==1){ echo "GR";}else { echo "UND";} ?></td>
							<td style="text-align: left"><?php echo $observacion;?></td>
							<td style="text-align: right; font-weight:bold"> <?php echo number_format($subtotal,0,',',',');?></td>
							
						</tr>
						<?php
						$total = $total + $subtotal;
					}
					$totalpedido = $total + $domicilio;
					?>
					<tr>
						<td colspan="6" rowspan="4"></td><td style="text-align: right; font-weight:bold">Sub Total:</td><td colspan="2" style="text-align: right"><?php echo number_format($total,0,',',',');?></td></tr>
					<tr>
						<td style="text-align: right; font-weight:bold">Domicilio:</td><td colspan="2" style="text-align: right"><?php echo number_format($domicilio,0,',',',');?></td>
					</tr>
					<tr>
						<td style="text-align: right; font-weight:bold">Total:</td><td colspan="2" style="text-align: right"><?php echo number_format($totalpedido,0,',',',');?></td>
					</tr>					
   	  			</table>
   	  			<?php
   	  		}
   	  		?>
   	  		</th>
   	  		</tr>
   	  		<tr>									
				<td colspan="7">
					<div style="text-align:left">
						<div style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;color:rgba(0,0,0,0.54);font-size:12px;line-height:20px;padding-top:10px">
							<div>Este mensaje es generado automáticamente por <strong>delasiembra.com</strong> , por favor no responda a este correo, cualquier duda adicional puede resolverla comunicandose al +57 310 861 1064
							</div>
							<div style="direction:ltr; font-weight: bold;color: blue">© <?php echo date('Y')?> DELASIEMBRA  
							</div>
						</div>
						<div style="display:none!important;max-height:0px;max-width:0px">et:100</div>
					</div>
				</td>
			</tr>
   	  	</table>
   	  </div>

