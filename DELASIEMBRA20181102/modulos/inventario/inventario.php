<?php 
  include ("../../data/Conexion.php");
  session_start();
  error_reporting(0);
  // variable login que almacena el login o nombre de usuario de la persona logueada
  $login= isset($_SESSION['persona']);
  // cookie que almacena el numero de identificacion de la persona logueada
  $usuario= $_COOKIE['usuario'];
  $idUsuario= $_SESSION["idusuario"];
  $clave= $_COOKIE["clave"];
  $identificacion = $_COOKIE["usIdentificacion"];
  date_default_timezone_set('America/Bogota');
  $fecha=date("Y/m/d H:i:s");

echo "<script>INICIALIZAR('Inventario');</script>";
//echo "<script>CRUDINVENTARIO('LISTAINVENTARIO','');</script>"; 
?>
<section class="content-header">
  <h1>
  Inventario
  <small>Control panel</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Configuración</a></li>
    <li class="active">Inventario</li>        
  </ol>
</section>
 <section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="input-group">
        <span class="input-group-addon" data-toggle="modal" data-target="#modalleft"><i class="fa fa-filter"></i></span>
            <select id="busproducto" onchange="CRUDINVENTARIO('LISTAPRODUCTOS','')" class="form-control selectpicker" placeholder="seleccione un producto" data-header="Buscar por codigo, nombre" multiple data-actions-box="true" data-live-search="true" data-selected-text-format="count > 6">
              <?php
              $concate = pg_query($dbconn,"select cat_clave_int,cat_nombre from tbl_categorias where cat_activo = 1");
              $numcate = pg_num_rows($concate);
              for($nc=0;$nc<$numcate;$nc++)
              {
                $datc = pg_fetch_array($concate);
                ?>

                 <optgroup label="<?php echo $datc['cat_nombre'];?>">  
                  <?php
                $conpro = pg_query($dbconn,"select pro_clave_int,pro_nombre from tbl_productos where cat_clave_int = '".$datc['cat_clave_int']."' and pro_activo!=2");
                $numpro = pg_num_rows($conpro);
                  for($np=0;$np<$numpro;$np++)
                  {
                    $datp = pg_fetch_array($conpro);
                    $idp = $datp['pro_clave_int'];
                    $nomp = $datp['pro_nombre'];
                    $con = pg_query($dbconn, "select pri_clave_int,pri_imagen from tbl_productos_imagen where pro_clave_int = '".$idp."' and pri_activo = 1 LIMIT 1");
                    $dat = pg_fetch_row($con);
                    $cimg = $dat[0];
                    $img = $dat[1];
                    $imagen = '<img  src="'.$img.'" alt="message user image">';
                    ?>
                    <option  value="<?php echo $idp;?>" data-content="<?php echo $nomp;?><img class='pull-right' src='<?php echo $img;?>'  width='30'/>"><?php echo $nomp;?></option>
                    <?php
                  }
                ?>
                 
                </optgroup>
                <?php
              }
              ?>

            </select>
            <span id="searchvista"  data-toggle="tooltip" title="Buscar Producto"  class="input-group-addon" onclick="CRUDINVENTARIO('LISTAPRODUCTOS','')"><i class="fa fa-search"></i></span>               
        </div>
      </div>
    </div>
      <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
            <div class="col-lg-1 col-md-1 col-sm-2 col-xs-2 bhoechie-tab-menu">
              <div class="list-group">
                <a href="#" class="list-group-item active text-center" onclick="INICIALIZAR('Inventario','')">
                  <h3 class="glyphicon glyphicon-folder-open"></h3><br/>Inventario
                </a>
                <a href="#" class="list-group-item text-center"  onclick="INICIALIZAR('Entradas','')">
                  <h3 class="glyphicon glyphicon-open"></h3><br/>Entradas
                </a>
                <a href="#" class="list-group-item text-center"  onclick="INICIALIZAR('Salidas','')">
                  <h3 class="glyphicon glyphicon-save"></h3><br/>Salidas
                </a>
                <a href="#" class="list-group-item text-center" style="display: none;">
                  <h4 class="glyphicon glyphicon-cutlery"></h4><br/>Restaurant
                </a>
                <a href="#" class="list-group-item text-center" style="display: none;>
                  <h4 class="glyphicon glyphicon-credit-card"></h4><br/>Credit Card
                </a>
              </div>
            </div>
            <div class="col-lg-11 col-md-11 col-sm-10 col-xs-10 bhoechie-tab">
                <!-- Contenido de inventario-->
                <div class="bhoechie-tab-content active" id="divinventario">
                 
                </div>
               <!-- Contenido de entradas --->
                <div class="bhoechie-tab-content" id="diventradas">
                   
                </div>    
                <!-- Contenido de salidas-->
                <div class="bhoechie-tab-content" id="divsalidas">
                  
                </div>
                <div class="bhoechie-tab-content" style="display: none;" >
                    <center>
                      <h1 class="glyphicon glyphicon-cutlery" style="font-size:12em;color:#55518a"></h1>
                      <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                      <h3 style="margin-top: 0;color:#55518a">Restaurant Diirectory</h3>
                    </center>
                </div>
                <div class="bhoechie-tab-content" style="display: none;">
                    <center>
                      <h1 class="glyphicon glyphicon-credit-card" style="font-size:12em;"></h1>
                      <h2 style="margin-top: 0;color:#55518a">Cooming Soon</h2>
                      <h3 style="margin-top: 0;color:#55518a">Credit Card</h3>
                    </center>
                </div>
            </div>
        </div>
  </div>
  </section>