<?php
include ("../../data/Conexion.php");
session_start();
error_reporting(0);
// variable login que almacena el login o nombre de usuario de la persona logueada
$login= isset($_SESSION['persona']);
// cookie que almacena el numero de identificacion de la persona logueada
$usuario= $_COOKIE['usuario'];
$idUsuario= $_SESSION["idusuario"];
$clave= $_COOKIE["clave"];
$identificacion = $_COOKIE["usIdentificacion"];
date_default_timezone_set('America/Bogota');
$fecha=date("Y/m/d H:i:s");
/*
 * DataTables example server-side processing script.
 *
 * Please note that this script is intentionally extremely simply to show how
 * server-side processing can be implemented, and probably shouldn't be used as
 * the basis for a large complex system. It is suitable for simple use cases as
 * for learning.
 *
 * See http://datatables.net/usage/server-side for full details on the server-
 * side processing requirements of DataTables.
 *
 * @license MIT - http://datatables.net/license_mit
 */

/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Easy set variables
 */
//Vvariable GET
//$Socios = $_GET['Socios'];

// DB table to use
$table = 'tbl_pedidos';
// Table's primary key
$primaryKey = 'p.ped_clave_int';

$fec = $_POST['desde'];
$fech = $_POST['hasta'];
if($fec!="" and $fech==""){$fech=$fec;}else if($fech!="" and $fec==""){$fec=$fech;}
if($fec=="" and $fech==""){$fec=date('Y-m-d'); $fech=date('Y-m-d');}
// Array of database columns which should be read and sent back to DataTables.
// The db parameter represents the column name in the database, while the dt
// parameter represents the DataTables column identifier - in this case object


// parameter names
$columns = array(
	array(
		'db' => 'p.ped_fec_programada',
		'dt' => 'DT_RowId', 'field' => 'ped_fec_programada',
		'formatter' => function( $d, $row ) {
			// Technically a DOM id cannot start with an integer, so we prefix
			// a string. This can also be useful if you have multiple tables
			// to ensure that the id is unique with a different prefix
			return 'rowp_'.$d;
		}
	),	
        
       
       
		array( 'db' => 'p.ped_fec_programada', 'dt' => 'FechaProgramada', 'field' => 'ped_fec_programada'),
        array( 'db' => 'pr.pro_codigo', 'dt' => 'Codigo', 'field' => 'pro_codigo'),
		array( 'db' => 'pr.pro_nombre', 'dt' => 'Producto', 'field' => 'pro_nombre' ),
        array( 'db' => 'pd.pde_tamano', 'dt' => 'Tamano', 'field' => 'pde_tamano' ),
        array( 'db' => 'pd.pde_estado', 'dt' => 'Estado', 'field' => 'pde_estado'),
        array( 'db' => "pd.cla_clave_int", 'dt' => 'Calidad',  'field' => 'cla_clave_int','formatter'=>function($d, $row){
            global $dbconn;
            $con = pg_query($dbconn, "select cla_nombre from tbl_clasificacion where cla_clave_int= '".$d."'");
            $dat = pg_fetch_array($con);
            $nomcla = $dat['cla_nombre'];
        	return $nomcla;
        }), 
        array('db' => "p.ped_fec_programada", 'dt' => 'Total',  'field' => 'ped_fec_programada','formatter'=>function($d,$row){
             global $dbconn,$fec,$fech;

            
             $con = pg_query($dbconn ,"SELECT SUM(pde_cantidad) cant FROM tbl_pedidos_detalle pd join tbl_pedidos p on p.ped_clave_int = pd.ped_clave_int WHERE pde_unidad = 0 and pd.pro_clave_int = '".$row[10]."' and p.ped_estado  !='4' and p.ped_estado!='0' and  ((p.ped_fec_programada BETWEEN '".$fec."' AND '".$fech."') or ('".$fec."' Is Null and '".$fech."' Is Null) or ('".$fec."' = '' and '".$fech."' = '')) and pde_tamano = '".$row[4]."' and pd.pde_estado = '".$row[5]."'");
             $dat = pg_fetch_array($con);
             $cant1 = $dat['cant'];

             $con = pg_query($dbconn ,"SELECT SUM(pde_cantidad*pde_unidad) cant FROM tbl_pedidos_detalle pd join tbl_pedidos p on p.ped_clave_int = pd.ped_clave_int WHERE pde_unidad = 1 and pd.pro_clave_int = '".$row[10]."' and p.ped_estado  !='4' and p.ped_estado!='0' and  ((p.ped_fec_programada BETWEEN '".$fec."' AND '".$fech."') or ('".$fec."' Is Null and '".$fech."' Is Null) or ('".$fec."' = '' and '".$fech."' = '')) and pde_tamano = '".$row[4]."' and pd.pde_estado = '".$row[5]."' ");
             $dat = pg_fetch_array($con);
             $cant2 = $dat['cant'];

             $cantidad = $cant1 + $cant2;
             return  round($cantidad,2);



        }), 
        array('db' => "p.ped_fec_programada", 'dt' => 'Estimado',  'field' => 'ped_fec_programada','formatter'=>function($d,$row){
             global $dbconn,$fec,$fech;
            $concos = pg_query($dbconn, "SELECT prm_costo,prm_peso_unidad FROM tbl_precios_mercado WHERE pro_clave_int = '".$row[10]."' and cla_clave_int = '".$row[6]."' ORDER BY prm_costo DESC LIMIT 1");
            $datcos = pg_fetch_array($concos);
            $costo = $datcos['prm_costo'];
            $pesund = $datcos['prm_peso_unidad'];
            
             $con = pg_query($dbconn ,"SELECT SUM(pde_cantidad) cant FROM tbl_pedidos_detalle pd join tbl_pedidos p on p.ped_clave_int = pd.ped_clave_int WHERE pde_unidad = 0 and pd.pro_clave_int = '".$row[10]."' and p.ped_estado  !='4' and p.ped_estado!='0' and   ((p.ped_fec_programada BETWEEN '".$fec."' AND '".$fech."') or ('".$fec."' Is Null and '".$fech."' Is Null) or ('".$fec."' = '' and '".$fech."' = '')) and pde_tamano = '".$row[4]."' and pd.pde_estado = '".$row[5]."'");
             $dat = pg_fetch_array($con);
             $cant1 = $dat['cant'];

             $con = pg_query($dbconn ,"SELECT SUM(pde_cantidad*pde_unidad) cant FROM tbl_pedidos_detalle pd join tbl_pedidos p on p.ped_clave_int = pd.ped_clave_int WHERE pde_unidad = 1 and pd.pro_clave_int = '".$row[10]."' and p.ped_estado  !='4' and p.ped_estado!='0' and   ((p.ped_fec_programada BETWEEN '".$fec."' AND '".$fech."') or ('".$fec."' Is Null and '".$fech."' Is Null) or ('".$fec."' = '' and '".$fech."' = '')) and pde_tamano = '".$row[4]."' and pd.pde_estado = '".$row[5]."' ");
             $dat = pg_fetch_array($con);
             $cant2 = $dat['cant'];

             $cantidad = $cant1 + $cant2;
             $pesoestimado = 0;
             //if($row[9]==1)
             //{
                 $pesoestimado  =  ($cantidad*$pesund)/1000;   
             //}

             return round($pesoestimado,2)."kG";

        }),    
        array( 'db'=> "pr.pro_kilo",'dt'=>'PrecioKilo','field'=>'pro_kilo'),
        array( 'db' => 'pr.pro_clave_int', 'dt' => 'ClavePro', 'field' => 'pro_clave_int')
     
);

$sql_details = array(
	'user' => 'usrpavas',
	'pass' => '9A12)WHFy$2p4v4s',
	'db'   => 'dbdelasiembra',
	'port'   => '5432',
	'host' => '127.0.0.1'
);


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

require( '../../data/ssp.class.php' );
$whereAll = "";
$groupBy = 'p.ped_fec_programada,pr.pro_clave_int,pr.pro_kilo,pr.pro_codigo,pr.pro_nombre,pd.pde_tamano, pd.pde_estado,pd.cla_clave_int';
$with = '';
$joinQuery = "FROM tbl_pedidos p JOIN tbl_pedidos_detalle pd ON pd.ped_clave_int = p .ped_clave_int
JOIN tbl_productos pr ON pr.pro_clave_int = pd.pro_clave_int ";
$extraWhere =" p.ped_estado in(1,2) and ((p.ped_fec_programada BETWEEN '".$fec."' AND '".$fech."') or ('".$fec."' Is Null and '".$fech."' Is Null) or ('".$fec."' = '' and '".$fech."' = ''))";

echo json_encode(
	SSP::simple($_POST, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere, $groupBy,$with )
);