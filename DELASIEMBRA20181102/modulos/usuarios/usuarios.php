<?php
include('../../data/Conexion.php');
?>
 <input type="hidden" name="tipo" id="tipo" value="Todos">

 <?php 
echo "<script>INICIALIZAR('Usuarios');</script>";
 //echo "<script>CRUDUSUARIOS('LISTAUSUARIOS','','Todos');</script>";

 ?>

<section class="content-header">
      <h1>
        Usuarios
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Seguridad</a></li>
        <li class="active">Usuarios</li>
        <li>
          <a class="text-green"  title="Añadir Usuario" tabindex="-1" data-toggle="modal" data-target="#modalregistro" onclick="CRUDUSUARIOS('NUEVO','')">    
            <span class="fa fa-plus-circle fa-2x"></span>    
        </a>
        </li>       
      </ol>
    </section>

    <section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="input-group">
                <span class="input-group-addon" data-toggle="modal" data-target="#modalleft"><i class="fa fa-filter"></i></span>
                <input type="text" class="form-control" placeholder="seleccione un usuario">
      </div>
    </div>
    <div class="nav-tabs-custom" id="divlistaestados">
                <ul class="nav nav-tabs pull-left">              
                  <li id="litodosusuarios"><a  onclick="CRUDUSUARIOS('LISTAUSUARIOS','','Todos')" data-toggle="tab" aria-expanded="true"><span id="todo">Todos
                    <?php
                      $con = pg_query($dbconn,"select COUNT(*) cant from tbl_usuario where est_clave_int!=2");
                      $dato = pg_fetch_array($con);
                      echo $dato['cant'];
                    ?>
                  </span></a>
                </li>
                <?php 
                $cons = pg_query($dbconn, "select p.prf_clave_int, upper(p.prf_descripcion) per from tbl_perfil p where p.est_clave_int = 1");
                while($dats = pg_fetch_array($cons))
                {
                  $desp = $dats['per'];
                  $clap  =$dats['prf_clave_int'];
                  
                  $con = pg_query($dbconn,"select COUNT(*) cant from tbl_usuario where prf_clave_int = '".$clap."' and est_clave_int!=2 and usu_confirmar = 1");
                  $dato = pg_fetch_array($con);
                  ?>
                  <li class="">
                  <a onclick="CRUDUSUARIOS('LISTAUSUARIOS','','<?php echo $clap;?>')"  data-toggle="tab" aria-expanded="false">
                  <span id="adm"><?php echo $desp." <span class='badge'>".$dato['cant']."</span>"; ?></span>
                  </a></li>
                  <?php
                }
                $condom = pg_query($dbconn, "SELECT * FROM tbl_usuario WHERE prf_clave_int = 2 and usu_confirmar = 0 and est_clave_int!=2");
                $numdom = pg_num_rows($condom);
                if($numdom>0)
                {
                  ?>
                  <li class="bg-danger" id="litodosdomiciliarios">
                  <a onclick="CRUDUSUARIOS('LISTACONFIRMAR','','2')"  data-toggle="tab" aria-expanded="false">
                  <span id="admd">CONFIRMAR DOMICILIARIOS <span class='badge'><?php echo $numdom;?></span></span>
                  </a>
                  </li>
                  <?php
                }
                ?>            
                </ul>
              </div>
  </div>
  <div class="row">
    <div class="col-md-12" id="tabladatos"></div>
  </div>
 </section>
<table style="display: none;">
<tr>
<td class="auto-style1"><a class="btn btn-success  btn-md" id="activar"><i class="glyphicon glyphicon-ok-circle"></i>&nbsp;Activar</a></td>
<td class="auto-style1"><a class="btn btn-warning  btn-md" id="inactivar"><i class="glyphicon glyphicon-ban-circle"></i>&nbsp;Inactivar</a></td>
<td class="auto-style1"><a class="btn btn-danger  btn-md" id="eliminar"><i class="glyphicon glyphicon-trash"></i>&nbsp;Eliminar</a></td>
<td><a class="btn btn-block btn-primary  btn-md" onClick="CRUDUSUARIOS('NUEVO','','')" role="button" data-toggle="modal" data-target="#modalregistro"><i class="glyphicon glyphicon-plus-sign"></i>&nbsp;Nuevo</a></td>
</tr>
</table>
