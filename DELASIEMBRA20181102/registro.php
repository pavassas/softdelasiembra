<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link href="dist/img/favicon.ico?<?php echo time();?>" rel="shortcut icon">
  <title>DELASIEMBRA | Registro</title>
 <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.5 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.css?<?php echo time();?>">

    <link rel="stylesheet" href="bootstrap/css/bootstrapds.css?<?php echo time();?>">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="dist/font-awesome-4.7.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css?<?php echo time();?>">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.css?<?php echo time();?>">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css?<?php echo time();?>">
  <link rel="stylesheet" href="dist/sweetalert/sweetalert2.css?<?php echo time ();?>">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition register-page">
<div class="register-box"  style="background-color: #FFFFFF">
  
 <div class="register-logo">
    <img src="dist/img/LOGO.png" height="80" class="img-login">
    <!--<h6>Sistema de Gestión de Tickets</h6>-->
   
  </div>
  <div class="register-box-body"> 
    <form action="login.php" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Nombre" id="txtnombre" name="txtnombre">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
       <div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Apellido" id="txtapellido" name="txtapellido">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email" id="txtemail" name="txtemail">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Contraseña" id="txtpass" name="txtpass">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Repetir contraseña" id="txtpass1" name="txtpass1">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" id="ckterminos" name="ckterminos" value="1"> Estoy de acuerdo con los <a href="#">terminos</a>
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4 col-xs-offset-8">
          <button onclick="CRUDUSUARIOS('NUEVOREGISTRO','')" type="button" class="btn btn-primary btn-block btn-flat">Registrarse</button>
        </div>
        <!-- /.col -->
      </div>
    </form>
    <a href="index.php" class="text-center">Ya tengo una cuenta</a>
  </div>
  <!-- /.form-box -->
</div>
<script src="plugins/jQuery/jQuery-2.1.4.min.js?<?php echo time();?>"></script>
<!-- Bootstrap 3.3.5 -->
<script src="bootstrap/js/bootstrap.js?<?php echo time();?>"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js?<?php echo time();?>"></script>
<script  type="text/javascript" src="dist/sweetalert/sweetalert2.js?<?php echo time();?>"></script>
<script src="llamadas.js?<?php echo time();?>" type="text/javascript"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
