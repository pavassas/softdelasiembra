/* Formatting function for row details - modify as you need */

// JavaScript Document
$(document).ready(function(e) {

		var selected = [];
		//var Socios = $('#idsocios').val();
		var estado  = $('#idestado').val();
		var cliente = $('#selcliente').val();
		
		var table2 = $('#tbpedidos').DataTable( {       
       	"dom": '<"top">rt<"bottom"l>p<"clear">',
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,15,20,-1], [10,15,20,"Todos"]],
		"language": {
		"lengthMenu": "Ver _MENU_",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},		
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/pedidos/pedidosjson.php",
                    "data": { estado:estado,cliente:cliente },//uni:uni,	
                    "type": "POST"
				},
		"columns": [
			
			{ "data" : "Codigo", "className": "dt-left" },		
			{ "data" : "Fecha", "className": "dt-left" },
			{ "data" : "Cliente", "className": "dt-left" },
			{ "data" : "Direccion", "className": "dt-left" },
			{ "data" : "Total", "className": "dt-right",render: $.fn.dataTable.render.number( ',', '.', 0, '$' ) },					
			{ "data" : "Domicilio", "className": "dt-right",render: $.fn.dataTable.render.number( ',', '.', 0, '$' ) },
			{ "data" : "Verificar", "className": "dt-left","orderable":false  },
			{ "data" : "Asociar", "className": "dt-left","orderable":false  },
		
			
		],
		"order": [[1, 'asc']],
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
               // $(row).addClass('selected');
            }  
        }
		
    } );
     
	 /*$('#tbproductos tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );*/
	
	   var table2 = $('#tbpedidos').DataTable();
 
    // Apply the search
    table2.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
});


