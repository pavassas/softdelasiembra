/* Formatting function for row details - modify as you need */

// JavaScript Document
$(document).ready(function(e) {

		var selected = [];
		//var Socios = $('#idsocios').val();
		var nom = $('#busnombre').val();
		var mar = $('#busmarca').val();
		var cat = $('#buscategoria').val();
		var cod = $('#buscodigo').val();
		//var uni = $('#busunidad').val();
		var pro = $('#busproducto').val();
		
		var table2 = $('#tbinventario').DataTable( {       
       	"dom": '<"top">rt<"bottom"lp><"clear">',
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,15,20,-1], [10,15,20,"Todos"]],
		"language": {
		"lengthMenu": "Ver _MENU_",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},		
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/inventario/inventariojson.php",
                    "data": { nom:nom,cod:cod,mar:mar,cat:cat,pro:pro },//uni:uni,	
                    "type": "POST"
				},
		"columns": [
			{ "data" : "Codigo", "className": "dt-left" },		
			{ "data" : "Nombre", "className": "dt-left" },			
			{ "data" : "Categoria", "className": "dt-left" },	
			{ "data" : "Clasificacion", "className": "dt-left" },			
			//{ "data" : "UniCompra", "className": "dt-center" },
			//{ "data" : "UniConsumo", "className": "dt-center" },
			{ "data" : "Entradas", "className": "dt-center currency3 details-control-entradas" },
			{ "data" : "Salidas", "className": "dt-center" },
			{ "data" : "Disponible", "className": "dt-center" },
		],
		"order": [[1, 'asc']],
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
               // $(row).addClass('selected');
            }  
        }
		
    } );
     
	//detallle de inventario salidas
	// Add event listener for opening and closing details
   var detailRows = [];
    $('#tbinventario tbody').on('click', 'td.details-control-entradas', function () {
      
		var tr = $(this).closest('tr');
        var row = table2.row( tr );
		var dat = row.data();
		var id = dat.Clave;
		//alert(id);
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else 
		{
            // Open this row
			var table1 = "<div id='diventradas"+id+"'  class='col-xs-12'>"+
			"<table  class='table table-condensed table-striped' id='tbentrada_"+id+"'>";
			table1+="<thead><tr>"+
			"<th class='dt-head-center'>Fecha Entrada</th>"+
			"<th class='dt-head-center'>Tipo Entrada</th>"+
			"<th class='dt-head-center'>Cant</th>"+
			"<th class='dt-head-center'>Vr.Unit</th>"+
			"<th class='dt-head-center'>Total</th>"+
			"<th class='dt-head-center'>Usuario</th>"+
			"<th class='dt-head-center'>Fec.Actualizacion</th>"+			
			"</tr></thead></div>";
			row.child(table1).show();
			convertirentradas(id);				
			tr.addClass('shown');
		}
	} );

	var table2 = $('#tbinventario').DataTable();
 
    // Apply the search
    table2.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
});

function convertirentradas(id)
{

	var tableent = $('#tbentrada_' + id).DataTable( {       
   	"dom": '<"top">rt<"bottom"lp><"clear">',
	"ordering": true,
	"info": true,
	"autoWidth": true,
	"pagingType": "simple_numbers",
	"lengthMenu": [[10,15,20,-1], [10,15,20,"Todos"]],
	"language": {
	"lengthMenu": "Ver _MENU_",
	"zeroRecords": "No se encontraron datos",
	"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
	"infoEmpty": "No se encontraron datos",
	"infoFiltered": "",
	"paginate": {"previous": "&#9668;","next":"&#9658;"}
	},		
	"processing": true,
    "serverSide": true,
    "ajax": {
                "url": "modulos/inventario/detalleentradasjson.php",
                "data": { id:id},//fi, ff	
                "type": "POST"
			},
	"columns": [
		{ "data" : "Fecha", "className": "dt-left" },
		{ "data" : "Tipo", "className": "dt-left" },
		{ "data" : "Cantidad", "className": "dt-left" },			
		{ "data" : "Valor", "className": "dt-center" },
		{ "data" : "Total", "className": "dt-center" },
		{ "data" : "Usuario", "className": "dt-center" },
		{ "data" : "FechaAct", "className": "dt-center" },

	],
	"order": [[0, 'asc']]			
	} );
}