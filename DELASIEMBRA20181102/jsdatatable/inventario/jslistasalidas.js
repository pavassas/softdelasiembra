/* Formatting function for row details - modify as you need */

// JavaScript Document
$(document).ready(function(e) {

		var selected = [];
		//var Socios = $('#idsocios').val();
		var nom = $('#busnombre').val();
		var mar = $('#busmarca').val();
		var cat = $('#buscategoria').val();
		var cod = $('#buscodigo').val();
		//var uni = $('#busunidad').val();
		var pro = $('#busproducto').val();
		
		var table2 = $('#tbsalidas').DataTable( {       
       	"dom": '<"top">rt<"bottom"lp><"clear">',
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,15,20,-1], [10,15,20,"Todos"]],
		"language": {
		"lengthMenu": "Ver _MENU_",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},		
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/inventario/salidasjson.php",
                    "data": { nom:nom,cod:cod,mar:mar,cat:cat,pro:pro },//uni:uni,	
                    "type": "POST"
				},
		"columns": [
			{
    			"class":          "details-control",
    			"orderable":      false,
    			"data":           null,
    			"defaultContent": ""
			},				
			
			{ "data" : "Codigo", "className": "dt-left" },
			{ "data" : "Nombre", "className": "dt-left" },
			{ "data" : "Categoria", "className": "dt-left" },			
			//{ "data" : "UniCompra", "className": "dt-center" },
			//{ "data" : "UniConsumo", "className": "dt-center" },
			{ "data" : "Cantidad", "className": "dt-center" },

		],
		"order": [[2, 'asc']],
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
               // $(row).addClass('selected');
            }  
        }
		
    } );
     
	 /*$('#tbproductos tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );*/
	
	   var table2 = $('#tbsalidas').DataTable();
 
    // Apply the search
    table2.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
});


