// JavaScript Document
$(document).ready(function(e) {
    
    	var fil = $('#filtromercado').val();
    	
    var table = $('#tbmercados').DataTable( {       
        "dom": '<"top">rt<"bottom"lp><"clear">',
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,20,30,-1 ], [10,20,30,"Todos" ]],
		"language": {
		"lengthMenu": "Ver _MENU_",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},		
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/mercados/mercadosjson.php",
                    "data": { fil:fil},	
                    "type": "POST"
				},
		"columns": [ 			
			{				
				"orderable":      false,
				"data":           "Editar"				
			},	
			{	
				"orderable":      false,
				"data":           "Eliminar",				
			},			
				
		
			{ "data": "Nombre" },			
			{ "data": "Clasificacion", "className":"dt-left" },	
			{ "data": "Porcentaje", "className":"dt-center",render: $.fn.dataTable.render.number( ',', '.', 0, '%' ) },				
		],
		"order": [[2, 'asc']]		
    } );
     
	 /* $('#tbunidades tfoot th').each( function () {
        var title = $('#tbunidades thead th').eq( $(this).index() ).text();
		if(title==""){$(this).html('');}else{
        $(this).html( '<input type="text" class="form-control" placeholder="'+title+'" />' );}
    } );*/
 
    // DataTable
    var table = $('#tbmercados').DataTable();
 
    // Apply the search
    table.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
  // Add event listener for opening and closing details
    $('#tbmercados tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
	
	// Add event listener for opening and closing details
 
});