/* Formatting function for row details - modify as you need */

// JavaScript Document
$(document).ready(function(e) {

		var selected = [];
		var nom = $('#busnombre').val();
		var ema = $('#buscorreo').val();	
		var usu = $('#bususuario').val();
		var table2 = $('#tbUsuariosConfirmar').DataTable( {       
       	"dom": '<"top"i>rt<"bottom"flp><"clear">',
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,15,20,-1], [10,15,20,"Todos"]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "Anterior","next":"siguiente"},
		"search":"",
		"sSearchPlaceholder":"Busqueda"
		},	
		"searching":false,	
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/usuarios/domiciliariojson.php",
                    "data": {nom:nom, ema:ema, usu:usu},
                    "type":"POST"
				},
		"columns": [
			
			{ "data" : "E-Mail" , "className": "dt-left"},			
			{ "data" : "Celular", "className": "dt-center" },
			{ "data" : "Ip", "className": "dt-center" },
			{ "data" : "FechaRegistro", "className": "dt-center" },			
			{ "orderable": false, "data": "Editar" },	
			{ "orderable": false, "data": "Descartar" },
		],
		"order": [[0, 'asc']],
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }  
			$('td:eq(0)', row).css('background-color',data.Color); 
			$('td:eq(1)', row).css('background-color',data.Color);
			$('td:eq(2)', row).css('background-color',data.Color);
			$('td:eq(3)', row).css('background-color',data.Color);
			$('td:eq(4)', row).css('background-color',data.Color);
			$('td:eq(5)', row).css('background-color',data.Color);					 
        }
		
    } );
     
	 $('#tbUsuariosConfirmar tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
	
	var table2 = $('#tbUsuariosConfirmar').DataTable();

});


