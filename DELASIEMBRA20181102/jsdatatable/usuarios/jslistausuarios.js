/* Formatting function for row details - modify as you need */

// JavaScript Document
$(document).ready(function(e) {

		var selected = [];
		var nom = $('#busnombre').val();
		var ema = $('#buscorreo').val();	
		var usu = $('#bususuario').val();
		var tip = $('#tipo').val();				
		
		var table2 = $('#tbUsuarios').DataTable( {       
       	"dom": '<"top"i>rt<"bottom"flp><"clear">',
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,15,20,-1], [10,15,20,"Todos"]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "Anterior","next":"siguiente"},
		"search":"",
		"sSearchPlaceholder":"Busqueda"
		},	
		"searching":false,	
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/usuarios/usuariosjson.php",
                    "data": {nom:nom, ema:ema, usu:usu ,tipo:tip},
                    "type":"POST"
				},
		"columns": [					
			{ "data" : "Nombre",   "className" : "dt-left" },
			{ "data" : "Perfil",   "className" : "dt-left" },
			{ "data" : "E-Mail" ,  "className" : "dt-left" },			
			{ "data" : "Telefono", "className" : "dt-center" },
			{ "data" : "Registro", "className" : "dt-center" },
			{ "data" : "Activo",   "className" : "dt-center" },			
			{ "data" : "Editar",   "className" : "dt-center", "orderable" : false },	
			{ "data" : "Eliminar", "className" : "dt-center", "orderable" : false },
		],
		"order": [[0, 'asc']],
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }  
			$('td:eq(0)', row).css('background-color',data.Color); 
			$('td:eq(1)', row).css('background-color',data.Color);
			$('td:eq(2)', row).css('background-color',data.Color);
			$('td:eq(3)', row).css('background-color',data.Color);
			$('td:eq(4)', row).css('background-color',data.Color);
			$('td:eq(5)', row).css('background-color',data.Color);
			$('td:eq(6)', row).css('background-color',data.Color);
			$('td:eq(7)', row).css('background-color',data.Color);
				 
        }
		
    } );
     
	 $('#tbUsuarios tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
	
	   var table2 = $('#tbUsuarios').DataTable();
 
    // Apply the search

	$('#activar').click( function() 
	{
		var table = $('#tbUsuarios').DataTable();
		var col = table.rows('.selected').data().length;
		if(col<=0 ) 
		{
			error('No ha seleccionado ningun usuario');		
		}
		else
		{		
			for(var i = 0; i<selected.length;i++)
			{				
				if(selected[i]=="" || selected[i]==null)
				{
				}
				else
				{
					var id = selected[i];
					var n=id.split("row_");			      
					CRUDUSUARIOS('ACTIVAR',n[1],'');	
				}
			}
			setTimeout(ok('Usuarios activados Correctamente'),1000);
						
		}
    } ); 
	
	$('#inactivar').click( function() 
	{
		var table = $('#tbUsuarios').DataTable();
		var col = table.rows('.selected').data().length;
		if(col<=0 ) 
		{
			error('No ha seleccionado ningun usuario');
			
		}
		else
		{		
			for(var i = 0; i<selected.length;i++)
			{				
				if(selected[i]=="" || selected[i]==null)
				{
				}
				else
				{
					var id = selected[i];
					var n=id.split("row_");			      
					CRUDUSUARIOS('INACTIVAR',n[1]);	
				}
			}
			setTimeout(ok('Usuarios Inactivados Correctamente'),1000);			//$('
			setTimeout(CRUDUSUARIOS('CARGARLISTAUSUARIOS','',''),1000);				
		}
    } ); 
	
	$('#eliminar').click( function() 
	{
		var table = $('#tbUsuarios').DataTable();
		var col = table.rows('.selected').data().length;
		if(col<=0 ) 
		{   
			error('No ha seleccionado ningun usuario');
		    
		}
		else
		{
			jConfirm('Realmente desee eliminar los usuarios seleccionados?', 'Dialogo de Confirmación HD',null, function(r) {
		    //var conf = confirm("Realmente desee eliminar los usuarios seleccionados");
				if(r==true)
				{
					for(var i = 0; i<selected.length;i++)
					{					
						if(selected[i]=="" || selected[i]==null)
						{
						}
						else
						{
							var id = selected[i];
							var n=id.split("row_");			      	
							CRUDUSUARIOS('ELIMINAR',n[1],'');	
						}
					}
					setTimeout(ok('Usuarios Eliminado Correctamente'),1000);										
				}
				else
				{
					return false;
				}
			});
		}
 	} );
});


