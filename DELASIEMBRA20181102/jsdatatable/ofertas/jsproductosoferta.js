function updateDataTableSelectAllCtrl(table){
   var $table             = table.table().node();
   var $chkbox_all        = $('tbody .sele', $table);
   var $chkbox_checked    = $('tbody .sele:checked', $table);
   var chkbox_select_all  = $('thead input[name="select_all"]', $table).get(0);

   // If none of the checkboxes are checked
   if($chkbox_checked.length === 0){
      chkbox_select_all.checked = false;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }

   // If all of the checkboxes are checked
   } else if ($chkbox_checked.length === $chkbox_all.length){
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = false;
      }

   // If some of the checkboxes are checked
   } else {
      chkbox_select_all.checked = true;
      if('indeterminate' in chkbox_select_all){
         chkbox_select_all.indeterminate = true;
      }
   }
}

var selected = [];
var codselected = [];

$(document).ready(function(e) {
   var table = $('#tblistaproductosofertas').DataTable( { 
      "dom": '<"top"fi>rt<"bottom"lp><"clear">',        
      'columnDefs': [{
         'targets': 0,
         'searchable':false,
         'orderable':false,
         'width':'1%',
         'className': 'dt-body-center',
         'render': function (data, type, full, meta){
             return '<input type="checkbox" class="sele" value="2">';
         }
         }],
      select: {
          style:    'multi',
          selector: 'td:first-child'
      },
      "searching":true,
      "ordering": true,
      "info": false,
      "autoWidth": true,
      "pagingType": "simple_numbers",
      "lengthMenu": [[10,15,20,-1], [10,15,20,"Todos"]],
      "language": {
      "lengthMenu": "Ver _MENU_ registros",
      "zeroRecords": "No se encontraron datos",
      "info": "Resultado _START_ - _END_ de _TOTAL_ registros",
      "infoEmpty": "No se encontraron datos",
      "infoFiltered": "",
      "paginate": {"previous": "Anterior","next":"Siguiente"},
      "search":"",
      "sSearchPlaceholder":"Busqueda"
      },    
    "processing": true,
    "serverSide": true,
    "ajax": {
                "url": "modulos/ofertas/productosofertasjson.php",
                "data": { nom:"",cod:"",mar:"",cat:"",uni:"",pro:"" }, 
                "type": "POST"
        }, 
    "columns": [        
        {
       //"class":          "select-checkbox",
         "orderable":      false,
         "data":           null,
         "defaultContent": ""
        },         
        { "data" : "Nombre", "className" : "dt-left"},         
        { "data" : "Codigo", "className" : "dt-left"},     
      ],
      "order": [[1, 'asc']],
      "rowCallback": function( row, data ) {
         if ( $.inArray(data.DT_RowId, selected) !== -1 ) 
             {
                 $(row).find('.sele').prop('checked', true);
             //$(row).addClass('selected');         
            }        
           
        }      
    } );
     
    $('#tblistaproductosofertas tbody').on('click','.sele', function (e) {
      // console.log("Hola");
      var $row = $(this).closest('tr');
     
      id = $row.attr('id');

      // Get row data
      //var data = table.row($row).data();

      // Get row ID
      //var rowId = data[0];

      // Determine whether row ID is in the list of selected row IDs 
      var index = $.inArray(id, selected);

      // If checkbox is checked and row ID is not in list of selected row IDs
      if(this.checked && index === -1){
         selected.push(id);

      // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
      } else if (!this.checked && index !== -1){
         selected.splice(index, 1);
      }

      if(this.checked){
        //console.log(this.value);
         $row.addClass('selected');
      } else {
        
         $row.removeClass('selected');
      }

      // Update state of "Select all" control
      updateDataTableSelectAllCtrl(table);

      // Prevent click event from propagating to parent
      e.stopPropagation();
    } );
   
    $('#tblistaproductosofertas').on('click', 'tbody td, thead th:first-child', function(e){
      //$(this).parent().find('.sele').trigger('click');
   });
   
   $('thead input[name="select_all"]', table.table().container()).on('click', function(e){
      if(this.checked){
         $('#tblistaproductosofertas tbody .sele:not(:checked)').trigger('click');
      } else {
         $('#tblistaproductosofertas tbody .sele:checked').trigger('click');
      }

      // Prevent click event from propagating to parent
      e.stopPropagation();
   });
   
   // Handle table draw event
   table.on('draw', function(){
      // Update state of "Select all" control
      updateDataTableSelectAllCtrl(table);
   });
    
  // var table3 = $('#tblistaproductos').DataTable();
});

function guardaroferta()
{
    var table = $('#tblistaproductosofertas').DataTable();
    var col = table.rows('.selected').data().length;
    var ruta = $('#rutaoferta').text();
    var tit = $('#txttitulo').val();
    var des = $('#txtdescripcion').val().replace(new RegExp("\n","g"), "<br>");  
    var mer = $('#selmercado').val();    
    var fi = $('#txtfechainicio').val();
    var ff = $('#txtfechafin').val();
    var hi = $('#txthorainicio').val();
    var hf = $('#txthorafin').val();
    var td = $('#seltipodescuento').val();
    var vd = $('#txtvalor').val();

   vd = vd.replace("$", "");
   vd = vd.replace(",", "").replace(",", "");
   vd = vd.replace(",", "").replace(",", "");
   var est = $('input:checkbox[name=ckestado]:checked').val();
   if(est<1){est = 0;}
    // var count = table.rows( { selected: true } ).count();
    var selecte1 = $.map(selected,function(elemento,i) { return elemento.replace('row_',''); } );
    if(tit=="")
    {
      error("Diligencia el nombre de la oferta");
    }
    else if(des=="")
    {
      error("Diligenciar una descripcion de la oferta")
    }
    else if(mer=="" || mer==null)
    {
      error("Seleccionar el mercado al cual aplica la oferta");
    }
    else if(fi=="" || ff=="")
    {
      error("Ingresar la vigenciar de la oferta");
    }
    else if(td=="")
    {
      error("Seleccionar el tipo de descuento");
    }
    else if(vd<=0)
    {
      error("Ingresar el valor o porcentaje del descuento");
    }
    else
    if(col<=0)
    {
        error("No ha seleccionado productos para asociar a la oferta. Verificar");
    }
    else
    {
        var selecte = selecte1.join('-');
       $.post('funciones/ofertas/fnOfertas.php',{opcion:"GUARDAR",tit:tit,des:des,fi:fi,ff:ff,hi:hi,hf:hf,ruta:ruta,td:td,vd:vd,pro:selecte,est:est,mer:mer},
         function(data)
         {
            var res = $.trim( data[0].res );
            var msn = data[0].msn;
            if(res=="error")
            {
               error(msn);      
            }
            else if(res=="ok")
            {
               ok(msn);
               selected.length = 0;
               setTimeout(CRUDOFERTAS('NUEVO',''),100);
            }
         },"json");      
    }
}