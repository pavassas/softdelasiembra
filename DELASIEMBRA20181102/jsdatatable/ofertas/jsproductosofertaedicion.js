

var selected = [];
var codselected = [];

$(document).ready(function(e) {
   var id = $('#tblistaproductosofertasedicion').attr('data-id');
   var table = $('#tblistaproductosofertasedicion').DataTable( { 
      "dom": '<"top"lfi>rt<"bottom">p<"clear">',  
      "searching":true,
      "ordering": true,
      "info": false,
      "autoWidth": true,
      "pagingType": "simple_numbers",
      "lengthMenu": [[10,15,20,-1], [10,15,20,"Todos"]],
      "language": {
      "lengthMenu": "Ver _MENU_ reg",
      "zeroRecords": "No se encontraron datos",
      "info": "Resultado _START_ - _END_ de _TOTAL_ registros",
      "infoEmpty": "No se encontraron datos",
      "infoFiltered": "",
      "paginate": {"previous": "Anterior","next":"Siguiente"},
      "search":"",
      "sSearchPlaceholder":"Busqueda"
      },    
    "processing": true,
    "serverSide": true,
    "ajax": {
                "url": "modulos/ofertas/productosofertasedicionjson.php",
                "data": { nom:"",cod:"",mar:"",cat:"",uni:"",pro:"",id:id }, 
                "type": "POST"
        }, 
    "columns": [        
        {
       //"class":          "select-checkbox",
         "orderable":      false,
         "data":           "Seleccion",
        "className" : "dt-center"
         
        },         
        { "data" : "Nombre", "className" : "dt-left"},         
        { "data" : "Codigo", "className" : "dt-left"},     
      ],
      "order": [[1, 'asc']],
      "rowCallback": function( row, data ) {
         if ( $.inArray(data.DT_RowId, selected) !== -1 ) 
             {
                 $(row).find('.sele').prop('checked', true);
             //$(row).addClass('selected');         
            }        
           
        }      
    } );
     
    $('#tblistaproductosofertasedicion tbody').on('click','.sele', function (e) {
      var $row = $(this).closest('tr');
     
      id = $row.attr('id');

      // Get row data
      //var data = table.row($row).data();

      // Get row ID
      //var rowId = data[0];

      // Determine whether row ID is in the list of selected row IDs 
      var index = $.inArray(id, selected);

      // If checkbox is checked and row ID is not in list of selected row IDs
      if(this.checked && index === -1){
         selected.push(id);

      // Otherwise, if checkbox is not checked and row ID is in list of selected row IDs
      } else if (!this.checked && index !== -1){
         selected.splice(index, 1);
      }

      if(this.checked){
        //console.log(this.value);
         $row.addClass('selected');
      } else {
        
         $row.removeClass('selected');
      }
      // Prevent click event from propagating to parent
      e.stopPropagation();
    } );
});
