/* Formatting function for row details - modify as you need */

// JavaScript Document
$(document).ready(function(e) {

		var selected = [];
		//var Socios = $('#idsocios').val();
		var nom = $('#busnombre').val();
		var tip = $('#bustipo').val();
		var num = $('#busnumero').val();
		var tel1 = $('#bustelefono1').val();
		var tel2 = $('#bustelefono2').val();
		var ema = $('#busemail').val();
		var dir = $('#busdireccion').val();
		var obs = $('#busobservacion').val();
		var act = $('#busactivo').val();
		var prov = $('#busproveedor').val();
		
		var table2 = $('#tbproveedor').DataTable( {       
       	"dom": '<"top">rt<"bottom"lp><"clear">',
		"ordering": true,
		"info": true,
		"autoWidth": false,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,15,20,-1], [10,15,20,"Todos"]],
		"language": {
		"lengthMenu": "Ver _MENU_",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},		
		"processing": true,
        "serverSide": true,
        "responsive": true,
        "ajax": {
                    "url": "modulos/proveedor/proveedorjson.php",
                    "data": { nom:nom,tip:tip,num:num,tel1:tel1,tel2:tel2,ema:ema,dir:dir,obs:obs,act:act,prov:prov},	
                    "type": "POST"
				},
		"columns": [

			{ 				
    			"orderable":      false,
    			"data":           null,
    			"defaultContent": ""
    		},
			{ "data" : "Editar", "className": "dt-center","orderable":false },
			{ "data" : "Nombre", "className": "dt-left" },
			{ "data" : "Tipo", "className": "dt-left" },
			{ "data" : "Documento", "className": "dt-left" },
			{ "data" : "Telefono", "className": "dt-left" },
			{ "data" : "Email", "className": "dt-left" },
			{ "data" : "Direccion", "className": "dt-left" },
			{ "data" : "Observacion", "className": "dt-left" },
			{ "data" : "Estado", "className": "dt-center" },
			
		],
		"order": [[2, 'asc']],
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
               // $(row).addClass('selected');
            }  
        }
		
    } );
     
	 /*$('#tbproductos tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );*/
	
	   var table2 = $('#tbproveedor').DataTable();
 
    // Apply the search
    table2.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
});


