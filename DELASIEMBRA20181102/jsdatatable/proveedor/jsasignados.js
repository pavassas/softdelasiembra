/* Formatting function for row details - modify as you need */

// JavaScript Document
$(document).ready(function(e) {

		var selected = [];
		//var Socios = $('#idsocios').val();
		var arcjs = document.getElementById('jsproductosasignados');
		var idproveedor=arcjs.getAttribute('data-proveedor');
		var idcalidad = arcjs.getAttribute('data-calidad');
		var producto = $('#selproductoproveedor').val();
		var estado  = $('#selestado').val(); 

		var table2 = $('#tbProductosProveedor').DataTable( {   
		 "columnDefs": 
		 [ 
			{ "targets": [4], "visible":false },
			
			       
         ],    
       	"dom": '<"top"f>rt<"bottom"lp><"clear">',
       	"searching":false,
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,15,20,-1], [10,15,20,"Todos"]],
		"language": {
		"lengthMenu": "Ver _MENU_",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "&#9668;","next":"&#9658;"}
		},		
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/proveedor/asignadosjson.php",
                    "data": { idproveedor:idproveedor, idcalidad: idcalidad ,producto:producto, estado:estado },//uni:uni,	
                    "type": "POST"
				},
		"columns": [
			{ "data" : "Seleccion", "className" : "dt-left", "orderable" : false },
			{ "data" : "Codigo", "className" : "dt-center" },
			{ "data" : "Nombre", "className" : "dt-left" },				
			{ "data" : "Compra", "className" : "dt-left", "orderable" : false },			
			{ "data" : "Boton", "className" : "dt-center", "orderable" : false }
		],
		"order": [[2, 'asc']],
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
               // $(row).addClass('selected');
            }  
        }		
    } );
     
	 /*$('#tbproductos tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );*/
	
	   var table2 = $('#tbProductosProveedor').DataTable();
 
    // Apply the search
    table2.columns().every( function () {
        var that = this;
 
        $( 'input', this.footer() ).on( 'keyup change', function () {
            that
                .search( this.value )
                .draw();
        } );
    } );
});


