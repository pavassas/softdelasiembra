/* Formatting function for row details - modify as you need */

// JavaScript Document
// JavaScript Document
function format ( d ) {
        return '<div class="row"><div class="col-md-6"></div><div class="col-md-6">'+d.Usuarios+ '</div></div>';
    }
var selected = [];
$(document).ready(function(e) {


		var epr = $('#empresa2').val();
        var tip = $('#tipo2').val();
		var doc = $('#documento2').val();
		var tel = $('#telefono2').val();
		var dir = $('#direccion2').val();
		var ema = $('#email2').val();
		var ciu = $('#ciudad2').val();
        var cue = $('#cuenta2').val();
		
		var table2 = $('#tbEmpresas').DataTable( {       
        "dom": '<"top"i>rt<"bottom"flp><"clear">',
		"ordering": true,
		"info": true,
		"autoWidth": true,
		"pagingType": "simple_numbers",
		"lengthMenu": [[10,15,20,30,-1], [10,15,20,30,"Todas" ]],
		"language": {
		"lengthMenu": "Ver _MENU_ registros",
		"zeroRecords": "No se encontraron datos",
		"info": "Resultado _START_ - _END_ de _TOTAL_ registros",
		"infoEmpty": "No se encontraron datos",
		"infoFiltered": "",
		"paginate": {"previous": "anterior","next":"siguiente"},
        "search":"",
        "sSearchPlaceholder":"Busqueda"
		},		
		"processing": true,
        "serverSide": true,
        "ajax": {
                    "url": "modulos/empresas/empresajson.php",
                    "data": {epr:epr, doc:doc, tel:tel, dir:dir, ema:ema, ciu:ciu,tip:tip,cue:cue}
				},
		"columns": [
            { "data" : "Tipo", "className" : "dt-left"},
			{ "data" : "Nombre", "className" : "dt-left"},
			{ "data" : "Documento", "className" : "dt-left" },
			{ "data" : "Telefono", "className" : "dt-left" },
			{ "data" : "Fax", "className" : "dt-left" },
			{ "data" : "Direccion" , "className" : "dt-left" },
			{ "data" : "Ciudad", "className" : "dt-left" },			
			{ "data" : "Email", "className": "dt-left" },			
			{ "data" : "Cuenta", "className" : "dt-center" },			
			{ "data" : "Editar", "orderable" : false },	
            { "class": "btn-ver", "orderable":false,"data":"CantUsuarios"}, 
		],
		"order": [[1, 'asc']],
		"rowCallback": function( row, data ) {
            if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
                $(row).addClass('selected');
            }
            $('td:eq(0)', row).css('background-color',data.Color);
            $('td:eq(1)', row).css('background-color',data.Color);
            $('td:eq(2)', row).css('background-color',data.Color);
            $('td:eq(3)', row).css('background-color',data.Color);
            $('td:eq(4)', row).css('background-color',data.Color);
            $('td:eq(5)', row).css('background-color',data.Color);
            $('td:eq(6)', row).css('background-color',data.Color);
            $('td:eq(7)', row).css('background-color',data.Color);
            $('td:eq(8)', row).css('background-color',data.Color);
            $('td:eq(8)', row).css('background-color',data.Color);
            $('td:eq(9)', row).css('background-color',data.Color);       
             $('td:eq(10)', row).css('background-color',data.Color);       

        }
		
    } );
     
	 $('#tbEmpresas tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
 
        if ( index === -1 ) {
            selected.push( id );
        } else {
            selected.splice( index, 1 );
        }
 
        $(this).toggleClass('selected');
    } );
	
	var table2 = $('#tbEmpresas').DataTable();
    //);
	
	//);
	var detailRows = [];
 
    $('#tbEmpresas tbody').on( 'click', 'tr td.btn-ver', function () {
        var tr = $(this).closest('tr');
        var row = table2.row( tr );
        var idx = $.inArray( tr.attr('id'), detailRows );
 
        if ( row.child.isShown() ) {
            tr.removeClass( 'shown' );
            row.child.hide();
 
            // Remove from the 'open' array
            detailRows.splice( idx, 1 );
        }
        else {
            tr.addClass( 'shown' );
            row.child( format( row.data() ) ).show();
 
            // Add to the 'open' array
            if ( idx === -1 ) {
                detailRows.push( tr.attr('id') );
            }
        }
    } );
 
    // On each draw, loop over the `detailRows` array and show any child rows
    table2.on( 'draw', function () {
        $.each( detailRows, function ( i, id ) {
            $('#'+id+' td.btn-ver').trigger( 'click' );
            } );

        });
    });



function eliminarempresas()//$('#eliminar').click( function()
{
    var table = $('#tbEmpresas').DataTable();
    var col = table.rows('.selected').data().length;
    if(col <=0)
    {
        error('No ha seleccionado ninguna empresa'); return false;
    }
    else
    {
        jConfirm('Realmente desee eliminar las empresas seleccionadas?', 'Dialogo de Confirmación HD',null, function(r) {
            //var conf = confirm("Realmente desee eliminar las empresas seleccionadas");
            if(r==true)
            {
                for(var i = 0; i<selected.length;i++)
                {
                    if(selected[i]=="" || selected[i]==null)
                    {
                    }
                    else
                    {
                        var id = selected[i];
                        var n=id.split("row_");
                        CRUDEMPRESA('ELIMINAR',n[1]);
                    }
                }
                ok('Empresa Eliminada Correctamente');
                selected.length = 0;
            }
            else
            {
                return false;
            }
        });
    }
} //);
function inactivaempresas ()//$('#inactivar').click( function()
{
    var table = $('#tbEmpresas').DataTable();
    var col = table.rows('.selected').data().length;
    if(col <=0)
    {
        error('No ha seleccionado ninguna empresa'); return false;
    }
    else
    {
        for(var i = 0; i<selected.length;i++)
        {
            if(selected[i]=="" || selected[i]==null)
            {
            }
            else
            {
                var id = selected[i];
                var n=id.split("row_");
                CRUDEMPRESA('INACTIVAR',n[1]);
            }
        }
        ok('Empresas Inactivada Correctamente');
        selected.length = 0;
       // setTimeout(CRUDEMPRESA('CARGARLISTAEMPRESAS',''),1000);
    }
}
function activarempresas()

//$('#activar').click( function()
{
    var table = $('#tbEmpresas').DataTable();
    var col = table.rows('.selected').data().length;
    if(col <=0)
    {
        error('No ha seleccionado ninguna empresa'); return false;
    }
    else
    {
        for(var i = 0; i<selected.length;i++)
        {
            if(selected[i]=="" || selected[i]==null)
            {
            }
            else
            {
                var id = selected[i];
                var n=id.split("row_");

                CRUDEMPRESA('ACTIVAR',n[1]);
            }
        }
        selected.length = 0;
        ok('Empresas activadas Correctamente');
        //setTimeout(CRUDEMPRESA('CARGARLISTAEMPRESAS',''),1000);

    }
}
